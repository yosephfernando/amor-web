const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.scripts([
   'public/js/jquery-1.10.2.min.js',
   'public/js/jquery-migrate-1.2.1.min.js',
   'public/js/jquery-ui.js',
   'public/js/cookie.js',
   'public/vendors/bootstrap/js/bootstrap.min.js',
   'public/vendors/dropzone/js/dropzone.min.js',
   'public/vendors/bootstrap-hover-dropdown/bootstrap-hover-dropdown.js',
   'public/js/html5shiv.js',
   'public/js/respond.min.js',
   'public/vendors/metisMenu/jquery.metisMenu.js',
   'public/vendors/slimScroll/jquery.slimscroll.js',
   'public/vendors/jquery-cookie/jquery.cookie.js',
   'public/js/jquery.menu.js',
   'public/vendors/jquery-pace/pace.min.js',
   'public/vendors/holder/holder.js',
   'public/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js',
   'public/vendors/bootstrap-daterangepicker/daterangepicker.js',
   'public/vendors/moment/moment.js',
   'public/vendors/bootstrap-clockface/js/clockface.js',
   'public/vendors/bootstrap-colorpicker/js/bootstrap-colorpicker.js',
   'public/vendors/bootstrap-switch/js/bootstrap-switch.min.js',
   'public/vendors/jquery-maskedinput/jquery-maskedinput.js',
   'public/js/form-components.js',
   'public/vendors/autocomplete/js/jquery.autocomplete.min.js',
   'public/vendors/yjra/jquery.dataTables.min.js',
   'public/vendors/jquery-toastr/toastr.min.js',
   'public/js/ui-toastr-notifications.js',
   'public/vendors/jquery-validate/jquery.validate.min.js',
   'public/js/main.js',
], 'public/js/app.js');

mix.styles([
   'public/vendors/jquery-ui-1.10.4.custom/css/ui-lightness/jquery-ui-1.10.4.custom.min.css',
   'public/vendors/font-awesome/css/font-awesome.min.css',
   'public/vendors/bootstrap/css/bootstrap.min.css',
   'public/vendors/yjra/jquery.dataTables.min.css',
   'public/vendors/bootstrap/css/bootstrap.print.min.css',
   'public/vendors/intro.js/introjs.css',
   'public/vendors/calendar/zabuto_calendar.min.css',
   'public/vendors/bootstrap-datepicker/css/datepicker.css',
   'public/vendors/bootstrap-daterangepicker/daterangepicker-bs3.css',
   'public/vendors/animate.css/animate.css',
   'public/vendors/jquery-pace/pace.css',
   'public/vendors/iCheck/skins/all.css',
   'public/vendors/jquery-news-ticker/jquery.news-ticker.css',
   'public/vendors/autocomplete/css/styles.css',
   'public/vendors/dropzone/css/dropzone.css',
   'public/css/themes/style1/pink-violet.css',
   'public/css/style-responsive.css',
   'public/vendors/jquery-toastr/toastr.min.css',
], 'public/css/app.css');
