@extends('layout.layout')
@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb n-print">
    <div class="page-header pull-left">
        <div class="page-title">{{$title->moduleLabel}}</div>
    </div>
    <ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i>&nbsp;<a href="#">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li><a href="#">Transaction purchasing</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li><a href="#">Purchasing return</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="active">{{$title->moduleLabel}}</li>
    </ol>
    <div class="clearfix"></div>
</div>

<div class="page-content">
   <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-green">
            <div class="panel-body pan">
                    <div class="form-body pal">
                      <div class="col-md-12">
                        @if($errors->any())
                            <script>
                                   toastr.error("{{$errors->first()}}");
                                   toastr.options = {
                                    "closeButton": false,
                                    "debug": false,
                                    "positionClass": "toast-top-right",
                                    "onclick": null,
                                    "showDuration": "300",
                                    "hideDuration": "1000",
                                    "timeOut": "5000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                  };

                            </script>
                        @endif
                        @if(session('status'))
                            <script>
                               @if(session('status') == 'Detail sales return added' || session('status') == 'Detail sales return updated' || session('status') == 'Detail sales return deleted')
                                 toastr.success("{{ session('status') }}");
                               @else
                                 toastr.error("{{ session('status') }}");
                               @endif

                                   toastr.options = {
                                    "closeButton": false,
                                    "debug": false,
                                    "positionClass": "toast-top-right",
                                    "onclick": null,
                                    "showDuration": "300",
                                    "hideDuration": "1000",
                                    "timeOut": "5000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                  };

                            </script>
                        @endif
                      </div>
            @if($srt_id->inv_status != "approve")
             @if($srt_id->si_flag != 1)
                  <form action="/detailsrt-add-action/saleTransaction/addDetailPrtAction"  method="post" class="form-group">
                    {{csrf_field()}}
                     <input type="hidden" name="inv_id" value="{{$srt_id->inv_id}}" />
                      <div class="col-md-4 col-sm-12 col-xs-12">
                        <label>Product :</label>
                            <input type="hidden"  id="prd_id" name="prd_id"  class="form-control">
                            <input type="text" id="autocompleteprd" placeholder="Please type the product article" name="prd_desc" class="form-control required">
                            <!-- Barang catalog -->
                             <script>
                                 $('#autocompleteprd').devbridgeAutocomplete({
                                         serviceUrl : '/lookupSugestion/product',
                                         minChars : 3,
                                         onSelect: function (suggestion) {
                                           $('#prd_id').val(suggestion.data);
                                           $('#autocompleteprd').val(suggestion.value);
                                           $('#price_format').val(suggestion.retail_price);

                                           $.get( "/ajax-uom-catalog/transactionPurchasing/prAjaxUomCatalog/"+suggestion.data)
                                             .done(function( data ) {
                                                 $("#uom").empty();
                                                 $("#uom").html(data);
                                             });

                                         }
                                 });
                             </script>
                      </div>
                      <div class="col-md-2 col-sm-12 col-xs-12">
                          <label>Qty :</label>
                          <input type="number" placeholder="Please type the product quantity" name="qty_inv" class="form-control required">
                      </div>
                      <div id="price_inv" class="col-md-2 col-sm-12 col-xs-12">
                          <label>Price :</label>
                          <input id="price_format" type="text" placeholder="Please type the product price" name="price_inv" data-a-sep="." data-a-dec="," data-a-form="false" data-a-pad="false" class="form-control required">
                          <script>
                            $('#price_format').autoNumeric('init');
                          </script>
                      </div>
                      <div id="uom" class="col-md-2 col-sm-12 col-xs-12">
                          <label>UOM :</label>
                          <select name="uom_id" class="form-control required">
                              <option value="">-- choose --</option>
                          </select>
                      </div>
                      <input id="uom_conv" type="hidden" name="uom_conversion">
                      <!--div class="col-md-9 col-sm-12 col-xs-12">
                          <label>UOM conversion :</label>
                          <input id="uom_conv" type="number" name="uom_conversion" class="form-control" readonly>
                      </div-->
                      <div class="col-md-2 col-sm-12 col-xs-12">
                         <div class="clear" style="height:20px"></div>
                          <button type="submit" class="btn btn-primary form-control"><i class="fa fa-plus"></i>&nbsp;Add</button>
                          <div class="clear" style="height:20px"></div>
                      </div>
                 </form>
            @else
                   <form class="form-group" method="post" action="/load-inv/saleTransaction/loadInv">
                               <div class="col-md-12"><label>Load from SI :</label></div>
                               <div class="col-md-10 col-sm-12 col-xs-12">
                                    {{csrf_field()}}
                                   <input type="hidden" name="inv_id_" value="{{$srt_id->inv_id}}" />
                                       <input type="hidden" id="inv_id" name="inv_id"  class="form-control">
                                       <input type="text"  id="inv_no" placeholder="Please type the invoice number" name="inv_no" class="form-control required">

                                       <!-- Barang catalog -->
                                        <script>
                                        var si = {!! $siSugest !!};
                                         $('#inv_no').devbridgeAutocomplete({
                                                 lookup: si,
                                                 onSelect: function (suggestion) {
                                                   $('#inv_id').val(suggestion.data);
                                                   $('#inv_no').val(suggestion.value);
                                                   $.get( "/ajax-si-detail-catalog/saleTransaction/srtAjaxSiDetailCatalog/"+suggestion.data)
                                                       .done(function( data ) {
                                                           $("#detailSi").empty();
                                                           $("#detailSi").html(data);
                                                       });
                                                 }
                                         });
                                        </script>
                                   <div id="detailSi" class="col-md-12 row">

                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-12 col-xs-12">
                                    <button type="submit" class="btn btn-primary form-control"><i class="fa fa-plus"></i>&nbsp;Load</button>
                                    &nbsp;
                                    <div class="clear" style="height:20px"></div>
                                </div>
                     </form>
                  @endif
                @endif
                        <div class="col-md-12">
                            @if($srt_detail_id != null)
                                <div class="col-md-6">
                                    <h3 style="margin-top:0px"># {{$srt_id->inv_no}}</h3>
                                </div>
                                <div class="col-md-6" style="text-align:right">
                                  <div class="dropdown">
                                      @if($privilaged->canPrint != 0)
                                            <button class="btn btn-success" onclick="window.print();"><i class="fa fa-print"></i>&nbsp;Print</button>
                                      @endif
                                      <a href="/list-sales-return/saleTransaction/srtListSrt">
                                        <button type="button" class="btn btn-default">Back</button>
                                      </a>
                                        <div class="clear" style="height:10px"></div>
                                  </div>
                                </div>

                           @endif
                        </div>
                        <div class="col-md-12">
                          <div class="table-responsive">
                            <table class="table table-bordered" id="srtD-table">
                              <thead>
                                   <th><input type="checkbox"  id="bulkDelete"  /></th>
                                   <th>Product</th>
                                   <th>Qty</th>
                                   <th>Sub total</th>
                                   @if($srt_id->inv_status != "approve")
                                       <th>Action</th>
                                   @endif
                               </thead>
                            </table>
                            <script>
                              $(function() {
                                var table = $('#srtD-table').DataTable({
                                  dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
                                     "<'row'<'col-xs-12't>>",
                                     processing: true,
                                     serverSide: true,
                                     ajax: {
                                        url: '/listSrtDetailData/saleTransaction/srtDetailSalesReturnDataTable/{{$srt_id->inv_id}}',
                                    },
                                     columns: [
                                         {data: 'delete_bulk', name: 'delete_bulk', orderable: false, searchable: false},
                                         { data: 'prd_desc', name: 'prd_desc' },
                                         { data: 'qty_inv', name: 'qty_inv' },
                                         { data: 'sub_total', name: 'sub_total' },

                                         @if($srt_id->inv_status != "approve")
                                            {data: 'action', name: 'action', className: 'text-center',orderable: false, searchable: false}
                                         @endif
                                     ],
                                     paginate : false
                                 });
                                  $('div.dataTables_filter input').addClass('form-control');

                                  $("#deleteTriger").click(function(){
                                     var allValue = [];
                                     var inv_id = {{$srt_id->inv_id}};
                                     $("input[name='inv_detail_id[]']:checked").each( function () {
                                         allValue.push($(this).val());
                                     });
                                     $.post( "/detailSrt-delete-action/saleTransaction/deleteDetailsrtAction",  {'inv_id': inv_id,'inv_detail_id': allValue, '_token' : '{{csrf_token()}}' })
                                       .done(function( data ) {
                                         if(data == "success"){
                                            location.reload();
                                         }else{
                                            alert(data);
                                         }
                                         console.log( "Data Loaded: " + data );
                                       });
                                   });

                                   $("#bulkDelete").click(function(){
                                      $("input[name='inv_detail_id[]']").prop("checked", $(this).prop('checked'))
                                      if($("input[name='inv_detail_id[]']:checked").length > 0){
                                        $("#deleteTriger").show();
                                      }else{
                                        $("#deleteTriger").hide();
                                      }
                                   });

                                   $("#srtD-table").on('click', ':checkbox', function(){
                                     if($("input[name='inv_detail_id[]']:checked").length > 0){
                                       $("#deleteTriger").show();
                                     }else{
                                       $("#deleteTriger").hide();
                                     }
                                   });

                                   $('#search-form').on('submit', function(e) {
                                       table.draw();
                                       e.preventDefault();
                                   });
                              });
                           </script>
                          </div>
                          <button class='btn btn-danger' id="deleteTriger" style="display:none">Delete cheked</button>
                          <div class="clear" style="height:20px"></div>
                        </div>
                    </div>
            </div>
        </div>
      </div>
    </div>
</div>
<script>
    $(document).ready(function() {
      $('form').each(function () {
         $(this).validate();
      });
    });
</script>
@include('salesreturn/srt_detail_sales_return/srtPrintTemplateDetailSrt')
@endsection
