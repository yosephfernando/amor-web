<div class="clear" style="height:10px"></div>
 <table class="table table-bordered table-striped table-hover">
    <tr>
      <thead>
        <th>#</th>
        <th>Produk id </th>
        <th>Code </th>
        <th>Desc </th>
        <th>Size </th>
        <th>Qty</th>
        <th>Price</th>
      </thead>
    </tr>
    <h4>Choose pr detail</h4>
    @foreach($prDetail as $data)
        <tr>
          <th><input type="checkbox" name="pr_detail_id[]" value="{{$data->pr_detail_id}}"/></th>
          <td>{{$data->prd_id}}</td>
          <td>{{$data->prd_code}}</td>
          <td>{{$data->prd_desc}}</td>
          <td>{{$data->size_desc}}</td>
          <td>{{number_format($data->qty_pr)}}</td>
          <td>
              <?php
                  echo "<input type='text' id='retail_price$data->pr_detail_id' name='retail_price$data->pr_detail_id' class='form-control' value='".number_format($data->retail_price, 0, "",".")."' data-a-sep='.' data-a-dec=',' data-a-form='false' data-a-pad='false'";
              ?>
          </td>
        </tr>
        <script>
          $('#retail_price{{$data->pr_detail_id}}').autoNumeric('init');
        </script>
    @endforeach
 </table>
