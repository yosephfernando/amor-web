@if($keyword != "null")
    {{buildSearch::searchMaster(
          'select ipm.prd_id, ipm.prd_desc, ips.size_desc, ipc.color_desc from "IM_PRD_MASTER" ipm JOIN "IM_PRD_SIZE" ips on ipm.size_id = ips.size_id
           JOIN "IM_PRD_COLOR" ipc on ipm.color_id = ipc.color_id where CAST(prd_desc as varchar) LIKE '."'".'%'.$keyword.'%'."'".'',
          "Product catalog", Request::segment(5), ["prd_id", "prd_desc", "size_desc"]
          )
    }}
@else
    {{buildSearch::searchMaster(
          'select ipm.prd_id, ipm.prd_desc, ips.size_desc, ipc.color_desc from "IM_PRD_MASTER" ipm JOIN "IM_PRD_SIZE" ips on ipm.size_id = ips.size_id
           JOIN "IM_PRD_COLOR" ipc on ipm.color_id = ipc.color_id ORDER BY ipm.prd_id DESC  LIMIT 10',
          "Product catalog", Request::segment(5), ["prd_id", "prd_desc", "size_desc"]
          )
    }}
@endif
