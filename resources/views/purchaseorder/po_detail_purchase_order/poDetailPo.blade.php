@extends('layout.layout')
@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb n-print">
    <div class="page-header pull-left">
        <div class="page-title">{{$title->moduleLabel}}</div>
    </div>
    <ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i>&nbsp;<a href="#">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li><a href="#">Transaction purchasing</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li><a href="#">Purchase order</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="active">{{$title->moduleLabel}}</li>
    </ol>
    <div class="clearfix"></div>
</div>

<div class="page-content">
   <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-green">
            <div class="panel-body pan">
                    <div class="form-body pal">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        @if($errors->any())
                            <script>
                                   toastr.error("{{$errors->first()}}");
                                   toastr.options = {
                                    "closeButton": false,
                                    "debug": false,
                                    "positionClass": "toast-top-right",
                                    "onclick": null,
                                    "showDuration": "300",
                                    "hideDuration": "1000",
                                    "timeOut": "5000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                  };

                            </script>
                        @endif
                        @if(session('status'))
                            <script>
                               @if(session('status') == 'Detail po added' || session('status') == 'Detail po updated' || session('status') == 'Detail po deleted')
                                 toastr.success("{{ session('status') }}");
                               @else
                                 toastr.error("{{ session('status') }}");
                               @endif

                                   toastr.options = {
                                    "closeButton": false,
                                    "debug": false,
                                    "positionClass": "toast-top-right",
                                    "onclick": null,
                                    "showDuration": "300",
                                    "hideDuration": "1000",
                                    "timeOut": "5000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                  };

                            </script>
                        @endif
                    </div>
                  @if($po_id->po_status != "finish")
                   @if($po_id->pr_flag != 1)
                    <form action="/detailpo-add-action/transactionPurchasing/addDetailPoAction"  method="post" class="form-group">
                      {{csrf_field()}}
                       <input type="hidden" name="po_id" value="{{$po_id->po_id}}" />
                      <div class="col-md-4 col-sm-12 col-xs-12">
                          <label>Product :</label>
                              <input type="hidden"  id="prd_id" name="prd_id"  class="form-control">
                              <input type="text" id="autocompleteprd" placeholder="Please type pruduct article" name="prd_desc" class="form-control required"/>
                              <!-- Barang catalog -->
                               <script>
                                   $('#autocompleteprd').devbridgeAutocomplete({
                                           serviceUrl : '/lookupSugestion/product',
                                           minChars : 3,
                                           onSelect: function (suggestion) {
                                             $('#prd_id').val(suggestion.data);
                                             $('#autocompleteprd').val(suggestion.value);
                                             $('#uom_id').val(suggestion.uom_id);
                                             $('#uom_conv').val(suggestion.uom_conversion);
                                             $('#uomdesc').val(suggestion.uomdesc)
                                           }
                                   });
                               </script>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <label>Qty :</label>
                            <input type="number" placeholder="Please type pruduct quantity" name="qty_pr" class="form-control required number">
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <label>Uom desc :</label>
                            <input id="uomdesc" type="text" class="form-control required" readonly>
                        </div>
                        <input id="uom_id" type="hidden" name="uom_id" />
                        <input id="uom_conv" type="hidden" name="uom_conversion">
                        <div class="col-md-2 col-sm-12 col-xs-12">
                           <div class="clear" style="height:20px"></div>
                            <button type="submit" class="btn btn-primary form-control"><i class="fa fa-plus"></i>&nbsp;Add</button>
                            <!--a href="/list-po/transactionPurchasing/poListPo">
                              <button type="button" class="btn btn-green">Cancel</button>
                            </a-->
                            <div class="clear" style="height:20px"></div>
                        </div>
                   </form>
                 @else
                   <form class="form-group" method="post" action="/lod-pr/transactionPurchasing/load">
                               <div class="col-md-12"><label>Load from PR :</label></div>
                               <div class="col-md-10 col-sm-12 col-xs-12">
                                    {{csrf_field()}}
                                   <input type="hidden" name="po_id" value="{{$po_id->po_id}}" required/>
                                       <input type="hidden"  id="pr_id" name="pr_id"  class="form-control" required>
                                       <input type="text"  id="pr_no" class="form-control" required>

                                       <!-- Barang catalog -->
                                        <script>
                                        var pr = {!! $prSugest !!};
                                         $('#pr_no').devbridgeAutocomplete({
                                                 lookup: pr,
                                                 onSelect: function (suggestion) {
                                                   $('#pr_id').val(suggestion.data);
                                                   $('#pr_no').val(suggestion.value);
                                                   $.get( "/ajax-pr-detail-catalog/transactionPurchasing/poAjaxPrDetailCatalog/"+suggestion.data)
                                                       .done(function( data ) {
                                                           $("#detailPr").empty();
                                                           $("#detailPr").html(data);
                                                       });
                                                 }
                                         });
                                        </script>
                                   <div id="detailPr" class="col-md-12 row">

                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-12 col-xs-12">
                                    <button type="submit" class="btn btn-primary form-control"><i class="fa fa-plus"></i>&nbsp;load</button>
                                    <div class="clear" style="height:20px"></div>
                                </div>
                     </form>
                  @endif
                @endif
                        <div class="col-md-12">
                            @if($po_detail_id != null)
                                <div class="col-md-6">
                                  <h3 style="margin-top:0px"># {{$po_id->po_no}}</h3>
                                </div>
                                <div class="col-md-6" style="text-align:right">
                                  <div class="dropdown">
                                      @if($privilaged->canPrint != 0)
                                            <button class="btn btn-success" onclick="window.print();"><i class="fa fa-print"></i>&nbsp;Print</button>
                                      @endif
                                      <a href="/list-po/transactionPurchasing/poListPo">
                                        <button class="btn btn-default">Back</button>
                                      </a>
                                        <div class="clear" style="height:10px"></div>
                                  </div>
                                </div>

                           @endif
                        </div>
                        <div class="col-md-12">
                          <div class="table-responsive">
                            <table class="table table-bordered" id="poD-table">
                              <thead>
                                   <th><input type="checkbox"  id="bulkDelete"  /></th>
                                   <th>Product</th>
                                   <th>Qty</th>
                                   <th>Sub total</th>
                                   @if($po_id->po_status != "finish")
                                       <th>Action</th>
                                   @endif
                               </thead>
                            </table>
                            <script>
                              $(function() {
                                var table = $('#poD-table').DataTable({
                                  dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
                                     "<'row'<'col-xs-12't>>"+
                                     "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
                                     processing: true,
                                     serverSide: true,
                                     ajax: {
                                        url: '/listPoDetailData/transactionPurchasing/poListPoDetailData/{{$po_id->po_id}}',
                                    },
                                     columns: [
                                         {data: 'delete_bulk', name: 'delete_bulk', orderable: false, searchable: false},
                                         { data: 'prd_desc', name: 'prd_desc' },
                                         { data: 'qty_pr', name: 'qty_pr' },
                                         { data: 'sub_total', name: 'sub_total' },

                                         @if($po_id->po_status != "finish")
                                            {data: 'action', name: 'action', className: 'text-center',orderable: false, searchable: false}
                                         @endif
                                     ],
                                     paginate:false
                                 });
                                  $('div.dataTables_filter input').addClass('form-control');

                                  $("#deleteTriger").click(function(){
                                     var allValue = [];
                                     var poid = '{{$po_id->po_id}}';
                                     $("input[name='po_detail_id[]']:checked").each( function () {
                                         allValue.push($(this).val());
                                     });
                                     $.post( "/detailPo-delete-action/transactionPurchasing/deleteDetailpoAction",  {'po_detail_id': allValue, 'po_id':poid, '_token' : '{{csrf_token()}}' })
                                       .done(function( data ) {
                                         if(data == "success"){
                                            location.reload();
                                         }else{
                                            alert(data);
                                         }
                                         console.log( "Data Loaded: " + data );
                                       });
                                   });

                                   $("#bulkDelete").click(function(){
                                      $("input[name='po_detail_id[]']").prop("checked", $(this).prop('checked'))
                                      if($("input[name='po_detail_id[]']:checked").length > 0){
                                        $("#deleteTriger").show();
                                      }else{
                                        $("#deleteTriger").hide();
                                      }
                                   });

                                   $("#poD-table").on('click', ':checkbox', function(){
                                     if($("input[name='po_detail_id[]']:checked").length > 0){
                                       $("#deleteTriger").show();
                                     }else{
                                       $("#deleteTriger").hide();
                                     }
                                   });

                                   $('#search-form').on('submit', function(e) {
                                       table.draw();
                                       e.preventDefault();
                                   });

                              });
                           </script>
                          </div>
                          <button class='btn btn-danger' id="deleteTriger" style="display:none">Delete cheked</button>
                          <div class="clear" style="height:20px"></div>
                        </div>
                    </div>
            </div>
        </div>
      </div>
     </div>
</div>
<script>
      $(document).ready(function() {
        $('form').each(function () {
           $(this).validate();
        });
      });
</script>
@include('purchaseorder/po_detail_purchase_order/poPrintTemplateDetailPo')
@endsection
