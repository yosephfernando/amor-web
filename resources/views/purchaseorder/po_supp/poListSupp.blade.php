@extends('layout.layout')
@section('content')
<!--BEGIN TITLE & BREADCRUMB PAGE-->
            <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                <div class="page-header pull-left">
                    <div class="page-title">{{$title->moduleLabel}}</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a href="#">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li><a href="#">Master purchasing</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li class="active">{{$title->moduleLabel}}</li>
                </ol>
                <div class="clearfix"></div>
            </div>
            <!--END TITLE & BREADCRUMB PAGE-->
           <div class="page-content">
              <div class="row">
                <div class="col-md-12">
                <div class="panel panel-green">
                        <div class="panel-body">
                          @if($errors->any())
                              <script>
                                     toastr.error("{{$errors->first()}}");
                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                          @if(session('status'))
                              <script>
                                 @if(session('status') == 'Supplier added' || session('status') == 'Supplier updated' || session('status') == 'Supplier deleted')
                                   toastr.success("{{ session('status') }}");
                                 @else
                                   toastr.error("{{ session('status') }}");
                                 @endif

                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                          <div class="row">
                            <div class="col-md-3">
                                @if($privilaged->canAdd != 0)
                                   <button type="button" class="btn btn-primary btn-md" data-target="#suppAdd" data-toggle="modal">
                                     <i class="fa fa-plus"></i>&nbsp;
                                       Add
                                   </button>
                                @endif
                                @if($privilaged->canExport != 0)
                                   <a href="/exportSupp/masterPurchasing/exportSuppToExcel" class="btn btn-success btn-md"><i class="fa fa-download"></i>&nbsp;Export to xls</a>
                                @endif
                                <div class="clear" style="height:20px"></div>
                            </div>
                          </div>

                              <div class="col-md-12">
                                  <div class="table-responsive">

                                    <table class="table table-bordered" id="supp-table">
                                        <thead>
                                          <tr>
                                            <th><input type="checkbox"  id="bulkDelete"  /></th>
                                            <th>No supplier</th>
                                            <th>desc</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                          </tr>
                                        </thead>
                                      </table>
                                      <script>
                                        $(function() {
                                          var table = $('#supp-table').DataTable({
                                               processing: true,
                                               serverSide: true,
                                               ajax: {
                                                  url: '/listSuppData/masterPurchasing/poSuppDataTable',
                                               },
                                               columns: [
                                                   {data: 'delete_bulk', name: 'delete_bulk', orderable: false, searchable: false},
                                                   { data: 'supp_code', name: 'supp_code' },
                                                   { data: 'supp_desc', name: 'supp_desc' },
                                                   { data: 'supp_active_flag', name: 'supp_active_flag' },
                                                   {data: 'action', name: 'action', className: 'text-center', orderable: false, searchable: false}
                                               ]
                                           });
                                            $('div.dataTables_filter input').addClass('form-control');

                                            $("#deleteTriger").click(function(){
                                               var allValue = [];
                                               $("input[name='supp_id[]']:checked").each( function () {
                                                   allValue.push($(this).val());
                                               });
                                               $.post( "/delete-bulk-supp/masterPurchasing/deleteBulkSupp",  {'supp_id': allValue, '_token' : '{{csrf_token()}}' })
                                                 .done(function( data ) {
                                                   if(data == "success"){
                                                      location.reload();
                                                   }else{
                                                      alert(data);
                                                   }
                                                   console.log( "Data Loaded: " + data );
                                                 });
                                             });

                                             $("#bulkDelete").click(function(){
                                                $("input[name='supp_id[]']").prop("checked", $(this).prop('checked'))
                                                if($("input[name='supp_id[]']:checked").length > 0){
                                                  $("#deleteTriger").show();
                                                }else{
                                                  $("#deleteTriger").hide();
                                                }
                                             });

                                             $("#supp-table").on('click', ':checkbox', function(){
                                               if($("input[name='supp_id[]']:checked").length > 0){
                                                 $("#deleteTriger").show();
                                               }else{
                                                 $("#deleteTriger").hide();
                                               }
                                             });

                                             $('#date-range').on('submit', function(e) {
                                                 table.draw();
                                                 e.preventDefault();
                                             });

                                        });
                                     </script>
                                  </div>
                                  <button class='btn btn-danger' id="deleteTriger" style="display:none">Delete cheked</button>
                                  <div class="clear" style="height:20px"></div>
                              </div>
                            </div>
                        </div>
                    </div>
                  </div>
              </div>
          </div>

          <!--Modal add user-->
           <div id="suppAdd" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
               <div class="modal-dialog">
                 <form role="form" method="post" action="/supp-add/masterPurchasing/add-supp">
                    {{csrf_field()}}
                   <div class="modal-content">
                       <div class="modal-header">
                           <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                           <h4 id="modal-responsive-label" class="modal-title">Add supplier</h4></div>
                       <div class="modal-body">
                           <div class="row">
                               <div class="col-md-12">
                                 <div class="mbm row">
                                   <div class="col-md-6">
                                     <label>Supplier code :</label>
                                     <input type="text" name="supp_code" class="form-control required" placeholder="Please type the supplier code"/>
                                   </div>
                                   <div class="col-md-6">
                                     <label>Supplier desc :</label>
                                     <input type="text" name="supp_desc" class="form-control required" placeholder="Please type the supplier description"/>
                                   </div>
                                 </div>
                                 <div class="mbm">
                                    <label>Supplier type :</label>
                                    <input type="hidden" id="supp_type_id" name="supp_type_id"/>
                                    <input type="text"  id="supp_type_desc" name="supp_type_desc"  class="form-control required" placeholder="Please type the supplier type">

                                    <!-- Barang catalog -->
                                     <script>
                                           var supps = {!! $supps !!};
                                            $('#supp_type_desc').devbridgeAutocomplete({
                                                    lookup: supps,
                                                    onSelect: function (suggestion) {
                                                      $('#supp_type_id').val(suggestion.data);
                                                      $('#supp_type_desc').val(suggestion.value);
                                                    }
                                            });
                                     </script>
                                 </div>
                                 <div class="mbm row">
                                   <div class="col-md-6">
                                      <label>Address 1 :</label>
                                      <textarea name="supp_address1" class="form-control required" placeholder="Please type the supplier address"></textarea>
                                   </div>
                                   <div class="col-md-6">
                                      <label>Address 2 :</label>
                                      <textarea name="supp_address2" class="form-control required" placeholder="Please type the supplier address"></textarea>
                                   </div>
                                 </div>

                                 <div class="mbm">
                                    <label>Address 3 :</label>
                                    <textarea name="supp_address3" class="form-control required" placeholder="Please type the supplier address"></textarea>
                                 </div>
                                 <div class="mbm row">
                                   <div class="col-md-6">
                                      <label>City :</label>
                                      <input type="text" name="supp_city" class="form-control required" placeholder="Please type the supplier city"/>
                                   </div>
                                   <div class="col-md-6">
                                      <label>Country :</label>
                                      <input type="text" name="supp_country" class="form-control required" placeholder="Please type the supplier country"/>
                                   </div>
                                 </div>
                                 <div class="mbm row">
                                   <div class="col-md-6">
                                      <label>Phone :</label>
                                      <input type="text" name="supp_phone" class="form-control required number" placeholder="Please type the supplier phone number"/>
                                   </div>
                                   <div class="col-md-6">
                                      <label>Fax :</label>
                                      <input type="text" name="supp_fax" class="form-control required number" placeholder="Please type the supplier fax number"/>
                                   </div>
                                 </div>
                                 <div class="mbm">
                                    <label>Email :</label>
                                    <input type="email" name="supp_email" class="form-control required email" placeholder="Please type the supplier email address"/>
                                 </div>
                                 <div class="mbm">
                                    <label>Post code :</label>
                                    <input type="text" name="supp_post_code" class="form-control required number" placeholder="Please type the supplier postal code" minlength="5" maxlength="5"/>
                                 </div>
                                 <div class="mbm">
                                    <label>Contact person :</label>
                                    <input type="text" name="supp_contact_person" class="form-control required" placeholder="Please type the supplier contact person"/>
                                 </div>
                                 <div class="mbm">
                                    <label>Term :</label>
                                    <input type="hidden" id="supp_term" name="supp_term"/>
                                    <input type="text"  id="supp_term_desc" name="supp_term_desc"  class="form-control required" placeholder="Please type the supplier term">

                                    <!-- Barang catalog -->
                                     <script>
                                           var terms = {!! $term !!};
                                            $('#supp_term_desc').devbridgeAutocomplete({
                                                    lookup: terms,
                                                    onSelect: function (suggestion) {
                                                      $('#supp_term').val(suggestion.data);
                                                      $('#supp_term_desc').val(suggestion.value);
                                                    }
                                            });
                                     </script>
                                 </div>
                                 <div class="mbm row">
                                   <div class="col-md-6">
                                      <input type="checkbox" name="supp_pkp_flag" value="1" /><span> PKP</span>
                                   </div>
                                   <div class="col-md-6">
                                      <input type="checkbox" name="supp_active_flag" value="1" /><span> Active</span>
                                   </div>
                                 </div>

                               </div>
                           </div>
                       </div>
                       <div class="modal-footer">
                           <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                           <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                       </div>
                   </div>
                  </form>
               </div>
           </div>
           <script>
                 $(document).ready(function() {
                   $('form').each(function () {
                      $(this).validate();
                   });
                 });
           </script>
@endsection
