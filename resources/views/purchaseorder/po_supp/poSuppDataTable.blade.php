@if($privilaged->canEdit != 0)
       <button type="button" class="btn btn-default btn-xs" data-target="#supp{{$data->supp_id}}" data-toggle="modal" >
         <i class="fa fa-pencil"></i>&nbsp;
           Edit
       </button>
@endif
@if($privilaged->canDelete != 0)
     <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->supp_id}}" data-toggle="modal">
       <i class="fa fa-trash"></i>&nbsp;
         Delete
     </button>
@endif
<!--Modal edit-->
 <div id="supp{{$data->supp_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
     <div class="modal-dialog">
       <form role="form" method="post" action="/supp-edit/masterPurchasing/edit-supp">
         {{csrf_field()}}
         <input type="hidden" name="supp_id" value="{{$data->supp_id}}">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                 <h4 id="modal-responsive-label" class="modal-title">Edit supplier {{$data->supp_code}}</h4></div>
             <div class="modal-body">
                 <div class="row">
                   <div class="col-md-12">
                     <div class="mbm row">
                       <div class="col-md-6">
                         <label>Supplier code :</label>
                         <input type="text" name="supp_code"  class="form-control required" value="{{$data->supp_code}}" placeholder="Please type the supplier code" />
                       </div>
                       <div class="col-md-6">
                         <label>Supplier desc :</label>
                         <input type="text" name="supp_desc" class="form-control required" value="{{$data->supp_desc}}" placeholder="Please type the supplier description"/>
                       </div>
                     </div>
                     <div class="mbm">
                        <label>Supplier type :</label>
                        <input type="hidden" id="supp_type_id{{$data->supp_id}}" name="supp_type_id" value="{{$data->supp_type_id}}"/>
                        <input type="text"  id="supp_type_desc{{$data->supp_id}}" name="supp_type_desc"  class="form-control required" placeholder="Please type the supplier type" value="{{$data->supp_type_desc}}">

                        <!-- Barang catalog -->
                         <script>
                               var supps = {!! $suppstype !!};
                                $('#supp_type_desc{{$data->supp_id}}').devbridgeAutocomplete({
                                        lookup: supps,
                                        onSelect: function (suggestion) {
                                          $('#supp_type_id{{$data->supp_id}}').val(suggestion.data);
                                          $('#supp_type_desc{{$data->supp_id}}').val(suggestion.value);
                                        }
                                });
                         </script>
                     </div>
                     <div class="mbm row">
                       <div class="col-md-6">
                          <label>Address 1 :</label>
                          <textarea name="supp_address1" class="form-control required" placeholder="Please type the supplier address">{{$data->supp_address1}}</textarea>
                       </div>
                       <div class="col-md-6">
                          <label>Address 2 :</label>
                          <textarea name="supp_address2" class="form-control required" placeholder="Please type the supplier address">{{$data->supp_address2}}</textarea>
                       </div>
                     </div>

                     <div class="mbm">
                        <label>Address 3 :</label>
                        <textarea name="supp_address3" class="form-control required" placeholder="Please type the supplier address">{{$data->supp_address3}}</textarea>
                     </div>
                     <div class="mbm row">
                       <div class="col-md-6">
                          <label>City :</label>
                          <input type="text" name="supp_city" class="form-control required" value="{{$data->supp_city}}" placeholder="Please type the supplier city"/>
                       </div>
                       <div class="col-md-6">
                          <label>Country :</label>
                          <input type="text" name="supp_country" class="form-control required"  value="{{$data->supp_country}}" placeholder="Please type the supplier country"/>
                       </div>
                     </div>
                     <div class="mbm row">
                       <div class="col-md-6">
                          <label>Phone :</label>
                          <input type="text" name="supp_phone" class="form-control required number" value="{{$data->supp_phone}}" placeholder="Please type the supplier phone number"/>
                       </div>
                       <div class="col-md-6">
                          <label>Fax :</label>
                          <input type="text" name="supp_fax" class="form-control required number" value="{{$data->supp_fax}}" placeholder="Please type the supplier fax number"/>
                       </div>
                     </div>
                     <div class="mbm">
                        <label>Email :</label>
                        <input type="email" name="supp_email" class="form-control required email" value="{{$data->supp_email}}" placeholder="Please type the supplier email address"/>
                     </div>
                     <div class="mbm">
                        <label>Post code :</label>
                        <input type="text" name="supp_post_code" class="form-control required number" value="{{$data->supp_post_code}}" placeholder="Please type the supplier postal code" minlength="5" maxlength="5"/>
                     </div>
                     <div class="mbm">
                        <label>Contact person :</label>
                        <input type="text" name="supp_contact_person" class="form-control required" value="{{$data->supp_contact_person}}" placeholder="Please type the supplier contact person"/>
                     </div>
                     <div class="mbm">
                        <label>Term :</label>
                        <input type="hidden" id="supp_term{{$data->supp_id}}" name="supp_term" value="{{$data->supp_term}}"/>
                        <input type="text"  id="supp_term_desc{{$data->supp_id}}" name="supp_term_desc"  class="form-control required" placeholder="Please type the supplier term" value="{{$data->pptc_term_desc}}">

                        <!-- Barang catalog -->
                         <script>
                               var terms = {!! $term !!};
                                $('#supp_term_desc{{$data->supp_id}}').devbridgeAutocomplete({
                                        lookup: terms,
                                        onSelect: function (suggestion) {
                                          $('#supp_term{{$data->supp_id}}').val(suggestion.data);
                                          $('#supp_term_desc{{$data->supp_id}}').val(suggestion.value);
                                        }
                                });
                         </script>
                     </div>
                     <div class="mbm row">
                       <div class="col-md-6">
                        @if($data->supp_pkp_flag != 1)
                          <input type="checkbox" name="supp_pkp_flag" value="1" /><span> PKP</span>
                        @else
                          <input type="checkbox" name="supp_pkp_flag" value="1" checked/><span> PKP</span>
                        @endif
                       </div>
                       <div class="col-md-6">
                          @if($data->supp_active_flag != 1)
                            <input type="checkbox" name="supp_active_flag" value="1" /><span> Active</span>
                          @else
                            <input type="checkbox" name="supp_active_flag" value="1" checked/><span> Active</span>
                          @endif
                       </div>
                     </div>

                   </div>
                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                 <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
             </div>
         </div>
       </form>
     </div>
 </div>

 <!--Modal delete-->
  <div id="delete{{$data->supp_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
      <div class="modal-dialog">
        <form role="form" method="post" action="/supp-delete/masterPurchasing/delete-supp">
          {{csrf_field()}}
          <input type="hidden" name="supp_id" value="{{$data->supp_id}}">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                  <h4 id="modal-responsive-label" class="modal-title">Delete supplier {{$data->supp_code}}</h4></div>
              <div class="modal-body">
                  <div class="row">
                        <h3 style="text-align:center">Are you sure want to delete this supplier ?</h3>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                  <button type="submit" class="btn btn-primary">Yes</button>
              </div>
          </div>
        </form>
      </div>
  </div>
  <script>
        $(document).ready(function() {
          $('form').each(function () {
             $(this).validate();
          });
        });
  </script>
