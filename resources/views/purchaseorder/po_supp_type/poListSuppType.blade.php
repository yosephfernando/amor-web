@extends('layout.layout')
@section('content')
<!--BEGIN TITLE & BREADCRUMB PAGE-->
            <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                <div class="page-header pull-left">
                    <div class="page-title">{{$title->moduleLabel}}</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a href="#">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li><a href="#">Master purchasing</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li class="active">{{$title->moduleLabel}}</li>
                </ol>
                <div class="clearfix"></div>
            </div>
            <!--END TITLE & BREADCRUMB PAGE-->
           <div class="page-content">
              <div class="row">
                <div class="col-md-12">
                <div class="panel panel-green">
                        <div class="panel-body">
                          @if($errors->any())
                              <script>
                                     toastr.error("{{$errors->first()}}");
                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                          @if(session('status'))
                              <script>
                                 @if(session('status') == 'Supplier type added' || session('status') == 'Supplier type updated' || session('status') == 'Supplier type deleted')
                                   toastr.success("{{ session('status') }}");
                                 @else
                                   toastr.error("{{ session('status') }}");
                                 @endif

                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                          <div class="row">
                            <div class="col-md-3">
                                @if($privilaged->canAdd != 0)
                                   <button type="button" class="btn btn-primary btn-md" data-target="#suppTypeAdd" data-toggle="modal">
                                     <i class="fa fa-plus"></i>&nbsp;
                                       Add
                                   </button>
                                @endif
                                @if($privilaged->canExport != 0)
                                   <a href="/exportSuppType/masterPurchasing/exportSuppTypeToExcel" class="btn btn-success btn-md"><i class="fa fa-download"></i>&nbsp;Export to xls</a>
                                @endif
                                <div class="clear" style="height:20px"></div>
                            </div>
                          </div>

                              <div class="col-md-12">
                                  <div class="table-responsive">

                                    <table class="table table-bordered" id="suppType-table">
                                        <thead>
                                          <tr>
                                            <th><input type="checkbox"  id="bulkDelete"  /></th>
                                            <th>No supplier type</th>
                                            <th>desc</th>
                                            <th>Action</th>
                                          </tr>
                                        </thead>
                                      </table>
                                      <script>
                                        $(function() {
                                          var table = $('#suppType-table').DataTable({
                                               processing: true,
                                               serverSide: true,
                                               ajax: {
                                                  url: '/listSuppTypeData/masterPurchasing/poSuppTypeDataTable',
                                               },
                                               columns: [
                                                   {data: 'delete_bulk', name: 'delete_bulk', orderable: false, searchable: false},
                                                   { data: 'supp_type_code', name: 'supp_type_code' },
                                                   { data: 'supp_type_desc', name: 'supp_type_desc' },
                                                   {data: 'action', name: 'action', className: 'text-center', orderable: false, searchable: false}
                                               ]
                                           });
                                            $('div.dataTables_filter input').addClass('form-control');

                                            $("#deleteTriger").click(function(){
                                               var allValue = [];
                                               $("input[name='supp_type_id[]']:checked").each( function () {
                                                   allValue.push($(this).val());
                                               });
                                               $.post( "/delete-bulk-supp-type/masterPurchasing/deleteBulkSuppType",  {'supp_type_id': allValue, '_token' : '{{csrf_token()}}' })
                                                 .done(function( data ) {
                                                   if(data == "success"){
                                                      location.reload();
                                                   }else{
                                                      alert(data);
                                                   }
                                                 });
                                             });

                                             $("#bulkDelete").click(function(){
                                                $("input[name='supp_type_id[]']").prop("checked", $(this).prop('checked'))
                                                if($("input[name='supp_type_id[]']:checked").length > 0){
                                                  $("#deleteTriger").show();
                                                }else{
                                                  $("#deleteTriger").hide();
                                                }
                                             });

                                             $("#suppType-table").on('click', ':checkbox', function(){
                                               if($("input[name='supp_type_id[]']:checked").length > 0){
                                                 $("#deleteTriger").show();
                                               }else{
                                                 $("#deleteTriger").hide();
                                               }
                                             });

                                             $('#date-range').on('submit', function(e) {
                                                 table.draw();
                                                 e.preventDefault();
                                             });

                                        });
                                     </script>
                                  </div>
                                  <button class='btn btn-danger' id="deleteTriger" style="display:none">Delete cheked</button>
                                  <div class="clear" style="height:20px"></div>
                              </div>
                            </div>
                        </div>
                    </div>
                  </div>
              </div>
          </div>

          <!--Modal add user-->
           <div id="suppTypeAdd" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
               <div class="modal-dialog">
                 <form role="form" method="post" action="/supp-type-add/masterPurchasing/add-supp-type">
                    {{csrf_field()}}
                    <input type="hidden"  id="coa_id_ap" name="coa_id_ap"/>
                    <input type="hidden"  id="coa_id_unbill" name="coa_id_unbill"/>
                    <input type="hidden"  id="coa_id_ppn" name="coa_id_ppn" />
                    <input type="hidden"  id="coa_id_ppv" name="coa_id_ppv" />
                   <div class="modal-content">
                       <div class="modal-header">
                           <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                           <h4 id="modal-responsive-label" class="modal-title">Add supplier type</h4></div>
                       <div class="modal-body">
                           <div class="row">
                               <div class="col-md-12">
                                   <div class="mbm row">
                                     <div class="col-md-6">
                                       <label>Supplier type code :</label>
                                       <input type="text" name="supp_type_code"  class="form-control required" placeholder="Please type the supplier type code"/>
                                     </div>
                                     <div class="col-md-6">
                                       <label>Supplier type desc :</label>
                                       <input type="text" name="supp_type_desc" class="form-control required" placeholder="Please type the supplier type description"/>
                                     </div>
                                   </div>
                                  <div class="mbm">
                                    <label>COA ap :</label>
                                    <input type="text" name="coa_ap_code"  id="coa_ap_code" class="form-control required" placeholder="Please type the supplier coa ap" />
                                  </div>
                                  <div class="mbm">
                                    <label>COA unbill :</label>
                                    <input type="text" name="coa_unbill_code"  id="coa_unbill_code" class="form-control required" placeholder="Please type the supplier coa unbill" />
                                  </div>
                                  <div class="mbm">
                                    <label>COA ppn :</label>
                                    <input type="text" name="coa_ppn_code"  id="coa_ppn_code" class="form-control required" placeholder="Please type the supplier coa ppn" />
                                  </div>
                                  <div class="mbm">
                                     <label>COA ppv :</label>
                                     <input type="text" name="coa_ppv_code"  id="coa_ppv_code" class="form-control required" placeholder="Please type the supplier coa ppv" />
                                  </div>
                               </div>
                           </div>
                       </div>
                       <div class="modal-footer">
                           <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                           <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                       </div>
                   </div>
                  </form>
               </div>
           </div>
           <script>
                 $(document).ready(function() {
                   $('form').each(function () {
                      $(this).validate();
                   });
                 });

                  $('#coa_ppv_code').devbridgeAutocomplete({
                          serviceUrl : '/lookupSugestion/coa',
                          minChars : 3,
                          onSelect: function (suggestion) {
                            $('#coa_id_ppv').val(suggestion.data);
                            $('#coa_ppv_code').val(suggestion.value);
                          }
                  });

                   $('#coa_ppn_code').devbridgeAutocomplete({
                           serviceUrl : '/lookupSugestion/coa',
                           minChars : 3,
                           onSelect: function (suggestion) {
                             $('#coa_id_ppn').val(suggestion.data);
                             $('#coa_ppn_code').val(suggestion.value);
                           }
                   });

                   $('#coa_unbill_code').devbridgeAutocomplete({
                           serviceUrl : '/lookupSugestion/coa',
                           minChars : 3,
                           onSelect: function (suggestion) {
                             $('#coa_id_unbill').val(suggestion.data);
                             $('#coa_unbill_code').val(suggestion.value);
                           }
                   });

                   $('#coa_ap_code').devbridgeAutocomplete({
                           serviceUrl : '/lookupSugestion/coa',
                           minChars : 3,
                           onSelect: function (suggestion) {
                             $('#coa_id_ap').val(suggestion.data);
                             $('#coa_ap_code').val(suggestion.value);
                           }
                   });

           </script>
@endsection
