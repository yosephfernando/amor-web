@if($privilaged->canEdit != 0)
       <button type="button" class="btn btn-default btn-xs" data-target="#suppType{{$data->supp_type_id}}" data-toggle="modal" >
         <i class="fa fa-pencil"></i>&nbsp;
           Edit
       </button>
@endif
@if($privilaged->canDelete != 0)
     <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->supp_type_id}}" data-toggle="modal">
       <i class="fa fa-trash"></i>&nbsp;
         Delete
     </button>
@endif
<!--Modal edit-->
 <div id="suppType{{$data->supp_type_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
     <div class="modal-dialog">
       <form role="form" method="post" action="/supp-type-edit/masterPurchasing/edit-supp-type">
         {{csrf_field()}}
         <input type="hidden" name="supp_type_id" value="{{$data->supp_type_id}}">
         <input type="hidden"  id="coa_id_ap{{$data->supp_type_id}}" name="coa_id_ap" value="{{$data->coa_id_ap}}">
         <input type="hidden"  id="coa_id_unbill{{$data->supp_type_id}}" name="coa_id_unbill" value="{{$data->coa_id_unbill}}">
         <input type="hidden"  id="coa_id_ppn{{$data->supp_type_id}}" name="coa_id_ppn" value="{{$data->coa_id_ppn}}">
         <input type="hidden"  id="coa_id_ppv{{$data->supp_type_id}}" name="coa_id_ppv" value="{{$data->coa_id_ppv}}">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                 <h4 id="modal-responsive-label" class="modal-title">Edit supplier type {{$data->supp_type_code}}</h4></div>
             <div class="modal-body">
                 <div class="row">
                   <div class="col-md-12">
                     <div class="mbm row">
                       <div class="col-md-6">
                         <label>Supplier type code :</label>
                         <input type="text" name="supp_type_code"  class="form-control required" value="{{$data->supp_type_code}}" placeholder="Please type the supplier type code"/>
                       </div>
                       <div class="col-md-6">
                         <label>Supplier type desc :</label>
                         <input type="text" name="supp_type_desc" class="form-control required" value="{{$data->supp_type_desc}}" placeholder="Please type the supplier type description"/>
                       </div>
                     </div>
                     <div class="mbm">
                        <label>COA ap :</label>
                            <input type="text" name="coa_ap_code"  id="coa_ap_code{{$data->supp_type_id}}" class="form-control required" value="{{$data->coa_code_ap}}-{{$data->coa_desc_ap}}" placeholder="Please type the supplier coa ap" />
                      </div>
                      <div class="mbm">
                         <label>COA unbill :</label>
                             <input type="text" name="coa_unbill_code"  id="coa_unbill_code{{$data->supp_type_id}}" class="form-control required" value="{{$data->coa_code_unbill}}-{{$data->coa_desc_unbill}}" placeholder="Please type the supplier coa unbill" />
                       </div>
                       <div class="mbm">
                          <label>COA ppn :</label>
                              <input type="text" name="coa_ppn_code"  id="coa_ppn_code{{$data->supp_type_id}}" class="form-control required" value="{{$data->coa_code_ppn}}-{{$data->coa_desc_ppn}}" placeholder="Please type the supplier coa ppn" />
                        </div>
                        <div class="mbm">
                           <label>COA ppv :</label>
                               <input type="text" name="coa_ppv_code"  id="coa_ppv_code{{$data->supp_type_id}}" class="form-control required" value="{{$data->coa_code_ppv}}-{{$data->coa_desc_ppv}}" placeholder="Please type the supplier coa ppv" />
                         </div>
                   </div>
                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                 <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
             </div>
         </div>
       </form>
     </div>
 </div>
 <!--Modal delete-->
  <div id="delete{{$data->supp_type_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
      <div class="modal-dialog">
        <form role="form" method="post" action="/supp-type-delete/masterPurchasing/delete-supp-type">
          {{csrf_field()}}
          <input type="hidden" name="supp_type_id" value="{{$data->supp_type_id}}">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                  <h4 id="modal-responsive-label" class="modal-title">Delete supplier type {{$data->supp_type_code}}</h4></div>
              <div class="modal-body">
                  <div class="row">
                        <h3 style="text-align:center">Are you sure want to delete this supplier type ?</h3>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                  <button type="submit" class="btn btn-primary">Yes</button>
              </div>
          </div>
        </form>
      </div>
  </div>

  <script>
        $(document).ready(function() {
          $('form').each(function () {
             $(this).validate();
          });
        });

        $('#coa_ap_code{{$data->supp_type_id}}').devbridgeAutocomplete({
                serviceUrl : '/lookupSugestion/coa',
                minChars : 3,
                onSelect: function (suggestion) {
                  $('#coa_id_ap{{$data->supp_type_id}}').val(suggestion.data);
                  $('#coa_ap_code{{$data->supp_type_id}}').val(suggestion.value);
                }
        });

        $('#coa_unbill_code{{$data->supp_type_id}}').devbridgeAutocomplete({
                serviceUrl : '/lookupSugestion/coa',
                minChars : 3,
                onSelect: function (suggestion) {
                  $('#coa_id_unbill{{$data->supp_type_id}}').val(suggestion.data);
                  $('#coa_unbill_code{{$data->supp_type_id}}').val(suggestion.value);
                }
        });

        $('#coa_ppn_code{{$data->supp_type_id}}').devbridgeAutocomplete({
                serviceUrl : '/lookupSugestion/coa',
                minChars : 3,
                onSelect: function (suggestion) {
                  $('#coa_id_ppn{{$data->supp_type_id}}').val(suggestion.data);
                  $('#coa_ppn_code{{$data->supp_type_id}}').val(suggestion.value);
                }
        });

        $('#coa_ppv_code{{$data->supp_type_id}}').devbridgeAutocomplete({
                serviceUrl : '/lookupSugestion/coa',
                minChars : 3,
                onSelect: function (suggestion) {
                  $('#coa_id_ppv{{$data->supp_type_id}}').val(suggestion.data);
                  $('#coa_ppv_code{{$data->supp_type_id}}').val(suggestion.value);
                }
        });
  </script>
