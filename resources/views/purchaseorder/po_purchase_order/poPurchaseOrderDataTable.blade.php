@if($privilaged->canEdit != 0)
   @if($data->po_status != "finish")
       <button type="button" class="btn btn-default btn-xs" data-target="#po{{$data->po_id}}" data-toggle="modal" >
         <i class="fa fa-pencil"></i>&nbsp;
           Edit
       </button>
   @else
       <button type="button" class="btn btn-default btn-xs" style="cursor:not-allowed">
         <i class="fa fa-pencil"></i>&nbsp;
           Edit
       </button>
   @endif
@endif
<a href="/add-detail-po/transactionPurchasing/poDetailPo/{{$data->po_id}}">
  <button type="button" class="btn btn-primary btn-xs">
    <i class="fa fa-plus"></i>&nbsp;
      Detail po
  </button>
</a>

@if($privilaged->canDelete != 0)
 @if($data->po_status != "finish")
     <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->po_id}}" data-toggle="modal">
       <i class="fa fa-trash"></i>&nbsp;
         Delete
     </button>
 @else
     <button type="button" class="btn btn-danger btn-xs" style="cursor:not-allowed">
       <i class="fa fa-trash"></i>&nbsp;
         Delete
     </button>
 @endif
@endif

<?php
   $dates = strtotime($data->po_date);
   $dateEdit = date("d-m-Y", $dates);
?>

<!--Modal edit-->
 <div id="po{{$data->po_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
     <div class="modal-dialog">
       <form role="form" method="post" action="/po-edit/transactionPurchasing/edit-po">
         {{csrf_field()}}
         <input type="hidden" name="po_id" value="{{$data->po_id}}">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                 <h4 id="modal-responsive-label" class="modal-title">Edit po {{$data->po_no}}</h4></div>
             <div class="modal-body">
                 <div class="row">
                     <div class="col-md-12">
                       <div class="mbm">
                          <label>Po no :</label>
                          <input type="text" placeholder="Please type purchase order number" name="po_no" class="form-control required" value="{{$data->po_no}}" />
                       </div>

                       <div class="mbm row">
                         <div class="col-md-6">
                              <label>Date :</label>
                              <input type="text" name="po_date" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" class="datepicker-default form-control required" value="{{$dateEdit}}"/>
                        </div>
                        <div class="col-md-6">
                              <label>Reg date :</label>
                              <input type="text" name="req_date" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" class="datepicker-default form-control required" value="{{$dateEdit}}"/>
                        </div>
                       </div>

                       <div class="mbm">
                          <label>PPN type :</label>
                          <select name="ppn_type" class="form-control required">
                              <option value="{{$data->ppn_type}}" selected>{{$data->ppn_type}}</option>
                          </select>
                       </div>
                       <div class="mbm">
                          <label>Notes :</label>
                          <textarea placeholder="Please type the purchase order note" name="notes" class="form-control required">{{$data->notes}}</textarea>
                       </div>
                       <div class="mbm">
                         <label>Supplier : </label>
                             <input type="hidden"  id="supp_id{{$data->po_id}}" name="supp_id"  class="form-control" value="{{$data->supp_id}}" required>
                             <input type="text"  id="autocompletesupp{{$data->po_id}}" placeholder="Please type the purchase order supplier" name="supp_desc" class="form-control required" value="{{$data->supp_desc}}">

                             <!-- warehouse catalog -->
                              <script>
                                   $('#autocompletesupp{{$data->po_id}}').devbridgeAutocomplete({
                                           serviceUrl : '/lookupSugestion/supplier',
                                           minChars : 3,
                                           onSelect: function (suggestion) {
                                             $('#supp_id{{$data->po_id}}').val(suggestion.data);
                                             $('#autocompletesupp{{$data->po_id}}').val(suggestion.value);
                                           }
                                   });
                              </script>
                       </div>
                       <div class="mbm">
                         <label>Gudang : </label>
                             <input type="hidden"  id="wh_id{{$data->po_id}}" name="wh_id"  class="form-control" value="{{$data->wh_id}}" required>
                             <input type="text"  id="autocompletewh{{$data->po_id}}" placeholder="Please type the purchase order warehouse" name="wh_desc" class="form-control required" value="{{$data->wh_desc}}">

                             <!-- warehouse catalog -->
                              <script>
                                   $('#autocompletewh{{$data->po_id}}').devbridgeAutocomplete({
                                           serviceUrl : '/lookupSugestion/warehouse',
                                           minChars : 2,
                                           onSelect: function (suggestion) {
                                             $('#wh_id{{$data->po_id}}').val(suggestion.data);
                                             $('#autocompletewh{{$data->po_id}}').val(suggestion.value);
                                           }
                                   });
                              </script>
                       </div>
                       <div class="mbm">
                          <label>Status :</label>
                          <select name="po_status" class="form-control required">
                                <option value="{{$data->po_status}}" selected>{{$data->po_status}}</option>
                                @if($privilaged->canApprove != 0)
                                  <option value="approve">Approve</option>
                                @endif
                                <option value="partial">Partial</option>
                                <option value="finish">Finish</option>
                          </select>
                       </div>

                     </div>
                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                 <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
             </div>
         </div>
       </form>
     </div>
 </div>

 <!--Modal delete-->
  <div id="delete{{$data->po_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
      <div class="modal-dialog">
        <form role="form" method="post" action="/po-delete/transactionPurchasing/delete-po">
          {{csrf_field()}}
          <input type="hidden" name="po_id" value="{{$data->po_id}}">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                  <h4 id="modal-responsive-label" class="modal-title">Delete po {{$data->po_id}}</h4></div>
              <div class="modal-body">
                  <div class="row">
                        <h3 style="text-align:center">Are you sure want to delete this po ?</h3>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                  <button type="submit" class="btn btn-primary">Yes</button>
              </div>
          </div>
        </form>
      </div>
  </div>
  <script>
      $(".datepicker-default").datepicker();

      $(document).ready(function() {
        $('form').each(function () {
           $(this).validate();
        });
      });
  </script>
