@if($privilaged->canEdit != 0)
  <button type="button" class="btn btn-default btn-xs" data-target="#group{{$data->group_id}}" data-toggle="modal" >
    <i class="fa fa-pencil"></i>&nbsp;
      Edit
  </button>
@endif
@if($privilaged->canDelete != 0)
  <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->group_id}}" data-toggle="modal">
    <i class="fa fa-trash"></i>&nbsp;
     Delete
  </button>
@endif

<!--Modal edit-->
 <div id="group{{$data->group_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
     <div class="modal-dialog">
       <form role="form" method="post" action="/editGroup/master/editGroup">
         {{csrf_field()}}
         <input type="hidden" name="group_id" value="{{$data->group_id}}" />
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                 <h4 id="modal-responsive-label" class="modal-title">Edit group {{$data->group_code}}</h4></div>
             <div class="modal-body">
                 <div class="row">
                     <div class="col-md-12">
                         <div class="mbm">
                            <label>Group code :</label>
                            <input type="text" placeholder="PLease type the group code" name="group_code" value="{{$data->group_code}}" class="form-control required" />
                         </div>
                         <div class="mbm">
                            <label>Group desc :</label>
                            <input type="text" placeholder="PLease type the group description" name="group_desc" value="{{$data->group_desc}}" class="form-control required"/>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                 <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
             </div>
         </div>
       </form>
     </div>
 </div>

 <!--Modal delete-->
  <div id="delete{{$data->group_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
      <div class="modal-dialog">
        <form role="form" method="post" action="/deleteGroup/master/deleteGroup">
          {{csrf_field()}}
          <input type="hidden" name="group_id" value="{{$data->group_id}}">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                  <h4 id="modal-responsive-label" class="modal-title">Delete group {{$data->group_desc}}</h4></div>
              <div class="modal-body">
                  <div class="row">
                        <h3 style="text-align:center">Are you sure want to delete this group ?</h3>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                  <button type="submit" class="btn btn-primary">Yes</button>
              </div>
          </div>
        </form>
      </div>
  </div>
  <script>
      $(document).ready(function() {
        $('form').each(function () {
           $(this).validate();
        });
      });
  </script>
