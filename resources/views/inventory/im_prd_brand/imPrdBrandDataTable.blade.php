@if($privilaged->canEdit != 0)
  <button type="button" class="btn btn-default btn-xs" data-target="#brand{{$data->brand_id}}" data-toggle="modal" >
    <i class="fa fa-pencil"></i>&nbsp;
      Edit
  </button>
@endif
@if($privilaged->canDelete != 0)
  <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->brand_id}}" data-toggle="modal">
    <i class="fa fa-trash"></i>&nbsp;
    Delete
  </button>
@endif

<!--Modal edit-->
<div id="brand{{$data->brand_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
    <div class="modal-dialog">
      <form role="form" method="post" action="/editBrand/master/editBrand">
        {{csrf_field()}}
        <input type="hidden" name="brand_id" value="{{$data->brand_id}}" />
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                <h4 id="modal-responsive-label" class="modal-title">Edit brand {{$data->brand_code}}</h4></div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="mbm">
                           <label>Brand code :</label>
                           <input type="text" placeholder="Please type the brand code" name="brand_code" value="{{$data->brand_code}}" class="form-control required"/>
                        </div>
                        <div class="mbm">
                           <label>Brand desc :</label>
                           <input type="text" placeholder="Please type the brand description" name="brand_desc" value="{{$data->brand_desc}}" class="form-control required"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
            </div>
        </div>
      </form>
    </div>
</div>

<!--Modal delete-->
 <div id="delete{{$data->brand_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
     <div class="modal-dialog">
       <form role="form" method="post" action="/deleteBrand/master/deleteBrand">
         {{csrf_field()}}
         <input type="hidden" name="brand_id" value="{{$data->brand_id}}">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                 <h4 id="modal-responsive-label" class="modal-title">Delete brand {{$data->brand_desc}}'</h4></div>
             <div class="modal-body">
                 <div class="row">
                       <h3 style="text-align:center">Are you sure want to delete this brand ?</h3>
                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                 <button type="submit" class="btn btn-primary">Yes</button>
             </div>
         </div>
       </form>
     </div>
 </div>

 <script>
     $(document).ready(function() {
       $('form').each(function () {
          $(this).validate();
       });
     });
 </script>
