@extends('layout.layout')
@section('content')
<!--BEGIN TITLE & BREADCRUMB PAGE-->
            <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                <div class="page-header pull-left">
                    <div class="page-title">{{$title->moduleLabel}}</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a href="index.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li><a href="#">Master inventory</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li class="active">{{$title->moduleLabel}}</li>
                </ol>
                <div class="clearfix"></div>
            </div>
            <!--END TITLE & BREADCRUMB PAGE-->
           <div class="page-content">
              <div class="row">
                <div class="col-md-12">
                <div class="panel panel-green">
                        <div class="panel-body">
                          @if($errors->any())
                              <script>
                                     toastr.error("{{$errors->first()}}");
                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                          @if(session('status'))
                              <script>
                                 @if(session('status') == 'Brand added' || session('status') == 'Brand updated' || session('status') == 'Brand deleted')
                                   toastr.success("{{ session('status') }}");
                                 @else
                                   toastr.error("{{ session('status') }}");
                                 @endif

                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                              <div class="col-md-12">
                                <div class="dropdown" style="float:left">
                                   @if($privilaged->canAdd != 0)
                                      <button type="button" class="btn btn-primary btn-md" data-target="#brandAdd" data-toggle="modal">
                                        <i class="fa fa-plus"></i>&nbsp;
                                          Add
                                      </button>
                                   @endif

                                   @if($privilaged->canExport != 0)
                                         <a href="/exportBrand/master/exportBrand">
                                           <button class="btn btn-success"><i class="fa fa-download"></i>&nbsp;Export to xls</button>
                                         </a>
                                   @endif
                                  <div class="clear" style="height:20px"></div>
                                </div>
                              </div>
                              <div class="col-md-12">
                                <div class="table-responsive">
                                        <table class="table table-bordered" id="brands-table">
                                              <thead>
                                                  <tr>
                                                    <th><input type="checkbox"  id="bulkDelete"  /></th>
                                                    <th>Brand code</th>
                                                    <th>Brand desc</th>
                                                    <th>Action</th>
                                                  </tr>
                                              </thead>
                                        </table>
                                        <script>
                                          $(function() {
                                            $('#brands-table').DataTable({
                                                 processing: true,
                                                 serverSide: true,
                                                 ajax: {url: '/listBrandData/master/imPrdBrandListData'},
                                                 columns: [
                                                     {data: 'delete_bulk', name: 'delete_bulk', orderable: false, searchable: false},
                                                     { data: 'brand_code', name: 'brand_code' },
                                                     { data: 'brand_desc', name: 'brand_desc' },
                                                     {data: 'action', name: 'action',  className: 'text-center', orderable: false, searchable: false}
                                                 ]
                                             });
                                              $('div.dataTables_filter input').addClass('form-control');

                                              $("#deleteTriger").click(function(){
                                                 var allValue = [];
                                                 $("input[name='brand_id[]']:checked").each( function () {
                                                     allValue.push($(this).val());
                                                 });
                                                 $.post( "/deleteBulkBrand/master/deleteBulkBrand",  {'brand_id': allValue, '_token' : '{{csrf_token()}}' })
                                                   .done(function( data ) {
                                                     if(data == "success"){
                                                        location.reload();
                                                     }else{
                                                        alert(data);
                                                     }
                                                     console.log( "Data Loaded: " + data );
                                                   });
                                               });

                                               $("#bulkDelete").click(function(){
                                                  $("input[name='brand_id[]']").prop("checked", $(this).prop('checked'));
                                                  if($("input[name='brand_id[]']:checked").length > 0){
                                                    $("#deleteTriger").show();
                                                  }else{
                                                    $("#deleteTriger").hide();
                                                  }
                                               });

                                               $("#brands-table").on('click', ':checkbox', function(){
                                                 if($("input[name='brand_id[]']:checked").length > 0){
                                                   $("#deleteTriger").show();
                                                 }else{
                                                   $("#deleteTriger").hide();
                                                 }
                                               });

                                          });
                                       </script>
                                 </div>
                                 <button class='btn btn-danger' id="deleteTriger" style="display:none">Delete cheked</button>
                                 <div class="clear" style="height:20px"></div>
                              </div>
                        </div>
                    </div>
                  </div>
              </div>
          </div>

          <!--Modal add user-->
           <div id="brandAdd" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
               <div class="modal-dialog">
                 <form role="form" method="post" action="/addBrand/master/addBrand">
                    {{csrf_field()}}
                   <div class="modal-content">
                       <div class="modal-header">
                           <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                           <h4 id="modal-responsive-label" class="modal-title">Add brand</h4></div>
                       <div class="modal-body">
                           <div class="row">
                               <div class="col-md-12">

                                       <div class="mbm">
                                          <label>Brand code :</label>
                                          <input type="text" placeholder="Please type the brand code" name="brand_code" class="form-control required"/>
                                       </div>
                                       <div class="mbm">
                                          <label>Brand desc :</label>
                                          <input type="text" placeholder="Please type the brand description" name="brand_desc" class="form-control required"/>
                                       </div>

                               </div>
                           </div>
                       </div>
                       <div class="modal-footer">
                           <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                           <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                       </div>
                   </div>
                  </form>
               </div>
           </div>
           <script>
               $(document).ready(function() {
                 $('form').each(function () {
                    $(this).validate();
                 });
               });
           </script>
@endsection
