@extends('layout.layout')
@section('content')
<!--BEGIN TITLE & BREADCRUMB PAGE-->
            <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                <div class="page-header pull-left">
                    <div class="page-title">{{$title->moduleLabel}}</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a href="index.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li><a href="#">{{Request::segment(2)}}</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li class="active">{{Request::segment(3)}}</li>
                </ol>
                <div class="clearfix"></div>
            </div>
            <!--END TITLE & BREADCRUMB PAGE-->
           <div class="page-content">
              <div class="row">
                <div class="col-md-12">
                <div class="panel panel-green">
                        <div class="panel-heading">Size data</div>
                        <div class="panel-body">
                          @if($errors->any())
                              <h4>{{$errors->first()}}</h4>
                          @endif
                          @if(session('status'))
                              <h4>{{ session('status') }}</h4>
                          @endif
                            <div class="table-responsive">

                              <div class="col-md-2 col-sm-12 col-xs-12 row">
                                 @if($privilaged->canAdd != 0)
                                    <button type="button" class="btn btn-success btn-md" data-target="#sizeAdd" data-toggle="modal">
                                        Add
                                    </button>
                                 @endif
                               </div>

                               <!-- bulk action -->
                               <div class="col-md-7">
                                 <div class="dropdown" style="float:right">
                                     <button class="btn btn-dafault" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Bulk action <span class="caret"></span></button>
                                     <ul class="dropdown-menu" aria-labelledby="dLabel">
                                        <li><a href="#" id="deletec">Delete</a></li>
                                      </ul>
                                       <button class="btn btn-danger" id="deleteC" style="display:none">Delete checked</button>
                                       <div class="clear" style="height:10px"></div>
                                 </div>
                                 <script>
                                     $("#deletec").click(function(){
                                         $("#deleteC").fadeIn(1000);
                                         $("#beforeDelete").hide();
                                         $("#afterDelete").show();
                                     });

                                     $("#deleteC").click(function(){
                                       var allValue = [];
                                       $("input[name='size_id[]']:checked").each( function () {
                                           allValue.push($(this).val());
                                       });
                                       $.post( "/delete-bulk-size/master/deleteBulkSize",  {'size_id': allValue, '_token' : '{{csrf_token()}}' })
                                         .done(function( data ) {
                                           if(data == "success"){
                                              location.reload();
                                           }else{
                                              alert(data);
                                           }
                                           console.log( "Data Loaded: " + data );
                                         });
                                     });
                                 </script>
                                <div class="clear" style="height:10px"></div>
                               </div>
                               <!-- end of bulk action -->

                               <!-- search -->
                                  <div class="col-md-3 nopadding">
                                      <div class="col-md-12 nopadding" style="float:right">
                                          <div class="input-group">
                                              <input id="searchKeyword" type="text" class="form-control" placeholder="Search">
                                              <span id="searchSize" style="cursor:pointer" class="input-group-addon"><i class="fa fa-search"></i></span>
                                          </div>
                                        <div class="clear" style="height:15px"></div>
                                      </div>
                                  </div>

                                  <script>
                                      $("#searchSize").click(function(){
                                          var keyword = $("#searchKeyword").val();
                                          if(keyword != ""){
                                            $.get( "/ajax-search-size/master/imPrdSizeAjaxSearch/"+keyword)
                                                .done(function( data ) {
                                                    $("#ajaxTableSearch").empty();
                                                    $("#ajaxTableSearch").html(data);
                                                });
                                          }else{
                                            alert('keyword empty');
                                          }
                                      });
                                  </script>
                               <!-- /search -->

                               <!-- table -->
                               <div id="ajaxTableSearch">
                                <table id="beforeDelete" class="table table-striped table-bordered table-hover">
                                  <div class="clear" style="height:10px"></div>
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Size code</th>
                                        <th>Size desc</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      <?php $i = $sizes->firstItem();?>
                                      @foreach($sizes as $data)
                                              <tr>
                                                  <td>{{$i}}</td>
                                                  <td>{{$data->size_code}}</td>
                                                  <td>{{$data->size_desc}}</td>
                                                  <td style="text-align:center">
                                                   @if($privilaged->canEdit != 0)
                                                          <button type="button" class="btn btn-default btn-md" data-target="#size{{$data->size_id}}" data-toggle="modal" >
                                                            <i class="fa fa-edit"></i>&nbsp;
                                                              Edit
                                                          </button>
                                                  @endif
                                                  @if($privilaged->canDelete != 0)
                                                        <button type="button" class="btn btn-danger btn-md" data-target="#delete{{$data->size_id}}" data-toggle="modal">
                                                            Delete
                                                        </button>
                                                  @endif
                                                  </td>
                                              </tr>

                                              <!--Modal edit-->
                                               <div id="size{{$data->size_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
                                                   <div class="modal-dialog">
                                                     <form role="form" method="post" action="/editSize/master/editSize">
                                                       {{csrf_field()}}
                                                       <input type="hidden" name="size_id" value="{{$data->size_id}}" />
                                                       <div class="modal-content">
                                                           <div class="modal-header">
                                                               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                                                               <h4 id="modal-responsive-label" class="modal-title">Edit size {{$data->size_code}}</h4></div>
                                                           <div class="modal-body">
                                                               <div class="row">
                                                                   <div class="col-md-12">
                                                                       <div class="mbm">
                                                                          <label>Size code :</label>
                                                                          <input type="text" name="size_code" value="{{$data->size_code}}" class="form-control" required/>
                                                                       </div>
                                                                       <div class="mbm">
                                                                          <label>Size desc :</label>
                                                                          <input type="text" name="size_desc" value="{{$data->size_desc}}" class="form-control" required/>
                                                                       </div>
                                                                   </div>
                                                               </div>
                                                           </div>
                                                           <div class="modal-footer">
                                                               <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                                                               <button type="submit" class="btn btn-primary">Save changes</button>
                                                           </div>
                                                       </div>
                                                     </form>
                                                   </div>
                                               </div>


                                               <!--Modal delete-->
                                                <div id="delete{{$data->size_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
                                                    <div class="modal-dialog">
                                                      <form role="form" method="post" action="/size-delete/master/deleteSize">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="size_id" value="{{$data->size_id}}">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                                                                <h4 id="modal-responsive-label" class="modal-title">Delete size {{$data->size_desc}}</h4></div>
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                      <h3 style="text-align:center">Are you sure want to delete this size ?</h3>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                                                                <button type="submit" class="btn btn-primary">Yes</button>
                                                            </div>
                                                        </div>
                                                      </form>
                                                    </div>
                                                </div>
                                            <?php $i++ ?>
                                       @endforeach
                                    </tbody>
                                </table>

                                <table id="afterDelete" class="table table-striped table-bordered table-hover" style="display:none">
                                  <div class="clear" style="height:10px"></div>
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Size code</th>
                                        <th>Size desc</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($sizes as $data)
                                              <tr>
                                                  <td><input type="checkbox" name="size_id[]" value="{{$data->size_id}}" /></td>
                                                  <td>{{$data->size_code}}</td>
                                                  <td>{{$data->size_desc}}</td>
                                              </tr>
                                       @endforeach
                                    </tbody>
                                </table>
                                {{$sizes->links()}}
                              </div>
                              <!-- /table -->
                            </div>
                        </div>
                    </div>
                  </div>
              </div>
          </div>

          <!--Modal add user-->
           <div id="sizeAdd" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
               <div class="modal-dialog">
                 <form role="form" method="post" action="/addSize/master/addSize">
                    {{csrf_field()}}
                   <div class="modal-content">
                       <div class="modal-header">
                           <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                           <h4 id="modal-responsive-label" class="modal-title">Add size</h4></div>
                       <div class="modal-body">
                           <div class="row">
                               <div class="col-md-12">

                                       <div class="mbm">
                                          <label>Size code :</label>
                                          <input type="text" name="size_code" class="form-control" required/>
                                       </div>
                                       <div class="mbm">
                                          <label>Size desc :</label>
                                          <input type="text" name="size_desc" class="form-control" required/>
                                       </div>

                               </div>
                           </div>
                       </div>
                       <div class="modal-footer">
                           <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                           <button type="submit" class="btn btn-primary">Save</button>
                       </div>
                   </div>
                  </form>
               </div>
           </div>

@endsection
