<h3>Search result of '{{$keyword}}'</h3>
<table id="beforeDelete" class="table table-striped table-bordered table-hover">
  <div class="clear" style="height:10px"></div>
    <thead>
    <tr>
        <th>#</th>
        <th>Size code</th>
        <th>Size desc</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
      <?php $i = 1;?>
      @foreach($sizeData as $data)
              <tr>
                  <td>{{$i}}</td>
                  <td>{{$data->size_code}}</td>
                  <td>{{$data->size_desc}}</td>
                  <td style="text-align:center">
                   @if($privilaged->canEdit != 0)
                          <button type="button" class="btn btn-default btn-md" data-target="#sizeAjax{{$data->size_id}}" data-toggle="modal" >
                            <i class="fa fa-edit"></i>&nbsp;
                              Edit
                          </button>
                  @endif
                  @if($privilaged->canDelete != 0)
                        <button type="button" class="btn btn-danger btn-md" data-target="#delete{{$data->size_id}}" data-toggle="modal">
                            Delete
                        </button>
                  @endif
                  </td>
              </tr>

              <!--Modal edit-->
               <div id="sizeAjax{{$data->size_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
                   <div class="modal-dialog">
                       <input type="hidden" id="idSize{{$data->size_id}}" value="{{$data->size_id}}" />
                       <div class="modal-content">
                           <div class="modal-header">
                               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                               <h4 id="modal-responsive-label" class="modal-title">Edit size {{$data->size_code}}</h4>
                           </div>
                           <div class="modal-body">
                               <div class="row">
                                   <div class="col-md-12">
                                       <h4 id="message{{$data->size_id}}" style="display:none">Data updated</h4>
                                       <h4 id="error{{$data->size_id}}" style="display:none">Error updating</h4>
                                       <div class="mbm">
                                          <label>Size code :</label>
                                          <input type="text" id="size_code{{$data->size_id}}" value="{{$data->size_code}}" class="form-control" required/>
                                       </div>
                                       <div class="mbm">
                                          <label>Size desc :</label>
                                          <input type="text" id="size_desc{{$data->size_id}}" value="{{$data->size_desc}}" class="form-control" required/>
                                       </div>
                                   </div>
                               </div>
                           </div>
                           <div class="modal-footer">
                               <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                               <button id="submit{{$data->size_id}}" class="btn btn-primary">Save changes</button>
                           </div>
                       </div>

                       <!-- ajax post data -->
                          <script>
                                $("#message{{$data->size_id}}").hide();
                                $("#error{{$data->size_id}}").hide();
                                $("#submit{{$data->size_id}}").click(function(){
                                  var id = $("#idSize{{$data->size_id}}").val();
                                  var size_desc = $("#size_desc{{$data->size_id}}").val();
                                  var size_code = $("#size_code{{$data->size_id}}").val();

                                  if(id != "" || size_desc != ""){
                                    $.post('/editSize/master/editSize', {'size_code' : size_code, 'size_desc' : size_desc, 'size_id' : id, '_token' : '{{csrf_token()}}'})
                                    .done(function(data){
                                        $("#message{{$data->size_id}}").show();
                                     })
                                     .error(function(xhr, status, error){
                                        $("#error{{$data->size_id}}").show();
                                     });
                                  }else{
                                    console.log("failed");
                                  }
                                });

                                $("#sizeAjax{{$data->size_id}}").on('hidden.bs.modal', function(e){
                                  $("#message{{$data->size_id}}").hide();
                                  $("#error{{$data->size_id}}").hide();
                                });
                          </script>
                       <!-- end of ajax post data -->

                   </div>
               </div>


               <!--Modal delete-->
                <div id="delete{{$data->size_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
                    <div class="modal-dialog">
                        <input type="hidden" id="sizeId{{$data->size_id}}" value="{{$data->size_id}}">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                                <h4 id="modal-responsive-label" class="modal-title">Delete size {{$data->size_code}}</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                      <h3 id="messageDelete{{$data->size_id}}" style="text-align:center">Are you sure want to delete this size ?</h3>
                                </div>
                            </div>
                            <div id="beforeDel{{$data->size_id}}" class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                                <button id="deleteSubmit{{$data->size_id}}" type="submit" class="btn btn-primary">Yes</button>
                            </div>
                            <div id="afterDel{{$data->size_id}}" class="modal-footer" style="display:none">
                                <button type="button" data-dismiss="modal" class="btn btn-default">Ok</button>
                            </div>
                        </div>

                      <!-- ajax post data -->
                         <script>
                               $("#deleteSubmit{{$data->size_id}}").click(function(){
                                 var id = $("#sizeId{{$data->size_id}}").val();

                                 if(id != ""){
                                   $.post('/size-delete/master/deleteSize', {'size_id' : id, '_token' : '{{csrf_token()}}' })
                                   .done(function(data){
                                       $("#beforeDel{{$data->size_id}}").hide();
                                       $("#afterDel{{$data->size_id}}").show();
                                       $("#messageDelete{{$data->size_id}}").text('Data deleted');
                                    })
                                    .error(function(xhr, status, error){
                                       $("#beforeDel{{$data->size_id}}").hide();
                                       $("#afterDel{{$data->size_id}}").show();
                                       $("#messageDelete{{$data->size_id}}").text('Error deleting');
                                    });
                                 }else{
                                   console.log("failed");
                                 }
                               });

                               $("#sizeAjax{{$data->size_id}}").on('hidden.bs.modal', function(e){
                                 $("#messageDelete{{$data->size_id}}").text('Are you sure want to delete this size ?');
                                 $("#beforeDel{{$data->size_id}}").show();
                                 $("#afterDel{{$data->size_id}}").hide();
                               });
                         </script>
                      <!-- end of ajax post data -->

                    </div>
                </div>
            <?php $i++ ?>
       @endforeach
    </tbody>
</table>
