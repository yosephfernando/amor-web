@if($privilaged->canEdit != 0)
       <button type="button" class="btn btn-default btn-xs" data-target="#wh{{$data->wh_id}}" data-toggle="modal" >
         <i class="fa fa-pencil"></i>&nbsp;
           Edit
       </button>
@endif
@if($privilaged->canDelete != 0)
     <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->wh_id}}" data-toggle="modal">
       <i class="fa fa-trash"></i>&nbsp;
         Delete
     </button>
@endif
<!--Modal edit-->
 <div id="wh{{$data->wh_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
     <div class="modal-dialog">
       <form role="form" method="post" action="/wh-edit/master/edit-wh">
         {{csrf_field()}}
         <input type="hidden" name="wh_id" value="{{$data->wh_id}}">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                 <h4 id="modal-responsive-label" class="modal-title">Edit warehouse {{$data->wh_code}}</h4></div>
             <div class="modal-body">
                 <div class="row">
                   <div class="col-md-12">
                     <div class="mbm row">
                       <div class="col-md-6">
                         <label>Warehouse code 1 :</label>
                         <input type="text" name="wh_code1"  class="form-control required" value="{{$data->wh_code1}}" placeholder="Please type the warehouse code 1" />
                       </div>
                       <div class="col-md-6">
                         <label>Warehouse code 2 :</label>
                         <input type="text" name="wh_code2"  class="form-control required" value="{{$data->wh_code2}}" placeholder="Please type the warehouse code 2" />
                       </div>
                     </div>
                     <div class="mbm">
                       <label>Warehouse desc :</label>
                       <input type="text" name="wh_desc" class="form-control required" value="{{$data->wh_desc}}" placeholder="Please type the warehouse description"/>
                     </div>
                     <div class="mbm">
                        <label>Warehouse type :</label>
                        <input type="hidden" id="wh_type_id{{$data->wh_id}}" name="wh_type_id" value="{{$data->wh_type_id}}"/>
                        <input type="text"  id="wh_type_desc{{$data->wh_id}}" name="wh_type_desc"  class="form-control required" placeholder="Please type the warehouse type" value="{{$data->wh_type_desc}}">

                        <!-- Barang catalog -->
                         <script>
                               var whs = {!! $whstype !!};
                                $('#wh_type_desc{{$data->wh_id}}').devbridgeAutocomplete({
                                        lookup: whs,
                                        onSelect: function (suggestion) {
                                          $('#wh_type_id{{$data->wh_id}}').val(suggestion.data);
                                          $('#wh_type_desc{{$data->wh_id}}').val(suggestion.value);
                                        }
                                });
                         </script>
                     </div>
                     <div class="mbm row">
                       <div class="col-md-6">
                          <label>Address 1 :</label>
                          <textarea name="wh_address1" class="form-control required" placeholder="Please type the warehouse address">{{$data->wh_address1}}</textarea>
                       </div>
                       <div class="col-md-6">
                          <label>Address 2 :</label>
                          <textarea name="wh_address2" class="form-control required" placeholder="Please type the warehouse address">{{$data->wh_address2}}</textarea>
                       </div>
                     </div>

                     <div class="mbm">
                        <label>Address 3 :</label>
                        <textarea name="wh_address3" class="form-control required" placeholder="Please type the warehouse address">{{$data->wh_address3}}</textarea>
                     </div>
                     <div class="mbm row">
                       <div class="col-md-6">
                          <label>City :</label>
                          <input type="text" name="wh_city" class="form-control required" value="{{$data->wh_city}}" placeholder="Please type the warehouse city"/>
                       </div>
                       <div class="col-md-6">
                          <label>Country :</label>
                          <input type="text" name="wh_country" class="form-control required"  value="{{$data->wh_country}}" placeholder="Please type the warehouse country"/>
                       </div>
                     </div>
                     <div class="mbm row">
                       <div class="col-md-6">
                          <label>Phone :</label>
                          <input type="text" name="wh_phone" class="form-control required number" value="{{$data->wh_phone}}" placeholder="Please type the warehouse phone number"/>
                       </div>
                       <div class="col-md-6">
                          <label>Fax :</label>
                          <input type="text" name="wh_fax" class="form-control required number" value="{{$data->wh_fax}}" placeholder="Please type the warehouse fax number"/>
                       </div>
                     </div>
                     <div class="mbm">
                        <label>Email :</label>
                        <input type="email" name="wh_email" class="form-control required email" value="{{$data->wh_email}}" placeholder="Please type the warehouse email address"/>
                     </div>
                     <div class="mbm">
                        <label>Post code :</label>
                        <input type="text" name="wh_post_code" class="form-control required number" value="{{$data->wh_post_code}}" placeholder="Please type the warehouse postal code" minlength="5" maxlength="5"/>
                     </div>
                     <div class="mbm">
                        <label>Contact person :</label>
                        <input type="text" name="wh_contact_person" class="form-control required" value="{{$data->wh_contact_person}}" placeholder="Please type the warehouse contact person"/>
                     </div>
                     <div class="mbm row">
                       <div class="col-md-6">
                          @if($data->active_flag != 1)
                            <input type="checkbox" name="active_flag" value="1" /><span> Active</span>
                          @else
                            <input type="checkbox" name="active_flag" value="1" checked/><span> Active</span>
                          @endif
                       </div>
                     </div>

                   </div>
                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                 <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
             </div>
         </div>
       </form>
     </div>
 </div>

 <!--Modal delete-->
  <div id="delete{{$data->wh_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
      <div class="modal-dialog">
        <form role="form" method="post" action="/wh-delete/master/delete-wh">
          {{csrf_field()}}
          <input type="hidden" name="wh_id" value="{{$data->wh_id}}">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                  <h4 id="modal-responsive-label" class="modal-title">Delete warehouse {{$data->wh_code}}</h4></div>
              <div class="modal-body">
                  <div class="row">
                        <h3 style="text-align:center">Are you sure want to delete this warehouse ?</h3>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                  <button type="submit" class="btn btn-primary">Yes</button>
              </div>
          </div>
        </form>
      </div>
  </div>
  <script>
        $(document).ready(function() {
          $('form').each(function () {
             $(this).validate();
          });
        });
  </script>
