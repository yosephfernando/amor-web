@if($privilaged->canEdit != 0)
       <button type="button" class="btn btn-default btn-xs" data-target="#whType{{$data->wh_type_id}}" data-toggle="modal" >
         <i class="fa fa-pencil"></i>&nbsp;
           Edit
       </button>
@endif
@if($privilaged->canDelete != 0)
     <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->wh_type_id}}" data-toggle="modal">
       <i class="fa fa-trash"></i>&nbsp;
         Delete
     </button>
@endif
<!--Modal edit-->
 <div id="whType{{$data->wh_type_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
     <div class="modal-dialog">
       <form role="form" method="post" action="/wh-type-edit/master/edit-wh-type">
         {{csrf_field()}}
         <input type="hidden" name="wh_type_id" value="{{$data->wh_type_id}}">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                 <h4 id="modal-responsive-label" class="modal-title">Edit warehouse type {{$data->wh_type_code}}</h4></div>
             <div class="modal-body">
                 <div class="row">
                   <div class="col-md-12">
                     <div class="mbm row">
                       <div class="col-md-6">
                         <label>Warehouse type code :</label>
                         <input type="text" name="wh_type_code"  class="form-control required" value="{{$data->wh_type_code}}" placeholder="Please type the warehouse type code"/>
                       </div>
                       <div class="col-md-6">
                         <label>Warehouse type desc :</label>
                         <input type="text" name="wh_type_desc" class="form-control required" value="{{$data->wh_type_desc}}" placeholder="Please type the warehouse type description"/>
                       </div>
                     </div>
                   </div>
                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                 <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
             </div>
         </div>
       </form>
     </div>
 </div>
 <!--Modal delete-->
  <div id="delete{{$data->wh_type_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
      <div class="modal-dialog">
        <form role="form" method="post" action="/wh-type-delete/master/delete-wh-type">
          {{csrf_field()}}
          <input type="hidden" name="wh_type_id" value="{{$data->wh_type_id}}">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                  <h4 id="modal-responsive-label" class="modal-title">Delete warehouse type {{$data->wh_type_code}}</h4></div>
              <div class="modal-body">
                  <div class="row">
                        <h3 style="text-align:center">Are you sure want to delete this warehouse type ?</h3>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                  <button type="submit" class="btn btn-primary">Yes</button>
              </div>
          </div>
        </form>
      </div>
  </div>

  <script>
        $(document).ready(function() {
          $('form').each(function () {
             $(this).validate();
          });
        });
  </script>
