@if($keyword != "null")
  {{buildSearch::searchMaster(
        'select brand_id, brand_code, brand_desc from "IM_PRD_BRAND" where brand_code LIKE '."'".'%'.$keyword.'%'."'".' OR brand_desc LIKE '."'".'%'.$keyword.'%'."'".'',
        "Brand catalog", Request::segment(5), ["brand_code", "brand_desc"]
        )
  }}
@else
  {{buildSearch::searchMaster(
        'select brand_id, brand_code, brand_desc from "IM_PRD_BRAND" order by brand_id desc LIMIT 10',
        "Brand catalog", Request::segment(5), ["brand_code", "brand_desc"]
        )
  }}
@endif
