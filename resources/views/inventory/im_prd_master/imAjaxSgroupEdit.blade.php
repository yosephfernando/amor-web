<table class="table">
    <tr>
      <thead>
        <td>#</td>
        <td>Size</td>
        <td>Order price</td>
        <td>Retail price</td>
        <td>Barcode</td>
      </thead>
    </tr>
      @foreach($prd as $prd)
          <tr>
            <td><input id="size_id{{$prd->size_id}}{{$prd->prd_id}}" name="size_id" type="checkbox" value="{{$prd->size_id}}" checked /></td>
            <td>{{$prd->size_desc}}</td>
            <td><input type="text" class="form-control" id="order_price{{$prd->size_id}}{{$prd->prd_id}}" name="order_price{{$prd->size_id}}{{$prd->prd_id}}" data-a-sep="." data-a-dec="," data-a-form="false" data-a-pad="false" value="{{number_format($prd->order_price, 0,'','.')}}" /></td>
            <td><input type="text" class="form-control" id="retail_price{{$prd->size_id}}{{$prd->prd_id}}" name="retail_price{{$prd->size_id}}{{$prd->prd_id}}" data-a-sep="." data-a-dec="," data-a-form="false" data-a-pad="false" value="{{number_format($prd->retail_price,0,'','.')}}" /></td>
            <td><input type="text" class="form-control" id="barcode{{$prd->size_id}}{{$prd->prd_id}}" name="barcode{{$prd->size_id}}{{$prd->prd_id}}" value="{{$prd->barcode}}" /></td>
          </tr>
          <script>
            $('#order_price{{$prd->size_id}}{{$prd->prd_id}}').autoNumeric('init');
            $('#retail_price{{$prd->size_id}}{{$prd->prd_id}}').autoNumeric('init');
          </script>
      @endforeach
</table>
