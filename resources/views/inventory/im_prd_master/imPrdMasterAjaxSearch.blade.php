<h3>Search result of '{{$keyword}}'</h3>
<table id="beforeDelete" class="table table-striped table-bordered table-hover">
  <div class="clear" style="height:10px"></div>
    <thead>
    <tr>
      <th>#</th>
      <th>Product code</th>
      <th>Product desc</th>
      <th>Product color</th>
      <th>Product size</th>
      <th>Action</th>
    </tr>
    </thead>
    <tbody>
      <?php $i = 1;?>
      @foreach($productData as $data)
              <tr>
                <td>{{$i}}</td>
                <td>{{$data->prd_code}}</td>
                <td>{{$data->prd_desc}}</td>
                <td>{{$data->color_desc}}</td>
                <td>{{$data->size_desc}}</td>
                <td style="text-align:center">
                 @if($privilaged->canEdit != 0)
                        <button type="button" class="btn btn-default btn-md" data-target="#product{{$data->prd_id}}" data-toggle="modal" >
                          <i class="fa fa-edit"></i>&nbsp;
                            Edit
                        </button>
                @endif
                @if($privilaged->canDelete != 0)
                      <button type="button" class="btn btn-danger btn-md" data-target="#delete{{$data->prd_id}}" data-toggle="modal">
                          Delete
                      </button>
                @endif
                </td>
              </tr>
              @include('inventory/im_prd_master/tesLoop')
            <?php $i++ ?>
       @endforeach
    </tbody>
</table>
