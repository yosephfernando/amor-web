@if($keyword != "null")
    {{buildSearch::searchMaster(
          'select sgroup_id, sgroup_code, sgroup_desc from "IM_PRD_SUB_GROUP" where sgroup_code LIKE '."'".'%'.$keyword.'%'."'".' OR sgroup_desc LIKE '."'".'%'.$keyword.'%'."'".'',
          "Sub group catalog", Request::segment(5), ["sgroup_code", "sgroup_desc"]
          )
    }}
@else
    {{buildSearch::searchMaster(
          'select sgroup_id, sgroup_code, sgroup_desc from "IM_PRD_SUB_GROUP" order by sgroup_id DESC LIMIT 10',
          "Sub group catalog", Request::segment(5), ["sgroup_code", "sgroup_desc"]
          )
    }}
@endif
