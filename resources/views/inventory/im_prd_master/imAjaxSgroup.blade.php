<table class="table">
    <tr>
      <thead>
        <td>#</td>
        <td>Size</td>
        <td>Order price</td>
        <td>Retail price</td>
        <td>Barcode</td>
      </thead>
    </tr>
      @foreach($sizeGroup as $sizeGroup)
          <tr>
            <td><input id="size{{$sizeGroup->size_id}}" name="size_id[]" type="checkbox" value="{{$sizeGroup->size_id}}" /></td>
            <td>{{$sizeGroup->size_desc}}</td>
            <td><input id="order_price_add{{$sizeGroup->size_id}}" type="text" class="form-control" placeholder="Please type the order price" name="order_price{{$sizeGroup->size_id}}"  data-a-sep="." data-a-dec="," data-a-form="false" data-a-pad="false" /></td>
            <td><input id="retail_price_add{{$sizeGroup->size_id}}" type="text" class="form-control" placeholder="Please type the retaiil price" name="retail_price{{$sizeGroup->size_id}}"  data-a-sep="." data-a-dec="," data-a-form="false" data-a-pad="false" /></td>
            <td><input id="barcode_add{{$sizeGroup->size_id}}" type="text" class="form-control" placeholder="Please type the barcode" name="barcode{{$sizeGroup->size_id}}" /></td>
          </tr>

          <script>
            $('#order_price_add{{$sizeGroup->size_id}}').autoNumeric('init');
            $('#retail_price_add{{$sizeGroup->size_id}}').autoNumeric('init');

            $("#size{{$sizeGroup->size_id}}").change(function(){
              if($("#size{{$sizeGroup->size_id}}").prop('checked') == true){
                $('#order_price_add{{$sizeGroup->size_id}}').addClass('required');
                $('#retail_price_add{{$sizeGroup->size_id}}').addClass('required');
                $('#barcode_add{{$sizeGroup->size_id}}').addClass('required');
              }else{
                $('#order_price_add{{$sizeGroup->size_id}}').removeClass('required');
                $('#retail_price_add{{$sizeGroup->size_id}}').removeClass('required');
                $('#barcode_add{{$sizeGroup->size_id}}').removeClass('required');
              }
            });


          </script>
      @endforeach
</table>
