@if($keyword != "null")
    {{buildSearch::searchMaster(
          'select group_id, group_code, group_desc from "IM_PRD_GROUP" where group_code LIKE '."'".'%'.$keyword.'%'."'".' OR group_desc LIKE '."'".'%'.$keyword.'%'."'".'',
          "Group catalog", Request::segment(5), ["group_code", "group_desc"]
          )
    }}
@else
    {{buildSearch::searchMaster(
          'select group_id, group_code, group_desc from "IM_PRD_GROUP" order by group_id DESC LIMIT 10',
          "Group catalog", Request::segment(5), ["group_code", "group_desc"]
          )
    }}
@endif
