<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
          <th>#</th>
          <th>Attribute name</th>
          <th>Code</th>
          <th>Desc</th>
        </tr>
    </thead>
  <?php $i = 1; ?>
  @foreach($fieldType as $data)
    <tr>
      <td>{{$i}}</td>
      <td>{{$data->prd_field_type_code}}</td>
      <td>
        {{ imPrdMaster::getFieldCodeEdit($data->prd_field_type_id, $prdId)}}
      </td>
      <td>{{$data->prd_field_type_desc}}</td>
    </tr>
   <?php $i++; ?>
  @endforeach
</table>
