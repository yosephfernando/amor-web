@extends('layout.layout')
@section('content')
<!--LOADING STYLESHEET FOR PAGE-->
<link type="text/css" rel="stylesheet" href="{{URL::asset('vendors/dropzone/css/dropzone.css')}}">
<!--BEGIN TITLE & BREADCRUMB PAGE-->
            <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                <div class="page-header pull-left">
                    <div class="page-title">{{$title->moduleLabel}}</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a href="index.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li><a href="#">Master inventory</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li class="active">{{$title->moduleLabel}}</li>
                </ol>
                <div class="clearfix"></div>
            </div>
            <!--END TITLE & BREADCRUMB PAGE-->
           <div class="page-content">
              <div class="row">
                <div class="col-md-12">
                <div class="panel panel-green">
                        <div class="panel-body">
                          @if($errors->any())
                              <script>
                                     toastr.error("{{$errors->first()}}");
                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                          @if(session('status'))
                              <script>
                                 @if(session('status') == 'Product added' || session('status') == 'Product updated' || session('status') == 'Product deleted')
                                   toastr.success("{{ session('status') }}");
                                 @else
                                   toastr.error("{{ session('status') }}");
                                 @endif

                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                              <div class="col-md-12">
                                <div class="col-md-2 col-sm-12 col-xs-12 row">
                                   @if($privilaged->canAdd != 0)
                                      <button type="button" class="btn btn-primary btn-md" data-target="#productAdd" data-toggle="modal">
                                        <i class="fa fa-plus"></i>&nbsp;
                                          Add
                                      </button>
                                   @endif
                                   <div class="clear" style="height:20px"></div>
                                 </div>
                              </div>
                              <div class="col-md-12">
                                  <div class="table-responsive">
                                    <table class="table table-bordered" id="prod-table">
                                          <thead>
                                              <tr>
                                                <th><input type="checkbox"  id="bulkDelete"  /></th>
                                                <th>Product code</th>
                                                <th>Product desc</th>
                                                <th>Product color</th>
                                                <th>Product size</th>
                                                <th>Action</th>
                                              </tr>
                                          </thead>
                                    </table>
                                    <script>
                                      $(function() {
                                        $('#prod-table').DataTable({
                                            "dom" : '<"pull-left clearT"f>t<"col-md-6"i><"col-md-6"p>',
                                             processing: true,
                                             serverSide: true,
                                             ajax: {url: '/listPrdMasterData/master/imPrdMasterListData'},
                                             columns: [
                                                 {data: 'delete_bulk', name: 'delete_bulk', orderable: false, searchable: false},
                                                 { data: 'prd_code', name: 'prd_code' },
                                                 { data: 'prd_desc', name: 'prd_desc', searchable: false },
                                                 { data: 'color_desc', name: 'color_desc', searchable: false },
                                                 { data: 'size_desc', name: 'size_desc', searchable: false },
                                                 {data: 'action', name: 'action', className: 'text-center', orderable: false, searchable: false}
                                             ]
                                         });
                                          $('div.dataTables_filter input').addClass('form-control');

                                          $("#deleteTriger").click(function(){
                                             var allValue = [];
                                             $("input[name='prd_id[]']:checked").each( function () {
                                                 allValue.push($(this).val());
                                             });
                                             $.post( "/deleteBulkProduct/inventory/deleteBulkProduct",  {'prd_id': allValue, '_token' : '{{csrf_token()}}' })
                                               .done(function( data ) {
                                                 if(data == "success"){
                                                    location.reload();
                                                 }else{
                                                    alert(data);
                                                 }
                                                 console.log( "Data Loaded: " + data );
                                               });
                                           });

                                           $("#bulkDelete").click(function(){
                                              $("input[name='prd_id[]']").prop("checked", $(this).prop('checked'));
                                              if($("input[name='prd_id[]']:checked").length > 0){
                                                $("#deleteTriger").show();
                                              }else{
                                                $("#deleteTriger").hide();
                                              }
                                           });

                                           $("#prod-table").on('click', ':checkbox', function(){
                                             if($("input[name='prd_id[]']:checked").length > 0){
                                               $("#deleteTriger").show();
                                             }else{
                                               $("#deleteTriger").hide();
                                             }
                                           });

                                      });
                                   </script>
                                  </div>
                                  <button class='btn btn-danger' id="deleteTriger" style="display:none">Delete cheked</button>
                                  <div class="clear" style="height:20px"></div>
                              </div>
                        </div>
                    </div>
                  </div>
              </div>
          </div>

          <!--Modal add user-->
           <div id="productAdd" tabindex="-1" role="dialog" aria-labelledby="modal-wide-width-label" aria-hidden="true" class="modal fade">
               <div class="modal-dialog modal-wide-width">
                 <form role="form" method="post" action="/addProduct/inventory/addProduct">
                    {{csrf_field()}}
                   <div class="modal-content">
                       <div class="modal-header">
                           <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                           <h4 id="modal-responsive-label" class="modal-title">Add product</h4></div>
                       <div class="modal-body">
                           <div class="row">
                               <div class="col-md-12">
                                        <ul id="myTab" class="nav nav-tabs">
                                            <li class="active"><a href="#home" data-toggle="tab">General information</a></li>
                                            <li><a href="#profile" data-toggle="tab">Additional information</a></li>
                                        </ul>
                                        <div id="myTabContent" class="tab-content" style="border:none">

                                            <div id="home" class="tab-pane fade in active">
                                                   <div class="col-md-4 col-sm-12 col-xs-12 row">
                                                      <div class="col-md-12 row">
                                                        <label>Brand : </label>
                                                            <input type="hidden"  id="brand_id" name="brand_id"  class="form-control">
                                                            <input type="text"  id="brand_code" placeholder="Please type the brand code" name="brand_code"  class="form-control required">

                                                            <!-- Barang catalog -->
                                                             <script>
                                                              $('#brand_code').devbridgeAutocomplete({
                                                                      serviceUrl : '/lookupSugestion/brand',
                                                                      minChars : 3,
                                                                      onSelect: function (suggestion) {
                                                                        $('#brand_id').val(suggestion.data);
                                                                        $('#brand_code').val(suggestion.value);
                                                                      }
                                                              });
                                                             </script>
                                                      </div>
                                                      <div class="col-md-12 row">
                                                        <label>Group : </label>
                                                            <input type="hidden"  id="group_id" name="group_id"  class="form-control">
                                                            <input type="text"  id="group_code" placeholder="Please type the brand code" name="group_code" class="form-control required">
                                                            <!-- Barang catalog -->
                                                             <script>
                                                              $('#group_code').devbridgeAutocomplete({
                                                                      serviceUrl : '/lookupSugestion/group',
                                                                      minChars : 3,
                                                                      onSelect: function (suggestion) {
                                                                        $('#group_id').val(suggestion.data);
                                                                        $('#group_code').val(suggestion.value);
                                                                      }
                                                              });
                                                             </script>
                                                      </div>
                                                      <div class="col-md-12 row">
                                                        <label>Sub group : </label>
                                                            <input type="hidden"  id="sgroup_id" name="sgroup_id"  class="form-control" >
                                                            <input type="text"  id="sgroup_code" placeholder="Please type the sub group code" class="form-control required">
                                                            <!-- Barang catalog -->
                                                             <script>
                                                                  $('#sgroup_code').devbridgeAutocomplete({
                                                                          serviceUrl : '/lookupSugestion/subgroup',
                                                                          minChars : 3,
                                                                          onSelect: function (suggestion) {
                                                                            $('#sgroup_id').val(suggestion.data);
                                                                            $('#sgroup_code').val(suggestion.value);
                                                                          }
                                                                  });
                                                             </script>
                                                      </div>
                                                      <div class="col-md-12 row">
                                                        <label>Color : </label>
                                                            <input type="hidden"  id="color_id" name="color_id"  class="form-control">
                                                            <input type="text"  id="color_code" placeholder="Please type the color code" name="color_code" class="form-control required">
                                                            <!-- Barang catalog -->
                                                             <script>
                                                                  $('#color_code').devbridgeAutocomplete({
                                                                          serviceUrl : '/lookupSugestion/color',
                                                                          minChars : 3,
                                                                          onSelect: function (suggestion) {
                                                                            $('#color_id').val(suggestion.data);
                                                                            $('#color_code').val(suggestion.value);
                                                                          }
                                                                  });
                                                             </script>
                                                      </div>
                                                      <div class="col-md-12 row">
                                                        <label>Produk code : </label>
                                                        <input type="text" class="form-control required" placeholder="Please type the product article" name="prd_code"/>
                                                      </div>
                                                      <div class="col-md-12 row">
                                                        <label>Produk desc : </label>
                                                        <textarea name="prd_desc" placeholder="Please type the product description" class="form-control required"></textarea>
                                                      </div>
                                                      <div class="col-md-12 row">
                                                        <label>Produk short desc : </label>
                                                        <textarea name="prd_short_desc" placeholder="Please type the product short description" class="form-control required"></textarea>
                                                        <input type="checkbox" name="active_flag" value="1"/>
                                                        <span>active</span>
                                                      </div>
                                                      <div class="col-md-6 row">
                                                          <label>Unit 1 : </label>
                                                          <select name="uom_id1" class="form-control required">
                                                            @foreach($unit as $data)
                                                              <option value="{{$data->uom_id}}">{{$data->uom_desc}} ({{$data->uom_code}})</option>
                                                            @endforeach
                                                          </select>
                                                      </div>
                                                      <div class="col-md-6">
                                                          <label>Unit 2 : </label>
                                                          <select name="uom_id2" class="form-control required">
                                                            @foreach($unit as $data)
                                                              <option value="{{$data->uom_id}}">{{$data->uom_desc}} ({{$data->uom_code}})</option>
                                                            @endforeach
                                                          </select>
                                                      </div>
                                                      <div class="col-md-12 row">
                                                        <label>Uom conveersion : </label>
                                                        <input type="number" class="form-control required number" name="uom_conversion" placeholder="Please type the product conversion"/>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-8 col-sm-12 col-xs-12 row">
                                                      <div class="col-md-12 row">
                                                          <label>Size group :</label>
                                                          <select name="size_group_code" id="sgroup" class="form-control required">
                                                                <option>Please choose one of the size group</option>
                                                             @foreach($sizeGroup as $size)
                                                                <option value="{{$size->size_group_code}}">{{sprintf("%'.03d\n", $size->size_group_code)}}</option>
                                                             @endforeach
                                                          </select>
                                                          <script>
                                                              $("#sgroup").change(function(){
                                                                var sgroupCode = $("#sgroup").val();
                                                                $.get( "/ajax-sgroup/inventory/imAjaxSgroup/"+sgroupCode)
                                                                    .done(function( data ) {
                                                                        $("#contentSizegroup").empty();
                                                                        $("#contentSizegroup").html(data);
                                                                   });
                                                              });
                                                          </script>
                                                          <div id="contentSizegroup"></div>
                                                      </div>
                                                   </div>
                                            </div>

                                            <div id="profile" class="tab-pane fade in">
                                                 <div class="col-md-12 col-sm-12 col-xs-12 row">
                                                    <div id="profile-node" class="col-md-12 table-responsive row">
                                                        <table class="table table-striped table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                  <th>#</th>
                                                                  <th>Attribute name</th>
                                                                  <th>Code</th>
                                                                  <th>Desc</th>
                                                                </tr>
                                                            </thead>
                                                          <?php $i = 1; ?>
                                                          @foreach($fieldType as $data)
                                                            <tr>
                                                              <td>{{$i}}</td>
                                                              <td>{{$data->prd_field_type_code}}</td>
                                                              <td>
                                                                {{ imPrdMaster::getFieldCode($data->prd_field_type_id)}}
                                                              </td>
                                                              <td>{{$data->prd_field_type_desc}}</td>
                                                            </tr>
                                                           <?php $i++; ?>
                                                          @endforeach
                                                        </table>
                                                    </div>
                                                 </div>
                                            </div>

                                        </div>
                               </div>
                           </div>
                       </div>
                       <div class="modal-footer">
                           <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                           <button id="save" type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                       </div>
                   </div>
                  </form>
               </div>
           </div>
           <!--LOADING SCRIPTS FOR PAGE
           <script src="{{URL::asset('vendors/dropzone/js/dropzone.js')}}"></script>
           <script>
               $('#productAdd').on('shown.bs.modal', function() {
                 if($(".dropzone").length == 0){
                   var d = document.createElement('div');
                   d.className = "dropzone";
                   $("#profile-node").append(d);

                   var myDropzone = new Dropzone(d, {
                     url: 'google.com',
                     dictDefaultMessage : '<span>Drop file here</span>',
                     paramName: "file", // The name that will be used to transfer the file
                     maxFilesize: 2,

                   });
                 }
                });


           </script>
           <!--script src="{{URL::asset('js/form-dropzone-file-upload.js')}}"></script-->
           <script>
               $(document).ready(function() {
                 $('form').each(function () {
                    $(this).validate();
                 });
               });
           </script>
@endsection
