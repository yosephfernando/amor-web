@if($privilaged->canEdit != 0)
       <button type="button" class="btn btn-default btn-xs" data-target="#product{{$data->prd_id}}" data-toggle="modal" >
         <i class="fa fa-pencil"></i>&nbsp;
           Edit
       </button>
@endif
@if($privilaged->canDelete != 0)
     <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->prd_id}}" data-toggle="modal">
       <i class="fa fa-trash"></i>&nbsp;
         Delete
     </button>
@endif

<!--Modal edit-->
 <div id="product{{$data->prd_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
     <div class="modal-dialog modal-wide-width">
       <!--form role="form" method="post" action="/editProduct/inventory/editProduct"-->
         <input type="hidden" id="prd_id{{$data->prd_id}}" name="prd_id" value="{{$data->prd_id}}" />
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                 <h4 id="modal-responsive-label" class="modal-title">Edit product {{$data->prd_id}}</h4></div>
             <div class="modal-body">
                 <div class="row">
                     <div class="col-md-12">
                       <ul id="myTabEdit{{$data->prd_id}}" class="nav nav-tabs">
                           <li class="active"><a href="#tabGen{{$data->prd_id}}" data-toggle="tab">General information</a></li>
                           <li><a href="#tabAdd{{$data->prd_id}}" data-toggle="tab">Additional information</a></li>
                       </ul>
                       <h4 id="succes-edit{{$data->prd_id}}" style="display:none">Product updated</h4>
                       <h4 id="error-edit{{$data->prd_id}}" style="display:none">Product updated</h4>
                       <div id="myTabContentEdit{{$data->prd_id}}" class="tab-content" style="border:none">

                           <div id="tabGen{{$data->prd_id}}" class="tab-pane fade in active">
                                  <div class="col-md-4 col-sm-12 col-xs-12 row">
                                     <div class="col-md-12 row">
                                       <label>Brand : </label>
                                           <input type="hidden"  id="brand_id{{$data->prd_id}}" name="brand_id"  class="form-control" value="{{$data->brand_id}}" required>
                                           <input type="text"  id="brand_code{{$data->prd_id}}" class="form-control" value="{{$data->brand_code}}-{{$data->brand_desc}}">
                                           <!-- Barang catalog -->
                                            <script>
                                                 $('#brand_code{{$data->prd_id}}').devbridgeAutocomplete({
                                                         serviceUrl : '/lookupSugestion/brand',
                                                         minChars : 3,
                                                         onSelect: function (suggestion) {
                                                           $('#brand_id{{$data->prd_id}}').val(suggestion.data);
                                                           $('#brand_code{{$data->prd_id}}').val(suggestion.value);
                                                         }
                                                 });
                                            </script>
                                     </div>
                                     <div class="col-md-12 row">
                                       <label>Group : </label>
                                           <input type="hidden"  id="group_id{{$data->prd_id}}" name="group_id"  class="form-control" value="{{$data->group_id}}" required>
                                           <input type="text"  id="group_code{{$data->prd_id}}" class="form-control" value="{{$data->group_code}}-{{$data->group_desc}}">
                                           <!-- Barang catalog -->
                                            <script>
                                               $('#group_code{{$data->prd_id}}').devbridgeAutocomplete({
                                                       serviceUrl : '/lookupSugestion/group',
                                                       minChars : 3,
                                                       onSelect: function (suggestion) {
                                                         $('#group_id{{$data->prd_id}}').val(suggestion.data);
                                                         $('#group_code{{$data->prd_id}}').val(suggestion.value);
                                                       }
                                               });
                                            </script>
                                     </div>
                                     <div class="col-md-12 row">
                                       <label>Sub group : </label>
                                           <input type="hidden"  id="sgroup_id{{$data->prd_id}}" name="sub_group_id"  class="form-control" value="{{$data->sub_group_id}}" required>
                                           <input type="text"  id="sgroup_code{{$data->prd_id}}" value="{{$data->sgroup_code}}-{{$data->sgroup_desc}}"  class="form-control">
                                           <!-- Barang catalog -->
                                            <script>
                                               $('#sgroup_code{{$data->prd_id}}').devbridgeAutocomplete({
                                                       serviceUrl : '/lookupSugestion/subgroup',
                                                       minChars : 3,
                                                       onSelect: function (suggestion) {
                                                         $('#sgroup_code{{$data->prd_id}}').val(suggestion.data);
                                                         $('#sgroup_code{{$data->prd_id}}').val(suggestion.value);
                                                       }
                                               });
                                            </script>
                                     </div>
                                     <div class="col-md-12 row">
                                       <label>Color : </label>
                                           <input type="hidden"  id="color_id{{$data->prd_id}}" name="color_id"  class="form-control" value="{{$data->color_id}}" required>
                                           <input type="text"  id="color_code{{$data->prd_id}}" class="form-control" value="{{$data->color_code}}-{{$data->color_desc}}">
                                           <!-- Barang catalog -->
                                            <script>
                                                 $('#color_code{{$data->prd_id}}').devbridgeAutocomplete({
                                                         serviceUrl : '/lookupSugestion/subgroup',
                                                         minChars : 3,
                                                         onSelect: function (suggestion) {
                                                           $('#color_id{{$data->prd_id}}').val(suggestion.data);
                                                           $('#color_code{{$data->prd_id}}').val(suggestion.value);
                                                         }
                                                 });
                                            </script>
                                     </div>
                                     <div class="col-md-12 row">
                                       <label>Produk code : </label>
                                       <input type="text" class="form-control" id="prd_code{{$data->prd_id}}" name="prd_code" value="{{$data->prd_code}}" required/>
                                     </div>
                                     <div class="col-md-12 row">
                                       <label>Produk desc : </label>
                                       <textarea id="prd_desc{{$data->prd_id}}" name="prd_desc" class="form-control" required>{{$data->prd_desc}}</textarea>
                                     </div>
                                     <div class="col-md-12 row">
                                       <label>Produk short desc : </label>
                                       <textarea id="prd_shord_desc{{$data->prd_id}}" name="prd_short_desc" class="form-control" required>{{$data->prd_short_desc}}</textarea>
                                       @if($data->active_flag == 1)
                                           <input type="checkbox" id="active_flag{{$data->prd_id}}" name="active_flag" value="1" checked/>
                                       @else
                                           <input type="checkbox" id="active_flag{{$data->prd_id}}" name="active_flag" value="0"/>
                                       @endif
                                            <span>active</span>
                                     </div>
                                     <div class="col-md-6 row">
                                         <label>Unit 1 : </label>
                                         <select id="uom1{{$data->prd_id}}" name="uom_id1" class="form-control" required>
                                             <option>-- choose --</option>
                                             <option selected value="{{$data->uom_id1}}">{{$data->uom_desc1}} ({{$data->uom_code1}})</option>
                                           @foreach($unit as $uom1)
                                             <option value="{{$uom1->uom_id}}">{{$uom1->uom_desc}} ({{$uom1->uom_code}})</option>
                                           @endforeach
                                         </select>
                                     </div>
                                     <div class="col-md-6">
                                         <label>Unit 2 : </label>
                                         <select id="uom2{{$data->prd_id}}" name="uom_id2" class="form-control" required>
                                             <option>-- choose --</option>
                                             <option selected value="{{$data->uom_id2}}">{{$data->uom_desc2}} ({{$data->uom_code2}})</option>
                                           @foreach($unit as $uom2)
                                             <option value="{{$uom2->uom_id}}">{{$uom2->uom_desc}} ({{$uom2->uom_code}})</option>
                                           @endforeach
                                         </select>
                                     </div>
                                     <div class="col-md-12 row">
                                       <label>Uom conveersion : </label>
                                       <input type="number" class="form-control" id="uom_conversion{{$data->prd_id}}" name="uom_conversion" value="{{number_format($data->uom_conversion)}}" required/>
                                     </div>
                                  </div>
                                  <div class="col-md-8 col-sm-12 col-xs-12 row">
                                     <div class="col-md-12 row">
                                         <label>Size group :</label>
                                         <select name="size_group_code" id="sgroup{{$data->prd_id}}" class="form-control" required>
                                               <option value="{{$data->size_group_code}}" selected>{{sprintf("%'.03d\n", $data->size_group_code)}}</option>
                                         </select>
                                         <script>
                                               $.get( "/ajaxSgroupEdit/inventory/imAjaxSgroupEdit/{{$data->prd_id}}")
                                                   .done(function( data ) {
                                                       $("#contentSizegroup{{$data->prd_id}}").empty();
                                                       $("#contentSizegroup{{$data->prd_id}}").html(data);
                                                  });
                                         </script>
                                         <div id="contentSizegroup{{$data->prd_id}}"></div>
                                     </div>
                                  </div>
                           </div>

                           <div id="tabAdd{{$data->prd_id}}" class="tab-pane fade in">
                                <div class="col-md-12 col-sm-12 col-xs-12 row">
                                   <script>
                                       $.get( "/ajaxAttrEdit/inventory/imAjaxAttrEdit/{{$data->prd_id}}")
                                           .done(function( data ) {
                                               $("#attr{{$data->prd_id}}").empty();
                                               $("#attr{{$data->prd_id}}").html(data);
                                          });
                                   </script>
                                   <div id="attr{{$data->prd_id}}" class="col-md-12 table-responsive row">

                                   </div>
                                </div>
                           </div>

                       </div>
                     </div>
                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                 <button id="submit{{$data->prd_id}}" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
             </div>
             <script>
                $("#submit{{$data->prd_id}}").click(function(){
                  var brand_id = $("#brand_id{{$data->prd_id}}").val();
                  var group_id = $("#group_id{{$data->prd_id}}").val();
                  var sgroup_id = $("#sgroup_id{{$data->prd_id}}").val();
                  var color_id = $("#color_id{{$data->prd_id}}").val();
                  var product_code = $("#prd_code{{$data->prd_id}}").val();
                  var product_desc = $("#prd_desc{{$data->prd_id}}").val();
                  var product_short_desc = $("#prd_shord_desc{{$data->prd_id}}").val();
                  var active_flag = $("#active_flag{{$data->prd_id}}").val();
                  var uom1 = $("#uom1{{$data->prd_id}}").val();
                  var uom2 = $("#uom2{{$data->prd_id}}").val();
                  var uom_conversion = $("#uom_conversion{{$data->prd_id}}").val();
                  var size_id = $("#size_id{{$data->size_id}}{{$data->prd_id}}").val();
                  var order_price = $("#order_price{{$data->size_id}}{{$data->prd_id}}").val();
                  var retail_price = $("#retail_price{{$data->size_id}}{{$data->prd_id}}").val();
                  var barcode = $("#barcode{{$data->size_id}}{{$data->prd_id}}").val();
                  var prd_id  = $("#prd_id{{$data->prd_id}}").val();
                  @foreach($fieldType as $fieldT)
                      var attrS{{$fieldT->prd_field_type_id}}{{$data->prd_id}} = $("#attrS{{$fieldT->prd_field_type_id}}{{$data->prd_id}}").val();
                      var prd_dim_id{{$fieldT->prd_field_type_id}}{{$data->prd_id}} = $("#prd_dim_id{{$fieldT->prd_field_type_id}}{{$data->prd_id}}").val();
                  @endforeach

                  if(brand_id != null && group_id != null && sgroup_id != null && color_id != null && product_code != null && product_desc != null && product_short_desc != null && active_flag != null && uom1 != null && uom2 != null && uom_conversion != null && size_id != null){
                    $.post('/editProduct/inventory/editProduct', {'prd_id' : prd_id, 'brand_id' : brand_id, 'group_id' : group_id, 'sgroup_id' : sgroup_id, 'color_id' : color_id, 'product_code' : product_code,
                    'product_desc' : product_desc, 'product_short_desc' : product_short_desc, 'active_flag' : active_flag, 'uom1' : uom1, 'uom2' : uom2,
                    'uom_conversion' : uom_conversion, 'size_id' : size_id, '_token' : '{{csrf_token()}}',
                    @foreach($fieldType as $fieldT)
                        'attrS{{$fieldT->prd_field_type_id}}{{$data->prd_id}}' : attrS{{$fieldT->prd_field_type_id}}{{$data->prd_id}}, 'prd_dim_id{{$fieldT->prd_field_type_id}}{{$data->prd_id}}' : prd_dim_id{{$fieldT->prd_field_type_id}}{{$data->prd_id}},
                    @endforeach
                    'order_price' : order_price, 'retail_price' : retail_price, 'barcode' : barcode
                    })
                    .done(function(data, xhr){
                          if(data == "success"){
                              toastr.success("data updated");
                              location.reload();
                          }else{
                              toastr.error("failed update data");
                              location.reload();
                          }

                     })
                     .error(function(xhr, status, error){
                        $("#error-edit{{$data->prd_id}}").show();
                     });

                     $("#product{{$data->prd_id}}").on('hidden.bs.modal', function(e){
                        $("#succes-edit{{$data->prd_id}}").hide();
                        $("#error-edit{{$data->prd_id}}").hide();
                     });

                  }else{
                      console.log('false');
                  }

                });
             </script>
         </div>
       <!--/form-->
     </div>
 </div>

 <!--Modal delete-->
   <div id="delete{{$data->prd_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
       <div class="modal-dialog">
         <form role="form" method="post" action="/deleteProduct/inventory/deleteProduct">
           {{csrf_field()}}
           <input type="hidden" name="prd_id" value="{{$data->prd_id}}">
           <div class="modal-content">
               <div class="modal-header">
                   <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                   <h4 id="modal-responsive-label" class="modal-title">Delete product {{$data->prd_short_desc}}</h4></div>
               <div class="modal-body">
                   <div class="row">
                         <h3 style="text-align:center">Are you sure want to delete this product ?</h3>
                   </div>
               </div>
               <div class="modal-footer">
                   <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                   <button type="submit" class="btn btn-primary">Yes</button>
               </div>
           </div>
         </form>
       </div>
   </div>
