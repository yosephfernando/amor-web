@if($privilaged->canEdit != 0)
   @if($data->do_status != "approve")
       <button type="button" class="btn btn-default btn-xs" data-target="#do{{$data->do_id}}" data-toggle="modal" >
         <i class="fa fa-pencil"></i>&nbsp;
           Edit
       </button>
   @else
       <button type="button" class="btn btn-default btn-xs" style="cursor:not-allowed">
         <i class="fa fa-pencil"></i>&nbsp;
           Edit
       </button>
   @endif
@endif
<a href="/add-detail-do/saleTransaction/doDetailDo/{{$data->do_id}}">
  <button type="button" class="btn btn-primary btn-xs">
      <i class="fa fa-plus"></i>&nbsp;
      Detail do
  </button>
</a>

@if($privilaged->canDelete != 0)
 @if($data->do_status != "approve")
     <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->do_id}}" data-toggle="modal">
       <i class="fa fa-trash"></i>&nbsp;
         Delete
     </button>
 @else
     <button type="button" class="btn btn-danger btn-xs" style="cursor:not-allowed">
       <i class="fa fa-trash"></i>&nbsp;
         Delete
     </button>
 @endif
@endif

<?php
   $dates = strtotime($data->do_date);
   $dateEdit = date("d-m-Y", $dates);
?>

<!--Modal edit-->
 <div id="do{{$data->do_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
     <div class="modal-dialog">
       <form role="form" method="post" action="/do-edit/saleTransaction/edit-do">
         {{csrf_field()}}
         <input type="hidden" name="do_id" value="{{$data->do_id}}">
         <input type="hidden" name="do_no" value="{{$data->do_no}}">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                 <h4 id="modal-responsive-label" class="modal-title">Edit do {{$data->do_no}}</h4></div>
             <div class="modal-body">
                 <div class="row">
                     <div class="col-md-12">
                       <div class="mbm">
                         <label>So no:</label>
                             <input type="hidden"  id="so_no{{$data->do_id}}" name="so_no" class="form-control" value="{{$data->so_no}}">
                             <input type="text" id="autocompleteso{{$data->do_id}}" placeholder="Please type the sales order number" name="so_no_code" class="form-control required" value="{{$data->so_no}}">

                             <!-- Barang catalog -->
                              <script>
                                 var so = {!! $so !!};

                                  $('#autocompleteso{{$data->do_id}}').devbridgeAutocomplete({
                                          lookup: so,
                                          onSelect: function (suggestion) {
                                            $('#so_no{{$data->do_id}}').val(suggestion.value);
                                            $('#autocompleteso{{$data->do_id}}').val(suggestion.value);
                                          }
                                  });
                              </script>
                       </div>
                       <div class="mbm">
                           <label>Date :</label>
                           <input type="text" name="do_date" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="{{$dateEdit}}" class="datepicker-default form-control" required/>
                       </div>
                       <div class="mbm">
                          <label>Notes :</label>
                          <textarea name="notes" class="form-control required">{{$data->notes}}</textarea>
                       </div>
                       <div class="mbm">
                         <label>Customer :</label>
                             <input type="hidden"  id="cust_id{{$data->do_id}}" name="cust_id"  class="form-control" value="{{$data->cust_id}}">
                             <input type="text" id="autocompletecust{{$data->do_id}}" placeholder="Please type the customer name" name="cust_desc" class="form-control required" value="{{$data->cust_desc}}">

                             <!-- Barang catalog -->
                              <script>
                                 var cust = {!! $cust !!};

                                  $('#autocompletecust{{$data->do_id}}').devbridgeAutocomplete({
                                          lookup: cust,
                                          onSelect: function (suggestion) {
                                            $('#cust_id{{$data->do_id}}').val(suggestion.data);
                                            $('#autocompletecust{{$data->do_id}}').val(suggestion.value);
                                          }
                                  });
                              </script>
                       </div>
                       <div class="mbm">
                         <label>Gudang : </label>
                             <input type="hidden"  id="wh_id{{$data->do_id}}" name="wh_id"  class="form-control" value="{{$data->wh_id}}" required>
                             <input type="text"  id="autocompletewh{{$data->do_id}}" placeholder="Please type the warehouse name" name="wh_desc" class="form-control required" value="{{$data->wh_desc}}">

                             <!-- warehouse catalog -->
                              <script>
                                  var wh = {!! $wh !!};

                                   $('#autocompletewh{{$data->do_id}}').devbridgeAutocomplete({
                                           lookup: wh,
                                           onSelect: function (suggestion) {
                                             $('#wh_id{{$data->do_id}}').val(suggestion.data);
                                             $('#autocompletewh{{$data->do_id}}').val(suggestion.value);
                                           }
                                   });
                              </script>
                       </div>
                       <div class="mbm">
                          <label>Status :</label>
                          <select name="do_status" class="form-control required">
                                <option value="{{$data->do_status}}" selected>{{$data->do_status}}</option>
                                @if($privilaged->canApprove != 0)
                                  <option value="approve">Approve</option>
                                @endif
                                <option value="partial">Partial</option>
                                <option value="finish">Finish</option>
                          </select>
                       </div>

                     </div>
                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                 <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
             </div>
         </div>
       </form>
     </div>
 </div>

 <!--Modal delete-->
  <div id="delete{{$data->do_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
      <div class="modal-dialog">
        <form role="form" method="post" action="/do-delete/saleTransaction/delete-do">
          {{csrf_field()}}
          <input type="hidden" name="do_id" value="{{$data->do_id}}">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                  <h4 id="modal-responsive-label" class="modal-title">Delete do {{$data->do_id}}</h4></div>
              <div class="modal-body">
                  <div class="row">
                        <h3 style="text-align:center">Are you sure want to delete this do ?</h3>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                  <button type="submit" class="btn btn-primary">Yes</button>
              </div>
          </div>
        </form>
      </div>
  </div>
  <script>
      $(".datepicker-default").datepicker();
      $('form').each(function () {
         $(this).validate();
      });
  </script>
