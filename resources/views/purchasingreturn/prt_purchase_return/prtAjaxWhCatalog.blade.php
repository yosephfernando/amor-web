@if($keyword != "null")
    {{buildSearch::searchMaster(
          'select wh_id, wh_desc from "IM_WH_LOC" where CAST(wh_id AS varchar) LIKE '."'".'%'.$keyword.'%'."'".' OR CAST(wh_desc AS varchar) LIKE '."'".'%'.$keyword.'%'."'".'',
          "Warehouse catalog", Request::segment(5), ["wh_id", "wh_desc"]
          )
    }}
@else
    {{buildSearch::searchMaster(
          'select wh_id, wh_desc from "IM_WH_LOC" ORDER BY wh_id LIMIT 10',
          "Warehouse catalog", Request::segment(5), ["wh_id", "wh_desc"]
          )
    }}
@endif
