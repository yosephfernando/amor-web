<style>
    #print{display:none}
    @media print{
      @page {
        size: potrait !important;
      }
      .page-content {display:none}
      #page-wrapper{border : 0px !important}
      #print {display:block}
    }
</style>

<div id="print" class="container-fluid">
  @if($privilaged->canPrint != 0)
  <div class="col-md-12 row">
    <h3>Purchase return</h3>
    <hr />
       <div class="col-sm-6">
         <p style="text-align:left">Kepada Yth<br />....</p>
         <p style="text-align:left">Alamat<br />....</p>
       </div>
       <div class="col-sm-6">
           <div class="col-sm-6">
             <p style="text-align:right">No</p>
             <p style="text-align:right">Tanggal </p>
           </div>
           <div class="col-sm-6" style="float:right">
              <p>: {{$prt_id->inv_no}}</p>
              <p>:
                <?php
                  $created_at = $prt_id->created_date;
                  $created_at = strToTime($created_at);
                  $created_at = date("d M Y", $created_at);
                  echo $created_at;
                 ?>
              </p>
           </div>
       </div>
   </div>
   <div class="col-md-12 row">
     <div class="clear" style="height:10px"></div>
     <div class="col-sm-3">
       <p>Dikirim ke</p>
       <p>Telpon</p>
       <p>Fax</p>
       <p>Contact P.</p>
     </div>
     <div class="col-sm-9">
       <p>: ....</p>
       <p>: ....</p>
       <p>: ....</p>
       <p>: ....</p>
     </div>
   </div>
   <div class="col-md-12">
       <div class="clear" style="height:10px"></div>
       <table class="table table-striped table-bordered table-hover">
         <thead>
           <tr>
             <th>#</th>
             <th>Nama barang</th>
             <th>Qty</th>
             <th>Unit</th>
             <th>Harga satuan</th>
             <th>Total harga</th>
           </tr>
         </thead>
         <?php $i = 1 ?>
         @foreach($detailPrt  as $data)
           <tr>
             <td class="detailPoId">{{$i}}</td>
             <td>{{$data->prd_desc}}</td>
             <td>{{number_format($data->qty_inv)}}</td>
             <td>{{$data->uom_desc}}</td>
             <td>
                <?php
                    echo $data->price_inv;
                 ?>
             </td>
             <td>
               <?php
                   echo $data->sub_total;
                ?>
             </td>
           </tr>
          <?php $i++ ?>
         @endforeach
       </table>
   </div>
   <hr>
   <div class="col-md-12 row">
     <div class="clear" style="height:10px"></div>
     <div class="col-sm-4">
       <p>Keterangan :</p>
     </div>
     <div class="col-sm-8">
        <div class="col-sm-8">
          <p style="text-align:right">Total</p>
          <p style="text-align:right">Disc</p>
          <p style="text-align:right">Total pembelian sebelum pajak</p>
          <p style="text-align:right">PPN</p>
          <p style="text-align:right">Total pembayaran</p>
        </div>
        <div class="col-sm-4">
          <p>:
            <?php
                echo $total;
             ?>
          </p>
          <p>: 0</p>
          <p>:
            <?php
                echo $total_bef_ppn;
             ?>
          </p>
          <p>: {{$total_ppn}}</p>
          <p>: 0</p>
        </div>
     </div>
   </div>
   <hr>
   <div class="col-md-12 row">
     <div class="clear" style="height:30px"></div>
      <div class="col-sm-4">
          <span>Dibuat/diminta oleh, </span>
          <div class="clear" style="height:100px"></div>
          ................................
      </div>
      <div class="col-sm-4">
          <span>Disetujui oleh, </span>
          <div class="clear" style="height:100px"></div>
          ................................
      </div>
      <div class="col-sm-4">
          <span>Diketahui oleh, </span>
          <div class="clear" style="height:100px"></div>
          ................................
      </div>
   </div>
  @else
    <h3 class="text-center">Not Permitted</h3>
  @endif

</div>
