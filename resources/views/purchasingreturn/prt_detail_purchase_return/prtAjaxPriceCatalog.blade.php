<label>Price :</label>
<input id="price_format" type="text" name="price_inv" class="form-control" value="{{number_format($retail_price->retail_price,3)}}" data-a-sep="." data-a-dec="," data-a-form="false">
<script>
  $('#price_format').autoNumeric('init');
</script>
