@if($privilaged->canEdit != 0)
       <button type="button" class="btn btn-default btn-xs" data-target="#cust{{$data->cust_id}}" data-toggle="modal" >
         <i class="fa fa-pencil"></i>&nbsp;
           Edit
       </button>
@endif
@if($privilaged->canDelete != 0)
     <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->cust_id}}" data-toggle="modal">
       <i class="fa fa-trash"></i>&nbsp;
         Delete
     </button>
@endif
<!--Modal edit-->
 <div id="cust{{$data->cust_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
     <div class="modal-dialog">
       <form role="form" method="post" action="/cust-edit/customerType/edit-cust">
         {{csrf_field()}}
         <input type="hidden" name="cust_id" value="{{$data->cust_id}}">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                 <h4 id="modal-responsive-label" class="modal-title">Edit customer {{$data->cust_code}}</h4></div>
             <div class="modal-body">
                 <div class="row">
                   <div class="col-md-12">
                     <div class="mbm row">
                       <div class="col-md-6">
                         <label>Customer code :</label>
                         <input type="text" placeholder="Please type the customer code" name="cust_code"  class="form-control required" value="{{$data->cust_code}}" />
                       </div>
                       <div class="col-md-6">
                         <label>Customer desc :</label>
                         <input type="text" placeholder="Please type the customer description" name="cust_desc" class="form-control required" value="{{$data->cust_desc}}"/>
                       </div>
                     </div>
                     <div class="mbm">
                        <label>Customer type :</label>
                        <input type="hidden" id="cust_type_id{{$data->cust_id}}" name="cust_type_id" value="{{$data->cust_type_id}}"/>
                        <input type="text"  id="cust_type_desc{{$data->cust_id}}" name="cust_type_desc"  class="form-control required" placeholder="Please type the customer type" value="{{$data->cust_type_desc}}">

                        <!-- Barang catalog -->
                         <script>
                               var custs = {!! $custType !!};
                                $('#cust_type_desc{{$data->cust_id}}').devbridgeAutocomplete({
                                        lookup: custs,
                                        onSelect: function (suggestion) {
                                          $('#cust_type_id{{$data->cust_id}}').val(suggestion.data);
                                          $('#cust_type_desc{{$data->cust_id}}').val(suggestion.value);
                                        }
                                });
                         </script>
                     </div>
                     <div class="mbm row">
                       <div class="col-md-6">
                          <label>Address 1 :</label>
                          <textarea placeholder="Please type the customer address" name="cust_address1" class="form-control required">{{$data->cust_address1}}</textarea>
                       </div>
                       <div class="col-md-6">
                          <label>Address 2 :</label>
                          <textarea placeholder="Please type the customer address" name="cust_address2" class="form-control required">{{$data->cust_address2}}</textarea>
                       </div>
                     </div>

                     <div class="mbm">
                        <label>Address 3 :</label>
                        <textarea placeholder="Please type the customer address" name="cust_address3" class="form-control required">{{$data->cust_address3}}</textarea>
                     </div>
                     <div class="mbm row">
                       <div class="col-md-6">
                          <label>City :</label>
                          <input type="text" placeholder="Please type the customer city" name="cust_city" class="form-control required" value="{{$data->cust_city}}"/>
                       </div>
                       <div class="col-md-6">
                          <label>Country :</label>
                          <input type="text" placeholder="Please type the customer country" name="cust_country" class="form-control required"  value="{{$data->cust_country}}"/>
                       </div>
                     </div>
                     <div class="mbm row">
                       <div class="col-md-6">
                          <label>Phone :</label>
                          <input type="text" placeholder="Please type the customer phone number" name="cust_phone" class="form-control required number" value="{{$data->cust_phone}}"/>
                       </div>
                       <div class="col-md-6">
                          <label>Fax :</label>
                          <input type="text" placeholder="Please type the customer fax number" name="cust_fax" class="form-control required number" value="{{$data->cust_fax}}"/>
                       </div>
                     </div>
                     <div class="mbm">
                        <label>Email :</label>
                        <input type="email" placeholder="Please type the customer email address" name="cust_email" class="form-control required email" value="{{$data->cust_email}}"/>
                     </div>
                     <div class="mbm">
                        <label>Post code :</label>
                        <input type="text" placeholder="Please type the customer postal code" name="cust_post_code" class="form-control required number" minlength="5" maxlength="5" value="{{$data->cust_post_code}}"/>
                     </div>
                     <div class="mbm">
                        <label>Contact person :</label>
                        <input type="text" placeholder="Please type the customer contact person" name="cust_contact_person" class="form-control required" value="{{$data->cust_contact_person}}"/>
                     </div>
                     <div class="mbm">
                        <label>Term :</label>
                        <input type="hidden" id="cust_term{{$data->cust_id}}" name="cust_term" value="{{$data->cust_term}}"/>
                        <input type="text"  id="cust_term_desc{{$data->cust_id}}" name="cust_term_desc"  class="form-control required" placeholder="Please type the supplier term" value="{{$data->pptc_term_desc}}">

                        <!-- Barang catalog -->
                         <script>
                               var terms = {!! $term !!};
                                $('#cust_term_desc{{$data->cust_id}}').devbridgeAutocomplete({
                                        lookup: terms,
                                        onSelect: function (suggestion) {
                                          $('#cust_term{{$data->cust_id}}').val(suggestion.data);
                                          $('#cust_term_desc{{$data->cust_id}}').val(suggestion.value);
                                        }
                                });
                         </script>
                     </div>
                     <div class="mbm row">
                       <div class="col-md-6">
                        @if($data->cust_pkp_flag != 1)
                          <input type="checkbox" name="cust_pkp_flag" value="1" /><span> PKP</span>
                        @else
                          <input type="checkbox" name="cust_pkp_flag" value="1" checked/><span> PKP</span>
                        @endif
                       </div>
                       <div class="col-md-6">
                          @if($data->cust_active_flag != 1)
                            <input type="checkbox" name="cust_active_flag" value="1" /><span> Active</span>
                          @else
                            <input type="checkbox" name="cust_active_flag" value="1" checked/><span> Active</span>
                          @endif
                       </div>
                     </div>

                   </div>
                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                 <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
             </div>
         </div>
       </form>
     </div>
 </div>

 <!--Modal delete-->
  <div id="delete{{$data->cust_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
      <div class="modal-dialog">
        <form role="form" method="post" action="/delete-customer/customerType/customer-delete">
          {{csrf_field()}}
          <input type="hidden" name="cust_id" value="{{$data->cust_id}}">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                  <h4 id="modal-responsive-label" class="modal-title">Delete customer {{$data->cust_code}}</h4></div>
              <div class="modal-body">
                  <div class="row">
                        <h3 style="text-align:center">Are you sure want to delete this customer ?</h3>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                  <button type="submit" class="btn btn-primary">Yes</button>
              </div>
          </div>
        </form>
      </div>
  </div>
  <script>
      $(document).ready(function() {
        $('form').each(function () {
           $(this).validate();
        });
      });
  </script>
