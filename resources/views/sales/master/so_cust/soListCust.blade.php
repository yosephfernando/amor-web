@extends('layout.layout')
@section('content')
<!--BEGIN TITLE & BREADCRUMB PAGE-->
            <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                <div class="page-header pull-left">
                    <div class="page-title">{{$title->moduleLabel}}</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a href="#">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li><a href="#">Master sales</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li class="active">{{$title->moduleLabel}}</li>
                </ol>
                <div class="clearfix"></div>
            </div>
            <!--END TITLE & BREADCRUMB PAGE-->
           <div class="page-content">
              <div class="row">
                <div class="col-md-12">
                <div class="panel panel-green">
                        <div class="panel-body">
                          @if($errors->any())
                              <script>
                                     toastr.error("{{$errors->first()}}");
                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                          @if(session('status'))
                              <script>
                                 @if(session('status') == 'Customer added' || session('status') == 'Customer updated' || session('status') == 'Customer deleted')
                                   toastr.success("{{ session('status') }}");
                                 @else
                                   toastr.error("{{ session('status') }}");
                                 @endif

                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                          <div class="row">
                            <div class="col-md-3">
                                @if($privilaged->canAdd != 0)
                                   <button type="button" class="btn btn-primary btn-md" data-target="#custAdd" data-toggle="modal">
                                     <i class="fa fa-plus"></i>&nbsp;
                                       Add
                                   </button>
                                @endif
                                @if($privilaged->canExport != 0)
                                   <a href="/exportCustomer/customerType/exportCustomerToExcel" class="btn btn-success btn-md"><i class="fa fa-download"></i>&nbsp;Export to xls</a>
                                @endif
                                <div class="clear" style="height:20px"></div>
                            </div>
                          </div>

                              <div class="col-md-12">
                                  <div class="table-responsive">

                                    <table class="table table-bordered" id="cust-table">
                                        <thead>
                                          <tr>
                                            <th><input type="checkbox"  id="bulkDelete"  /></th>
                                            <th>No customer</th>
                                            <th>desc</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                          </tr>
                                        </thead>
                                      </table>
                                      <script>
                                        $(function() {
                                          var table = $('#cust-table').DataTable({
                                               processing: true,
                                               serverSide: true,
                                               ajax: {
                                                  url: '/listCustData/customerType/soCustDataTable',
                                               },
                                               columns: [
                                                   {data: 'delete_bulk', name: 'delete_bulk', orderable: false, searchable: false},
                                                   { data: 'cust_code', name: 'cust_code' },
                                                   { data: 'cust_desc', name: 'cust_desc' },
                                                   { data: 'cust_active_flag', name: 'cust_active_flag' },
                                                   {data: 'action', name: 'action', className: 'text-center', orderable: false, searchable: false}
                                               ]
                                           });
                                            $('div.dataTables_filter input').addClass('form-control');

                                            $("#deleteTriger").click(function(){
                                               var allValue = [];
                                               $("input[name='cust_id[]']:checked").each( function () {
                                                   allValue.push($(this).val());
                                               });
                                               $.post( "/delete-bulk-cust/customerType/deleteBulkCust",  {'cust_id': allValue, '_token' : '{{csrf_token()}}' })
                                                 .done(function( data ) {
                                                   if(data == "success"){
                                                      location.reload();
                                                   }else{
                                                      alert(data);
                                                   }
                                                   console.log( "Data Loaded: " + data );
                                                 });
                                             });

                                             $("#bulkDelete").click(function(){
                                                $("input[name='cust_id[]']").prop("checked", $(this).prop('checked'))
                                                if($("input[name='cust_id[]']:checked").length > 0){
                                                  $("#deleteTriger").show();
                                                }else{
                                                  $("#deleteTriger").hide();
                                                }
                                             });

                                             $("#cust-table").on('click', ':checkbox', function(){
                                               if($("input[name='cust_id[]']:checked").length > 0){
                                                 $("#deleteTriger").show();
                                               }else{
                                                 $("#deleteTriger").hide();
                                               }
                                             });

                                             $('#date-range').on('submit', function(e) {
                                                 table.draw();
                                                 e.preventDefault();
                                             });

                                        });
                                     </script>
                                  </div>
                                  <button class='btn btn-danger' id="deleteTriger" style="display:none">Delete cheked</button>
                                  <div class="clear" style="height:20px"></div>
                              </div>
                            </div>
                        </div>
                    </div>
                  </div>
              </div>
          </div>

          <!--Modal add user-->
           <div id="custAdd" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
               <div class="modal-dialog">
                 <form role="form" method="post" action="/add-cust/customerType/cust-add">
                    {{csrf_field()}}
                   <div class="modal-content">
                       <div class="modal-header">
                           <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                           <h4 id="modal-responsive-label" class="modal-title">Add customer</h4></div>
                       <div class="modal-body">
                           <div class="row">
                               <div class="col-md-12">
                                 <div class="mbm row">
                                   <div class="col-md-6">
                                     <label>Customer code :</label>
                                     <input type="text" placeholder="Please type the customer code" name="cust_code"  class="form-control required"/>
                                   </div>
                                   <div class="col-md-6">
                                     <label>Customer desc :</label>
                                     <input type="text" placeholder="Please type the customer description" name="cust_desc" class="form-control required"/>
                                   </div>
                                 </div>
                                 <div class="mbm">
                                    <label>Customer type :</label>
                                    <input type="hidden" id="cust_type_id" name="cust_type_id"/>
                                    <input type="text"  id="cust_type_desc" name="cust_type_desc"  class="form-control required" placeholder="Please type the customer type">

                                    <!-- Barang catalog -->
                                     <script>
                                           var custs = {!! $custs !!};
                                            $('#cust_type_desc').devbridgeAutocomplete({
                                                    lookup: custs,
                                                    onSelect: function (suggestion) {
                                                      $('#cust_type_id').val(suggestion.data);
                                                      $('#cust_type_desc').val(suggestion.value);
                                                    }
                                            });
                                     </script>
                                 </div>
                                 <div class="mbm row">
                                   <div class="col-md-6">
                                      <label>Address 1 :</label>
                                      <textarea placeholder="Please type the customer address" name="cust_address1" class="form-control required"></textarea>
                                   </div>
                                   <div class="col-md-6">
                                      <label>Address 2 :</label>
                                      <textarea placeholder="Please type the customer address" name="cust_address2" class="form-control required"></textarea>
                                   </div>
                                 </div>

                                 <div class="mbm">
                                    <label>Address 3 :</label>
                                    <textarea placeholder="Please type the customer address" name="cust_address3" class="form-control required"></textarea>
                                 </div>
                                 <div class="mbm row">
                                   <div class="col-md-6">
                                      <label>City :</label>
                                      <input type="text" placeholder="Please type the customer city" name="cust_city" class="form-control required"/>
                                   </div>
                                   <div class="col-md-6">
                                      <label>Country :</label>
                                      <input type="text" placeholder="Please type the customer country" name="cust_country" class="form-control required"/>
                                   </div>
                                 </div>
                                 <div class="mbm row">
                                   <div class="col-md-6">
                                      <label>Phone :</label>
                                      <input type="text" placeholder="Please type the customer phone number" name="cust_phone" class="form-control required number"/>
                                   </div>
                                   <div class="col-md-6">
                                      <label>Fax :</label>
                                      <input type="text" placeholder="Please type the customer fax number" name="cust_fax" class="form-control required number"/>
                                   </div>
                                 </div>
                                 <div class="mbm">
                                    <label>Email :</label>
                                    <input type="email" placeholder="Please type the customer email address" name="cust_email" class="form-control required email"/>
                                 </div>
                                 <div class="mbm">
                                    <label>Post code :</label>
                                    <input type="text" placeholder="Please type the customer postal code" name="cust_post_code" class="form-control required number" minlength="5" maxlength="5"/>
                                 </div>
                                 <div class="mbm">
                                    <label>Contact person :</label>
                                    <input type="text" placeholder="Please type the customer contact person" name="cust_contact_person" class="form-control required"/>
                                 </div>
                                 <div class="mbm">
                                    <label>Term :</label>
                                    <input type="hidden" id="cust_term" name="cust_term"/>
                                    <input type="text"  id="cust_term_desc" name="cust_term_desc"  class="form-control required" placeholder="Please type the customer term">

                                    <!-- Barang catalog -->
                                     <script>
                                           var terms = {!! $term !!};
                                            $('#cust_term_desc').devbridgeAutocomplete({
                                                    lookup: terms,
                                                    onSelect: function (suggestion) {
                                                      $('#cust_term').val(suggestion.data);
                                                      $('#cust_term_desc').val(suggestion.value);
                                                    }
                                            });
                                     </script>
                                 </div>
                                 <div class="mbm row">
                                   <div class="col-md-6">
                                      <input type="checkbox" name="cust_pkp_flag" value="1" /><span> PKP</span>
                                   </div>
                                   <div class="col-md-6">
                                      <input type="checkbox" name="cust_active_flag" value="1" /><span> Active</span>
                                   </div>
                                 </div>

                               </div>
                           </div>
                       </div>
                       <div class="modal-footer">
                           <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                           <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                       </div>
                   </div>
                  </form>
               </div>
           </div>
           <script>
               $(document).ready(function() {
                 $('form').each(function () {
                    $(this).validate();
                 });
               });
           </script>
@endsection
