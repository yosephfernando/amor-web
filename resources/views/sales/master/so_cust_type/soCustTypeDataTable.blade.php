@if($privilaged->canEdit != 0)
       <button type="button" class="btn btn-default btn-xs" data-target="#custType{{$data->cust_type_id}}" data-toggle="modal" >
         <i class="fa fa-pencil"></i>&nbsp;
           Edit
       </button>
@endif
@if($privilaged->canDelete != 0)
     <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->cust_type_id}}" data-toggle="modal">
       <i class="fa fa-trash"></i>&nbsp;
         Delete
     </button>
@endif
<!--Modal edit-->
 <div id="custType{{$data->cust_type_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
     <div class="modal-dialog">
       <form role="form" method="post" action="/edit-customer-type/customerType/customer-type-edit">
         {{csrf_field()}}
         <input type="hidden" name="cust_type_id" value="{{$data->cust_type_id}}">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                 <h4 id="modal-responsive-label" class="modal-title">Edit customer type {{$data->cust_type_code}}</h4></div>
             <div class="modal-body">
                 <div class="row">
                   <div class="col-md-12">
                     <div class="mbm row">
                       <div class="col-md-6">
                         <label>Customer type code :</label>
                         <input type="text" placeholder="Please type the customer type code" name="cust_type_code"  class="form-control required" value="{{$data->cust_type_code}}" />
                       </div>
                       <div class="col-md-6">
                         <label>Customer type desc :</label>
                         <input type="text" placeholder="Please type the customer type description" name="cust_type_desc" class="form-control required" value="{{$data->cust_type_desc}}"/>
                       </div>
                     </div>
                     <div class="mbm">
                        <label>COA ar :</label>
                            <input type="hidden"  id="coa_id_ar{{$data->cust_type_id}}" name="coa_id_ar"  class="form-control" value="{{$data->coa_id_ar}}">
                            <input type="text"  id="coa_ar_code{{$data->cust_type_id}}" placeholder="Please type the customer type COA AR" name="coa_ar_code" class="form-control required" value="{{$data->coa_code_ar}}-{{$data->coa_desc_ar}}">
                            <!-- Coa catalog -->
                             <script>
                                 var coa_ar = {!! $coaSugest !!};

                                  $('#coa_ar_code{{$data->cust_type_id}}').devbridgeAutocomplete({
                                          lookup: coa_ar,
                                          onSelect: function (suggestion) {
                                            $('#coa_id_ar{{$data->cust_type_id}}').val(suggestion.data);
                                            $('#coa_ar_code{{$data->cust_type_id}}').val(suggestion.value);
                                          }
                                  });
                             </script>
                      </div>
                      <div class="mbm">
                        <label>COA dp :</label>
                            <input type="hidden"  id="coa_id_dp{{$data->cust_type_id}}" name="coa_id_dp"  class="form-control" value="{{$data->coa_id_dp}}">
                            <input type="text"  id="coa_dp_code{{$data->cust_type_id}}" placeholder="Please type the customer type COA DP" name="coa_dp_code" class="form-control required" value="{{$data->coa_code_dp}}-{{$data->coa_desc_dp}}">
                            <!-- Coa catalog -->
                             <script>
                                 var coa_dp = {!! $coaSugest !!};

                                  $('#coa_dp_code{{$data->cust_type_id}}').devbridgeAutocomplete({
                                          lookup: coa_ar,
                                          onSelect: function (suggestion) {
                                            $('#coa_id_dp{{$data->cust_type_id}}').val(suggestion.data);
                                            $('#coa_dp_code{{$data->cust_type_id}}').val(suggestion.value);
                                          }
                                  });
                             </script>
                      </div>
                      <div class="mbm">
                        <label>COA ppn :</label>
                            <input type="hidden"  id="coa_id_ppn{{$data->cust_type_id}}" name="coa_id_ppn"  class="form-control" value="{{$data->coa_id_ppn}}">
                            <input type="text"  id="coa_ppn_code{{$data->cust_type_id}}" placeholder="Please type the customer type COA PPN" name="coa_ppn_code" class="form-control required" value="{{$data->coa_code_ppn}}-{{$data->coa_desc_ppn}}">
                            <!-- Coa catalog -->
                             <script>
                                   var coa_ppn = {!! $coaSugest !!};

                                    $('#coa_ppn_code{{$data->cust_type_id}}').devbridgeAutocomplete({
                                            lookup: coa_ppn,
                                            onSelect: function (suggestion) {
                                              $('#coa_id_ppn{{$data->cust_type_id}}').val(suggestion.data);
                                              $('#coa_ppn_code{{$data->cust_type_id}}').val(suggestion.value);
                                            }
                                    });
                             </script>
                        </div>
                        <div class="mbm">
                          <label>COA sales :</label>
                              <input type="hidden"  id="coa_id_sales{{$data->cust_type_id}}" name="coa_id_sales"  class="form-control" value="{{$data->coa_id_sales}}">
                              <input type="text"  id="coa_sales_code{{$data->cust_type_id}}" placeholder="Please type the customer type COA SALES" name="coa_sales_code" class="form-control required" value="{{$data->coa_code_sales}}-{{$data->coa_desc_sales}}">
                              <!-- Coa catalog -->
                               <script>
                                   var coa_sales = {!! $coaSugest !!};

                                    $('#coa_sales_code{{$data->cust_type_id}}').devbridgeAutocomplete({
                                            lookup: coa_sales,
                                            onSelect: function (suggestion) {
                                              $('#coa_id_sales{{$data->cust_type_id}}').val(suggestion.data);
                                              $('#coa_sales_code{{$data->cust_type_id}}').val(suggestion.value);
                                            }
                                    });
                               </script>
                        </div>
                        <div class="mbm">
                            <label>COA sales return :</label>
                                <input type="hidden"  id="coa_id_sales_return{{$data->cust_type_id}}" name="coa_id_sales_return"  class="form-control" value="{{$data->coa_id_sales_return}}">
                                <input type="text"  id="coa_sales_return_code{{$data->cust_type_id}}" placeholder="Please type the customer type COA SALES RETURN" name="coa_sales_return_code" class="form-control required" value="{{$data->coa_code_sales_return}}-{{$data->coa_desc_sales_return}}">
                                <!-- Coa catalog -->
                                 <script>
                                       var coa_sales_return = {!! $coaSugest !!};

                                        $('#coa_sales_return_code{{$data->cust_type_id}}').devbridgeAutocomplete({
                                                lookup: coa_sales_return,
                                                onSelect: function (suggestion) {
                                                  $('#coa_id_sales_return{{$data->cust_type_id}}').val(suggestion.data);
                                                  $('#coa_sales_return_code{{$data->cust_type_id}}').val(suggestion.value);
                                                }
                                        });
                                 </script>
                        </div>
                        <div class="mbm">
                              <label>COA disc :</label>
                                  <input type="hidden"  id="coa_id_disc{{$data->cust_type_id}}" name="coa_id_disc"  class="form-control" value="{{$data->coa_id_disc}}">
                                  <input type="text"  id="coa_disc_code{{$data->cust_type_id}}" placeholder="Please type the customer type COA DISC" name="coa_disc_code" class="form-control required" value="{{$data->coa_code_disc}}-{{$data->coa_desc_disc}}">
                                  <!-- Coa catalog -->
                                   <script>
                                         var coa_disc = {!! $coaSugest !!};

                                          $('#coa_disc_code{{$data->cust_type_id}}').devbridgeAutocomplete({
                                                  lookup: coa_disc,
                                                  onSelect: function (suggestion) {
                                                    $('#coa_id_disc{{$data->cust_type_id}}').val(suggestion.data);
                                                    $('#coa_disc_code{{$data->cust_type_id}}').val(suggestion.value);
                                                  }
                                          });
                                   </script>
                        </div>
                   </div>
                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                 <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
             </div>
         </div>
       </form>
     </div>
 </div>

 <!--Modal delete-->
  <div id="delete{{$data->cust_type_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
      <div class="modal-dialog">
        <form role="form" method="post" action="/delete-customer-type/customerType/customer-type-delete">
          {{csrf_field()}}
          <input type="hidden" name="cust_type_id" value="{{$data->cust_type_id}}">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                  <h4 id="modal-responsive-label" class="modal-title">Delete customer type {{$data->cust_type_code}}</h4></div>
              <div class="modal-body">
                  <div class="row">
                        <h3 style="text-align:center">Are you sure want to delete this customer type ?</h3>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                  <button type="submit" class="btn btn-primary">Yes</button>
              </div>
          </div>
        </form>
      </div>
  </div>
  <script>
      $(document).ready(function() {
        $('form').each(function () {
           $(this).validate();
        });
      });
  </script>
