@if($gr_id->gr_status != "finish")
      @if($privilaged->canEdit != 0)
             <button type="button" class="btn btn-default btn-xs" data-target="#dgr{{$data->gr_detail_id}}" data-toggle="modal" >
               <i class="fa fa-pencil"></i>&nbsp;
                 Edit
             </button>
      @endif
      @if($privilaged->canDelete != 0)
           <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->gr_detail_id}}" data-toggle="modal">
             <i class="fa fa-trash"></i>&nbsp;
               Delete
           </button>
      @endif

      <!--Modal edit-->
       <div id="dgr{{$data->gr_detail_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
           <div class="modal-dialog">
             <form role="form" method="post" action="/detail-gr-edit/transactionPurchasing/edit-dgr">
               {{csrf_field()}}
               <input type="hidden" name="gr_detail_id" value="{{$data->gr_detail_id}}">
               <input type="hidden" name="po_detail_id" value="{{$data->po_detail_id}}">
               <input type="hidden" name="gr_id" value="{{$gr_id->gr_id}}">
               <div class="modal-content">
                   <div class="modal-header">
                       <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                       <h4 id="modal-responsive-label" class="modal-title">Edit detail gr {{$data->gr_detail_id}}</h4></div>
                   <div class="modal-body">
                       <div class="row">
                           <div class="col-md-12">
                             <div class="mbm">
                               <label>Product : </label>
                                   <input type="hidden"  id="prd_id{{$data->gr_detail_id}}" name="prd_id"  class="form-control" value="{{$data->prd_id}}" />
                                   <input type="text"  id="prd_desc{{$data->gr_detail_id}}" placeholder="Please type pruduct article" name="prd_desc" class="form-control required" value="{{$data->prd_desc}}-{{$data->color_desc}}-{{$data->size_desc}}" />

                                   <!-- warehouse catalog -->
                                    <script>
                                         $('#prd_desc{{$data->gr_detail_id}}').devbridgeAutocomplete({
                                                 serviceUrl : '/lookupSugestion/product',
                                                 minChars : 3,
                                                 onSelect: function (suggestion) {
                                                   $('#prd_id{{$data->gr_detail_id}}').val(suggestion.data);
                                                   $('#prd_desc{{$data->gr_detail_id}}').val(suggestion.value);
                                                 }
                                         });
                                    </script>
                             </div>
                             <div class="mbm">
                               <label>Qty :</label>
                               <input type="number" placeholder="Please type pruduct quantity" name="qty_receive" class="form-control required" value="{{number_format($data->qty_receive)}}">
                             </div>
                             <div class="mbm">
                                 <label>UOM :</label>
                                 <select name="uom_id" class="form-control required">
                                     <option value="{{$data->uom_id}}" selected>{{$data->uom_desc}}</option>
                                   @foreach($uoms as $uom)
                                     <option value="{{$uom->uom_id}}">{{$uom->uom_desc}}</option>
                                   @endforeach
                                 </select>
                             </div>
                             <div class="mbm">
                               <label>UOM conversion :</label>
                               <input type="number" placeholder="Please type pruduct conversion" name="uom_conversion" class="form-control required" value="{{number_format($data->uom_conversion)}}">
                             </div>
                           </div>
                       </div>
                   </div>
                   <div class="modal-footer">
                       <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                       <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
                   </div>
               </div>
             </form>
           </div>
       </div>
      <!--Modal delete-->
       <div id="delete{{$data->gr_detail_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
           <div class="modal-dialog">
             <form role="form" method="post" action="/detail-gr-delete/transactionPurchasing/delete-detail-gr">
               {{csrf_field()}}
               <input type="hidden" name="gr_detail_id" value="{{$data->gr_detail_id}}">
               <input type="hidden" name="gr_id" value="{{$gr_id->gr_id}}">
               <div class="modal-content">
                   <div class="modal-header">
                       <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                       <h4 id="modal-responsive-label" class="modal-title">Delete detail gr {{$data->gr_detail_id}}</h4></div>
                   <div class="modal-body">
                       <div class="row">
                             <h3 style="text-align:center">Are you sure want to delete this detail gr ?</h3>
                       </div>
                   </div>
                   <div class="modal-footer">
                       <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                       <button type="submit" class="btn btn-primary">Yes</button>
                   </div>
               </div>
             </form>
           </div>
        </div>
        <script>
              $(document).ready(function() {
                $('form').each(function () {
                   $(this).validate();
                });
              });
        </script>
@endif
