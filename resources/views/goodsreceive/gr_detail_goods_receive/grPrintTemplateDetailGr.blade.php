<style>
    #print{display:none}
    @media print{
      @page {
        size: potrait !important;
      }
      .page-content {display:none}
      #page-wrapper{border : 0px !important}
      #print {display:block}
    }
</style>

<div id="print" class="container-fluid">
  @if($privilaged->canPrint != 0)
  <div class="col-md-12 row">
    <h3>Goods receive</h3>
    <hr />
       <div class="col-sm-6">
         <p style="text-align:left">Kepada Yth<br />....</p>
         <p style="text-align:left">Alamat<br />....</p>
       </div>
       <div class="col-sm-6">
           <div class="col-sm-6">
             <p style="text-align:right">No</p>
             <p style="text-align:right">Tanggal </p>
           </div>
           <div class="col-sm-6" style="float:right">
              <p>: {{$gr_id->gr_no}}</p>
              <p>:
                <?php
                  $created_at = $gr_id->created_date;
                  $created_at = strToTime($created_at);
                  $created_at = date("d M Y", $created_at);
                  echo $created_at;
                 ?>
              </p>
           </div>
       </div>
   </div>
   <div class="col-md-12 row">
     <div class="clear" style="height:10px"></div>
     <div class="col-sm-3">
       <p>Dikirim ke</p>
       <p>Telpon</p>
       <p>Fax</p>
       <p>Contact P.</p>
     </div>
     <div class="col-sm-9">
       <p>: ....</p>
       <p>: ....</p>
       <p>: ....</p>
       <p>: ....</p>
     </div>
   </div>
   <div class="col-md-12">
       <div class="clear" style="height:10px"></div>
       <table class="table table-striped table-bordered table-hover">
         <thead>
           <tr>
             <th>#</th>
             <th>Nama barang</th>
             <th>Size</th>
             <th>Qty</th>
             <th>Unit</th>
           </tr>
         </thead>
         <?php $i = 1 ?>
         @foreach($detailGr as $data)
           <tr>
             <td class="detailPoId">{{$i}}</td>
             <td>{{$data->prd_desc}}</td>
             <td>{{$data->size_desc}}</td>
             <td>{{number_format($data->qty_receive)}}</td>
             <td>{{$data->uom_desc}}</td>
           </tr>
          <?php $i++ ?>
         @endforeach
       </table>
   </div>
   <hr>
   <div class="col-md-12 row">
     <div class="clear" style="height:30px"></div>
      <div class="col-sm-4">
          <span>Dibuat/diminta oleh, </span>
          <div class="clear" style="height:100px"></div>
          ................................
      </div>
      <div class="col-sm-4">
          <span>Disetujui oleh, </span>
          <div class="clear" style="height:100px"></div>
          ................................
      </div>
      <div class="col-sm-4">
          <span>Diketahui oleh, </span>
          <div class="clear" style="height:100px"></div>
          ................................
      </div>
   </div>
  @else
    <h3 class="text-center">Not Permitted</h3>
  @endif

</div>
