@extends('layout.layout')
@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb n-print">
    <div class="page-header pull-left">
        <div class="page-title">{{$title->moduleLabel}}</div>
    </div>
    <ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i>&nbsp;<a href="#">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li><a href="#">Transaction purchasing</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li><a href="#">good(s)</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="active">{{$title->moduleLabel}}</li>
    </ol>
    <div class="clearfix"></div>
</div>

<div class="page-content">
   <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-green">
            <div class="panel-body pan">
                    <div class="form-body pal">
                      @if($errors->any())
                          <script>
                                 toastr.error("{{$errors->first()}}");
                                 toastr.options = {
                                  "closeButton": false,
                                  "debug": false,
                                  "positionClass": "toast-top-right",
                                  "onclick": null,
                                  "showDuration": "300",
                                  "hideDuration": "1000",
                                  "timeOut": "5000",
                                  "extendedTimeOut": "1000",
                                  "showEasing": "swing",
                                  "hideEasing": "linear",
                                  "showMethod": "fadeIn",
                                  "hideMethod": "fadeOut"
                                };

                          </script>
                      @endif
                      @if(session('status'))
                          <script>
                             @if(session('status') == 'Detail gr added' || session('status') == 'Detail gr updated' || session('status') == 'Detail gr deleted')
                               toastr.success("{{ session('status') }}");
                             @else
                               toastr.error("{{ session('status') }}");
                             @endif

                                 toastr.options = {
                                  "closeButton": false,
                                  "debug": false,
                                  "positionClass": "toast-top-right",
                                  "onclick": null,
                                  "showDuration": "300",
                                  "hideDuration": "1000",
                                  "timeOut": "5000",
                                  "extendedTimeOut": "1000",
                                  "showEasing": "swing",
                                  "hideEasing": "linear",
                                  "showMethod": "fadeIn",
                                  "hideMethod": "fadeOut"
                                };

                          </script>
                      @endif
                  <!--@if($gr_id->gr_status != "approve")
                    <form action="/detailgr-add-action/grGoodsReceive/addDetailGrAction"  method="post" class="form-group">
                      {{csrf_field()}}
                       <input type="hidden" name="po_id" value="{{$gr_id->gr_id}}" />
                        <div class="col-md-9 col-sm-12 col-xs-12">
                          @if($errors->any())
                              <h4>{{$errors->first()}}</h4>
                          @endif
                          @if(session('status'))
                              <h4>{{ session('status') }}</h4>
                          @endif
                          <label>Product :</label>
                          <div class="input-group">
                              <input type="hidden"  id="content-productCodeVal" name="prd_id"  class="form-control">
                              <input type="text"  id="contentVal-productCodeVal" class="form-control">
                              <div id="productCatalogData"></div>
                              <span style="cursor:pointer" id="productCode" class="input-group-addon"><i class="fa fa-search"></i></span>

                               <script>
                                  $("#productCode").click(function(){
                                    var keyword = $("#contentVal-productCodeVal").val();
                                    $.get( "/ajax-prd-catalog/purchaseOrder/poAjaxPrdCatalog/"+keyword+"/productCodeVal")
                                        .done(function( data ) {
                                            $("#productCatalogData").empty();
                                            $("#productCatalogData").html(data);
                                        });
                                      //$("#barangCatalog").modal('show');
                                  });
                               </script>
                          </div>
                        </div>
                        <div class="col-md-9 col-sm-12 col-xs-12">
                            <label>Qty :</label>
                            <input type="number" name="qty_receive" class="form-control">
                        </div>
                        <div class="col-md-9 col-sm-12 col-xs-12">
                            <label>UOM :</label>
                            <select name="uom_id" class="form-control">
                                <option value="">-- choose --</option>
                              @foreach($uoms as $uom)
                                <option value="{{$uom->uom_id}}">{{$uom->uom_desc}}</option>
                              @endforeach
                            </select>
                        </div>
                        <div class="col-md-9 col-sm-12 col-xs-12">
                            <label>UOM conversion :</label>
                            <input type="number" name="uom_conversion" class="form-control">
                        </div>
                        <div class="col-md-9 col-sm-12 col-xs-12">
                           <div class="clear" style="height:20px"></div>
                            <input type="submit" class="btn btn-primary" value="add">
                            &nbsp;
                            <a href="/list-gr/grGoodsReceive/grListGr">
                              <button type="button" class="btn btn-green">Cancel</button>
                            </a>
                            <div class="clear" style="height:20px"></div>
                        </div>
                   </form>
                @endif -->
                        <div class="col-md-12">
                                <div class="col-md-6">
                                    <h3 style="margin-top:0px"># {{$gr_id->gr_no}}</h3>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                  <div class="dropdown" style="text-align:right">
                                      @if($privilaged->canPrint != 0)
                                            <button class="btn btn-success" onclick="window.print();"><i class="fa fa-print"></i>&nbsp;Print</button>
                                      @endif
                                      <a href="/list-goods/transactionPurchasing/grListGr">
                                        <button class="btn btn-default">Back</button>
                                      </a>
                                        <div class="clear" style="height:10px"></div>
                                  </div>
                                </div>

                                <div class="col-md-12">
                                  <div class="table-responsive">
                                      <table class="table table-bordered" id="grD-table">
                                        <thead>
                                            <tr>
                                                <th><input type="checkbox"  id="bulkDelete"  /></th>
                                                <th>Product</th>
                                                <th>Qty</th>
                                                <th>Sub total</th>
                                                @if($gr_id->gr_status != "finish")
                                                    <th>Action</th>
                                                @endif
                                            </tr>
                                          </thead>
                                      </table>
                                      <script>
                                        $(function() {
                                          var table = $('#grD-table').DataTable({
                                            dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
                                               "<'row'<'col-xs-12't>>"+
                                               "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
                                               processing: true,
                                               serverSide: true,
                                                 ajax: {
                                                    url: '/list-goods-detail-data/transactionPurchasing/grDetailListGrData/{{$gr_id->gr_id}}',
                                                },
                                               columns: [
                                                   {data: 'delete_bulk', name: 'delete_bulk', orderable: false, searchable: false},
                                                   { data: 'prd_desc', name: 'prd_desc' },
                                                   { data: 'qty_receive', name: 'qty_receive' },
                                                   { data: 'sub_total', name: 'sub_total' },

                                                   @if($gr_id->gr_status != "finish")
                                                      {data: 'action', name: 'action', className: 'text-center', orderable: false, searchable: false}
                                                   @endif
                                               ],
                                               paginate: false
                                           });
                                            $('div.dataTables_filter input').addClass('form-control');

                                            $("#deleteTriger").click(function(){
                                               var allValue = [];
                                               $("input[name='gr_detail_id[]']:checked").each( function () {
                                                   allValue.push($(this).val());
                                               });
                                               $.post( "/detailGr-delete-action/transactionPurchasing/deleteDetailgrAction",  {'gr_detail_id': allValue, '_token' : '{{csrf_token()}}' })
                                                 .done(function( data ) {
                                                   if(data == "success"){
                                                      location.reload();
                                                   }else{
                                                      alert(data);
                                                   }
                                                   console.log( "Data Loaded: " + data );
                                                 });
                                             });

                                             $("#bulkDelete").click(function(){
                                                $("input[name='gr_detail_id[]']").prop("checked", $(this).prop('checked'))
                                                if($("input[name='gr_detail_id[]']:checked").length > 0){
                                                  $("#deleteTriger").show();
                                                }else{
                                                  $("#deleteTriger").hide();
                                                }
                                             });

                                             $("#grD-table").on('click', ':checkbox', function(){
                                               if($("input[name='gr_detail_id[]']:checked").length > 0){
                                                 $("#deleteTriger").show();
                                               }else{
                                                 $("#deleteTriger").hide();
                                               }
                                             });
                                        });
                                     </script>
                                  </div>
                                  <button class='btn btn-danger' id="deleteTriger" style="display:none">Delete cheked</button>
                                  <div class="clear" style="height:20px"></div>
                                </div>
                        </div>
                    </div>
            </div>
        </div>
      </div>
     </div>
</div>
@include('goodsreceive/gr_detail_goods_receive/grPrintTemplateDetailGr')
@endsection
