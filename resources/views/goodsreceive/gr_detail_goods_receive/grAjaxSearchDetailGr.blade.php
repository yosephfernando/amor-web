<h3>Search result of '{{$keyword}}'</h3>
<table id="beforeDelete" class="table table-striped table-bordered table-hover">
  <div class="clear" style="height:10px"></div>
    <thead>
    <tr>
      <th>#</th>
      <th>Nama barang</th>
      <th>Qty</th>
      <th>Action</th>
    </tr>
    </thead>
    <tbody>
      <?php $i = 1;?>
      @foreach($detailPo as $data)
              <tr>
                <td class="detailPoId">{{$i}}</td>
                <td>{{$data->barangName}}</td>
                <td>{{$data->qty}}</td>
                <td>
                        @if($privilaged->canEdit != 0)
                               <button type="button" class="btn btn-default btn-md" data-target="#dp{{$data->detailPoId}}" data-toggle="modal" >
                                 <i class="fa fa-edit"></i>&nbsp;
                                   Edit
                               </button>
                       @endif
                       @if($privilaged->canDelete != 0)
                             <button type="button" class="btn btn-danger btn-md" data-target="#delete{{$data->detailPoId}}" data-toggle="modal">
                                 Delete
                             </button>
                       @endif
              </tr>

              <!--Modal edit-->
               <div id="dp{{$data->detailPoId}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
                   <div class="modal-dialog">
                       <input type="hidden" id="detailPoId{{$data->detailPoId}}" value="{{$data->detailPoId}}" />
                       <div class="modal-content">
                           <div class="modal-header">
                               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                               <h4 id="modal-responsive-label" class="modal-title">Edit detail po {{$data->detailPoCode}}</h4>
                           </div>
                           <div class="modal-body">
                               <div class="row">
                                   <div class="col-md-12">
                                       <h4 id="message{{$data->detailPoId}}" style="display:none">Data updated</h4>
                                       <h4 id="error{{$data->detailPoId}}" style="display:none">Error updating</h4>
                                       <div class="mbm">
                                             <label>Qty :</label>
                                             <input type="text" id="qty{{$data->detailPoId}}" class="form-control" value="{{$data->qty}}">
                                       </div>
                                   </div>
                               </div>
                           </div>
                           <div class="modal-footer">
                               <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                               <button id="submit{{$data->detailPoId}}" class="btn btn-primary">Save changes</button>
                           </div>
                       </div>

                       <!-- ajax post data -->
                          <script>
                                $("#message{{$data->detailPoId}}").hide();
                                $("#error{{$data->detailPoId}}").hide();
                                $("#submit{{$data->detailPoId}}").click(function(){
                                  var id = $("#detailPoId{{$data->detailPoId}}").val();
                                  var qty = $("#qty{{$data->detailPoId}}").val();
                                  if(id != "" || qty != ""){
                                    $.post('/detail-po-edit/transactionPurchasing/edit-dp', {'qty' : qty, 'detailPoId' : id, '_token' : '{{csrf_token()}}' })
                                    .done(function(data){
                                        $("#message{{$data->detailPoId}}").show();
                                     })
                                     .error(function(xhr, status, error){
                                        $("#error{{$data->detailPoId}}").show();
                                     });
                                  }else{
                                    console.log("failed");
                                  }
                                });

                                $("#dp{{$data->detailPoId}}").on('hidden.bs.modal', function(e){
                                  $("#message{{$data->detailPoId}}").hide();
                                  $("#error{{$data->detailPoId}}").hide();
                                });
                          </script>
                       <!-- end of ajax post data -->

                   </div>
               </div>


               <!--Modal delete-->
                <div id="delete{{$data->detailPoId}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
                    <div class="modal-dialog">
                        <input type="hidden" id="detailPoId{{$data->detailPoId}}" value="{{$data->detailPoId}}">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                                <h4 id="modal-responsive-label" class="modal-title">Delete detail po {{$data->detailPoCode}}</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                      <h3 id="messageDelete{{$data->detailPoId}}" style="text-align:center">Are you sure want to delete this detail po ?</h3>
                                </div>
                            </div>
                            <div id="beforeDel{{$data->detailPoId}}" class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                                <button id="deleteSubmit{{$data->detailPoId}}" type="submit" class="btn btn-primary">Yes</button>
                            </div>
                            <div id="afterDel{{$data->detailPoId}}" class="modal-footer" style="display:none">
                                <button type="button" data-dismiss="modal" class="btn btn-default">Ok</button>
                            </div>
                        </div>

                      <!-- ajax post data -->
                         <script>
                               $("#deleteSubmit{{$data->detailPoId}}").click(function(){
                                 var detailPoId = $("#detailPoId{{$data->detailPoId}}").val();
                                 if(detailPoId != ""){
                                   $.post('/detail-po-delete/transactionPurchasing/delete-detail-po', {'detailPoId' : detailPoId, '_token' : '{{csrf_token()}}' })
                                   .done(function(data){
                                       $("#beforeDel{{$data->detailPoId}}").hide();
                                       $("#afterDel{{$data->detailPoId}}").show();
                                       $("#messageDelete{{$data->detailPoId}}").text('Data deleted');
                                    })
                                    .error(function(xhr, status, error){
                                       $("#beforeDel{{$data->detailPoId}}").hide();
                                       $("#afterDel{{$data->detailPoId}}").show();
                                       $("#messageDelete{{$data->detailPoId}}").text('Error deleting');
                                    });
                                 }else{
                                   console.log("failed");
                                 }
                               });

                               $("#dp{{$data->detailPoId}}").on('hidden.bs.modal', function(e){
                                 $("#messageDelete{{$data->detailPoId}}").text('Are you sure want to delete this detail gr ?');
                                 $("#beforeDel{{$data->detailPoId}}").show();
                                 $("#afterDel{{$data->detailPoId}}").hide();
                               });
                         </script>
                      <!-- end of ajax post data -->

                    </div>
                </div>
            <?php $i++ ?>
       @endforeach
    </tbody>
</table>
