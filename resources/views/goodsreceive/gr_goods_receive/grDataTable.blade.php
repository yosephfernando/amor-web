  @if($privilaged->canEdit != 0)
     @if($data->gr_status != "finish")
         <button type="button" class="btn btn-default btn-xs" data-target="#gr{{$data->gr_id}}" data-toggle="modal" >
           <i class="fa fa-pencil"></i>&nbsp;
             Edit
         </button>
     @else
         <button type="button" class="btn btn-default btn-xs" style="cursor:not-allowed">
           <i class="fa fa-pencil"></i>&nbsp;
             Edit
         </button>
     @endif
 @endif

 <a href="/add-detail-gr/transactionPurchasing/grDetailGr/{{$data->gr_id}}">
    <button type="button" class="btn btn-primary btn-xs">
      <i class="fa fa-plus"></i>&nbsp;
        Detail gr
    </button>
  </a>

 @if($privilaged->canDelete != 0)
  @if($data->gr_status != "finish")
       <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->gr_id}}" data-toggle="modal">
         <i class="fa fa-trash"></i>&nbsp;
           Delete
       </button>
  @else
        <button type="button" class="btn btn-danger btn-xs" style="cursor:not-allowed">
          <i class="fa fa-trash"></i>&nbsp;
            Delete
        </button>
  @endif
 @endif

 <?php
      $dates = strtotime($data->gr_date);
      $date = date("d M Y", $dates);

      $dateEdit = date("d-m-Y", $dates);
  ?>


 <!--Modal edit-->
<div id="gr{{$data->gr_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
   <div class="modal-dialog">
     <form role="form" method="post" action="/gr-edit/transactionPurchasing/edit-gr">
       {{csrf_field()}}
       <input type="hidden" name="gr_id" value="{{$data->gr_id}}">
       <div class="modal-content">
           <div class="modal-header">
               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
               <h4 id="modal-responsive-label" class="modal-title">Edit gr {{$data->gr_no}}</h4></div>
           <div class="modal-body">
               <div class="row">
                   <div class="col-md-12">
                     <div class="mbm">
                       <label>PO : </label>
                           <input type="hidden"  id="po_no{{$data->gr_id}}" name="po_no"  class="form-control" value="{{$data->po_no}}">
                           <input type="text"  id="autocompletepo{{$data->gr_id}}" placeholder="Please type po number" name="po_code" class="form-control required" value="{{$data->po_no}}">

                           <!-- warehouse catalog -->
                            <script>
                                 $('#autocompletepo{{$data->gr_id}}').devbridgeAutocomplete({
                                         serviceUrl: '/lookupSugestion/po',
                                         minChars: 3,
                                         onSelect: function (suggestion) {
                                           $('#po_no{{$data->gr_id}}').val(suggestion.data);
                                           $('#autocompletepo{{$data->gr_id}}').val(suggestion.value);
                                         }
                                 });
                            </script>
                     </div>
                     <div class="mbm">
                            <label>Date :</label>
                            <input type="text" name="gr_date" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" class="datepicker-default form-control required" value="{{$dateEdit}}"/>
                     </div>

                     <div class="mbm">
                        <label>PPN type :</label>
                        <select name="ppn_type" class="form-control required">
                            <option value="{{$data->ppn_type}}" selected>{{$data->ppn_type}}</option>
                        </select>
                     </div>
                     <div class="mbm">
                        <label>Notes :</label>
                        <textarea placeholder="Please type goods receive note" name="notes" class="form-control required">{{$data->notes}}</textarea>
                     </div>
                     <div class="mbm">
                        <label>Status :</label>
                        <select name="gr_status" class="form-control required">
                              <option value="{{$data->gr_status}}" selected>{{$data->gr_status}}</option>
                              <option value="invoice">Invoice</option>
                        </select>
                     </div>

                   </div>
               </div>
           </div>
           <div class="modal-footer">
               <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
               <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
           </div>
       </div>
     </form>
   </div>
</div>

<!--Modal delete-->
<div id="delete{{$data->gr_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
    <div class="modal-dialog">
      <form role="form" method="post" action="/gr-delete/transactionPurchasing/delete-gr">
        {{csrf_field()}}
        <input type="hidden" name="gr_id" value="{{$data->gr_id}}">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                <h4 id="modal-responsive-label" class="modal-title">Delete gr {{$data->gr_id}}</h4></div>
            <div class="modal-body">
                <div class="row">
                      <h3 style="text-align:center">Are you sure want to delete this gr ?</h3>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                <button type="submit" class="btn btn-primary">Yes</button>
            </div>
        </div>
      </form>
    </div>
</div>
<script>
      $(".datepicker-default").datepicker();
      $(document).ready(function() {
        $('form').each(function () {
           $(this).validate();
        });
      });
</script>
