@if($keyword != "null")
  {{buildSearch::searchMaster(
        'select po_no, notes from "PO_HEADER" where CAST(po_no AS varchar) LIKE '."'".'%'.$keyword.'%'."'".'',
        "Purchase order catalog", Request::segment(5), ["po_no"]
        )
  }}
@else
  {{buildSearch::searchMaster(
        'select po_no, notes from "PO_HEADER" ORDER BY po_no DESC LIMIT 10',
        "Purchase order catalog", Request::segment(5), ["po_no"]
        )
  }}
@endif
