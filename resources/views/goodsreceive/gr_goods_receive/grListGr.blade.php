@extends('layout.layout')
@section('content')
<!--BEGIN TITLE & BREADCRUMB PAGE-->
            <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                <div class="page-header pull-left">
                    <div class="page-title">{{$title->moduleLabel}}</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a href="#">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li><a href="#">Transaction purchasing</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li class="active">{{$title->moduleLabel}}</li>
                </ol>
                <div class="clearfix"></div>
            </div>
            <!--END TITLE & BREADCRUMB PAGE-->
           <div class="page-content">
              <div class="row">
                <div class="col-md-12">
                <div class="panel panel-green">
                        <div class="panel-body">
                          @if($errors->any())
                              <script>
                                     toastr.error("{{$errors->first()}}");
                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                          @if(session('status'))
                              <script>
                                 @if(session('status') == 'Gr added' || session('status') == 'Gr updated' || session('status') == 'Gr deleted')
                                   toastr.success("{{ session('status') }}");
                                 @else
                                   toastr.error("{{ session('status') }}");
                                 @endif

                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                          <div class="row">
                            <div class="col-md-3">
                              @if($privilaged->canAdd != 0)
                                 <button type="button" class="btn btn-primary btn-md" data-target="#grAdd" data-toggle="modal">
                                   <i class="fa fa-plus"></i>&nbsp;
                                     Add
                                 </button>
                              @endif
                              @if($privilaged->canExport != 0)
                                 <a href="/exportGr/transactionPurchasing/exportGrToExcel" class="btn btn-success btn-md"><i class="fa fa-download"></i>&nbsp;Export to xls</a>
                              @endif
                              <div class="clear" style="height:10px"></div>
                            </div>
                            <div class="col-md-6">
                              <form method="POST" id="date-range" class="form-inline" role="form">
                                  <div class="form-group">
                                      <div class="col-md-1" style="padding-right: 0px;padding-left: 0px;">
                                        <h5>Date: </h5>
                                      </div>
                                      <div class="col-md-8" style="padding-right: 0px;padding-left: 0px;">
                                          <div class="input-group input-daterange">
                                              <input type="text" name="start_date" id="start_date"  class="form-control" />
                                              <span class="input-group-addon">to</span>
                                              <input type="text" name="end_date" id="end_date" class="form-control"/>
                                          </div>
                                      </div>
                                      <div class="col-md-1">
                                          <button type="submit" class="btn btn-primary">Search</button>
                                      </div>
                                  </div>
                                  <div class="clear" style="height:10px"></div>
                              </form>
                            </div>
                          </div>
                               <div class="col-md-12">
                                  <div class="table-responsive">
                                    <table class="table table-bordered" id="gr-table">
                                      <thead>
                                          <tr>
                                            <th><input type="checkbox"  id="bulkDelete"  /></th>
                                            <th>No GR</th>
                                            <th>No PO</th>
                                            <th>Date</th>
                                            <th>Total price</th>
                                            <th>Total qty</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                          </tr>
                                      </thead>
                                    </table>
                                    <script>
                                      $(function() {
                                        var table = $('#gr-table').DataTable({
                                             processing: true,
                                             serverSide: true,
                                               ajax: {
                                                  url: '/list-goods-data/transactionPurchasing/grListGrData',
                                                  data: function (d) {
                                                      d.start_date = $('input[name=start_date]').val();
                                                      d.end_date = $('input[name=end_date]').val();
                                                  }
                                              },
                                             columns: [
                                                 {data: 'delete_bulk', name: 'delete_bulk', orderable: false, searchable: false},
                                                 { data: 'gr_no', name: 'gr_no' },
                                                 { data: 'po_no', name: 'po_no' },
                                                 { data: 'gr_date', name: 'gr_date' },
                                                 { data: 'total_nett', name: 'total_nett' },
                                                 { data: 'total_qty', name: 'total_qty' },
                                                 { data: 'gr_status', name: 'gr_status' },
                                                 {data: 'action', name: 'action', className: 'text-center', orderable: false, searchable: false}
                                             ]
                                         });
                                          $('div.dataTables_filter input').addClass('form-control');

                                          $("#deleteTriger").click(function(){
                                             var allValue = [];
                                             $("input[name='gr_id[]']:checked").each( function () {
                                                 allValue.push($(this).val());
                                             });
                                             $.post( "/delete-bulk-gr/transactionPurchasing/deleteBulkGr",  {'gr_id': allValue, '_token' : '{{csrf_token()}}' })
                                               .done(function( data ) {
                                                 if(data == "success"){
                                                   location.reload();
                                                 }else{
                                                   console.log(data);
                                                 }
                                               }).error(function(xhr){
                                                 console.log(xhr.responseText);
                                               });
                                           });

                                           $("#bulkDelete").click(function(){
                                              $("input[name='gr_id[]']").prop("checked", $(this).prop('checked'))
                                              if($("input[name='gr_id[]']:checked").length > 0){
                                                $("#deleteTriger").show();
                                              }else{
                                                $("#deleteTriger").hide();
                                              }
                                           });

                                           $("#gr-table").on('click', ':checkbox', function(){
                                             if($("input[name='gr_id[]']:checked").length > 0){
                                               $("#deleteTriger").show();
                                             }else{
                                               $("#deleteTriger").hide();
                                             }
                                           });

                                           $('#date-range').on('submit', function(e) {
                                               table.draw();
                                               e.preventDefault();
                                           });
                                      });
                                   </script>
                                  </div>
                                  <button class='btn btn-danger' id="deleteTriger" style="display:none">Delete cheked</button>
                               </div>
                        </div>
                    </div>
                  </div>
              </div>
          </div>

          <!--Modal add user-->
           <div id="grAdd" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
               <div class="modal-dialog">
                 <form role="form" method="post" action="/gr-add/transactionPurchasing/add-gr">
                    {{csrf_field()}}
                   <div class="modal-content">
                       <div class="modal-header">
                           <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                           <h4 id="modal-responsive-label" class="modal-title">Add gr</h4></div>
                       <div class="modal-body">
                           <div class="row">
                               <div class="col-md-12">
                                 <div class="mbm">
                                   <label>PO : </label>
                                       <input type="hidden"  id="po_id" name="po_id"  class="form-control">
                                       <input type="text"  id="autocompletepo" placeholder="Please type po number" name="po_no"  class="form-control required">

                                       <!-- warehouse catalog -->
                                        <script>
                                             $('#autocompletepo').devbridgeAutocomplete({
                                                     serviceUrl: '/lookupSugestion/po',
                                                     minChars: 3,
                                                     onSelect: function (suggestion) {
                                                       $('#po_id').val(suggestion.data);
                                                       $('#autocompletepo').val(suggestion.value);
                                                     }
                                             });
                                        </script>
                                 </div>

                                 <div class="mbm">
                                     <label>Date :</label>
                                     <input type="text" name="gr_date" data-date-format="dd-mm-yyyy" value="{{date('d-m-Y')}}" placeholder="dd-mm-yyyy" class="datepicker-default form-control required"/>
                                 </div>
                                 <div class="mbm">
                                    <label>PPN type :</label>
                                    <select name="ppn_type" class="form-control required">
                                        <option value="E">E</option>
                                    </select>
                                 </div>
                                 <div class="mbm">
                                    <label>Notes :</label>
                                    <textarea placeholder="Please type goods receive note" name="notes" class="form-control required"></textarea>
                                 </div>
                                 <div class="mbm">
                                    <label>Status :</label>
                                    <select name="gr_status" class="form-control required">
                                        <option value="open" selected>Open</option>
                                    </select>
                                 </div>

                               </div>
                           </div>
                       </div>
                       <div class="modal-footer">
                           <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                           <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                       </div>
                   </div>
                  </form>
               </div>
           </div>
           <script>
                 $(document).ready(function() {
                   $('form').each(function () {
                      $(this).validate();
                   });
                 });
           </script>
@endsection
