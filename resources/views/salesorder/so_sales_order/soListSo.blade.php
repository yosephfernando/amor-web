@extends('layout.layout')
@section('content')
<!--BEGIN TITLE & BREADCRUMB PAGE-->
            <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                <div class="page-header pull-left">
                    <div class="page-title">{{$title->moduleLabel}}</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a href="#">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li><a href="#">Transaction sales</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li class="active">{{$title->moduleLabel}}</li>
                </ol>
                <div class="clearfix"></div>
            </div>
            <!--END TITLE & BREADCRUMB PAGE-->
           <div class="page-content">
              <div class="row">
                <div class="col-md-12">
                <div class="panel panel-green">
                        <div class="panel-body">
                          @if($errors->any())
                              <script>
                                     toastr.error("{{$errors->first()}}");
                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                          @if(session('status'))
                              <script>
                                 @if(session('status') == 'So added' || session('status') == 'So updated' || session('status') == 'So deleted')
                                   toastr.success("{{ session('status') }}");
                                 @else
                                   toastr.error("{{ session('status') }}");
                                 @endif

                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                          <div class="row">
                            <div class="col-md-3">
                                @if($privilaged->canAdd != 0)
                                   <button type="button" class="btn btn-primary btn-md" data-target="#soAdd" data-toggle="modal">
                                     <i class="fa fa-plus"></i>&nbsp;
                                       Add
                                   </button>
                                @endif
                                @if($privilaged->canExport != 0)
                                   <a href="/exportSo/saleTransaction/exportSoToExcel" class="btn btn-success btn-md"><i class="fa fa-download"></i>&nbsp;Export to xls</a>
                                @endif
                                <div class="clear" style="height:20px"></div>
                            </div>
                            <div class="col-md-6">
                              <form method="POST" id="date-range" class="form-inline" role="form">
                                  <div class="form-group">
                                      <div class="col-md-1" style="padding-right: 0px;padding-left: 0px;">
                                        <h5>Date: </h5>
                                      </div>
                                      <div class="col-md-8" style="padding-right: 0px;padding-left: 0px;">
                                          <div class="input-group input-daterange">
                                              <input type="text" name="start_date" id="start_date"  class="form-control" />
                                              <span class="input-group-addon">to</span>
                                              <input type="text" name="end_date" id="end_date" class="form-control"/>
                                          </div>
                                      </div>
                                      <div class="col-md-1">
                                          <button type="submit" class="btn btn-primary">Search</button>
                                      </div>
                                  </div>
                                  <div class="clear" style="height:10px"></div>
                              </form>
                            </div>
                          </div>

                              <div class="col-md-12">
                                  <div class="table-responsive">

                                    <table class="table table-bordered" id="so-table">
                                        <thead>
                                          <tr>
                                            <th><input type="checkbox"  id="bulkDelete"  /></th>
                                            <th>No so</th>
                                            <th>Date</th>
                                            <th>Total price</th>
                                            <th>Total qty</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                          </tr>
                                        </thead>
                                      </table>
                                      <script>
                                        $(function() {
                                          var table = $('#so-table').DataTable({
                                               processing: true,
                                               serverSide: true,
                                               ajax: {
                                                  url: '/listSoData/saleTransaction/soSalesOrderDataTable',
                                                  data: function (d) {
                                                      d.start_date = $('input[name=start_date]').val();
                                                      d.end_date = $('input[name=end_date]').val();
                                                  }
                                               },
                                               columns: [
                                                   {data: 'delete_bulk', name: 'delete_bulk', orderable: false, searchable: false},
                                                   { data: 'so_no', name: 'so_no' },
                                                   { data: 'so_date', name: 'so_date' },
                                                   { data: 'total_nett', name: 'total_nett' },
                                                   { data: 'total_qty', name: 'total_qty' },
                                                   { data: 'so_status', name: 'so_status' },
                                                   {data: 'action', name: 'action', className: 'text-center', orderable: false, searchable: false}
                                               ]
                                           });
                                            $('div.dataTables_filter input').addClass('form-control');

                                            $("#deleteTriger").click(function(){
                                               var allValue = [];
                                               $("input[name='so_id[]']:checked").each( function () {
                                                   allValue.push($(this).val());
                                               });
                                               $.post( "/delete-bulk-so/saleTransaction/deleteBulkSo",  {'so_id': allValue, '_token' : '{{csrf_token()}}' })
                                                 .done(function( data ) {
                                                   if(data == "success"){
                                                      location.reload();
                                                   }else{
                                                      alert(data);
                                                   }
                                                   console.log( "Data Loaded: " + data );
                                                 });
                                             });

                                             $("#bulkDelete").click(function(){
                                                $("input[name='so_id[]']").prop("checked", $(this).prop('checked'))
                                                if($("input[name='so_id[]']:checked").length > 0){
                                                  $("#deleteTriger").show();
                                                }else{
                                                  $("#deleteTriger").hide();
                                                }
                                             });

                                             $("#so-table").on('click', ':checkbox', function(){
                                               if($("input[name='so_id[]']:checked").length > 0){
                                                 $("#deleteTriger").show();
                                               }else{
                                                 $("#deleteTriger").hide();
                                               }
                                             });

                                             $('#date-range').on('submit', function(e) {
                                                 table.draw();
                                                 e.preventDefault();
                                             });

                                        });
                                     </script>
                                  </div>
                                  <button class='btn btn-danger' id="deleteTriger" style="display:none">Delete cheked</button>
                                  <div class="clear" style="height:20px"></div>
                              </div>
                            </div>
                        </div>
                    </div>
                  </div>
              </div>
          </div>

          <!--Modal add user-->
           <div id="soAdd" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
               <div class="modal-dialog">
                 <form role="form" method="post" action="/so-add/saleTransaction/add-so">
                    {{csrf_field()}}
                   <div class="modal-content">
                       <div class="modal-header">
                           <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                           <h4 id="modal-responsive-label" class="modal-title">Add so</h4></div>
                       <div class="modal-body">
                           <div class="row">
                               <div class="col-md-12">
                                 <div class="mbm row">
                                   <div class="col-md-6">
                                     <label>Date :</label>
                                     <input type="text" name="so_date" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="{{date('d-m-Y')}}" class="datepicker-default form-control required"/>
                                   </div>
                                   <div class="col-md-6">
                                     <label>Reg date :</label>
                                     <input type="text" name="req_date" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="{{date('d-m-Y')}}" class="datepicker-default form-control required"/>
                                   </div>
                                 </div>
                                 <div class="mbm">
                                    <input type="checkbox" name="ppn" value="1" /><span> PPN ( 10% )</span>
                                 </div>
                                 <div class="mbm">
                                    <label>PPN type :</label>
                                    <select name="ppn_type" class="form-control required">
                                        <option value="E">E</option>
                                    </select>
                                 </div>
                                 <div class="mbm">
                                    <label>Notes :</label>
                                    <textarea placeholder="Please type sales order note" name="notes" class="form-control required"></textarea>
                                 </div>
                                 <div class="mbm">
                                   <label>Customer :</label>
                                       <input type="hidden"  id="cust_id" name="cust_id"  class="form-control">
                                       <input type="text" id="autocompletecust" placeholder="Please type sales order customer" name="cust_desc" class="form-control required">

                                       <!-- Barang catalog -->
                                        <script>
                                           var cust = {!! $cust !!};

                                            $('#autocompletecust').devbridgeAutocomplete({
                                                    lookup: cust,
                                                    onSelect: function (suggestion) {
                                                      $('#cust_id').val(suggestion.data);
                                                      $('#autocompletecust').val(suggestion.value);
                                                    }
                                            });
                                        </script>
                                 </div>
                                 <div class="mbm">
                                   <label>Gudang : </label>
                                       <input type="hidden"  id="wh_id" name="wh_id"  class="form-control">
                                       <input type="text"  id="autocompletewh" placeholder="Please type sales order warehouse" name="wh_desc"  class="form-control required">

                                       <!-- warehouse catalog -->
                                        <script>
                                            var wh = {!! $wh !!};

                                             $('#autocompletewh').devbridgeAutocomplete({
                                                     lookup: wh,
                                                     onSelect: function (suggestion) {
                                                       $('#wh_id').val(suggestion.data);
                                                       $('#autocompletewh').val(suggestion.value);
                                                     }
                                             });
                                        </script>
                                 </div>
                                 <div class="mbm">
                                    <label>Status :</label>
                                    <select name="so_status" class="form-control required">
                                        <option value="open" selected>Open</option>
                                    </select>
                                 </div>

                               </div>
                           </div>
                       </div>
                       <div class="modal-footer">
                           <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                           <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                       </div>
                   </div>
                  </form>
               </div>
           </div>
           <script>
               $(document).ready(function() {
                 $('form').each(function () {
                    $(this).validate();
                 });
               });
           </script>
@endsection
