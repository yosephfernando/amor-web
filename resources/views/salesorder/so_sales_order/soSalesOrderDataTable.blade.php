@if($privilaged->canEdit != 0)
   @if($data->so_status != "approve")
       <button type="button" class="btn btn-default btn-xs" data-target="#so{{$data->so_id}}" data-toggle="modal" >
         <i class="fa fa-pencil"></i>&nbsp;
           Edit
       </button>
   @else
       <button type="button" class="btn btn-default btn-xs" style="cursor:not-allowed">
         <i class="fa fa-pencil"></i>&nbsp;
           Edit
       </button>
   @endif
@endif
<a href="/add-detail-so/saleTransaction/soDetailSo/{{$data->so_id}}">
  <button type="button" class="btn btn-primary btn-xs">
    <i class="fa fa-plus"></i>&nbsp;
      Detail so
  </button>
</a>

@if($privilaged->canDelete != 0)
   @if($data->so_status != "approve")
       <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->so_id}}" data-toggle="modal">
         <i class="fa fa-trash"></i>&nbsp;
           Delete
       </button>
   @else
       <button type="button" class="btn btn-danger btn-xs" style="cursor:not-allowed">
         <i class="fa fa-trash"></i>&nbsp;
           Delete
       </button>
   @endif
@endif

<?php
   $dates = strtotime($data->so_date);
   $dateEdit = date("d-m-Y", $dates);
?>

<!--Modal edit-->
 <div id="so{{$data->so_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
     <div class="modal-dialog">
       <form role="form" method="post" action="/so-edit/saleTransaction/edit-so">
         {{csrf_field()}}
         <input type="hidden" name="so_id" value="{{$data->so_id}}">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                 <h4 id="modal-responsive-label" class="modal-title">Edit so {{$data->so_no}}</h4></div>
             <div class="modal-body">
                 <div class="row">
                     <div class="col-md-12">
                       <div class="mbm">
                          <label>So no :</label>
                          <input type="text" placeholder="Please type the sales order number" name="so_no" class="form-control required" value="{{$data->so_no}}" />
                       </div>

                       <div class="mbm row">
                         <div class="col-md-6">
                              <label>Date :</label>
                              <input type="text" name="so_date" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" class="datepicker-default form-control required" value="{{$dateEdit}}"/>
                        </div>
                        <div class="col-md-6">
                              <label>Reg date :</label>
                              <input type="text" name="req_date" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" class="datepicker-default form-control required" value="{{$dateEdit}}"/>
                        </div>
                       </div>

                       <div class="mbm">
                          <label>PPN type :</label>
                          <select name="ppn_type" class="form-control required">
                              <option value="{{$data->ppn_type}}" selected>{{$data->ppn_type}}</option>
                          </select>
                       </div>
                       <div class="mbm">
                          <label>Notes :</label>
                          <textarea placeholder="Please type sales order note" name="notes" class="form-control required">{{$data->notes}}</textarea>
                       </div>
                       <div class="mbm">
                         <label>Customer :</label>
                             <input type="hidden"  id="cust_id{{$data->so_id}}" name="cust_id"  class="form-control" value="{{$data->cust_id}}">
                             <input type="text" id="autocompletecust{{$data->so_id}}" name="cust_code" placeholder="Please type sales order customer" name="cust_desc" class="form-control required" value="{{$data->cust_desc}}">

                             <!-- Barang catalog -->
                              <script>
                                 var cust = {!! $cust !!};

                                  $('#autocompletecust{{$data->so_id}}').devbridgeAutocomplete({
                                          lookup: cust,
                                          onSelect: function (suggestion) {
                                            $('#cust_id{{$data->so_id}}').val(suggestion.data);
                                            $('#autocompletecust{{$data->so_id}}').val(suggestion.value);
                                          }
                                  });
                              </script>
                       </div>
                       <div class="mbm">
                         <label>Gudang : </label>
                             <input type="hidden"  id="wh_id{{$data->so_id}}" name="wh_id"  class="form-control" value="{{$data->wh_id}}" required>
                             <input type="text"  id="autocompletewh{{$data->so_id}}" placeholder="Please type sales order warehouse" name="wh_desc" class="form-control required" value="{{$data->wh_desc}}">

                             <!-- warehouse catalog -->
                              <script>
                                  var wh = {!! $wh !!};

                                   $('#autocompletewh{{$data->so_id}}').devbridgeAutocomplete({
                                           lookup: wh,
                                           onSelect: function (suggestion) {
                                             $('#wh_id{{$data->so_id}}').val(suggestion.data);
                                             $('#autocompletewh{{$data->so_id}}').val(suggestion.value);
                                           }
                                   });
                              </script>
                       </div>
                       <div class="mbm">
                          <label>Status :</label>
                          <select name="so_status" class="form-control required">
                                <option value="{{$data->so_status}}" selected>{{$data->so_status}}</option>
                                @if($privilaged->canApprove != 0)
                                  <option value="approve">Approve</option>
                                @endif
                                <option value="partial">Partial</option>
                                <option value="finish">Finish</option>
                          </select>
                       </div>

                     </div>
                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                 <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
             </div>
         </div>
       </form>
     </div>
 </div>

 <!--Modal delete-->
  <div id="delete{{$data->so_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
      <div class="modal-dialog">
        <form role="form" method="post" action="/so-delete/saleTransaction/delete-so">
          {{csrf_field()}}
          <input type="hidden" name="so_id" value="{{$data->so_id}}">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                  <h4 id="modal-responsive-label" class="modal-title">Delete so {{$data->so_id}}</h4></div>
              <div class="modal-body">
                  <div class="row">
                        <h3 style="text-align:center">Are you sure want to delete this so ?</h3>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                  <button type="submit" class="btn btn-primary">Yes</button>
              </div>
          </div>
        </form>
      </div>
  </div>
  <script>
      $(".datepicker-default").datepicker();
      $('form').each(function () {
         $(this).validate();
      });
  </script>
