 @if($so_id->so_status != "approve")
     @if($privilaged->canEdit != 0)
           <button type="button" class="btn btn-default btn-xs" data-target="#ds{{$data->so_detail_id}}" data-toggle="modal" >
             <i class="fa fa-pencil"></i>&nbsp;
               Edit
           </button>
    @endif
    @if($privilaged->canDelete != 0)
         <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->so_detail_id}}" data-toggle="modal">
           <i class="fa fa-trash"></i>&nbsp;
             Delete
         </button>
    @endif

    <!--Modal edit-->
    <div id="ds{{$data->so_detail_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
        <div class="modal-dialog">
          <form role="form" method="post" action="/detail-so-edit/saleTransaction/edit-ds">
            {{csrf_field()}}
            <input type="hidden" name="so_detail_id" value="{{$data->so_detail_id}}">
            <input type="hidden" name="so_id" value="{{$so_id->so_id}}">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                    <h4 id="modal-responsive-label" class="modal-title">Edit detail so {{$data->so_detail_id}}</h4></div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                          <div class="mbm">
                            <label>Product :</label>
                                <input type="hidden"  id="prd_id{{$data->so_detail_id}}" name="prd_id"  class="form-control" value="{{$data->prd_id}}">
                                <input type="text" id="autocompleteprd{{$data->so_detail_id}}" placeholder="Please type the product article" name="prd_desc" class="form-control required" value="{{$data->prd_desc}}-{{$data->color_desc}}-{{$data->size_desc}}" />
                                <!-- Barang catalog -->
                                 <script>
                                    var prd = {!! $prdSugest !!};

                                     $('#autocompleteprd{{$data->so_detail_id}}').devbridgeAutocomplete({
                                             lookup: prd,
                                             onSelect: function (suggestion) {
                                               $('#prd_id{{$data->so_detail_id}}').val(suggestion.data);
                                               $('#autocompleteprd{{$data->so_detail_id}}').val(suggestion.value);
                                             }
                                     });
                                 </script>
                          </div>
                          <div class="mbm">
                            <label>Price :</label>
                            <input type="text" id="retail_price{{$data->so_detail_id}}" placeholder="Please type the product price" name="retail_price" class="form-control required" value="{{$data->order_price}}" data-a-sep='.' data-a-dec=',' data-a-form='false' data-a-pad='false'>
                            <script>
                              $('#retail_price{{$data->so_detail_id}}').autoNumeric('init');
                            </script>
                          </div>
                          <div class="mbm">
                            <label>Qty :</label>
                            <input type="number" placeholder="Please type the product quantity" name="qty_order" class="form-control required" value="{{number_format($data->qty_order)}}">
                          </div>
                          <div class="mbm">
                              <label>UOM :</label>
                              <select name="uom_id" class="form-control required">
                                  <option value="{{$data->uom_id}}" selected>{{$data->uom_desc}}</option>
                                @foreach($uoms as $uom)
                                  <option value="{{$uom->uom_id}}">{{$uom->uom_desc}}</option>
                                @endforeach
                              </select>
                          </div>
                          <div class="mbm">
                            <label>UOM conversion :</label>
                            <input type="number" placeholder="Please type the product conversion" name="uom_conversion" class="form-control required" value="{{number_format($data->uom_conversion)}}">
                          </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
                </div>
            </div>
          </form>
        </div>
    </div>

   <!--Modal delete-->
    <div id="delete{{$data->so_detail_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
        <div class="modal-dialog">
          <form role="form" method="post" action="/detail-so-delete/saleTransaction/delete-detail-so">
            {{csrf_field()}}
            <input type="hidden" name="so_detail_id" value="{{$data->so_detail_id}}">
            <input type="hidden" name="so_id" value="{{$so_id->so_id}}">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                    <h4 id="modal-responsive-label" class="modal-title">Delete detail so {{$data->so_detail_id}}</h4></div>
                <div class="modal-body">
                    <div class="row">
                          <h3 style="text-align:center">Are you sure want to delete this detail so ?</h3>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                    <button type="submit" class="btn btn-primary">Yes</button>
                </div>
            </div>
          </form>
        </div>
     </div>
 @endif
 <script>
     $(document).ready(function() {
       $('form').each(function () {
          $(this).validate();
       });
     });
 </script>
