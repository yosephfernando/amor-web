@extends('layout.layout')
@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb n-print">
    <div class="page-header pull-left">
        <div class="page-title">{{$title->moduleLabel}}</div>
    </div>
    <ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i>&nbsp;<a href="#">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li><a href="#">Transaction sales</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li><a href="#">Sales order</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="active">{{$title->moduleLabel}}</li>
    </ol>
    <div class="clearfix"></div>
</div>

<div class="page-content">
   <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-green">
            <div class="panel-body pan">
                    <div class="form-body pal">
                  @if($so_id->so_status != "approve")
                   @if($so_id->pr_flag != 1)
                    <form action="/detailso-add-action/saleTransaction/addDetailSoAction"  method="post" class="form-group">
                      {{csrf_field()}}
                       <input type="hidden" name="so_id" value="{{$so_id->so_id}}" />
                       <div class="col-md-12 col-sm-12 col-xs-12">
                         @if($errors->any())
                             <script>
                                    toastr.error("{{$errors->first()}}");
                                    toastr.options = {
                                     "closeButton": false,
                                     "debug": false,
                                     "positionClass": "toast-top-right",
                                     "onclick": null,
                                     "showDuration": "300",
                                     "hideDuration": "1000",
                                     "timeOut": "5000",
                                     "extendedTimeOut": "1000",
                                     "showEasing": "swing",
                                     "hideEasing": "linear",
                                     "showMethod": "fadeIn",
                                     "hideMethod": "fadeOut"
                                   };

                             </script>
                         @endif
                         @if(session('status'))
                             <script>
                                @if(session('status') == 'Detail so added' || session('status') == 'Detail so updated' || session('status') == 'Detail so deleted')
                                  toastr.success("{{ session('status') }}");
                                @else
                                  toastr.error("{{ session('status') }}");
                                @endif

                                    toastr.options = {
                                     "closeButton": false,
                                     "debug": false,
                                     "positionClass": "toast-top-right",
                                     "onclick": null,
                                     "showDuration": "300",
                                     "hideDuration": "1000",
                                     "timeOut": "5000",
                                     "extendedTimeOut": "1000",
                                     "showEasing": "swing",
                                     "hideEasing": "linear",
                                     "showMethod": "fadeIn",
                                     "hideMethod": "fadeOut"
                                   };

                             </script>
                         @endif
                       </div>
                       <div class="col-md-4 col-sm-12 col-xs-12">
                                <label>Product :</label>
                                    <input type="hidden"  id="prd_id" name="prd_id"  class="form-control">
                                    <input type="text" id="autocompleteprd" placeholder="Please type the product article" name="prd_desc" class="form-control required">
                                    <!-- Barang catalog -->
                                     <script>
                                        var prd = {!! $prdSugest !!};

                                         $('#autocompleteprd').devbridgeAutocomplete({
                                                 lookup: prd,
                                                 onSelect: function (suggestion) {
                                                   $('#prd_id').val(suggestion.data);
                                                   $('#autocompleteprd').val(suggestion.value);
                                                   $('#uom_id').val(suggestion.uom_id);
                                                   $('#uom_conv').val(suggestion.uom_conversion);
                                                   $('#uomdesc').val(suggestion.uomdesc);
                                                   $('#retail_price').val(Math.round(suggestion.retail_price));
                                                 }
                                         });
                                     </script>
                        </div>
                        <div class="col-md-2 col-sm-12 col-xs-12">
                            <label>Price :</label>
                            <input id="retail_price" type="text" placeholder="Please type the product price" name="retail_price" class="form-control required" data-a-sep='.' data-a-dec=',' data-a-form='false' data-a-pad='false'>
                            <script>
                              $('#retail_price').autoNumeric('init');
                            </script>
                        </div>
                        <div class="col-md-2 col-sm-12 col-xs-12">
                            <label>Qty :</label>
                            <input type="number" placeholder="Please type the product quantity" name="qty_order" class="form-control required">
                        </div>
                        <div class="col-md-2 col-sm-12 col-xs-12">
                            <label>Uom desc :</label>
                            <input id="uomdesc" placeholder="Please type the product conversion" name="uom_desc" type="text" class="form-control required" readonly>
                        </div>
                        <input id="uom_id" type="hidden" name="uom_id" />
                        <input id="uom_conv" type="hidden" name="uom_conversion">

                        <div class="col-md-2 col-sm-12 col-xs-12">
                           <div class="clear" style="height:20px"></div>
                              <button type="submit" class="btn btn-primary form-control"><i class="fa fa-plus"></i>&nbsp;Add</button>
                        </div>
                   </form>
                 @endif
                @endif
                        <div class="col-md-12">
                          <div class="clear" style="height:30px"></div>
                            @if($so_detail_id != null)
                                <div class="col-md-6">
                                    <h3 style="margin-top:0px"># {{$so_id->so_no}}</h3>
                                </div>
                                <div class="col-md-6" style="text-align:right">
                                  @if($privilaged->canPrint != 0)
                                        <button class="btn btn-success" onclick="window.print();"><i class="fa fa-print"></i>&nbsp;Print</button>
                                  @endif
                                  <a href="/list-so-header/saleTransaction/soListSo">
                                    <button type="button" class="btn btn-default">Back</button>
                                  </a>
                                </div>

                           @endif
                        </div>
                        <div class="col-md-12">
                          <div class="table-responsive">
                            <table class="table table-bordered" id="soD-table">
                              <thead>
                                   <th><input type="checkbox"  id="bulkDelete"  /></th>
                                   <th>Article</th>
                                   <th>Color</th>
                                   <th>Size</th>
                                   <th>Price</th>
                                   <th>Qty</th>
                                   <th>Sub total</th>
                                   @if($so_id->so_status != "approve")
                                       <th>Action</th>
                                   @endif
                               </thead>
                            </table>
                            <script>
                              $(function() {
                                var table = $('#soD-table').DataTable({
                                  dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
                                     "<'row'<'col-xs-12't>>"+
                                     "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
                                     processing: true,
                                     serverSide: true,
                                     ajax: {
                                        url: '/listSoDetailData/saleTransaction/soDetailSalesOrderDataTable/{{$so_id->so_id}}',
                                    },
                                     columns: [
                                         {data: 'delete_bulk', name: 'delete_bulk', orderable: false, searchable: false},
                                         { data: 'prd_code', name: 'prd_code' },
                                         { data: 'color_desc', name: 'color_desc' },
                                         { data: 'size_desc', name: 'size_desc' },
                                         { data: 'order_price', name: 'order_price' },
                                         { data: 'qty_order', name: 'qty_order' },
                                         { data: 'sub_total', name: 'sub_total' },

                                         @if($so_id->so_status != "approve")
                                            {data: 'action', name: 'action', className: 'text-center',orderable: false, searchable: false}
                                         @endif
                                     ],
                                     paginate: false
                                 });
                                  $('div.dataTables_filter input').addClass('form-control');

                                  $("#deleteTriger").click(function(){
                                     var allValue = [];
                                     var soid = '{{$so_id->so_id}}';
                                     $("input[name='so_detail_id[]']:checked").each( function () {
                                         allValue.push($(this).val());
                                     });
                                     $.post( "/detailSo-delete-action/saleTransaction/deleteDetailsoAction",  {'so_detail_id': allValue, 'so_id': soid, '_token' : '{{csrf_token()}}' })
                                       .done(function( data ) {
                                         if(data == "success"){
                                            location.reload();
                                         }else{
                                            alert(data);
                                         }
                                         console.log( "Data Loaded: " + data );
                                       });
                                   });

                                   $("#bulkDelete").click(function(){
                                      $("input[name='so_detail_id[]']").prop("checked", $(this).prop('checked'))
                                      if($("input[name='so_detail_id[]']:checked").length > 0){
                                        $("#deleteTriger").show();
                                      }else{
                                        $("#deleteTriger").hide();
                                      }
                                   });

                                   $("#soD-table").on('click', ':checkbox', function(){
                                     if($("input[name='so_detail_id[]']:checked").length > 0){
                                       $("#deleteTriger").show();
                                     }else{
                                       $("#deleteTriger").hide();
                                     }
                                   });

                                   $('#search-form').on('submit', function(e) {
                                       table.draw();
                                       e.preventDefault();
                                   });

                              });
                           </script>
                          </div>
                          <button class='btn btn-danger' id="deleteTriger" style="display:none">Delete cheked</button>
                          <div class="clear" style="height:20px"></div>
                        </div>
                    </div>
            </div>
        </div>
      </div>
     </div>
</div>
<script>
    $(document).ready(function() {
      $('form').each(function () {
         $(this).validate();
      });
    });
</script>
@include('salesorder/so_detail_sales_order/soPrintTemplateDetailSo')
@endsection
