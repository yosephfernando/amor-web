@extends('layout.layout')
@section('content')
<!--BEGIN TITLE & BREADCRUMB PAGE-->
            <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                <div class="page-header pull-left">
                    <div class="page-title">{{$title->moduleLabel}}</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a href="index.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li><a href="#">{{Request::segment(2)}}</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li class="active">{{Request::segment(3)}}</li>
                </ol>
                <div class="clearfix"></div>
            </div>
            <!--END TITLE & BREADCRUMB PAGE-->
           <div class="page-content">
              <div class="row">
                <div class="col-md-12">
                <div class="panel panel-green">
                        <div class="panel-heading">Supplier</div>
                        <div class="panel-body">
                          @if($errors->any())
                              <h4>{{$errors->first()}}</h4>
                          @endif
                          @if(session('status'))
                              <h4>{{ session('status') }}</h4>
                          @endif
                            <div class="table-responsive">
                              <div class="col-md-2 col-sm-12 col-xs-12 row">
                                 @if($privilaged->canAdd != 0)
                                    <button type="button" class="btn btn-success btn-md" data-target="#supplierAdd" data-toggle="modal">
                                        Add
                                    </button>
                                 @endif
                               </div>
                               <div class="col-md-7">
                                 <div class="dropdown" style="float:right">
                                     <button class="btn btn-dafault" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Bulk action <span class="caret"></span></button>
                                     <ul class="dropdown-menu" aria-labelledby="dLabel">
                                        <li><a href="#" id="deletec">Delete</a></li>
                                      </ul>
                                       <button class="btn btn-danger" id="deleteC" style="display:none">Delete checked</button>
                                       <div class="clear" style="height:10px"></div>
                                 </div>
                                 <script>
                                     $("#deletec").click(function(){
                                         $("#deleteC").fadeIn(1000);
                                         $("#beforeDelete").hide();
                                         $("#afterDelete").show();
                                     });

                                     $("#deleteC").click(function(){
                                       var allValue = [];
                                       $("input[name='supplierId[]']:checked").each( function () {
                                           allValue.push($(this).val());
                                       });
                                       $.post( "/delete-bulk-supplier/masterForm/deleteBulkSupplier",  {'supplierId': allValue, '_token' : '{{csrf_token()}}' })
                                         .done(function( data ) {
                                           if(data == "success"){
                                              location.reload();
                                           }else{
                                              alert(data);
                                           }
                                           console.log( "Data Loaded: " + data );
                                         });
                                     });
                                 </script>
                                <div class="clear" style="height:10px"></div>
                               </div>

                               <!-- search -->
                                  <div class="col-md-3 nopadding">
                                      <div class="col-md-12 nopadding" style="float:right">
                                          <div class="input-group">
                                              <input id="searchKeyword" type="text" class="form-control" placeholder="Search">
                                              <span id="searchUser" style="cursor:pointer" class="input-group-addon"><i class="fa fa-search"></i></span>
                                          </div>
                                        <div class="clear" style="height:15px"></div>
                                      </div>
                                  </div>

                                  <script>
                                      $("#searchUser").click(function(){
                                          var keyword = $("#searchKeyword").val();
                                          if(keyword != ""){
                                            $.get( "/ajax-supplier-search/masterForm/mfAjaxSearchSupplier/"+keyword)
                                                .done(function( data ) {
                                                    $("#ajaxTableSearch").empty();
                                                    $("#ajaxTableSearch").html(data);
                                                });
                                          }else{
                                            alert('keyword empty');
                                          }
                                      });
                                  </script>
                               <!-- /search -->
                               <div id="ajaxTableSearch">
                                <table id="beforeDelete" class="table table-striped table-bordered table-hover">
                                  <div class="clear" style="height:10px"></div>
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Kode supplier</th>
                                        <th>Nama supplier</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      <?php $i = $supplierData->firstItem();?>
                                      @foreach($supplierData as $data)
                                              <tr>
                                                  <td>{{$i}}</td>
                                                  <td>{{$data->supplierCode}}</td>
                                                  <td>{{$data->supplierName}}</td>
                                                  <td style="text-align:center">
                                                   @if($privilaged->canEdit != 0)
                                                          <button type="button" class="btn btn-default btn-md" data-target="#supplier{{$data->supplierCode}}" data-toggle="modal" >
                                                            <i class="fa fa-edit"></i>&nbsp;
                                                              Edit
                                                          </button>
                                                  @endif
                                                  @if($privilaged->canDelete != 0)
                                                        <button type="button" class="btn btn-danger btn-md" data-target="#delete{{$data->supplierCode}}" data-toggle="modal">
                                                            Delete
                                                        </button>
                                                  @endif
                                                  </td>
                                              </tr>

                                              <!--Modal edit-->
                                               <div id="supplier{{$data->supplierCode}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
                                                   <div class="modal-dialog">
                                                     <form role="form" method="post" action="/supplier-edit/masterForm/edit-supplier">
                                                       {{csrf_field()}}
                                                       <input type="hidden" name="supplierId" value="{{$data->supplierId}}">
                                                       <div class="modal-content">
                                                           <div class="modal-header">
                                                               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                                                               <h4 id="modal-responsive-label" class="modal-title">Edit supplier {{$data->supplierName}}</h4></div>
                                                           <div class="modal-body">
                                                               <div class="row">
                                                                   <div class="col-md-12">
                                                                       <div class="mbm">
                                                                          <label>Supplier name :</label>
                                                                          <input type="text" name="supplierName" value="{{$data->supplierName}}" class="form-control" required/>
                                                                       </div>
                                                                   </div>
                                                               </div>
                                                           </div>
                                                           <div class="modal-footer">
                                                               <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                                                               <button type="submit" class="btn btn-primary">Save changes</button>
                                                           </div>
                                                       </div>
                                                     </form>
                                                   </div>
                                               </div>


                                               <!--Modal delete-->
                                                <div id="delete{{$data->supplierCode}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
                                                    <div class="modal-dialog">
                                                      <form role="form" method="post" action="/supplier-delete/masterForm/delete-supplier">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="supplierId" value="{{$data->supplierId}}">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                                                                <h4 id="modal-responsive-label" class="modal-title">Delete supplier {{$data->supplierName}}</h4></div>
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                      <h3 style="text-align:center">Are you sure want to delete this supplier ?</h3>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                                                                <button type="submit" class="btn btn-primary">Yes</button>
                                                            </div>
                                                        </div>
                                                      </form>
                                                    </div>
                                                </div>
                                            <?php $i++ ?>
                                       @endforeach
                                    </tbody>
                                </table>

                                <table id="afterDelete" class="table table-striped table-bordered table-hover" style="display:none">
                                  <div class="clear" style="height:10px"></div>
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Supplier code</th>
                                        <th>Supplier name</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($supplierData as $data)
                                              <tr>
                                                  <td><input type="checkbox" name="supplierId[]" value="{{$data->supplierId}}" /></td>
                                                  <td>{{$data->supplierCode}}</td>
                                                  <td>{{$data->supplierName}}</td>
                                              </tr>
                                       @endforeach
                                    </tbody>
                                </table>
                                {{$supplierData->links()}}
                              </div>
                            </div>
                        </div>
                    </div>
                  </div>
              </div>
          </div>

          <!--Modal add user-->
           <div id="supplierAdd" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
               <div class="modal-dialog">
                 <form role="form" method="post" action="/supplier-add/masterForm/add-supplier">
                    {{csrf_field()}}
                   <div class="modal-content">
                       <div class="modal-header">
                           <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                           <h4 id="modal-responsive-label" class="modal-title">Add supplier</h4></div>
                       <div class="modal-body">
                           <div class="row">
                               <div class="col-md-12">
                                       <div class="mbm">
                                          <label>Supplier name :</label>
                                          <input type="text" name="supplierName" class="form-control" required/>
                                       </div>
                               </div>
                           </div>
                       </div>
                       <div class="modal-footer">
                           <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                           <button type="submit" class="btn btn-primary">Save</button>
                       </div>
                   </div>
                  </form>
               </div>
           </div>

@endsection
