<h3>Search result of '{{$keyword}}'</h3>
<table id="beforeDelete" class="table table-striped table-bordered table-hover">
  <div class="clear" style="height:10px"></div>
    <thead>
    <tr>
      <th>#</th>
      <th>Kode supplier</th>
      <th>Nama supplier</th>
      <th>Action</th>
    </tr>
    </thead>
    <tbody>
      <?php $i = 1;?>
      @foreach($supplierData as $data)
              <tr>
                <td>{{$i}}</td>
                <td>{{$data->supplierCode}}</td>
                <td>{{$data->supplierName}}</td>
                <td style="text-align:center">
                 @if($privilaged->canEdit != 0)
                        <button type="button" class="btn btn-default btn-md" data-target="#supplier{{$data->supplierId}}" data-toggle="modal" >
                          <i class="fa fa-edit"></i>&nbsp;
                            Edit
                        </button>
                @endif
                @if($privilaged->canDelete != 0)
                      <button type="button" class="btn btn-danger btn-md" data-target="#delete{{$data->supplierId}}" data-toggle="modal">
                          Delete
                      </button>
                @endif
                </td>
              </tr>

              <!--Modal edit-->
               <div id="supplier{{$data->supplierId}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
                   <div class="modal-dialog">
                       <input type="hidden" id="idSupplier{{$data->supplierId}}" value="{{$data->supplierId}}" />
                       <div class="modal-content">
                           <div class="modal-header">
                               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                               <h4 id="modal-responsive-label" class="modal-title">Edit supplier {{$data->suplierCode}}</h4>
                           </div>
                           <div class="modal-body">
                               <div class="row">
                                   <div class="col-md-12">
                                       <h4 id="message{{$data->supplierId}}" style="display:none">Data updated</h4>
                                       <h4 id="error{{$data->supplierId}}" style="display:none">Error updating</h4>
                                       <div class="mbm">
                                          <label>Supplier name :</label>
                                          <input type="text" id="supplierName{{$data->supplierId}}" value="{{$data->supplierName}}" class="form-control" required/>
                                       </div>
                                   </div>
                               </div>
                           </div>
                           <div class="modal-footer">
                               <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                               <button id="submit{{$data->supplierId}}" class="btn btn-primary">Save changes</button>
                           </div>
                       </div>

                       <!-- ajax post data -->
                          <script>
                                $("#message{{$data->supplierId}}").hide();
                                $("#error{{$data->supplierId}}").hide();
                                $("#submit{{$data->supplierId}}").click(function(){
                                  var id = $("#idSupplier{{$data->supplierId}}").val();
                                  var supplierName = $("#supplierName{{$data->supplierId}}").val();

                                  if(supplierName != "" || id != ""){
                                    $.post('/supplier-edit/masterForm/edit-supplier', {'supplierName' : supplierName, 'supplierId' : id, '_token' : '{{csrf_token()}}' })
                                    .done(function(data){
                                        $("#message{{$data->supplierId}}").show();
                                     })
                                     .error(function(xhr, status, error){
                                        $("#error{{$data->supplierId}}").show();
                                     });
                                  }else{
                                    console.log("failed");
                                  }
                                });

                                $("#supplier{{$data->supplierId}}").on('hidden.bs.modal', function(e){
                                  $("#message{{$data->supplierId}}").hide();
                                  $("#error{{$data->supplierId}}").hide();
                                });
                          </script>
                       <!-- end of ajax post data -->

                   </div>
               </div>


               <!--Modal delete-->
                <div id="delete{{$data->supplierId}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
                    <div class="modal-dialog">
                        <input type="hidden" id="idSupplier{{$data->supplierId}}" value="{{$data->supplierId}}">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                                <h4 id="modal-responsive-label" class="modal-title">Delete supplier {{$data->supplierCode}}</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                      <h3 id="messageDelete{{$data->supplierId}}" style="text-align:center">Are you sure want to delete this supplier ?</h3>
                                </div>
                            </div>
                            <div id="beforeDel{{$data->supplierId}}" class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                                <button id="deleteSubmit{{$data->supplierId}}" type="submit" class="btn btn-primary">Yes</button>
                            </div>
                            <div id="afterDel{{$data->supplierId}}" class="modal-footer" style="display:none">
                                <button type="button" data-dismiss="modal" class="btn btn-default">Ok</button>
                            </div>
                        </div>

                      <!-- ajax post data -->
                         <script>
                               $("#deleteSubmit{{$data->supplierId}}").click(function(){
                                 var id = $("#idSupplier{{$data->supplierId}}").val();

                                 if(id != ""){
                                   $.post('/supplier-delete/masterForm/delete-supplier', {'supplierId' : id, '_token' : '{{csrf_token()}}' })
                                   .done(function(data){
                                       $("#beforeDel{{$data->supplierId}}").hide();
                                       $("#afterDel{{$data->supplierId}}").show();
                                       $("#messageDelete{{$data->supplierId}}").text('Data deleted');
                                    })
                                    .error(function(xhr, status, error){
                                       $("#beforeDel{{$data->supplierId}}").hide();
                                       $("#afterDel{{$data->supplierId}}").show();
                                       $("#messageDelete{{$data->supplierId}}").text('Error deleting');
                                    });
                                 }else{
                                   console.log("failed");
                                 }
                               });

                               $("#supplier{{$data->supplierId}}").on('hidden.bs.modal', function(e){
                                 $("#messageDelete{{$data->supplierId}}").text('Are you sure want to delete this supplier ?');
                                 $("#beforeDel{{$data->supplierId}}").show();
                                 $("#afterDel{{$data->supplierId}}").hide();
                               });
                         </script>
                      <!-- end of ajax post data -->

                    </div>
                </div>
            <?php $i++ ?>
       @endforeach
    </tbody>
</table>
