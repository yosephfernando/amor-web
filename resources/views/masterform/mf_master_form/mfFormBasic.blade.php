@extends('layout.layout')
@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">{{$title->moduleLabel}}</div>
    </div>
    <ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i>&nbsp;<a href="index.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li><a href="#">{{Request::segment(2)}}</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="active">{{Request::segment(3)}}</li>
    </ol>
    <div class="clearfix"></div>
</div>

<div class="page-content">
   <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-green">
            <div class="panel-heading">Form {{$title->moduleLabel}}</div>
            <div class="panel-body pan">
                 <div class="form-body pal">
                   <form action="/form-basic-action/masterForm/formBasicAdd"  method="post" enctype="multipart/form-data" class="form-group">
                     {{csrf_field()}}
                        <div class="col-md-9 col-sm-12 col-xs-12">
                          @if($errors->any())
                              <h4>{{$errors->first()}}</h4>
                          @endif
                          @if(session('status'))
                              <h4>{{ session('status') }}</h4>
                          @endif
                        </div>

                        <!-- input text start -->
                        <div class="col-md-9 col-sm-12 col-xs-12">
                            <label>Input text :</label>
                            <input type="text" name="masterFormText" class="form-control" />
                          <div class="clear" style="height:10px"></div>
                        </div>
                        <!-- input text end -->

                        <!-- select option start -->
                        <div class="col-md-9 col-sm-12 col-xs-12">
                            <label>Select option :</label>
                            <select name="masterFormSelect" class="form-control">
                              <option value="option1"><b>option1</b></option>
                              <option value="option2"><b>option2</b></option>
                            </select>
                          <div class="clear" style="height:10px"></div>
                        </div>
                        <!-- select option end -->

                        <!-- Checkboxes start -->
                        <div class="col-md-9 col-sm-12 col-xs-12">
                               <label>Checkboxes :</label><br />
                               <input name="masterFormCheckBox" type="checkbox" value="checkbox1" />&nbsp;&nbsp;checkbox1
                               <input name="masterFormCheckBox2" type="checkbox" value="checkbox2" />&nbsp;&nbsp;checkbox2
                           <div class="clear" style="height:10px"></div>
                        </div>
                        <!-- Checkboxes end -->

                        <!-- Radio start -->
                        <div class="col-md-9 col-sm-12 col-xs-12">
                               <label>Radio button :</label><br />
                               <input name="masterFormRadio" type="radio" value="radio1" checked="true"/>&nbsp;&nbsp;radio1
                               <input name="masterFormRadio" type="radio" value="radio2" />&nbsp;&nbsp;radio2
                           <div class="clear" style="height:10px"></div>
                        </div>
                        <!-- Radio end -->

                        <!-- textarea start -->
                        <div class="col-md-9 col-sm-12 col-xs-12">
                               <label>Text area :</label><br />
                               <textarea name="masterFormTextArea" class="form-control">
                               </textarea>
                           <div class="clear" style="height:10px"></div>
                        </div>
                        <!-- textarea end -->

                        <!-- input file start -->
                        <div class="col-md-9 col-sm-12 col-xs-12">
                               <label>Input file :</label><br />
                               <input type="file" name="masterFormFile" />
                           <div class="clear" style="height:10px"></div>
                        </div>
                        <!-- input file end -->

                        <!-- datepicker range start -->
                        <div class="col-md-9 col-sm-12 col-xs-12">
                              <div class="col-md-5 col-sm-12 col-xs-12 row">
                                     <label>Datepicker range :</label><br />
                                      <div class="input-group input-daterange">
                                            <input type="text" name="masterFormDateFrom" class="form-control"/>
                                            <span class="input-group-addon">to</span>
                                            <input type="text" name="masterFormDateTo" class="form-control"/>
                                      </div>
                                 <div class="clear" style="height:10px"></div>
                              </div>
                        </div>
                        <!-- datepicker range end -->

                        <!-- datepicker start -->
                        <div class="col-md-9 col-sm-12 col-xs-12">
                            <div class="col-md-5 col-sm-12 col-xs-12 row">
                                   <label>Datepicker :</label><br />
                                  <input type="text" name="masterFormDate" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" class="datepicker-default form-control"/>
                               <div class="clear" style="height:10px"></div>
                            </div>
                        </div>
                        <!-- datepicker end -->


                        <!-- action button start -->
                        <div class="col-md-9 col-sm-12 col-xs-12">
                            <input type="submit" class="btn btn-primary" value="submit">
                            &nbsp;
                            <a href="/form-basic/masterForm/formBasic">
                              <button type="button" class="btn btn-green">Cancel</button>
                            </a>
                            <div class="clear" style="height:20px"></div>
                        </div>
                        <!-- action button end -->
                   </form>

               </div>
            </div>
        </div>
      </div>
     </div>
</div>
@endsection
