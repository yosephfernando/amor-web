<h3>Search result of '{{$keyword}}'</h3>
<table id="beforeDelete" class="table table-striped table-bordered table-hover">
  <div class="clear" style="height:10px"></div>
    <thead>
    <tr>
      <th>#</th>
      <th>Kode barang</th>
      <th>Nama barang</th>
      <th>Supplier</th>
      <th>Action</th>
    </tr>
    </thead>
    <tbody>
      <?php $i = 1;?>
      @foreach($barangData as $data)
              <tr>
                  <td>{{$i}}</td>
                  <td>{{$data->barangCode}}</td>
                  <td>{{$data->barangName}}</td>
                  <td>{{$data->supplierName}}</td>
                  <td style="text-align:center">
                   @if($privilaged->canEdit != 0)
                          <button type="button" class="btn btn-default btn-md" data-target="#barang{{$data->barangId}}" data-toggle="modal" >
                            <i class="fa fa-edit"></i>&nbsp;
                              Edit
                          </button>
                  @endif
                  @if($privilaged->canDelete != 0)
                        <button type="button" class="btn btn-danger btn-md" data-target="#delete{{$data->barangId}}" data-toggle="modal">
                            Delete
                        </button>
                  @endif
                  </td>
              </tr>

              <!--Modal edit-->
               <div id="barang{{$data->barangId}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
                   <div class="modal-dialog">
                       <input type="hidden" id="barangId{{$data->barangId}}" value="{{$data->barangId}}" />
                       <div class="modal-content">
                           <div class="modal-header">
                               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                               <h4 id="modal-responsive-label" class="modal-title">Edit barang {{$data->barangCode}}</h4>
                           </div>
                           <div class="modal-body">
                               <div class="row">
                                   <div class="col-md-12">
                                       <h4 id="message{{$data->barangId}}" style="display:none">Data updated</h4>
                                       <h4 id="error{{$data->barangId}}" style="display:none">Error updating</h4>
                                       <div class="mbm">
                                          <label>Barang name :</label>
                                          <input type="text" id="barangName{{$data->barangId}}" value="{{$data->barangName}}" class="form-control" required/>
                                       </div>
                                       <div class="mbm">
                                          <label>Supplier :</label>
                                          <select class="form-control" id="supplierCode{{$data->barangId}}" required>
                                            @foreach($supplier as $supp)
                                              @if($supp->supplierCode == $data->supplierCode)
                                                <option value="{{$supp->supplierCode}}" selected>{{$supp->supplierName}}</option>
                                              @else
                                                 <option value="{{$supp->supplierCode}}">{{$supp->supplierName}}</option>
                                              @endif

                                            @endforeach
                                          </select>
                                       </div>
                                   </div>
                               </div>
                           </div>
                           <div class="modal-footer">
                               <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                               <button id="submit{{$data->barangId}}" class="btn btn-primary">Save changes</button>
                           </div>
                       </div>

                       <!-- ajax post data -->
                          <script>
                                $("#message{{$data->barangId}}").hide();
                                $("#error{{$data->barangId}}").hide();
                                $("#submit{{$data->barangId}}").click(function(){
                                  var id = $("#barangId{{$data->barangId}}").val();
                                  var supplierCode = $("#supplierCode{{$data->barangId}}").val();

                                  if(supplierCode != "" || id != ""){
                                    $.post('/barang-edit/masterForm/edit-barang', {'supplierCode' : supplierCode, 'barangId' : id, '_token' : '{{csrf_token()}}' })
                                    .done(function(data){
                                        $("#message{{$data->barangId}}").show();
                                     })
                                     .error(function(xhr, status, error){
                                        $("#error{{$data->barangId}}").show();
                                     });
                                  }else{
                                    console.log("failed");
                                  }
                                });

                                $("#barang{{$data->barangId}}").on('hidden.bs.modal', function(e){
                                  $("#message{{$data->barangId}}").hide();
                                  $("#error{{$data->barangId}}").hide();
                                });
                          </script>
                       <!-- end of ajax post data -->

                   </div>
               </div>


               <!--Modal delete-->
                <div id="delete{{$data->barangId}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
                    <div class="modal-dialog">
                        <input type="hidden" id="barangId{{$data->barangId}}" value="{{$data->barangId}}">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                                <h4 id="modal-responsive-label" class="modal-title">Delete barang {{$data->barangCode}}</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                      <h3 id="messageDelete{{$data->barangId}}" style="text-align:center">Are you sure want to delete this barang ?</h3>
                                </div>
                            </div>
                            <div id="beforeDel{{$data->barangId}}" class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                                <button id="deleteSubmit{{$data->barangId}}" type="submit" class="btn btn-primary">Yes</button>
                            </div>
                            <div id="afterDel{{$data->barangId}}" class="modal-footer" style="display:none">
                                <button type="button" data-dismiss="modal" class="btn btn-default">Ok</button>
                            </div>
                        </div>

                      <!-- ajax post data -->
                         <script>
                               $("#deleteSubmit{{$data->barangId}}").click(function(){
                                 var id = $("#barangId{{$data->barangId}}").val();

                                 if(id != ""){
                                   $.post('/barang-delete/masterForm/delete-barang', {'barangId' : id, '_token' : '{{csrf_token()}}' })
                                   .done(function(data){
                                       $("#beforeDel{{$data->barangId}}").hide();
                                       $("#afterDel{{$data->barangId}}").show();
                                       $("#messageDelete{{$data->barangId}}").text('Data deleted');
                                    })
                                    .error(function(xhr, status, error){
                                       $("#beforeDel{{$data->barangId}}").hide();
                                       $("#afterDel{{$data->barangId}}").show();
                                       $("#messageDelete{{$data->barangId}}").text('Error deleting');
                                    });
                                 }else{
                                   console.log("failed");
                                 }
                               });

                               $("#barang{{$data->barangId}}").on('hidden.bs.modal', function(e){
                                 $("#messageDelete{{$data->barangId}}").text('Are you sure want to delete this barang ?');
                                 $("#beforeDel{{$data->barangId}}").show();
                                 $("#afterDel{{$data->barangId}}").hide();
                               });
                         </script>
                      <!-- end of ajax post data -->

                    </div>
                </div>
            <?php $i++ ?>
       @endforeach
    </tbody>
</table>
