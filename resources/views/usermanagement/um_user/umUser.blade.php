@extends('layout.layout')
@section('content')
<!--BEGIN TITLE & BREADCRUMB PAGE-->
            <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                <div class="page-header pull-left">
                    <div class="page-title">{{$title->moduleLabel}}</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a href="index.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li><a href="#">User management</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li class="active">{{$title->moduleLabel}}</li>
                </ol>
                <div class="clearfix"></div>
            </div>
            <!--END TITLE & BREADCRUMB PAGE-->
           <div class="page-content">
              <div class="row">
                <div class="col-md-12">
                <div class="panel panel-green">
                        <div class="panel-body">
                          @if($errors->any())
                              <script>
                                     toastr.error("{{$errors->first()}}");
                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                          @if(session('status'))
                              <script>
                                 @if(session('status') == 'User added' || session('status') == 'User updated' || session('status') == 'User deleted')
                                   toastr.success("{{ session('status') }}");
                                 @else
                                   toastr.error("{{ session('status') }}");
                                 @endif

                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                            <div class="table-responsive">

                              <div class="col-md-2 col-sm-12 col-xs-12 row">
                                 @if($privilaged->canAdd != 0)
                                    <button type="button" class="btn btn-primary btn-md" data-target="#userAdd" data-toggle="modal">
                                      <i class="fa fa-plus"></i>&nbsp;
                                        Add
                                    </button>
                                 @endif
                               </div>

                               <!-- bulk action -->
                               <div class="col-md-7">
                                 <div class="dropdown" style="float:right">
                                     <button class="btn btn-dafault" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Bulk action <span class="caret"></span></button>
                                     <ul class="dropdown-menu" aria-labelledby="dLabel">
                                        <li><a href="#" id="deletec">Delete</a></li>
                                      </ul>
                                       <button class="btn btn-danger" id="deleteC" style="display:none">Delete checked</button>
                                       <div class="clear" style="height:10px"></div>
                                 </div>
                                 <script>
                                     $("#deletec").click(function(){
                                         $("#deleteC").fadeIn(1000);
                                         $("#beforeDelete").hide();
                                         $("#afterDelete").show();
                                     });

                                     $("#deleteC").click(function(){
                                       var allValue = [];
                                       $("input[name='id[]']:checked").each( function () {
                                           allValue.push($(this).val());
                                       });
                                       $.post( "/delete-bulk-user/userManagement/deleteBulkUser",  {'id': allValue, '_token' : '{{csrf_token()}}' })
                                         .done(function( data ) {
                                           if(data == "success"){
                                              location.reload();
                                           }else{
                                              alert(data);
                                           }
                                           console.log( "Data Loaded: " + data );
                                         });
                                     });
                                 </script>
                                <div class="clear" style="height:10px"></div>
                               </div>
                               <!-- end of bulk action -->

                               <!-- search -->
                                  <div class="col-md-3 nopadding">
                                      <div class="col-md-12 nopadding" style="float:right">
                                          <div class="input-group">
                                              <input id="searchKeyword" type="text" class="form-control" placeholder="Search">
                                              <span id="searchUser" style="cursor:pointer" class="input-group-addon"><i class="fa fa-search"></i></span>
                                          </div>
                                        <div class="clear" style="height:15px"></div>
                                      </div>
                                  </div>

                                  <script>
                                      $("#searchUser").click(function(){
                                          var keyword = $("#searchKeyword").val();
                                          if(keyword != ""){
                                            $.get( "/ajax-user-search/userManagement/umAjaxSearchUser/"+keyword)
                                                .done(function( data ) {
                                                    $("#ajaxTableSearch").empty();
                                                    $("#ajaxTableSearch").html(data);
                                                });
                                          }else{
                                            alert('keyword empty');
                                          }
                                      });
                                  </script>
                               <!-- /search -->

                               <!-- table -->
                               <div id="ajaxTableSearch">
                                <table id="beforeDelete" class="table table-striped table-bordered table-hover">
                                  <div class="clear" style="height:10px"></div>
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>User name</th>
                                        <th>User email</th>
                                        <th>User group</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      <?php $i = $userData->firstItem();?>
                                      @foreach($userData as $data)
                                              <tr>
                                                  <td>{{$i}}</td>
                                                  <td>{{$data->name}}</td>
                                                  <td>{{$data->email}}</td>
                                                  <td>{{$data->groupName}}</td>
                                                  <td style="text-align:center">
                                                   @if($privilaged->canEdit != 0)
                                                          <button type="button" class="btn btn-default btn-xs" data-target="#user{{$data->id}}" data-toggle="modal" >
                                                            <i class="fa fa-pencil"></i>&nbsp;
                                                              Edit
                                                          </button>
                                                  @endif
                                                  @if($privilaged->canDelete != 0)
                                                        <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->id}}" data-toggle="modal">
                                                          <i class="fa fa-trash"></i>&nbsp;
                                                            Delete
                                                        </button>
                                                  @endif
                                                  </td>
                                              </tr>
                                               <!--Modal delete-->
                                                <div id="delete{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
                                                    <div class="modal-dialog">
                                                      <form role="form" method="post" action="/user-delete/userManagement/delete-user">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="id" value="{{$data->id}}">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                                                                <h4 id="modal-responsive-label" class="modal-title">Delete user {{$data->email}}</h4></div>
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                      <h3 style="text-align:center">Are you sure want to delete this user ?</h3>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                                                                <button type="submit" class="btn btn-primary">Yes</button>
                                                            </div>
                                                        </div>
                                                      </form>
                                                    </div>
                                                </div>
                                            <?php $i++ ?>
                                       @endforeach
                                    </tbody>
                                </table>
                              @foreach($userData as $data)
                                <form role="form" method="post" action="/user-edit/userManagement/edit-user" id="form-edit-{{$data->id}}">
                                  {{csrf_field()}}
                                  <input type="hidden" name="id" value="{{$data->id}}" />
                                    <!--Modal edit-->
                                     <div id="user{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
                                         <div class="modal-dialog">
                                             <div class="modal-content">
                                                 <div class="modal-header">
                                                     <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                                                     <h4 id="modal-responsive-label" class="modal-title">Edit user {{$data->email}}</h4></div>
                                                 <div class="modal-body">
                                                     <div class="row">
                                                         <div class="col-md-12">
                                                             <div class="mbm">
                                                                <label>User name :</label>
                                                                <input type="text" name="name" value="{{$data->name}}" class="form-control required" placeholder="Please type the user name"/>
                                                             </div>
                                                             <div class="mbm">
                                                                <label>User email :</label>
                                                                <input type="email" name="email" value="{{$data->email}}" class="form-control required email" placeholder="Please type the user email" />
                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                                 <div class="modal-footer">
                                                     <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                                                     <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                               </form>
                              @endforeach
                                <table id="afterDelete" class="table table-striped table-bordered table-hover" style="display:none">
                                  <div class="clear" style="height:10px"></div>
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>User name</th>
                                        <th>User email</th>
                                        <th>User group</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($userData as $data)
                                              <tr>
                                                  <td><input type="checkbox" name="id[]" value="{{$data->id}}" /></td>
                                                  <td>{{$data->name}}</td>
                                                  <td>{{$data->email}}</td>
                                                  <td>{{$data->groupName}}</td>
                                              </tr>
                                       @endforeach
                                    </tbody>
                                </table>
                                {{$userData->links()}}
                              </div>
                              <!-- /table -->
                            </div>
                        </div>
                    </div>
                  </div>
              </div>
          </div>

          <!--Modal add user-->
           <div id="userAdd" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
               <div class="modal-dialog">
                 <form role="form" method="post" action="/user-add/userManagement/add-user">
                    {{csrf_field()}}
                   <div class="modal-content">
                       <div class="modal-header">
                           <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                           <h4 id="modal-responsive-label" class="modal-title">Add user</h4></div>
                       <div class="modal-body">
                           <div class="row">
                               <div class="col-md-12">

                                       <div class="mbm">
                                          <label>User name :</label>
                                          <input type="text" name="name" class="form-control required" placeholder="Please type the user name"/>
                                       </div>
                                       <div class="mbm">
                                          <label>User email :</label>
                                          <input type="email" name="email" class="form-control required email" placeholder="Please type the user email"/>
                                       </div>
                                       <div class="mbm">
                                          <label>User password :</label>
                                          <input type="password" name="password" class="form-control required password" placeholder="Please type the user password"/>
                                       </div>
                               </div>
                           </div>
                       </div>
                       <div class="modal-footer">
                           <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                           <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                       </div>
                   </div>
                  </form>
               </div>
           </div>
           <script>
                 $(document).ready(function() {
                   $('form').each(function () {
                      $(this).validate();
                   });
                 });
           </script>
@endsection
