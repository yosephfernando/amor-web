<h3>Search result of '{{$keyword}}'</h3>
<table id="beforeDelete" class="table table-striped table-bordered table-hover">
  <div class="clear" style="height:10px"></div>
    <thead>
    <tr>
        <th>#</th>
        <th>User name</th>
        <th>User email</th>
        <th>User group</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
      <?php $i = 1;?>
      @foreach($userData as $data)
              <tr>
                  <td>{{$i}}</td>
                  <td>{{$data->name}}</td>
                  <td>{{$data->email}}</td>
                  <td>{{$data->groupName}}</td>
                  <td style="text-align:center">
                   @if($privilaged->canEdit != 0)
                          <button type="button" class="btn btn-default btn-xs" data-target="#userAjax{{$data->id}}" data-toggle="modal" >
                            <i class="fa fa-pencil"></i>&nbsp;
                              Edit
                          </button>
                  @endif
                  @if($privilaged->canDelete != 0)
                        <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->id}}" data-toggle="modal">
                          <i class="fa fa-trash"></i>&nbsp;
                            Delete
                        </button>
                  @endif
                  </td>
              </tr>
               <!--Modal delete-->
                <div id="delete{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
                    <div class="modal-dialog">
                        <input type="hidden" id="userId{{$data->id}}" value="{{$data->id}}">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                                <h4 id="modal-responsive-label" class="modal-title">Delete user {{$data->email}}</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                      <h3 id="messageDelete{{$data->id}}" style="text-align:center">Are you sure want to delete this user ?</h3>
                                </div>
                            </div>
                            <div id="beforeDel{{$data->id}}" class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                                <button id="deleteSubmit{{$data->id}}" type="submit" class="btn btn-primary">Yes</button>
                            </div>
                            <div id="afterDel{{$data->id}}" class="modal-footer" style="display:none">
                                <button type="button" data-dismiss="modal" class="btn btn-default">Ok</button>
                            </div>
                        </div>

                      <!-- ajax post data -->
                         <script>
                               $("#deleteSubmit{{$data->id}}").click(function(){
                                 var id = $("#userId{{$data->id}}").val();

                                 if(id != ""){
                                   $.post('/user-delete/userManagement/delete-user', {'id' : id, '_token' : '{{csrf_token()}}' })
                                   .done(function(data){
                                       $("#beforeDel{{$data->id}}").hide();
                                       $("#afterDel{{$data->id}}").show();
                                       $("#messageDelete{{$data->id}}").text('Data deleted');
                                    })
                                    .error(function(xhr, status, error){
                                       $("#beforeDel{{$data->id}}").hide();
                                       $("#afterDel{{$data->id}}").show();
                                       $("#messageDelete{{$data->id}}").text('Error deleting');
                                    });
                                 }else{
                                   console.log("failed");
                                 }
                               });

                               $("#userAjax{{$data->id}}").on('hidden.bs.modal', function(e){
                                 $("#messageDelete{{$data->id}}").text('Are you sure want to delete this user ?');
                                 $("#beforeDel{{$data->id}}").show();
                                 $("#afterDel{{$data->id}}").hide();
                               });
                         </script>
                      <!-- end of ajax post data -->

                    </div>
                </div>
            <?php $i++ ?>
       @endforeach
    </tbody>
</table>
@foreach($userData as $data)
<!--Modal edit-->
 <div id="userAjax{{$data->id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
     <div class="modal-dialog">
      <form>
         <input type="hidden" id="idUser{{$data->id}}" value="{{$data->id}}" />
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                 <h4 id="modal-responsive-label" class="modal-title">Edit user {{$data->email}}</h4>
             </div>
             <div class="modal-body">
                 <div class="row">
                     <div class="col-md-12">
                         <h4 id="message{{$data->id}}" style="display:none">Data updated</h4>
                         <h4 id="error{{$data->id}}" style="display:none">Error updating</h4>
                         <div class="mbm">
                            <label>User name :</label>
                            <input type="text" id="name{{$data->id}}" value="{{$data->name}}" class="form-control" placeholder="Please type the user name" required/>
                         </div>
                         <div class="mbm">
                            <label>User email :</label>
                            <input type="email" id="email{{$data->id}}" value="{{$data->email}}" class="form-control" placeholder="Please type the user email" required/>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                 <button id="submit{{$data->id}}" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
             </div>
         </div>

         <!-- ajax post data -->
            <script>
                  $("#message{{$data->id}}").hide();
                  $("#error{{$data->id}}").hide();
                  $("#submit{{$data->id}}").click(function(){
                    var id = $("#idUser{{$data->id}}").val();
                    var name = $("#name{{$data->id}}").val();
                    var email = $("#email{{$data->id}}").val();
                    var userGroupId = $("#group{{$data->id}}").val();

                    if(id != "" || name != "" || email != "" || userGroupId != ""){
                      $.post('/user-edit/userManagement/edit-user', {'group' : userGroupId, 'id' : id, '_token' : '{{csrf_token()}}', 'name' : name, 'email' : email })
                      .done(function(data){
                          $("#message{{$data->id}}").show();
                       })
                       .error(function(xhr, status, error){
                          $("#error{{$data->id}}").show();
                       });
                    }else{
                      console.log("failed");
                    }
                  });

                  $("#userAjax{{$data->id}}").on('hidden.bs.modal', function(e){
                    $("#message{{$data->id}}").hide();
                    $("#error{{$data->id}}").hide();
                  });
            </script>
         <!-- end of ajax post data -->
       </form>
     </div>
 </div>
 @endforeach
<script>
      $(document).ready(function() {
        $('form').each(function () {
           $(this).validate();
        });
      });
</script>
