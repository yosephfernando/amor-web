@extends('layout.layout')
@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
    <div class="page-header pull-left">
        <div class="page-title">{{$title->moduleLabel}}</div>
    </div>
    <ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i>&nbsp;<a href="index.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li><a href="#">User management</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="active">User group(s)<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="active">{{$title->moduleLabel}}</li>
    </ol>
    <div class="clearfix"></div>
</div>

<div class="page-content">
   <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-green">
            <div class="panel-body pan">
                    <div class="form-body pal">
                        <div class="col-md-9 col-sm-12 col-xs-12">
                          @if($errors->any())
                              <script>
                                     toastr.error("{{$errors->first()}}");
                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                          @if(session('status'))
                              <script>
                                 @if(session('status') == 'Permission added' || session('status') == 'Permission updated' || session('status') == 'Permission deleted')
                                   toastr.success("{{ session('status') }}");
                                 @else
                                   toastr.error("{{ session('status') }}");
                                 @endif

                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                    <form action="/permission-add-action/userManagement/addPermissionAction"  method="post" class="form-group">
                      {{csrf_field()}}
                       <input type="hidden" name="groupId" value="{{$groupId}}" />
                            <label>Module name :</label>
                            <input type="hidden"  id="moduleId" name="moduleId"  class="form-control">
                            <input type="text" id="autocompletemod" placeholder="Please type module name" name="moduleName" class="form-control required"/>
                            <!-- Barang catalog -->
                             <script>
                                 $('#autocompletemod').devbridgeAutocomplete({
                                        serviceUrl : '/lookupSugestion/module',
                                        minChars : 3,
                                         onSelect: function (suggestion) {
                                           $('#moduleId').val(suggestion.data);
                                           $('#autocompletemod').val(suggestion.value);
                                         }
                                 });
                             </script>
                        </div>
                        <div class="col-md-9 col-sm-12 col-xs-12">
                           <div class="col-md-7">
                               <input name="canAdd1" type="checkbox" value="1" />&nbsp;&nbsp;add
                               <input name="canEdit1" type="checkbox" value="1" />&nbsp;&nbsp;edit
                               <input name="canDelete1" type="checkbox" value="1" />&nbsp;&nbsp;delete
                               <input name="canView1" type="checkbox" value="1" />&nbsp;&nbsp;view
                               <input name="canPrint1" type="checkbox" value="1" />&nbsp;&nbsp;print
                               <input name="canExport1" type="checkbox" value="1" />&nbsp;&nbsp;export
                               <input name="canApprove1" type="checkbox" value="1" />&nbsp;&nbsp;Approve
                               <div class="clear" style="height:20px"></div>
                           </div>
                        </div>
                        <div class="col-md-9 col-sm-12 col-xs-12">
                            <button class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;Add</button>
                            &nbsp;
                            <a href="/user-group/userManagement/umUserGroup">
                              <button type="button" class="btn btn-default">Cancel</button>
                            </a>
                            <div class="clear" style="height:20px"></div>
                        </div>
                   </form>
                     @if(!is_null($permission->count()))
                        <div class="col-md-12">
                          <h3><b style="color:#046b99">{{$groupLabel->groupName}}</b> permisson settings</h3>
                            {{treeView::treeTableShow($groupId)}}
                           <div class="clear" style="height:35px"></div>
                        </div>
                     @endif
                    </div>
            </div>
        </div>
      </div>
     </div>
</div>
<script>
      $(document).ready(function() {
        $('form').each(function () {
           $(this).validate();
        });
      });
</script>
@endsection
