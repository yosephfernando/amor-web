<h3>Search result of '{{$keyword}}'</h3>
<table id="beforeDelete" class="table table-striped table-bordered table-hover">
  <div class="clear" style="height:10px"></div>
    <thead>
    <tr>
      <th>#</th>
      <th>Module method name</th>
      <th>Add</th>
      <th>Edit</th>
      <th>Delete</th>
      <th>View</th>
      <th>Print</th>
      <th>Export</th>
      <th>Approve</th>
      <th>Action</th>
    </tr>
    </thead>
    <tbody>
      <?php $i = 1;?>
      @foreach($permission as $data)
              <h4 id="message{{$data->permissionId}}" style="display:none">Data updated</h4>
              <h4 id="error{{$data->permissionId}}" style="display:none">Error updating</h4>
              <tr>
                  {{csrf_field()}}
                  <input type="hidden" id="idPermission{{$data->permissionId}}" value="{{$data->permissionId}}">
                  <td class="permissionId">{{$i}}</td>
                  <td>
                    @if($data->moduleParent == null)
                      <b>{{$data->moduleName}}</b>
                    @else
                      {{$data->moduleMethod}}
                    @endif
                  </td>
                  <td>
                      @if($data->canAdd == 1)
                          <input id="canAdd{{$data->permissionId}}" type="checkbox" name="canAdd" value="1" checked />
                      @else
                          <input id="canAdd{{$data->permissionId}}" type="checkbox" name="canAdd" value="1" />
                      @endif
                  </td>
                  <td>
                      @if($data->canEdit == 1)
                          <input id="canEdit{{$data->permissionId}}" type="checkbox" name="canEdit" value="1" checked />
                      @else
                          <input id="canEdit{{$data->permissionId}}" type="checkbox" name="canEdit" value="1" />
                      @endif
                  </td>
                  <td>
                      @if($data->canDelete == 1)
                          <input id="canDelete{{$data->permissionId}}" type="checkbox" name="canDelete" value="1" checked />
                      @else
                          <input id="canDelete{{$data->permissionId}}" type="checkbox" name="canDelete" value="1" />
                      @endif
                  </td>
                  <td>
                      @if($data->canView == 1)
                          <input id="canView{{$data->permissionId}}" type="checkbox" name="canView" value="1" checked />
                      @else
                          <input id="canView{{$data->permissionId}}" type="checkbox" name="canView" value="1" />
                      @endif
                  </td>
                  <td>
                      @if($data->canPrint == 1)
                          <input id="canPrint{{$data->permissionId}}" type="checkbox" name="canPrint" value="1" checked />
                      @else
                          <input id="canPrint{{$data->permissionId}}" type="checkbox" name="canPrint" value="1" />
                      @endif
                  </td>
                  <td>
                      @if($data->canExport == 1)
                          <input id="canExport{{$data->permissionId}}" type="checkbox" name="canExport" value="1" checked />
                      @else
                          <input id="canExport{{$data->permissionId}}" type="checkbox" name="canExport" value="1" />
                      @endif
                  </td>
                  <td>
                      @if($data->canApprove == 1)
                          <input id="canApprove{{$data->permissionId}}" type="checkbox" name="canApprove" value="1" checked />
                      @else
                          <input id="canApprove{{$data->permissionId}}" type="checkbox" name="canApprove" value="1" />
                      @endif
                  </td>
                  <td>
                    <button id="submit{{$data->permissionId}}" class="btn btn-warning btn-xs">Update</button>
                  </td>
              </tr>
              <!-- ajax post data -->
                 <script>
                       $("#message{{$data->permissionId}}").hide();
                       $("#error{{$data->permissionId}}").hide();
                       $("#submit{{$data->permissionId}}").click(function(){
                         var id = $("#idPermission{{$data->permissionId}}").val();

                         if($("#canAdd{{$data->permissionId}}").is(":checked")){
                            var canAdd = 1;
                         }else{
                           var canAdd = 0;
                         }

                         if($("#canEdit{{$data->permissionId}}").is(":checked")){
                            var canEdit = 1;
                         }else{
                           var canEdit = 0;
                         }

                         if($("#canView{{$data->permissionId}}").is(":checked")){
                           var canView = 1;
                         }else{
                           var canView = 0;
                         }

                         if($("#canDelete{{$data->permissionId}}").is(":checked")){
                           var canDelete = 1;
                         }else{
                           var canDelete = 0;
                         }

                         if($("#canPrint{{$data->permissionId}}").is(":checked")){
                           var canPrint = 1;
                         }else{
                           var canPrint = 0;
                         }

                         if($("#canExport{{$data->permissionId}}").is(":checked")){
                           var canExport = 1;
                         }else{
                           var canExport = 0;
                         }

                         if($("#canApprove{{$data->permissionId}}").is(":checked")){
                           var canApprove = 1;
                         }else{
                           var canApprove = 0;
                         }

                         if(id != ""){
                           $.post('/update-permission/userManagement/updatePermission', {'id' : id, 'canAdd' : canAdd, 'canEdit' : canEdit, 'canDelete' : canDelete, 'canView' : canView, 'canPrint' : canPrint, 'canExport' : canExport, 'canApprove' : canApprove, '_token' : '{{csrf_token()}}' })
                           .done(function(data, status, error){
                               $("#message{{$data->permissionId}}").show();
                            })
                            .error(function(xhr, status, error){
                               $("#error{{$data->permissionId}}").show();
                            });
                         }else{
                           console.log("failed");
                         }
                       });
                 </script>
              <!-- end of ajax post data -->
            <?php $i++ ?>
            @endforeach
  </tbody>
</table>
