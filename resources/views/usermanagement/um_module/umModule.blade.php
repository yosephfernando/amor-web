@extends('layout.layout')
@section('content')
<!--BEGIN TITLE & BREADCRUMB PAGE-->
            <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                <div class="page-header pull-left">
                    <div class="page-title">{{$title->moduleLabel}}</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a href="index.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li><a href="#">User management</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li class="active">{{$title->moduleLabel}}</li>
                </ol>
                <div class="clearfix"></div>
            </div>
            <!--END TITLE & BREADCRUMB PAGE-->
           <div class="page-content">
              <div class="row">
                <div class="col-md-12">
                <div class="panel panel-green">
                        <div class="panel-body">
                          @if($errors->any())
                              <script>
                                     toastr.error("{{$errors->first()}}");
                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                          @if(session('status'))
                              <script>
                                 @if(session('status') == 'Module added' || session('status') == 'Module updated' || session('status') == 'Module deleted')
                                   toastr.success("{{ session('status') }}");
                                 @else
                                   toastr.error("{{ session('status') }}");
                                 @endif

                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                            <div class="table-responsive">
                              <div class="col-md-7 col-sm-12 col-xs-12 row">
                                 @if($privilaged->canAdd != 0)
                                    <button type="button" class="btn btn-primary btn-md" data-target="#moduleAdd" data-toggle="modal">
                                      <i class="fa fa-plus"></i>&nbsp;
                                        Add
                                    </button>
                                 @endif
                               </div>
                             <!-- bulk action -->
                               <div class="col-md-2 col-sm-12 col-xs-12">
                                 <div class="dropdown" style="float:right">
                                     <button class="btn btn-dafault" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Bulk action <span class="caret"></span></button>
                                     <ul class="dropdown-menu" aria-labelledby="dLabel">
                                        <li><a href="#" id="deletec">Delete</a></li>
                                      </ul>
                                       <button class="btn btn-danger" id="deleteC" style="display:none">Delete checked</button>
                                       <div class="clear" style="height:10px"></div>
                                 </div>
                                 <script>
                                     $("#deletec").click(function(){
                                         $("#deleteC").fadeIn(1000);
                                         $("#beforeDelete").hide();
                                         $("#afterDelete").show();
                                     });

                                     $("#deleteC").click(function(){
                                       var allValue = [];
                                       $("input[name='moduleId[]']:checked").each( function () {
                                           allValue.push($(this).val());
                                       });
                                       $.post( "/module-delete-bulk/userManagement/moduleDeleteBulk",  {'moduleId': allValue, '_token' : '{{csrf_token()}}' })
                                         .done(function( data ) {
                                           if(data == "success"){
                                              location.reload();
                                           }else{
                                              alert(data);
                                           }
                                           console.log( "Data Loaded: " + data );
                                         });
                                     });
                                 </script>
                                <div class="clear" style="height:10px"></div>
                               </div>
                              <!-- end of bulk action -->

                              <!-- search -->
                                 <div class="col-md-3 nopadding">
                                     <div class="col-md-12 nopadding" style="float:right">
                                         <div class="input-group">
                                             <input id="searchKeyword" type="text" class="form-control" placeholder="Search">
                                             <span id="searchGroup" style="cursor:pointer" class="input-group-addon"><i class="fa fa-search"></i></span>
                                         </div>
                                       <div class="clear" style="height:15px"></div>
                                     </div>
                                 </div>

                                 <script>
                                     $("#searchGroup").click(function(){
                                         var keyword = $("#searchKeyword").val();
                                         if(keyword != ""){
                                           $.get( "/ajax-module-search/userManagement/umAjaxSearchModule/"+keyword)
                                               .done(function( data ) {
                                                   $("#ajaxTableSearch").empty();
                                                   $("#ajaxTableSearch").html(data);
                                               });
                                         }else{
                                           alert('keyword empty');
                                         }
                                     });
                                 </script>
                              <!-- /search -->
                              <div id="ajaxTableSearch">
                                <table id="beforeDelete" class="table table-striped table-bordered table-hover">
                                  <div class="clear" style="height:10px"></div>
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Method</th>
                                        <th>View</th>
                                        <th>Parent</th>
                                        <th>Show in sidebar</th>
                                        <th>Order</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      <?php $i = $modulesData->firstItem(); ?>
                                      @foreach($modulesData as $data)
                                              <tr>
                                                  <td>{{$i}}</td>
                                                  <td>{{$data->moduleName}}</td>
                                                  <td>{{$data->moduleMethod}}</td>
                                                  <td>{{$data->moduleView}}</td>
                                                  <td>{{$data->moduleParent}}</td>
                                                  <td>
                                                    @if($data->moduleShowInSidebar == 1)
                                                      Yes
                                                    @else
                                                      No
                                                    @endif
                                                  </td>
                                                  <td>{{$data->moduleOrder}}</td>
                                                  <td style="text-align:center">
                                                   @if($privilaged->canEdit != 0)
                                                          <button type="button" class="btn btn-default btn-xs" data-target="#user{{$data->moduleId}}" data-toggle="modal" >
                                                            <i class="fa fa-pencil"></i>&nbsp;
                                                              Edit
                                                          </button>
                                                  @endif
                                                  @if($privilaged->canDelete != 0)
                                                        <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->moduleId}}" data-toggle="modal">
                                                          <i class="fa fa-trash"></i>&nbsp;
                                                            Delete
                                                        </button>
                                                  @endif
                                                  </td>
                                              </tr>
                                               <!--Modal delete-->
                                                <div id="delete{{$data->moduleId}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
                                                    <div class="modal-dialog">
                                                      <form role="form" method="post" action="/delete-module/userManagement/moduleDelete">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="moduleId" value="{{$data->moduleId}}">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                                                                <h4 id="modal-responsive-label" class="modal-title">Delete module {{$data->moduleName}} {{$data->moduleMethod}}</h4></div>
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                      <h3 style="text-align:center">Are you sure want to delete this module ?</h3>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                                                                <button type="submit" class="btn btn-primary">Yes</button>
                                                            </div>
                                                        </div>
                                                      </form>
                                                    </div>
                                                </div>
                                            <?php $i++ ?>
                                       @endforeach
                                    </tbody>
                                </table>
                            @foreach($modulesData as $data)
                                <!--Modal edit-->
                                 <div id="user{{$data->moduleId}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
                                     <div class="modal-dialog">
                                       <form role="form" method="post" action="/edit-module/userManagement/editModule">
                                         {{csrf_field()}}
                                         <input type="hidden" name="moduleId" value="{{$data->moduleId}}">
                                         <div class="modal-content">
                                             <div class="modal-header">
                                                 <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                                                 <h4 id="modal-responsive-label" class="modal-title">Edit module {{$data->moduleName}} {{$data->moduleMethod}}</h4></div>
                                             <div class="modal-body">
                                                 <div class="row">
                                                     <div class="col-md-12">
                                                             <div class="mbm">
                                                                <label>Module name :</label>
                                                                <input type="text" name="moduleName" class="form-control required" value="{{$data->moduleName}}" placeholder="Please type module name" />
                                                             </div>
                                                             <div class="mbm">
                                                                <label>Module view :</label>
                                                                <input id="moduleView{{$data->moduleId}}" type="text" name="moduleView" class="form-control" value="{{$data->moduleView}}" placeholder="Optional"/>
                                                             </div>
                                                             <div class="mbm">
                                                                <label>Module method :</label>
                                                                <input id="moduleMethod{{$data->moduleId}}" type="text" name="moduleMethod" class="form-control" value="{{$data->moduleMethod}}" placeholder="Optional"/>
                                                             </div>
                                                             <div class="mbm">
                                                                <label>Module link :</label>
                                                                <input id="moduleLink{{$data->moduleId}}" type="text" name="moduleLink" class="form-control" value="{{$data->moduleLink}}" placeholder="Optional"/>
                                                             </div>
                                                             <div class="mbm">
                                                                <label>Module params :</label>
                                                                <input type="text" name="moduleParams" class="form-control" value="{{$data->moduleParams}}" placeholder="Optional"/>
                                                             </div>
                                                             <div class="mbm">
                                                                <label>Module label :</label>
                                                                <input type="text" name="moduleLabel" class="form-control" value="{{$data->moduleLabel}}" placeholder="Optional"/>
                                                             </div>
                                                             <div class="mbm">
                                                                <label>Module parent :</label>
                                                                <input type="text" name="moduleParent" class="form-control" value="{{$data->moduleParent}}"  id="autocomplete{{$data->moduleId}}" placeholder="Optional"/>
                                                                <div class="mbm" id="selection{{$data->moduleId}}"></div>
                                                                <script>
                                                                      var parent = [{!! $modulesAllP !!}];
                                                                      var method = [{!! $modulesAllA !!}];
                                                                      var parent = $.map(parent, function (items) { return { value: items, data: { category: 'parent' }}; });
                                                                      var method = $.map(method, function (items) { return { value: items, data: { category: 'method' } }; });
                                                                      var items = parent.concat(method);

                                                                      // Initialize autocomplete with local lookup:
                                                                      $('#autocomplete{{$data->moduleId}}').devbridgeAutocomplete({
                                                                          lookup: items,
                                                                          minChars: 1,
                                                                          onSelect: function (suggestion) {
                                                                              $('#selection{{$data->moduleId}}').html(suggestion.value + ', ' + suggestion.data.category);
                                                                          },
                                                                          showNoSuggestionNotice: true,
                                                                          noSuggestionNotice: 'Sorry, no matching results',
                                                                          groupBy: 'category'
                                                                      });
                                                                </script>
                                                             </div>
                                                             <div class="mbm">
                                                               @if($data->moduleShowInSidebar == 1)
                                                                  <input type="checkbox" name="moduleShowInSidebar" value="1" checked/>
                                                               @else
                                                                  <input type="checkbox" name="moduleShowInSidebar" value="1"/>
                                                               @endif
                                                                  Show in sidebar (optional)
                                                             </div>
                                                             <div class="mbm">
                                                                <label>Order :</label>
                                                                <input class="form-control" type="number" name="moduleOrder" value="{{$data->moduleOrder}}" placeholder="Optional"/>
                                                             </div>
                                                             <div class="mbm">
                                                                <label>Group :</label>
                                                                <input class="form-control" type="text" name="moduleGroup" value="{{$data->moduleGroup}}" placeholder="Optional"/>
                                                             </div>
                                                             <div class="mbm">
                                                                <label>Module icon(font-awesome) :</label>
                                                                <input class="form-control" type="text" name="moduleIcon" value="{{$data->moduleIcon}}" placeholder="Optional"/>
                                                             </div>
                                                             <div class="mbm">
                                                                <label>Module http method :</label>
                                                                <select name="moduleHttpMethod" class="form-control required">
                                                                    <option>Please choose one of the method</option>
                                                                  @if($data->moduleHttpMethod == 'get')
                                                                      <option value="get" selected>get</option>
                                                                      <option value="post">post</option>
                                                                  @else
                                                                      <option value="get">get</option>
                                                                      <option value="post" selected>post</option>
                                                                  @endif

                                                                </select>
                                                             </div>
                                                      </div>
                                                 </div>
                                             </div>
                                             <div class="modal-footer">
                                                 <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                                                 <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
                                             </div>
                                         </div>
                                       </form>
                                     </div>
                                 </div>
                              @endforeach

                                <table id="afterDelete" class="table table-striped table-bordered table-hover" style="display:none">
                                  <div class="clear" style="height:10px"></div>
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Method</th>
                                        <th>View</th>
                                        <th>Parent</th>
                                        <th>Show in sidebar</th>
                                        <th>Order</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($modulesData as $data)
                                              <tr>
                                                  <td><input type="checkbox" name="moduleId[]" value="{{$data->moduleId}}" /></td>
                                                  <td>{{$data->moduleMethod}}</td>
                                                  <td>{{$data->moduleView}}</td>
                                                  <td>{{$data->moduleParent}}</td>
                                                  <td>{{$data->moduleShowInSidebar}}</td>
                                                  <td>{{$data->moduleOrder}}</td>
                                              </tr>
                                       @endforeach
                                    </tbody>
                                </table>
                                {{$modulesData->links()}}
                              </div>
                            </div>
                        </div>
                    </div>
                  </div>
              </div>
          </div>

          <!--Modal add user-->
           <div id="moduleAdd" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
               <div class="modal-dialog">
                 <form role="form" method="post" action="/add-module/userManagement/addModule">
                    {{csrf_field()}}
                   <div class="modal-content">
                       <div class="modal-header">
                           <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                           <h4 id="modal-responsive-label" class="modal-title">Add module</h4></div>
                       <div class="modal-body">
                           <div class="row">
                               <div class="col-md-12">
                                       <div class="mbm">
                                          <label>Module name :</label>
                                          <input type="text" name="moduleName" class="form-control required" placeholder="Please type module name"/>
                                       </div>
                                       <div class="mbm">
                                          <label>Module view :</label>
                                          <input type="text" id="moduleView" name="moduleView" class="form-control" placeholder="Optional"/>
                                       </div>
                                       <div class="mbm">
                                          <label>Module method :</label>
                                          <input type="text" id="moduleMethod" name="moduleMethod" class="form-control" placeholder="Optional"/>
                                       </div>
                                       <div class="mbm">
                                          <label>Module link :</label>
                                          <input type="text" id="moduleLink" name="moduleLink" class="form-control" placeholder="Optional" />
                                       </div>
                                       <div class="mbm">
                                          <label>Module params :</label>
                                          <input type="text" name="moduleParams" class="form-control" placeholder="Optional" />
                                       </div>
                                       <div class="mbm">
                                          <label>Module label :</label>
                                          <input type="text" name="moduleLabel" class="form-control" placeholder="Optional" />
                                       </div>
                                       <div class="mbm">
                                          <label>Module parent :</label>
                                          <input type="text"  name="moduleParent" class="form-control"  id="autocompleteAdd" placeholder="Optional"/>
                                          <div class="mbm" id="selectionAdd"></div>
                                          <script>
                                                var parent = [{!! $modulesAllP !!}];
                                                var method = [{!! $modulesAllA !!}];
                                                var parent = $.map(parent, function (items) { return { value: items, data: { category: 'parent' }}; });
                                                var method = $.map(method, function (items) { return { value: items, data: { category: 'method' }}; });
                                                var items = parent.concat(method);

                                                // Initialize autocomplete with local lookup:
                                                $('#autocompleteAdd').devbridgeAutocomplete({
                                                    lookup: items,
                                                    minChars: 1,
                                                    onSelect: function (suggestion) {
                                                        $('#selectionAdd').html(suggestion.value + ', ' + suggestion.data.category);
                                                    },
                                                    showNoSuggestionNotice: true,
                                                    noSuggestionNotice: 'Sorry, no matching results',
                                                    groupBy: 'category'
                                                });
                                          </script>
                                       </div>
                                       <div class="mbm">
                                          <input type="checkbox" name="moduleShowInSidebar" value="1"/>
                                            Show in sidebar (optional)
                                       </div>
                                       <div class="mbm">
                                         <label>Order :</label>
                                          <input class="form-control" type="number" name="moduleOrder" value="0" placeholder="Optional"/>
                                       </div>
                                       <div class="mbm">
                                         <label>Group :</label>
                                          <input class="form-control" type="text" name="moduleGroup" value="" placeholder="Optional"/>
                                       </div>
                                       <div class="mbm">
                                          <label>Module icon( font-awesome ) :</label>
                                          <input class="form-control" type="text" name="moduleIcon" value="" placeholder="Optional"/>
                                       </div>
                                       <div class="mbm">
                                          <label>Module http method :</label>
                                          <select name="moduleHttpMethod" class="form-control required">
                                              <option value="get">get</option>
                                              <option value="post">post</option>
                                          </select>
                                       </div>
                           </div>
                       </div>
                       <div class="modal-footer">
                           <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                           <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                       </div>
                   </div>
                  </form>
               </div>
           </div>
           <script>
                 $(document).ready(function() {
                   $('form').each(function () {
                      $(this).validate();
                   });
                 });
           </script>
@endsection
