<h3>Search result of '{{$keyword}}'</h3>
<table id="beforeDelete" class="table table-striped table-bordered table-hover">
  <div class="clear" style="height:10px"></div>
    <thead>
    <tr>
      <th>#</th>
      <th>Name</th>
      <th>Method</th>
      <th>View</th>
      <th>Parent</th>
      <th>Show in sidebar</th>
      <th>Order</th>
      <th>Action</th>
    </tr>
    </thead>
    <tbody>
      <?php $i = 1;?>
      @foreach($modulesData as $data)
              <tr>
                <td>{{$i}}</td>
                <td>{{$data->moduleName}}</td>
                <td>{{$data->moduleMethod}}</td>
                <td>{{$data->moduleView}}</td>
                <td>{{$data->moduleParent}}</td>
                <td>
                  @if($data->moduleShowInSidebar == 1)
                    Yes
                  @else
                    No
                  @endif
                </td>
                <td>{{$data->moduleOrder}}</td>
                <td style="text-align:center">
                 @if($privilaged->canEdit != 0)
                        <button type="button" class="btn btn-default btn-xs" data-target="#userAjax{{$data->moduleId}}" data-toggle="modal" >
                          <i class="fa fa-pencil"></i>&nbsp;
                            Edit
                        </button>
                @endif
                @if($privilaged->canDelete != 0)
                      <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->moduleId}}" data-toggle="modal">
                        <i class="fa fa-trash"></i>&nbsp;
                          Delete
                      </button>
                @endif
                </td>
              </tr>
               <!--Modal delete-->
                <div id="delete{{$data->moduleId}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
                    <div class="modal-dialog">
                        <input type="hidden" id="moduleId{{$data->moduleId}}" value="{{$data->moduleId}}">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                                <h4 id="modal-responsive-label" class="modal-title">Delete module {{$data->moduleName}}</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                      <h3 id="messageDelete{{$data->moduleId}}" style="text-align:center">Are you sure want to delete this module ?</h3>
                                </div>
                            </div>
                            <div id="beforeDel{{$data->moduleId}}" class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                                <button id="deleteSubmit{{$data->moduleId}}" type="submit" class="btn btn-primary">Yes</button>
                            </div>
                            <div id="afterDel{{$data->moduleId}}" class="modal-footer" style="display:none">
                                <button type="button" data-dismiss="modal" class="btn btn-default">Ok</button>
                            </div>
                        </div>

                      <!-- ajax post data -->
                         <script>
                               $("#deleteSubmit{{$data->moduleId}}").click(function(){
                                 var id = $("#idModule{{$data->moduleId}}").val();
                                 if(id != ""){
                                   $.post('/delete-module/userManagement/moduleDelete', {'moduleId' : id, '_token' : '{{csrf_token()}}' })
                                   .done(function(data){
                                       $("#beforeDel{{$data->moduleId}}").hide();
                                       $("#afterDel{{$data->moduleId}}").show();
                                       $("#messageDelete{{$data->moduleId}}").text('Data deleted');
                                    })
                                    .error(function(xhr, status, error){
                                       $("#beforeDel{{$data->moduleId}}").hide();
                                       $("#afterDel{{$data->moduleId}}").show();
                                       $("#messageDelete{{$data->moduleId}}").text('Error deleting');
                                    });
                                 }else{
                                   console.log("failed");
                                 }
                               });

                               $("#userAjax{{$data->moduleId}}").on('hidden.bs.modal', function(e){
                                 $("#messageDelete{{$data->moduleId}}").text('Are you sure want to delete this module ?');
                                 $("#beforeDel{{$data->moduleId}}").show();
                                 $("#afterDel{{$data->moduleId}}").hide();
                               });
                         </script>
                      <!-- end of ajax post data -->

                    </div>
                </div>
            <?php $i++ ?>
       @endforeach
    </tbody>
</table>
@foreach($modulesData as $data)
<!--Modal edit-->
 <div id="userAjax{{$data->moduleId}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
     <div class="modal-dialog">
       <form>
         <input type="hidden" id="idModule{{$data->moduleId}}" value="{{$data->moduleId}}" />
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                 <h4 id="modal-responsive-label" class="modal-title">Edit module {{$data->moduleName}}</h4>
             </div>
             <div class="modal-body">
                 <div class="row">
                     <div class="col-md-12">
                         <h4 id="message{{$data->moduleId}}" style="display:none">Data updated</h4>
                         <h4 id="error{{$data->moduleId}}" style="display:none">Error updating</h4>
                         <div class="mbm">
                            <label>Module name :</label>
                            <input type="text" id="moduleName{{$data->moduleId}}" class="form-control required" value="{{$data->moduleName}}" placeholder="Please type module name" />
                         </div>
                         <div class="mbm">
                            <label>Module view :</label>
                            <input id="moduleView{{$data->moduleId}}" type="text" class="form-control" value="{{$data->moduleView}}" placeholder="Optional"/>
                         </div>
                         <div class="mbm">
                            <label>Module method :</label>
                            <input id="moduleMethod{{$data->moduleId}}" type="text" class="form-control" value="{{$data->moduleMethod}}" placeholder="Optional"/>
                         </div>
                         <div class="mbm">
                            <label>Module link :</label>
                            <input id="moduleLink{{$data->moduleId}}" type="text" class="form-control" value="{{$data->moduleLink}}" placeholder="Optional"/>
                         </div>
                         <div class="mbm">
                            <label>Module params :</label>
                            <input type="text" id="moduleParams{{$data->moduleId}}" class="form-control" value="{{$data->moduleParams}}" placeholder="Optional" />
                         </div>
                         <div class="mbm">
                            <label>Module label :</label>
                            <input type="text" id="moduleLabel{{$data->moduleId}}" class="form-control" value="{{$data->moduleLabel}}" placeholder="Optional" />
                         </div>
                         <div class="mbm">
                            <label>Module parent :</label>
                            <input type="text" id="moduleParent{{$data->moduleId}}" class="form-control" value="{{$data->moduleParent}}"  id="autocomplete{{$data->moduleId}}" placeholder="Optional"/>
                            <div class="mbm" id="selection{{$data->moduleId}}"></div>
                            <script>
                                  var parent = [{!! $modulesAllP !!}];
                                  var method = [{!! $modulesAllA !!}];
                                  var parent = $.map(parent, function (items) { return { value: items, data: { category: 'parent' }}; });
                                  var method = $.map(method, function (items) { return { value: items, data: { category: 'method' } }; });
                                  var items = parent.concat(method);

                                  // Initialize autocomplete with local lookup:
                                  $('#autocomplete{{$data->moduleId}}').devbridgeAutocomplete({
                                      lookup: items,
                                      minChars: 1,
                                      onSelect: function (suggestion) {
                                          $('#selection{{$data->moduleId}}').html(suggestion.value + ', ' + suggestion.data.category);
                                      },
                                      showNoSuggestionNotice: true,
                                      noSuggestionNotice: 'Sorry, no matching results',
                                      groupBy: 'category'
                                  });
                            </script>
                         </div>
                         <div class="mbm">
                           @if($data->moduleShowInSidebar == 1)
                              <input type="checkbox" id="moduleShowInSidebar{{$data->moduleId}}" value="1" checked/>
                           @else
                              <input type="checkbox" id="moduleShowInSidebar{{$data->moduleId}}" value="0"/>
                           @endif
                              Show in sidebar (optional)
                         </div>
                         <div class="mbm">
                            <label>Order :</label>
                            <input class="form-control" type="number" id="moduleOrder{{$data->moduleId}}" value="{{$data->moduleOrder}}" placeholder="Optional"/>
                         </div>
                         <div class="mbm">
                            <label>Group :</label>
                            <input class="form-control" type="text" id="moduleGroup{{$data->moduleId}}" value="{{$data->moduleGroup}}" placeholder="Optional"/>
                         </div>
                         <div class="mbm">
                            <label>Module icon( forn-awesome ) :</label>
                            <input class="form-control" type="text" id="moduleIcon{{$data->moduleId}}" value="{{$data->moduleIcon}}" placeholder="Optional"/>
                         </div>
                         <div class="mbm">
                            <label>Module http method :</label>
                            <select id="moduleHttpMethod{{$data->moduleId}}" class="form-control required">
                                  <option>Please choose one of the method</option>
                              @if($data->moduleHttpMethod == 'get')
                                  <option value="get" selected>get</option>
                                  <option value="post">post</option>
                              @else
                                  <option value="get">get</option>
                                  <option value="post" selected>post</option>
                              @endif

                            </select>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                 <button id="submit{{$data->moduleId}}" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
             </div>
         </div>

         <!-- ajax post data -->
            <script>
                  $("#message{{$data->moduleId}}").hide();
                  $("#error{{$data->moduleId}}").hide();
                  $("#submit{{$data->moduleId}}").click(function(){
                    var id = $("#idModule{{$data->moduleId}}").val();
                    var moduleName = $("#moduleName{{$data->moduleId}}").val();
                    var moduleView = $("#moduleView{{$data->moduleId}}").val();
                    var moduleMethod = $("#moduleMethod{{$data->moduleId}}").val();
                    var moduleLink = $("#moduleLink{{$data->moduleId}}").val();
                    var moduleParams = $("#moduleParams{{$data->moduleId}}").val();
                    var moduleLabel = $("#moduleLabel{{$data->moduleId}}").val();
                    var moduleParent = $("#moduleParent{{$data->moduleId}}").val();

                    if($("#moduleShowInSidebar{{$data->moduleId}}:checked").length == 1){
                      var moduleShowInSidebar = 1;
                    }else{
                      var moduleShowInSidebar = 0;
                    }

                    var moduleOrder = $("#moduleOrder{{$data->moduleId}}").val();
                    var moduleGroup = $("#moduleGroup{{$data->moduleId}}").val();
                    var moduleIcon = $("#moduleIcon{{$data->moduleId}}").val();
                    var moduleHttpMethod = $("#moduleHttpMethod{{$data->moduleId}}").val();

                    if(id != "" || moduleName != "" || moduleView != "" || moduleMethod != "" || moduleLink != "" || moduleHttpMethod != ""){
                        $.post('/edit-module/userManagement/editModule', {'moduleIcon' : moduleIcon, 'moduleGroup' : moduleGroup, 'moduleId' : id, 'moduleName' : moduleName, 'moduleView' : moduleView, 'moduleMethod' : moduleMethod, 'moduleLink' : moduleLink, 'moduleParams' : moduleParams, 'moduleLabel' : moduleLabel, 'moduleParent' : moduleParent, 'moduleShowInSidebar' : moduleShowInSidebar, 'moduleOrder' : moduleOrder, 'moduleHttpMethod' : moduleHttpMethod,  '_token' : '{{csrf_token()}}' })
                            .done(function(data){
                              console.log(moduleGroup);
                                $("#message{{$data->moduleId}}").show();
                             })
                             .error(function(xhr, status, error){
                                $("#error{{$data->moduleId}}").show();
                             });
                    }else{
                       console.log('failed');
                    }

                });
            </script>
         <!-- end of ajax post data -->
       </form>
     </div>
 </div>
 @endforeach
<script>
      $(document).ready(function() {
        $('form').each(function () {
           $(this).validate();
        });
      });
</script>
