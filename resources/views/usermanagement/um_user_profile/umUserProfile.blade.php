@extends('layout.layout')
@section('content')
<!--BEGIN TITLE & BREADCRUMB PAGE-->
            <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                <div class="page-header pull-left">
                    <div class="page-title">{{$title->moduleLabel}}</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a href="index.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li><a href="#">User management</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li class="active">{{$title->moduleLabel}}</li>
                </ol>
                <div class="clearfix"></div>
            </div>
            <!--END TITLE & BREADCRUMB PAGE-->
           <div class="page-content">
              <div class="row">
                <div class="col-md-12">
                <div class="panel panel-green">
                        <div class="panel-body">
                          @if($errors->any())
                              <script>
                                     toastr.error("{{$errors->first()}}");
                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                          @if(session('status'))
                              <script>
                                 @if(session('status') == 'Profile added' || session('status') == 'Profile updated' || session('status') == 'Profile deleted' || session('status') == 'Password changed')
                                   toastr.success("{{ session('status') }}");
                                 @else
                                   toastr.error("{{ session('status') }}");
                                 @endif

                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                            <div class="col-md-6">
                              @if($userData->profpic == "")
                                  <div class="dropzone" id="dropZone">
                                    <div class="dz-message" data-dz-message>
                                        <h3 class="text-center">Drop picture or click here!</h3>
                                    </div>
                                  </div>
                              @else
                                  <div class="dropzone" id="dropZone">
                                    <div class="dz-message" data-dz-message>
                                        <img src="/get-image/userManagement/get-image-user/{{Auth::user()->id}}" class="img-responsive center-block"/>
                                        <h3 class="text-center">Drop picture or click here!</h3>
                                    </div>
                                  </div>

                              @endif
                                  <div class="clear" style="height:10px"></div>
                                  <b>{{$userData->groupName}}</b>
                            </div>
                            <div class="col-md-6">
                                <form class="form-group" action="/edit-profile/userManagement/profile-edit" method="post" >
                                    {{csrf_field()}}
                                    <input type="hidden" name="id" value="{{$userData->id}}" />
                                    <label>Name</label>
                                    <input type="text" name="name" value="{{$userData->name}}" class="form-control"/>
                                    <label>Email</label>
                                    <input type="email" name="email" value="{{$userData->email}}" class="form-control"/>
                                    <div class="clear" style="height:10px"></div>
                                      <button class="btn btn-default" data-toggle="modal" data-target="#cP"><i class="fa fa-pencil"></i>&nbsp;Change password</button>
                                    <button type="submit" class="btn btn-info"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                                    <div class="clear" style="height:20px"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                  </div>
              </div>
          </div>

          <!--Modal add user-->
           <div id="cP" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
               <div class="modal-dialog">
                 <form role="form" method="post" action="/change-password/userManagement/password-change" autocomplete="on">
                    {{csrf_field()}}
                   <input type="hidden" name="id" value="{{$userData->id}}" />
                   <div class="modal-content">
                       <div class="modal-header">
                           <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                           <h4 id="modal-responsive-label" class="modal-title">Change password</h4></div>
                       <div class="modal-body">
                           <div class="row">
                               <div class="col-md-12">
                                       <div class="mbm">
                                          <label>Old password :</label>
                                          <input type="password" name="ol_password" class="form-control" required />
                                       </div>
                                       <div class="mbm">
                                          <label>New password :</label>
                                          <input type="password" name="new_password" class="form-control" required />
                                       </div>
                               </div>
                           </div>
                       </div>
                       <div class="modal-footer">
                           <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                           <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                       </div>
                   </div>
                  </form>
               </div>
           </div>

          <script>
              Dropzone.autoDiscover = false;
              $("div#dropZone").dropzone(
                {
                    url: "/post/userManagement/picture",
                    paramName: "picture",
                    maxFiles: 1,
                    headers:{'X-CSRF-Token': '{{csrf_token()}}'}
                }
              );
          </script>
@endsection
