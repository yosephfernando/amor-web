<h3>Search result of '{{$keyword}}'</h3>
<table id="beforeDelete" class="table table-striped table-bordered table-hover">
  <div class="clear" style="height:10px"></div>
    <thead>
    <tr>
        <th>#</th>
        <th>User group name</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
      <?php $i = 1;?>
      @foreach($userGroupData as $data)
              <tr>
                  <td>{{$i}}</td>
                  <td>{{$data->groupName}}</td>
                  <td style="text-align:center">
                    <a href="/permission-add/userManagement/umAddPermission/{{$data->userGroupId}}">
                      <button type="button" class="btn btn-primary btn-xs">
                        <i class="fa fa-plus"></i>&nbsp;
                        Add permission
                      </button>
                    </a>
                   @if($privilaged->canEdit != 0)
                          <button type="button" class="btn btn-default btn-xs" data-target="#userGroupAjax{{$data->userGroupId}}" data-toggle="modal" >
                            <i class="fa fa-pencil"></i>&nbsp;
                              Edit
                          </button>
                  @endif
                  @if($privilaged->canDelete != 0)
                        <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->userGroupId}}" data-toggle="modal">
                          <i class="fa fa-trash"></i>&nbsp;
                            Delete
                        </button>
                  @endif
                  </td>
              </tr>
               <!--Modal delete-->
                <div id="delete{{$data->userGroupId}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
                    <div class="modal-dialog">
                        <input type="hidden" id="userGroupId{{$data->userGroupId}}" value="{{$data->userGroupId}}">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                                <h4 id="modal-responsive-label" class="modal-title">Delete group {{$data->groupName}}</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                      <h3 id="messageDelete{{$data->userGroupId}}" style="text-align:center">Are you sure want to delete this group ?</h3>
                                </div>
                            </div>
                            <div id="beforeDel{{$data->userGroupId}}" class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                                <button id="deleteSubmit{{$data->userGroupId}}" type="submit" class="btn btn-primary">Yes</button>
                            </div>
                            <div id="afterDel{{$data->userGroupId}}" class="modal-footer" style="display:none">
                                <button type="button" data-dismiss="modal" class="btn btn-default">Ok</button>
                            </div>
                        </div>

                      <!-- ajax post data -->
                         <script>
                               $("#deleteSubmit{{$data->userGroupId}}").click(function(){
                                 var id = $("#userGroupId{{$data->userGroupId}}").val();

                                 if(id != ""){
                                   $.post('/group-delete/userManagement/delete-group', {'userGroupId' : id, '_token' : '{{csrf_token()}}' })
                                   .done(function(data){
                                       $("#beforeDel{{$data->userGroupId}}").hide();
                                       $("#afterDel{{$data->userGroupId}}").show();
                                       $("#messageDelete{{$data->userGroupId}}").text('Data deleted');
                                    })
                                    .error(function(xhr, status, error){
                                       $("#beforeDel{{$data->userGroupId}}").hide();
                                       $("#afterDel{{$data->userGroupId}}").show();
                                       $("#messageDelete{{$data->userGroupId}}").text('Error deleting');
                                    });
                                 }else{
                                   console.log("failed");
                                 }
                               });

                               $("#userGroupAjax{{$data->userGroupId}}").on('hidden.bs.modal', function(e){
                                 $("#messageDelete{{$data->userGroupId}}").text('Are you sure want to delete this group ?');
                                 $("#beforeDel{{$data->userGroupId}}").show();
                                 $("#afterDel{{$data->userGroupId}}").hide();
                               });
                         </script>
                      <!-- end of ajax post data -->

                    </div>
                </div>
            <?php $i++ ?>
       @endforeach
    </tbody>
</table>
@foreach($userGroupData as $data)
<!--Modal edit-->
 <div id="userGroupAjax{{$data->userGroupId}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
     <div class="modal-dialog">
       <form>
         <input type="hidden" id="idUsergroup{{$data->userGroupId}}" value="{{$data->userGroupId}}" />
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                 <h4 id="modal-responsive-label" class="modal-title">Edit group {{$data->groupName}}</h4>
             </div>
             <div class="modal-body">
                 <div class="row">
                     <div class="col-md-12">
                         <h4 id="message{{$data->userGroupId}}" style="display:none">Data updated</h4>
                         <h4 id="error{{$data->userGroupId}}" style="display:none">Error updating</h4>
                         <div class="mbm">
                           <label>Group name :</label>
                           <input type="text" id="groupName{{$data->userGroupId}}" value="{{$data->groupName}}" class="form-control" placeholder="Please type user group name" required/>
                         </div>
                         <div class="mbm">
                           <label>Group name :</label>
                           <input type="text" id="groupDashboard{{$data->userGroupId}}" value="{{$data->groupDashboard}}" class="form-control" placeholder="Please type user group dashboard" required/>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                 <button id="submit{{$data->userGroupId}}" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
             </div>
         </div>

         <!-- ajax post data -->
            <script>
                  $("#message{{$data->userGroupId}}").hide();
                  $("#error{{$data->userGroupId}}").hide();
                  $("#submit{{$data->userGroupId}}").click(function(){
                    var id = $("#idUsergroup{{$data->userGroupId}}").val();
                    var groupName = $("#groupName{{$data->userGroupId}}").val();
                    var groupDashboard = $("#groupDashboard{{$data->userGroupId}}").val();

                    if(groupName != "" || id != "" || groupDashboard != ""){
                      $.post('/group-edit/userManagement/edit-group', {'groupDashboard' : groupDashboard, 'groupName' : groupName, 'id' : id, '_token' : '{{csrf_token()}}' })
                      .done(function(data){
                          $("#message{{$data->userGroupId}}").show();
                       })
                       .error(function(xhr, status, error){
                          $("#error{{$data->userGroupId}}").show();
                       });
                    }else{
                      console.log("failed");
                    }
                  });

                  $("#userGroupAjax{{$data->userGroupId}}").on('hidden.bs.modal', function(e){
                    $("#message{{$data->userGroupId}}").hide();
                    $("#error{{$data->userGroupId}}").hide();
                  });
            </script>
         <!-- end of ajax post data -->
         </form>
     </div>
 </div>
 @endforeach
<script>
      $(document).ready(function() {
        $('form').each(function () {
           $(this).validate();
        });
      });
</script>
