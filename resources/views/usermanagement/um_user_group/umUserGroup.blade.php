@extends('layout.layout')
@section('content')
<!--BEGIN TITLE & BREADCRUMB PAGE-->
            <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                <div class="page-header pull-left">
                    <div class="page-title">{{$title->moduleLabel}}</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a href="index.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li><a href="#">User management</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li class="active">{{$title->moduleLabel}}</li>
                </ol>
                <div class="clearfix"></div>
            </div>
            <!--END TITLE & BREADCRUMB PAGE-->
           <div class="page-content">
              <div class="row">
                <div class="col-md-12">
                <div class="panel panel-green">
                        <div class="panel-body">
                          @if($errors->any())
                              <script>
                                     toastr.error("{{$errors->first()}}");
                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                          @if(session('status'))
                              <script>
                                 @if(session('status') == 'Group added' || session('status') == 'Group updated' || session('status') == 'Group deleted')
                                   toastr.success("{{ session('status') }}");
                                 @else
                                   toastr.error("{{ session('status') }}");
                                 @endif

                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                            <div class="table-responsive">
                               <div class="col-md-2 col-sm-12 col-xs-12 row">
                                   @if($privilaged->canAdd != 0)
                                      <button type="button" class="btn btn-primary btn-md" data-target="#userAdd" data-toggle="modal">
                                        <i class="fa fa-plus"></i>&nbsp;
                                          Add
                                      </button>
                                   @endif
                                </div>

                              <!-- bulk action -->
                                <div class="col-md-7">
                                  <div class="dropdown" style="float:right">
                                      <button class="btn btn-dafault" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Bulk action <span class="caret"></span></button>
                                      <ul class="dropdown-menu" aria-labelledby="dLabel">
                                         <li><a href="#" id="deletec">Delete</a></li>
                                       </ul>
                                        <button class="btn btn-danger" id="deleteC" style="display:none">Delete checked</button>
                                        <div class="clear" style="height:10px"></div>
                                  </div>

                                  <script>
                                      $("#deletec").click(function(){
                                          $("#deleteC").fadeIn(1000);
                                          $("#beforeDelete").hide();
                                          $("#afterDelete").show();
                                      });

                                      $("#deleteC").click(function(){
                                        var allValue = [];
                                        $("input[name='userGroupId[]']:checked").each( function () {
                                            allValue.push($(this).val());
                                        });
                                        $.post( "/delete-bulk-user-group/userManagement/deleteBulkUserGroup",  {'userGroupId': allValue, '_token' : '{{csrf_token()}}' })
                                          .done(function( data ) {
                                            if(data == "success"){
                                               location.reload();
                                            }else{
                                               alert(data);
                                            }
                                            console.log( "Data Loaded: " + data );
                                          });
                                      });
                                  </script>
                                 <div class="clear" style="height:10px"></div>
                                </div>
                               <!-- end of bulk action -->

                                <!-- search -->
                                   <div class="col-md-3 nopadding">
                                       <div class="col-md-12 nopadding" style="float:right">
                                           <div class="input-group">
                                               <input id="searchKeyword" type="text" class="form-control" placeholder="Search">
                                               <span id="searchGroup" style="cursor:pointer" class="input-group-addon"><i class="fa fa-search"></i></span>
                                           </div>
                                         <div class="clear" style="height:15px"></div>
                                       </div>
                                   </div>

                                   <script>
                                       $("#searchGroup").click(function(){
                                           var keyword = $("#searchKeyword").val();
                                           if(keyword != ""){
                                             $.get( "/ajax-usergroup-search/userManagement/umAjaxSearchUsergroup/"+keyword)
                                                 .done(function( data ) {
                                                     $("#ajaxTableSearch").empty();
                                                     $("#ajaxTableSearch").html(data);
                                                 });
                                           }else{
                                             alert('keyword empty');
                                           }
                                       });
                                   </script>
                                <!-- /search -->

                              <!-- table -->
                               <div id="ajaxTableSearch">
                                <table id="beforeDelete" class="table table-striped table-bordered table-hover">
                                  <div class="clear" style="height:10px"></div>
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>User group name</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      <?php $i = 1 ?>
                                      @foreach($userGroupData as $data)
                                              <tr>
                                                  <td>{{$i}}</td>
                                                  <td>{{$data->groupName}}</td>
                                                  <td style="text-align:center">
                                                    <a href="/permission-add/userManagement/umAddPermission/{{$data->userGroupId}}">
                                                      <button type="button" class="btn btn-primary btn-xs">
                                                       <i class="fa fa-plus"></i>&nbsp;
                                                        Add permission
                                                      </button>
                                                    </a>
                                                   @if($privilaged->canEdit != 0)
                                                          <button type="button" class="btn btn-default btn-xs" data-target="#group{{$data->userGroupId}}" data-toggle="modal" >
                                                            <i class="fa fa-pencil"></i>&nbsp;
                                                              Edit
                                                          </button>
                                                  @endif
                                                  @if($privilaged->canDelete != 0)
                                                        <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->userGroupId}}" data-toggle="modal">
                                                          <i class="fa fa-trash"></i>&nbsp;
                                                            Delete
                                                        </button>
                                                  @endif
                                                  </td>
                                              </tr>

                                               <!--Modal delete-->
                                                <div id="delete{{$data->userGroupId}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
                                                    <div class="modal-dialog">
                                                      <form role="form" method="post" action="/group-delete/userManagement/delete-group">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="userGroupId" value="{{$data->userGroupId}}">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                                                                <h4 id="modal-responsive-label" class="modal-title">Delete group {{$data->groupName}}</h4></div>
                                                            <div class="modal-body">
                                                                <div class="row">
                                                                      <h3 style="text-align:center">Are you sure want to delete this group ?</h3>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                                                                <button type="submit" class="btn btn-primary">Yes</button>
                                                            </div>
                                                        </div>
                                                      </form>
                                                    </div>
                                                </div>
                                          <?php $i++ ?>
                                       @endforeach
                                    </tbody>
                                </table>

                                <table id="afterDelete" class="table table-striped table-bordered table-hover" style="display:none">
                                  <div class="clear" style="height:10px"></div>
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>User group name</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      <?php $i = 1 ?>
                                      @foreach($userGroupData as $data)
                                              <tr>
                                                  <td><input type="checkbox" name="userGroupId[]" value="{{$data->userGroupId}}" /></td>
                                                  <td>{{$data->groupName}}</td>
                                                  </td>
                                              </tr>
                                          <?php $i++ ?>
                                       @endforeach
                                    </tbody>
                                </table>
                                {{$userGroupData->links()}}
                              </div>
                              <!-- /table -->
                            </div>
                            <div class="col-md-12 row">
                              <h3 style="margin-bottom: 20px;">Assign user to group</h3>
                                <form method="post" class="form-group" action="/assign-user/userManagement/assignUser">
                                  {{csrf_field()}}
                                    <div class="mbm row">
                                        <div class="col-md-4">
                                          <label>User :</label>
                                          <select name="user[]" multiple="multiple" class="form-control required">
                                            @foreach($usersOption as $data)
                                                <option value="{{$data->id}}">{{$data->email}}</option>
                                            @endforeach
                                          </select>
                                        </div>
                                        <div class="col-md-2">
                                          <div class="clear hidden-xs hidden-sm" style="height:46px"></div>
                                           <div class="clear visible-xs visible-sm" style="height:10px"></div>
                                            <button class="btn btn-default center-block">Assign <i class="fa fa-angle-double-right"></i></button>
                                        </div>
                                        <div class="col-md-4">
                                            <label>Group :</label>
                                            <select name="group" class="form-control required">
                                              @foreach($groupsOption as $data)
                                                  <option value="{{$data->userGroupId}}">{{$data->groupName}}</option>
                                              @endforeach
                                            </select>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                  </div>

              </div>
          </div>

          <!--Modal add user-->
           <div id="userAdd" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
               <div class="modal-dialog">
                 <form role="form" method="post" action="/group-add/userManagement/add-group">
                    {{csrf_field()}}
                   <div class="modal-content">
                       <div class="modal-header">
                           <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                           <h4 id="modal-responsive-label" class="modal-title">Add group</h4></div>
                       <div class="modal-body">
                           <div class="row">
                               <div class="col-md-12">

                                       <div class="mbm">
                                          <label>Group name :</label>
                                          <input type="text" name="groupName" class="form-control required" placeholder="Please type user group name"/>
                                       </div>
                                       <div class="mbm">
                                          <label>Group dashboard :</label>
                                          <input type="text" name="groupDashboard" class="form-control required" placeholder="Please type user group dashboard"/>
                                       </div>

                               </div>
                           </div>
                       </div>
                       <div class="modal-footer">
                           <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                           <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                       </div>
                   </div>
                  </form>
               </div>
           </div>
@foreach($userGroupData as $data)
           <!--Modal edit-->
            <div id="group{{$data->userGroupId}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
                <div class="modal-dialog">
                  <form role="form" method="post" action="/group-edit/userManagement/edit-group">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{$data->userGroupId}}">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                            <h4 id="modal-responsive-label" class="modal-title">Edit group {{$data->groupName}}</h4></div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mbm">
                                       <label>Group name :</label>
                                       <input type="text" name="groupName" value="{{$data->groupName}}" class="form-control required" placeholder="Please type user group name"/>
                                    </div>
                                    <div class="mbm">
                                       <label>Group dashboard :</label>
                                       <input type="text" name="groupDashboard" value="{{$data->groupDashboard}}" class="form-control required" placeholder="Please type user group dashboard"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                            <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
                        </div>
                    </div>
                  </form>
                </div>
            </div>
@endforeach
 <script>
       $(document).ready(function() {
         $('form').each(function () {
            $(this).validate();
         });
       });
 </script>
@endsection
