<!DOCTYPE html>
<html lang="en">
<head><title>Dashboard | Dashboard</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="images/icons/favicon.ico">
    <link rel="apple-touch-icon" href="images/icons/favicon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{{URL::asset('images/icons/favicon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{URL::asset('images/icons/favicon-114x114.png')}}">
    <!--Loading bootstrap css-->
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,700">
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Oswald:400,700,300">
    <script src="{{ asset('js/app.js') }}"></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      <style>
          .n-print {display:block}
          @media print{
            .n-print {display:none}
          }
          input::placeholder, textarea::placeholder {
            font-size: small;
          }
          .clearT {
            margin-bottom:10px
          }
      </style>
</head>
<body class="sidebar-colors">
<div>

    <!--BEGIN BACK TO TOP--><a class="n-print" id="totop" href="#"><i class="fa fa-angle-up"></i></a><!--END BACK TO TOP--><!--BEGIN TOPBAR-->

    <!--BEGIN TOPBAR-->
    <div id="header-topbar-option-demo" class="page-header-topbar n-print">
        <nav id="topbar" role="navigation" style="margin-bottom: 0;" data-step="3" data-intro="&lt;b&gt;Topbar&lt;/b&gt; has other styles with live demo. Go to &lt;b&gt;Layouts-&gt;Header&amp;Topbar&lt;/b&gt; and check it out." class="navbar navbar-default navbar-static-top">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" data-target=".sidebar-collapse" class="navbar-toggle"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
              <div class="col-md-9">
                <a id="logo" href="/home" class="navbar-brand">
                    <!--span class="fa fa-rocket"></span-->
                    <i id="menu-toggle" class="fa fa-bars hidden-xs" style="position: absolute;left: 10px;right:auto;font-size: large;"></i>&nbsp;&nbsp;&nbsp;<span>Amor web</span>
                    <span style="display: none" class="logo-text-icon">A</span>
                 </a>
              </div>

               </div>
            <div class="topbar-main"><!--a id="menu-toggle" href="#" class="hidden-xs hidden-lg"><i class="fa fa-bars"></i></a-->
                <form id="topbar-search" class="hidden-sm hidden-xs">
                    <div class="input-icon right">
                      <a href="#">
                        <i class="fa fa-search"></i>
                      </a>
                      <input id="autocomplete" type="text" placeholder="Search here..." class="form-control"/>
                      <script>
                          var mod = {!! buildMenu::listModulesSearch($userGroupId) !!};

                          $('#autocomplete').devbridgeAutocomplete({
                                  lookup: mod,
                                  onSelect: function (suggestion) {
                                      window.location.href = suggestion.data+"/"+suggestion.parent+"/"+suggestion.view;
                                  }
                          });
                      </script>
                    </div>
                </form>
                <!--div class="news-update-box hidden-xs"><span class="text-uppercase mrm pull-left">News:</span>
                    <ul id="news-update" class="ticker list-unstyled">
                        <li>Welcome to amorweb</li>
                        <li>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque.</li>
                    </ul>
                </div>
                <script>
                  $("#news-update").ticker();
                </script-->
                <ul class="nav navbar navbar-top-links navbar-right mbn">
                    <li class="dropdown topbar-user" style="float:right"><a data-hover="dropdown" href="#" class="dropdown-toggle"><img src="/get-image/userManagement/get-image-user/{{Auth::user()->id}}" alt="" class="img-responsive img-circle"/>&nbsp;<span class="hidden-xs">{{Auth::user()->name}}</span>&nbsp;<span class="caret"></span></a>
                        <ul class="dropdown-menu dropdown-user pull-right">
                            <li><a href="/profile/userManagement/umUserProfile/{{Auth::user()->id}}"><i class="fa fa-user"></i>Profile setting</a></li>
                            <li>
                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                  {{ csrf_field() }}
                              </form>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    <i class="fa fa-key"></i>Log Out</a>
                            </li>
                        </ul>
                    </li>
                    <!--li id="topbar-chat" class="hidden-xs"><a href="javascript:void(0)" data-step="4" data-intro="&lt;b&gt;Form chat&lt;/b&gt; keep you connecting with other coworker" data-position="left" class="btn-chat"><i class="fa fa-comments"></i><span class="badge badge-info">3</span></a></li-->
                </ul>
            </div>
        </nav>
    </div>
    <!--END TOPBAR -->

    <div id="wrapper">
      <!--BEGIN SIDEBAR MENU -->
      <nav id="sidebar" role="navigation" data-step="2" data-intro="Template has &lt;b&gt;many navigation styles&lt;/b&gt;" data-position="right" class="navbar-default navbar-static-side n-print">
          <div class="sidebar-collapse menu-scroll">
              <ul id="side-menu" class="nav">
                  <li class="user-panel">
                      <div class="thumb"><img src="https://s3.amazonaws.com/uifaces/faces/twitter/kolage/128.jpg" alt="" class="img-circle"/></div>
                      <div class="info"><p>John Doe</p>
                          <ul class="list-inline list-unstyled">
                              <li><a href="extra-profile.html" data-hover="tooltip" title="Profile"><i class="fa fa-user"></i></a></li>
                              <li><a href="email-inbox.html" data-hover="tooltip" title="Mail"><i class="fa fa-envelope"></i></a></li>
                              <li><a href="#" data-hover="tooltip" title="Setting" data-toggle="modal" data-target="#modal-config"><i class="fa fa-cog"></i></a></li>
                              <li><a href="extra-signin.html" data-hover="tooltip" title="Logout"><i class="fa fa-sign-out"></i></a></li>
                          </ul>
                      </div>
                      <div class="clearfix"></div>
                  </li>
                  <!-- menus -->
                    {{buildMenu::buildMenu($userGroupId, $listModules)}}
                  <!-- /menus -->
              </ul>
          </div>
      </nav>
      <!--END SIDEBAR MENU-->

      <!--BEGIN PAGE WRAPPER-->
        <div id="page-wrapper">
            @yield('content')
            <!--BEGIN FOOTER-->
            <div class="n-print" id="footer">
                <div class="copyright">2017 © Surya data infokreasi</div>
            </div>
            <!--END FOOTER-->
        </div>
    </div>
</div>
</body>
</html>
