@if($privilaged->canEdit != 0)
       <button type="button" class="btn btn-default btn-xs" data-target="#curr{{$data->curr_rate_id}}" data-toggle="modal" >
         <i class="fa fa-pencil"></i>&nbsp;
           Edit
       </button>
@endif
@if($privilaged->canDelete != 0)
     <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->curr_rate_id}}" data-toggle="modal">
       <i class="fa fa-trash"></i>&nbsp;
         Delete
     </button>
@endif
<!--Modal edit-->
 <div id="curr{{$data->curr_rate_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
     <div class="modal-dialog">
       <form role="form" method="post" action="/curr-rate-edit/gLM/edit-curr-rate">
         {{csrf_field()}}
         <input type="hidden" name="curr_rate_id" value="{{$data->curr_rate_id}}" required>
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                 <h4 id="modal-responsive-label" class="modal-title">Edit currency rate {{$data->curr_rate_id}}</h4></div>
             <div class="modal-body">
                 <div class="row">
                   <div class="col-md-12">
                     <div class="mbm">
                         <label>Currency rate from :</label>
                         <select name="curr_rate_from" class="form-control required">
                           @foreach($currency as $curr)
                            @if($curr->curr_id == $data->curr_rate_from)
                              <option value="{{$curr->curr_id}}" selected>{{$curr->curr_code}}</option>
                            @else
                              <option value="{{$curr->curr_id}}">{{$curr->curr_code}}</option>
                            @endif

                           @endforeach
                         </select>
                     </div>
                     <div class="mbm">
                       <label>Currency rate to :</label>
                       <select name="curr_rate_to" class="form-control required">
                         @foreach($currency as $curr)
                          @if($curr->curr_id == $data->curr_rate_to)
                            <option value="{{$curr->curr_id}}" selected>{{$curr->curr_code}}</option>
                          @else
                            <option value="{{$curr->curr_id}}">{{$curr->curr_code}}</option>
                          @endif

                         @endforeach
                       </select>
                     </div>
                     <div class="mbm">
                         <label>Currency exchange rate :</label>
                         <input type="number" placeholder="Please type the currency exchange rate" name="curr_rate_exchange"  class="form-control required" value="{{$data->curr_rate_exchange}}"/>
                     </div>
                     <div class="mbm">
                       <label>Cerrency exchange date :</label>
                       <input type="text" placeholder="Please type the currency rate exchange date" name="curr_rate_exchange_date" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" value="{{$data->curr_rate_exchange_date}}" class="datepicker-default form-control required"/>
                     </div>
                   </div>
                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                 <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
             </div>
         </div>
       </form>
     </div>
 </div>


 <!--Modal delete-->
  <div id="delete{{$data->curr_rate_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
      <div class="modal-dialog">
        <form role="form" method="post" action="/curr-rate-delete/gLM/delete-curr-rate">
          {{csrf_field()}}
          <input type="hidden" name="curr_rate_id" value="{{$data->curr_rate_id}}">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                  <h4 id="modal-responsive-label" class="modal-title">Delete currency rate {{$data->curr_rate_id}}</h4></div>
              <div class="modal-body">
                  <div class="row">
                        <h3 style="text-align:center">Are you sure want to delete this currency rate ?</h3>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                  <button type="submit" class="btn btn-primary">Yes</button>
              </div>
          </div>
        </form>
      </div>
  </div>
  <script>
      $(document).ready(function() {
        $('form').each(function () {
           $(this).validate();
        });
        $(".datepicker-default").datepicker();
      });
  </script>
