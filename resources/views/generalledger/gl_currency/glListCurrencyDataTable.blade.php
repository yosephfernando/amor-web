@if($privilaged->canEdit != 0)
       <button type="button" class="btn btn-default btn-xs" data-target="#curr{{$data->curr_id}}" data-toggle="modal" >
         <i class="fa fa-pencil"></i>&nbsp;
           Edit
       </button>
@endif
@if($privilaged->canDelete != 0)
     <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->curr_id}}" data-toggle="modal">
       <i class="fa fa-trash"></i>&nbsp;
         Delete
     </button>
@endif
<!--Modal edit-->
 <div id="curr{{$data->curr_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
     <div class="modal-dialog">
       <form role="form" method="post" action="/curr-edit/gLM/edit-curr">
         {{csrf_field()}}
         <input type="hidden" name="curr_id" value="{{$data->curr_id}}" required>
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                 <h4 id="modal-responsive-label" class="modal-title">Edit currency {{$data->curr_code}}</h4></div>
             <div class="modal-body">
                 <div class="row">
                   <div class="col-md-12">
                     <div class="mbm">
                         <label>Currency code :</label>
                         <input type="text" placeholder="Please type the currency code" name="curr_code"  class="form-control required" value="{{$data->curr_code}}"/>
                     </div>
                     <div class="mbm">
                       <label>Currency desc :</label>
                       <input type="text" placeholder="Please type the currency description" name="curr_desc" class="form-control required" value="{{$data->curr_desc}}"/>
                     </div>
                     <div class="mbm">
                         <label>Currency symbol :</label>
                         <input type="text" placeholder="Please type the currency symbol" name="curr_symbol"  class="form-control required" value="{{$data->curr_symbol}}"/>
                     </div>
                   </div>
                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                 <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
             </div>
         </div>
       </form>
     </div>
 </div>


 <!--Modal delete-->
  <div id="delete{{$data->curr_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
      <div class="modal-dialog">
        <form role="form" method="post" action="/curr-delete/gLM/delete-curr">
          {{csrf_field()}}
          <input type="hidden" name="curr_id" value="{{$data->curr_id}}">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                  <h4 id="modal-responsive-label" class="modal-title">Delete currency {{$data->curr_code}}</h4></div>
              <div class="modal-body">
                  <div class="row">
                        <h3 style="text-align:center">Are you sure want to delete this currency ?</h3>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                  <button type="submit" class="btn btn-primary">Yes</button>
              </div>
          </div>
        </form>
      </div>
  </div>
  <script>
      $(document).ready(function() {
        $('form').each(function () {
           $(this).validate();
        });
      });
  </script>
