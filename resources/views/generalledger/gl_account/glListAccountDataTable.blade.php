@if($privilaged->canEdit != 0)
       <button type="button" class="btn btn-default btn-xs" data-target="#acc{{$data->coa_id}}" data-toggle="modal" >
         <i class="fa fa-pencil"></i>&nbsp;
           Edit
       </button>
@endif
@if($privilaged->canDelete != 0)
     <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->coa_id}}" data-toggle="modal">
       <i class="fa fa-trash"></i>&nbsp;
         Delete
     </button>
@endif
<!--Modal edit-->
 <div id="acc{{$data->coa_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
     <div class="modal-dialog">
       <form role="form" method="post" action="/coa-edit/gLM/edit-coa">
         {{csrf_field()}}
         <input type="hidden" name="coa_id" value="{{$data->coa_id}}" required>
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                 <h4 id="modal-responsive-label" class="modal-title">Edit account {{$data->coa_code}}</h4></div>
             <div class="modal-body">
                 <div class="row">
                   <div class="col-md-12">
                     <div class="mbm">
                         <label>Account number :</label>
                         <input type="text" placeholder="Please type the account number" name="coa_code"  class="form-control required" value="{{$data->coa_code}}"/>
                     </div>
                     <div class="mbm">
                         <label>Account alias :</label>
                         <input type="text" placeholder="Please type the account alias" name="coa_alias"  class="form-control required" value="{{$data->coa_alias}}"/>
                     </div>
                     <div class="mbm">
                       <label>Account desc :</label>
                       <input type="text" placeholder="Please type the account description" name="coa_desc" class="form-control required" value="{{$data->coa_desc}}"/>
                     </div>
                     <div class="mbm">
                        <label>Account group :</label>
                        <select name="coa_group_id" class="form-control required">
                          @foreach($coa_group as $cg)
                           @if($cg->coa_group_id == $data->coa_group_id)
                                <option value="{{$cg->coa_group_id}}" selected>{{$cg->coa_group_desc}}</option>
                           @else
                                <option value="{{$cg->coa_group_id}}">{{$cg->coa_group_desc}}</option>
                           @endif

                          @endforeach
                        </select>
                     </div>
                     <div class="mbm">
                        <label>Account position :</label>
                        <select name="position" class="form-control required">
                          @if($data->child_flag == 0 || $data->child_flag == null)
                            <option value="header" selected>Header</option>
                            <option value="detail">Detail</option>
                          @else
                            <option value="header">Header</option>
                            <option value="detail" selected>Detail</option>
                          @endif
                        </select>
                     </div>
                     <div class="mbm">
                        <label>Currency :</label>
                        <select name="curr_id" class="form-control required">
                            @foreach($currency as $curr)
                              @if($curr->curr_id == $data->curr_id)
                                <option value="{{$curr->curr_id}}" selected>{{$curr->curr_code}}</option>
                              @else
                                <option value="{{$curr->curr_id}}">{{$curr->curr_code}}</option>
                              @endif
                            @endforeach
                        </select>
                     </div>
                     <div class="mbm">
                       <label>Begining amount :</label>
                       <?php
                          $formatted_number = $data->beginning_amt;
                          $formatted_number = preg_replace("/[^A-Za-z0-9 ]/", '', $formatted_number);
                          $formatted_number = str_replace('Rp', '', $formatted_number);
                        ?>
                       <input type="text" id="begining_amt{{$data->coa_id}}" placeholder="Please type the account begining amount"  name="beginning_amt" value="{{number_format($formatted_number, 0,'','.')}}" data-a-sep="." data-a-dec="," data-a-form="false" data-a-pad="false" class="form-control required"/>
                       <script>
                         $('#begining_amt{{$data->coa_id}}').autoNumeric('init');
                       </script>
                     </div>
                   </div>
                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                 <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
             </div>
         </div>
       </form>
     </div>
 </div>


 <!--Modal delete-->
  <div id="delete{{$data->coa_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
      <div class="modal-dialog">
        <form role="form" method="post" action="/coa-delete/gLM/delete-coa">
          {{csrf_field()}}
          <input type="hidden" name="coa_id" value="{{$data->coa_id}}">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                  <h4 id="modal-responsive-label" class="modal-title">Delete account {{$data->coa_code}}</h4></div>
              <div class="modal-body">
                  <div class="row">
                        <h3 style="text-align:center">Are you sure want to delete this account ?</h3>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                  <button type="submit" class="btn btn-primary">Yes</button>
              </div>
          </div>
        </form>
      </div>
  </div>
  <script>
      $(document).ready(function() {
        $('form').each(function () {
           $(this).validate();
        });
      });
  </script>
