@extends('layout.layout')
@section('content')
<!--BEGIN TITLE & BREADCRUMB PAGE-->
            <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                <div class="page-header pull-left">
                    <div class="page-title">{{$title->moduleLabel}}</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a href="#">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li><a href="#">Financial management</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li class="active">{{$title->moduleLabel}}</li>
                </ol>
                <div class="clearfix"></div>
            </div>
            <!--END TITLE & BREADCRUMB PAGE-->
           <div class="page-content">
              <div class="row">
                <div class="col-md-12">
                <div class="panel panel-green">
                        <div class="panel-body">
                          @if($errors->any())
                              <script>
                                     toastr.error("{{$errors->first()}}");
                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                          @if(session('status'))
                              <script>
                                 @if(session('status') == 'Account added' || session('status') == 'Account updated' || session('status') == 'Account deleted')
                                   toastr.success("{{ session('status') }}");
                                 @else
                                   toastr.error("{{ session('status') }}");
                                 @endif

                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                          <div class="row">
                            <div class="col-md-7">
                                @if($privilaged->canAdd != 0)
                                   <button type="button" class="btn btn-primary btn-md" data-target="#accAdd" data-toggle="modal">
                                     <i class="fa fa-plus"></i>&nbsp;
                                       Add
                                   </button>
                                @endif
                                @if($privilaged->canExport != 0)
                                   <a href="/exportAcc/gLM/exportAccToExcel" class="btn btn-success btn-md"><i class="fa fa-download"></i>&nbsp;Export to xls</a>
                                @endif
                                <div class="clear" style="height:20px"></div>
                            </div>
                            <div class="col-md-5">
                              <form method="POST" id="search-group" class="form-inline" role="form" style="float: right">
                                  <div class="form-group">
                                    <span>Group</span>
                                    <select class="form-control" id="coa_group_id_filter" name="coa_group_id_filter">
                                        <option value="">all</option>
                                      @foreach($coa_group as $group)
                                        <option value="{{$group->coa_group_id}}">{{$group->coa_group_desc}}</option>
                                      @endforeach
                                    </select>
                                  </div>
                                  <button type="submit" class="btn btn-primary">Go</button>
                                  <div class="clear" style="height:10px"></div>
                              </form>
                            </div>
                          </div>

                              <div class="col-md-12">
                                  <div class="table-responsive">
                                    <table class="table table-bordered" id="acc-table">
                                        <thead>
                                          <tr>
                                            <th><input type="checkbox"  id="bulkDelete"  /></th>
                                            <th>No coa</th>
                                            <th>coa alias</th>
                                            <th>coa desc</th>
                                            <th>group</th>
                                            <th>Action</th>
                                          </tr>
                                        </thead>
                                      </table>
                                      <script>
                                        $(function() {
                                          var table = $('#acc-table').DataTable({
                                               processing: true,
                                               serverSide: true,
                                               ajax: {
                                                  url: '/listAccData/gLM/glListAcountDataTable',
                                                  data: function (d) {
                                                      d.coa_group_id_filter = $('select[name=coa_group_id_filter]').val();
                                                  }
                                               },
                                               columns: [
                                                   {data: 'delete_bulk', name: 'delete_bulk', orderable: false, searchable: false},
                                                   { data: 'coa_code', name: 'coa_code' },
                                                   { data: 'coa_alias', name: 'coa_alias' },
                                                   { data: 'coa_desc', name: 'coa_desc' },
                                                   { data: 'coa_group_desc', name: 'coa_group_desc', searchable:false },
                                                   {data: 'action', name: 'action', className: 'text-center', orderable: false, searchable: false}
                                               ]
                                           });
                                            $('div.dataTables_filter input').addClass('form-control');

                                            $("#deleteTriger").click(function(){
                                               var allValue = [];
                                               $("input[name='coa_id[]']:checked").each( function () {
                                                   allValue.push($(this).val());
                                               });
                                               $.post( "/delete-bulk-acc/gLM/deleteBulkAcc",  {'coa_id': allValue, '_token' : '{{csrf_token()}}' })
                                                 .done(function( data ) {
                                                   if(data == "success"){
                                                      location.reload();
                                                   }else{
                                                      alert(data);
                                                   }
                                                   console.log( "Data Loaded: " + data );
                                                 });
                                             });

                                             $("#bulkDelete").click(function(){
                                                $("input[name='coa_id[]']").prop("checked", $(this).prop('checked'))
                                                if($("input[name='coa_id[]']:checked").length > 0){
                                                  $("#deleteTriger").show();
                                                }else{
                                                  $("#deleteTriger").hide();
                                                }
                                             });

                                             $("#acc-table").on('click', ':checkbox', function(){
                                               if($("input[name='coa_id[]']:checked").length > 0){
                                                 $("#deleteTriger").show();
                                               }else{
                                                 $("#deleteTriger").hide();
                                               }
                                             });

                                             $('#search-group').on('submit', function(e) {
                                                 table.draw();
                                                 e.preventDefault();
                                             });

                                        });
                                     </script>
                                  </div>
                                  <button class='btn btn-danger' id="deleteTriger" style="display:none">Delete cheked</button>
                                  <div class="clear" style="height:20px"></div>
                              </div>
                            </div>
                        </div>
                    </div>
                  </div>
              </div>
          </div>

          <!--Modal add user-->
           <div id="accAdd" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
               <div class="modal-dialog">
                 <form role="form" method="post" action="/coa-add/gLM/add-coa">
                    {{csrf_field()}}
                   <div class="modal-content">
                       <div class="modal-header">
                           <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                           <h4 id="modal-responsive-label" class="modal-title">Add account</h4></div>
                       <div class="modal-body">
                           <div class="row">
                               <div class="col-md-12">
                                 <div class="mbm">
                                     <label>Account number :</label>
                                     <input type="text" placeholder="Please type the account number" name="coa_code"  class="form-control required"/>
                                 </div>
                                 <div class="mbm">
                                     <label>Account alias :</label>
                                     <input type="text" placeholder="Please type the account alias" name="coa_alias"  class="form-control required"/>
                                 </div>
                                 <div class="mbm">
                                   <label>Account desc :</label>
                                   <input type="text" placeholder="Please type the account description" name="coa_desc" class="form-control required"/>
                                 </div>
                                 <div class="mbm">
                                    <label>Account group :</label>
                                    <select name="coa_group_id" class="form-control required">
                                      @foreach($coa_group as $data)
                                        <option value="{{$data->coa_group_id}}">{{$data->coa_group_desc}}</option>
                                      @endforeach
                                    </select>
                                 </div>
                                 <div class="mbm">
                                    <label>Account position :</label>
                                    <select name="position" class="form-control required">
                                        <option value="header">Header</option>
                                        <option value="detail">Detail</option>
                                    </select>
                                 </div>
                                 <div class="mbm">
                                    <label>Currency :</label>
                                    <select name="curr_id" class="form-control required">
                                      @foreach($currency as $curr)
                                        <option value="{{$curr->curr_id}}">{{$curr->curr_code}}</option>
                                      @endforeach
                                    </select>
                                 </div>
                                 <div class="mbm">
                                   <label>Begining amount :</label>
                                   <input type="text" id="begining_amt" placeholder="Please type the account begining amount"  name="beginning_amt" data-a-sep="." data-a-dec="," data-a-form="false" data-a-pad="false" class="form-control required"/>
                                   <script>
                                     $('#begining_amt').autoNumeric('init');
                                   </script>
                                 </div>
                               </div>
                           </div>
                       </div>
                       <div class="modal-footer">
                           <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                           <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                       </div>
                   </div>
                  </form>
               </div>
           </div>
           <script>
               $(document).ready(function() {
                 $('form').each(function () {
                    $(this).validate();
                 });
               });
           </script>
@endsection
