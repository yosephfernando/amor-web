@extends('layout.layout')
@section('content')
<!--BEGIN TITLE & BREADCRUMB PAGE-->
            <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                <div class="page-header pull-left">
                    <div class="page-title">{{$title->moduleLabel}}</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a href="#">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li><a href="#">Financial management</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li class="active">{{$title->moduleLabel}}</li>
                </ol>
                <div class="clearfix"></div>
            </div>
            <!--END TITLE & BREADCRUMB PAGE-->
           <div class="page-content">
              <div class="row">
                <div class="col-md-12">
                <div class="panel panel-green">
                        <div class="panel-body">
                          @if($errors->any())
                              <script>
                                     toastr.error("{{$errors->first()}}");
                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                          @if(session('status'))
                              <script>
                                 @if(session('status') == 'Bank added' || session('status') == 'Bank updated' || session('status') == 'Bank deleted')
                                   toastr.success("{{ session('status') }}");
                                 @else
                                   toastr.error("{{ session('status') }}");
                                 @endif

                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                          <div class="row">
                            <div class="col-md-7">
                                @if($privilaged->canAdd != 0)
                                   <button type="button" class="btn btn-primary btn-md" data-target="#bankAdd" data-toggle="modal">
                                     <i class="fa fa-plus"></i>&nbsp;
                                       Add
                                   </button>
                                @endif
                                @if($privilaged->canExport != 0)
                                   <a href="/exportBank/gLM/exportBankToExcel" class="btn btn-success btn-md"><i class="fa fa-download"></i>&nbsp;Export to xls</a>
                                @endif
                                <div class="clear" style="height:20px"></div>
                            </div>
                          </div>

                              <div class="col-md-12">
                                  <div class="table-responsive">
                                    <table class="table table-bordered" id="curr-table">
                                        <thead>
                                          <tr>
                                            <th><input type="checkbox"  id="bulkDelete"  /></th>
                                            <th>No bank</th>
                                            <th>bank desc</th>
                                            <th>bank rek no</th>
                                            <th>Action</th>
                                          </tr>
                                        </thead>
                                      </table>
                                      <script>
                                        $(function() {
                                          var table = $('#curr-table').DataTable({
                                               processing: true,
                                               serverSide: true,
                                               ajax: {
                                                  url: '/listBankData/gLM/glListBankDataTable',
                                               },
                                               columns: [
                                                   {data: 'delete_bulk', name: 'delete_bulk', orderable: false, searchable: false},
                                                   { data: 'bank_code', name: 'bank_code' },
                                                   { data: 'bank_desc', name: 'bank_desc' },
                                                   { data: 'bank_rek_no', name: 'bank_rek_no' },
                                                   {data: 'action', name: 'action', className: 'text-center', orderable: false, searchable: false}
                                               ]
                                           });
                                            $('div.dataTables_filter input').addClass('form-control');

                                            $("#deleteTriger").click(function(){
                                               var allValue = [];
                                               $("input[name='bank_id[]']:checked").each( function () {
                                                   allValue.push($(this).val());
                                               });
                                               $.post( "/delete-bulk-bank/gLM/deleteBulkBank",  {'bank_id': allValue, '_token' : '{{csrf_token()}}' })
                                                 .done(function( data ) {
                                                   if(data == "success"){
                                                      location.reload();
                                                   }else{
                                                      alert(data);
                                                   }
                                                   console.log( "Data Loaded: " + data );
                                                 });
                                             });

                                             $("#bulkDelete").click(function(){
                                                $("input[name='bank_id[]']").prop("checked", $(this).prop('checked'))
                                                if($("input[name='bank_id[]']:checked").length > 0){
                                                  $("#deleteTriger").show();
                                                }else{
                                                  $("#deleteTriger").hide();
                                                }
                                             });

                                             $("#curr-table").on('click', ':checkbox', function(){
                                               if($("input[name='bank_id[]']:checked").length > 0){
                                                 $("#deleteTriger").show();
                                               }else{
                                                 $("#deleteTriger").hide();
                                               }
                                             });

                                             $('#search-group').on('submit', function(e) {
                                                 table.draw();
                                                 e.preventDefault();
                                             });

                                        });
                                     </script>
                                  </div>
                                  <button class='btn btn-danger' id="deleteTriger" style="display:none">Delete cheked</button>
                                  <div class="clear" style="height:20px"></div>
                              </div>
                            </div>
                        </div>
                    </div>
                  </div>
              </div>
          </div>

          <!--Modal add user-->
           <div id="bankAdd" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
               <div class="modal-dialog">
                 <form role="form" method="post" action="/bank-add/gLM/add-bank">
                    {{csrf_field()}}
                   <div class="modal-content">
                       <div class="modal-header">
                           <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                           <h4 id="modal-responsive-label" class="modal-title">Add bank</h4></div>
                       <div class="modal-body">
                           <div class="row">
                               <div class="col-md-12">
                                 <div class="mbm">
                                     <label>Bank code :</label>
                                     <input type="text" placeholder="Please type the bank code" name="bank_code"  class="form-control required"/>
                                 </div>
                                 <div class="mbm">
                                   <label>Bank desc :</label>
                                   <input type="text" placeholder="Please type the bank description" name="bank_desc" class="form-control required"/>
                                 </div>
                                 <div class="mbm">
                                   <label>Bank rekening no :</label>
                                   <input type="text" placeholder="Please type the bank account number" name="bank_rek_no" class="form-control required"/>
                                 </div>
                                 <div class="mbm">
                                   <label>Bank COA :</label>
                                   <input type="hidden" id="coa_id" name="coa_id" />
                                   <input type="text" placeholder="Please type the bank coa" id="coa_desc" name="coa_desc" class="form-control required"/>
                                   <script>
                                         $('#coa_desc').devbridgeAutocomplete({
                                                 serviceUrl : '/lookupSugestion/coa',
                                                 minChars : 3,
                                                 onSelect: function (suggestion) {
                                                   $('#coa_id').val(suggestion.data);
                                                   $('#coa_desc').val(suggestion.value);
                                                 }
                                         });
                                   </script>
                                 </div>
                                 <div class="mbm">
                                   <label>Bank currency :</label>
                                   <input type="hidden" id="curr_id" name="curr_id" />
                                   <input type="text" placeholder="Please type the bank currency" id="curr_desc" name="curr_desc" class="form-control required"/>
                                   <script>
                                         $('#curr_desc').devbridgeAutocomplete({
                                                 serviceUrl : '/lookupSugestion/curr',
                                                 minChars : 3,
                                                 onSelect: function (suggestion) {
                                                   $('#curr_id').val(suggestion.data);
                                                   $('#curr_desc').val(suggestion.value);
                                                 }
                                         });
                                   </script>
                                 </div>
                                 <div class="mbm">
                                   <label>Bank cost center :</label>
                                   <input type="text" placeholder="Please type the bank cost center" name="bank_cost_center" class="form-control required"/>
                                 </div>
                                 <div class="mbm">
                                   <label>Bank kas :</label>
                                   <select class="form-control required" name="bank_kas">
                                      <option value="B">Bank</option>
                                      <option value="C">Cash</option>
                                   </select>
                                 </div>
                               </div>
                           </div>
                       </div>
                       <div class="modal-footer">
                           <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                           <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                       </div>
                   </div>
                  </form>
               </div>
           </div>
           <script>
               $(document).ready(function() {
                 $('form').each(function () {
                    $(this).validate();
                 });
               });
           </script>
@endsection
