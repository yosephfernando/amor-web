@if($privilaged->canEdit != 0)
       <button type="button" class="btn btn-default btn-xs" data-target="#bank{{$data->bank_id}}" data-toggle="modal" >
         <i class="fa fa-pencil"></i>&nbsp;
           Edit
       </button>
@endif
@if($privilaged->canDelete != 0)
     <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->bank_id}}" data-toggle="modal">
       <i class="fa fa-trash"></i>&nbsp;
         Delete
     </button>
@endif
<!--Modal edit-->
 <div id="bank{{$data->bank_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
     <div class="modal-dialog">
       <form role="form" method="post" action="/bank-edit/gLM/edit-bank">
         {{csrf_field()}}
         <input type="hidden" name="bank_id" value="{{$data->bank_id}}" required>
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                 <h4 id="modal-responsive-label" class="modal-title">Edit bank {{$data->bank_code}}</h4></div>
             <div class="modal-body">
                 <div class="row">
                   <div class="col-md-12">
                     <div class="mbm">
                         <label>Bank code :</label>
                         <input type="text" placeholder="Please type the bank code" name="bank_code"  class="form-control required" value="{{$data->bank_code}}"/>
                     </div>
                     <div class="mbm">
                       <label>Bank desc :</label>
                       <input type="text" placeholder="Please type the bank description" name="bank_desc" class="form-control required" value="{{$data->bank_desc}}"/>
                     </div>
                     <div class="mbm">
                       <label>Bank rekening no :</label>
                       <input type="text" placeholder="Please type the bank account number" name="bank_rek_no" class="form-control required" value="{{$data->bank_rek_no}}"/>
                     </div>
                     <div class="mbm">
                       <label>Bank COA :</label>
                       <input type="hidden" id="coa_id{{$data->bank_id}}" name="coa_id" value="{{$data->coa_id}}"/>
                       <input type="text" placeholder="Please type the bank coa" id="coa_desc{{$data->bank_id}}" name="coa_desc" class="form-control required" value="{{$data->coa_desc}}"/>
                       <script>
                             $('#coa_desc{{$data->bank_id}}').devbridgeAutocomplete({
                                     serviceUrl : '/lookupSugestion/coa',
                                     minChars : 3,
                                     onSelect: function (suggestion) {
                                       $('#coa_id{{$data->bank_id}}').val(suggestion.data);
                                       $('#coa_desc{{$data->bank_id}}').val(suggestion.value);
                                     }
                             });
                       </script>
                     </div>
                     <div class="mbm">
                       <label>Bank currency :</label>
                       <input type="hidden" id="curr_id{{$data->bank_id}}" name="curr_id" value="{{$data->curr_id}}"/>
                       <input type="text" placeholder="Please type the bank currency" id="curr_desc{{$data->bank_id}}" name="curr_desc" class="form-control required" value="{{$data->curr_code}}"/>
                       <script>
                             $('#curr_desc{{$data->bank_id}}').devbridgeAutocomplete({
                                     serviceUrl : '/lookupSugestion/curr',
                                     minChars : 3,
                                     onSelect: function (suggestion) {
                                       $('#curr_id{{$data->bank_id}}').val(suggestion.data);
                                       $('#curr_desc{{$data->bank_id}}').val(suggestion.value);
                                     }
                             });
                       </script>
                     </div>
                     <div class="mbm">
                       <label>Bank cost center :</label>
                       <input type="text" placeholder="Please type the bank cost center" name="bank_cost_center" class="form-control required" value="{{$data->bank_cost_center}}"/>
                     </div>
                     <div class="mbm">
                       <label>Bank kas :</label>
                       <select class="form-control required" name="bank_kas">
                            @if($data->bank_kas == "B")
                              <option value="B" selected>Bank</option>
                              <option value="C">Cash</option>
                            @else
                              <option value="B">Bank</option>
                              <option value="C" selected>Cash</option>
                            @endif
                       </select>
                     </div>
                   </div>
                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                 <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
             </div>
         </div>
       </form>
     </div>
 </div>


 <!--Modal delete-->
  <div id="delete{{$data->bank_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
      <div class="modal-dialog">
        <form role="form" method="post" action="/bank-delete/gLM/delete-bank">
          {{csrf_field()}}
          <input type="hidden" name="bank_id" value="{{$data->bank_id}}">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                  <h4 id="modal-responsive-label" class="modal-title">Delete bank {{$data->bank_code}}</h4></div>
              <div class="modal-body">
                  <div class="row">
                        <h3 style="text-align:center">Are you sure want to delete this bank ?</h3>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                  <button type="submit" class="btn btn-primary">Yes</button>
              </div>
          </div>
        </form>
      </div>
  </div>
  <script>
      $(document).ready(function() {
        $('form').each(function () {
           $(this).validate();
        });
      });
  </script>
