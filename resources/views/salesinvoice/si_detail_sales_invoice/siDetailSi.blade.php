@extends('layout.layout')
@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb n-print">
    <div class="page-header pull-left">
        <div class="page-title">{{$title->moduleLabel}}</div>
    </div>
    <ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i>&nbsp;<a href="#">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li><a href="#">Transaction purchasing</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li><a href="#">Purchasing invoice</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="active">{{$title->moduleLabel}}</li>
    </ol>
    <div class="clearfix"></div>
</div>

<div class="page-content">
   <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-green">
            <div class="panel-body pan">
                    <div class="form-body pal">
                      @if($errors->any())
                          <script>
                                 toastr.error("{{$errors->first()}}");
                                 toastr.options = {
                                  "closeButton": false,
                                  "debug": false,
                                  "positionClass": "toast-top-right",
                                  "onclick": null,
                                  "showDuration": "300",
                                  "hideDuration": "1000",
                                  "timeOut": "5000",
                                  "extendedTimeOut": "1000",
                                  "showEasing": "swing",
                                  "hideEasing": "linear",
                                  "showMethod": "fadeIn",
                                  "hideMethod": "fadeOut"
                                };

                          </script>
                      @endif
                      @if(session('status'))
                          <script>
                             @if(session('status') == 'Detail si added' || session('status') == 'Detail si updated' || session('status') == 'Detail si deleted')
                               toastr.success("{{ session('status') }}");
                             @else
                               toastr.error("{{ session('status') }}");
                             @endif

                                 toastr.options = {
                                  "closeButton": false,
                                  "debug": false,
                                  "positionClass": "toast-top-right",
                                  "onclick": null,
                                  "showDuration": "300",
                                  "hideDuration": "1000",
                                  "timeOut": "5000",
                                  "extendedTimeOut": "1000",
                                  "showEasing": "swing",
                                  "hideEasing": "linear",
                                  "showMethod": "fadeIn",
                                  "hideMethod": "fadeOut"
                                };

                          </script>
                      @endif
                  @if($si_id->inv_status != "approve")
                   <form class="form-group" method="post" action="/lod-do/saleTransaction/load">
                               <div class="col-md-12"><label>Load from DO :</label></div>
                               <div class="col-md-9 col-sm-12 col-xs-12">
                                    {{csrf_field()}}
                                   <input type="hidden" name="inv_id" value="{{$si_id->inv_id}}" required/>
                                       <input type="hidden"  id="do_id" name="do_id"  class="form-control">
                                       <input type="text"  id="do_no" placeholder="Please type the delivery order number" name="do_no" class="form-control" required>

                                       <!-- Barang catalog -->
                                        <script>
                                          var dor = {!! $doSugest !!};
                                           $('#do_no').devbridgeAutocomplete({
                                                   lookup: dor,
                                                   onSelect: function (suggestion) {
                                                     $('#do_id').val(suggestion.data);
                                                     $('#do_no').val(suggestion.value);
                                                     $.get( "/ajax-do-detail-catalog/saleTransaction/siAjaxDoDetailCatalog/"+suggestion.data)
                                                         .done(function( data ) {
                                                             $("#detailDo").empty();
                                                             $("#detailDo").html(data);
                                                         });
                                                   }
                                           });
                                        </script>
                                   <div id="detailDo" class="col-md-12 row">

                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <button type="submit" class="btn btn-primary form-control"><i class="fa fa-plus"></i>&nbsp;Load</button>
                                    <div class="clear" style="height:20px"></div>
                                </div>
                     </form>
                @endif
                        <div class="col-md-12">
                            @if($si_detail_id != null)
                                <div class="col-md-6">
                                  <h3 style="margin-top:0px"># {{$si_id->inv_no}}</h3>
                                </div>
                                <div class="col-md-6" style="text-align:right">
                                  <div class="dropdown">
                                      @if($privilaged->canPrint != 0)
                                            <button class="btn btn-success" onclick="window.print();"><i class="fa fa-print"></i>&nbsp;Print</button>
                                      @endif
                                      <a href="/list-si/saleTransaction/siListSi">
                                        <button class="btn btn-default">back</button>
                                      </a>
                                        <div class="clear" style="height:10px"></div>
                                  </div>
                                </div>

                           @endif
                        </div>
                        <div class="col-md-12">
                          <div class="table-responsive">
                            <table class="table table-bordered" id="siD-table">
                              <thead>
                                   <th><input type="checkbox"  id="bulkDelete"  /></th>
                                   <th>Product</th>
                                   <th>Qty</th>
                                   <th>Sub total</th>
                                   @if($si_id->inv_status != "approve")
                                       <th>Action</th>
                                   @endif
                               </thead>
                            </table>
                            <script>
                              $(function() {
                                var table = $('#siD-table').DataTable({
                                  dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
                                     "<'row'<'col-xs-12't>>"+
                                     "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
                                     processing: true,
                                     serverSide: true,
                                     ajax: {
                                        url: '/listSiDetailData/saleTransaction/siDetailSalesInvoiceDataTable/{{$si_id->inv_id}}',
                                        data: function (d) {
                                            d.prd_desc = $('input[name=prd_desc]').val();
                                        }
                                    },
                                     columns: [
                                         {data: 'delete_bulk', name: 'delete_bulk', orderable: false, searchable: false},
                                         { data: 'prd_desc', name: 'prd_desc' },
                                         { data: 'qty_inv', name: 'qty_inv' },
                                         { data: 'sub_total', name: 'sub_total' },

                                         @if($si_id->inv_status != "approve")
                                            {data: 'action', name: 'action', className: 'text-center',orderable: false, searchable: false}
                                         @endif
                                     ],
                                     paginate: false
                                 });
                                  $('div.dataTables_filter input').addClass('form-control');

                                  $("#deleteTriger").click(function(){
                                     var allValue = [];
                                     var inv_id = '{{$si_id->inv_id}}';
                                     $("input[name='inv_detail_id[]']:checked").each( function () {
                                         allValue.push($(this).val());
                                     });
                                     $.post( "/delete-bulk-si-detail/saleTransaction/deleteBulkSiDetail",  {'inv_id': inv_id, 'inv_detail_id': allValue, '_token' : '{{csrf_token()}}' })
                                       .done(function( data ) {
                                         if(data == "success"){
                                            location.reload();
                                         }else{
                                            console.log(data);
                                         }
                                       }).error(function(xhr){
                                         console.log(xhr.responseText);
                                       });
                                   });

                                   $("#bulkDelete").click(function(){
                                      $("input[name='inv_detail_id[]']").prop("checked", $(this).prop('checked'))
                                      if($("input[name='inv_detail_id[]']:checked").length > 0){
                                        $("#deleteTriger").show();
                                      }else{
                                        $("#deleteTriger").hide();
                                      }
                                   });

                                   $("#siD-table").on('click', ':checkbox', function(){
                                     if($("input[name='inv_detail_id[]']:checked").length > 0){
                                       $("#deleteTriger").show();
                                     }else{
                                       $("#deleteTriger").hide();
                                     }
                                   });

                                   $('#search-form').on('submit', function(e) {
                                       table.draw();
                                       e.preventDefault();
                                   });

                              });
                           </script>
                          </div>
                          <button class='btn btn-danger' id="deleteTriger" style="display:none">Delete cheked</button>
                          <div class="clear" style="height:20px"></div>
                        </div>
                    </div>
            </div>
        </div>
      </div>
     </div>
</div>
<script>
    $(document).ready(function() {
      $('form').each(function () {
         $(this).validate();
      });
    });
</script>
@include('salesinvoice/si_detail_sales_invoice/siPrintTemplateDetailSi')
@endsection
