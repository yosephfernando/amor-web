<div class="clear" style="height:10px"></div>
 <table class="table table-bordered table-striped table-hover">
    <tr>
      <thead>
        <th>#</th>
        <th>Produk id </th>
        <th>Code </th>
        <th>Desc </th>
        <th>Size </th>
        <th>Qty</th>
        <th>Price</th>
      </thead>
    </tr>
    <h4>Choose gr detail</h4>
    @foreach($grDetail as $data)
        <tr>
          <th><input type="checkbox" name="gr_detail_id[]" value="{{$data->gr_detail_id}}"/></th>
          <td>{{$data->prd_id}}</td>
          <td>{{$data->prd_code}}</td>
          <td>{{$data->prd_desc}}</td>
          <td>{{$data->size_desc}}</td>
          <td>{{number_format($data->qty_receive)}}</td>
          <td>
              <?php
                  $retail_price = $data->retail_price;
                  //$total = $retail_price * $data->qty_receive;
                  echo "<input id='price_format$data->gr_detail_id' type='text' name='retail_price$data->gr_detail_id' class='form-control' value='".number_format($retail_price,0,"",".")."' data-a-sep='.' data-a-dec=',' data-a-form='false' data-a-pad='false'";
              ?>
          </td>
        </tr>
        <script>
          $('#price_format{{$data->gr_detail_id}}').autoNumeric('init');
        </script>
    @endforeach
 </table>
