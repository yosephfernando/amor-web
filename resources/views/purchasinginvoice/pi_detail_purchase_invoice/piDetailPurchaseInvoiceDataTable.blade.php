@if($privilaged->canEdit != 0)
     <button type="button" class="btn btn-default btn-xs" data-target="#dp{{$data->inv_detail_id}}" data-toggle="modal" >
       <i class="fa fa-pencil"></i>&nbsp;
         Edit
     </button>
 @endif
 @if($privilaged->canDelete != 0)
     <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->inv_detail_id}}" data-toggle="modal">
       <i class="fa fa-trash"></i>&nbsp;
         Delete
     </button>
 @endif

 <!--Modal edit-->
  <div id="dp{{$data->inv_detail_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
      <div class="modal-dialog">
        <form role="form" method="post" action="/detail-pi-edit/transactionPurchasing/edit-dpi">
          {{csrf_field()}}
          <input type="hidden" name="inv_detail_id" value="{{$data->inv_detail_id}}">
          <input type="hidden" name="inv_id" value="{{$pi_id->inv_id}}">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                  <h4 id="modal-responsive-label" class="modal-title">Edit detail pi {{$data->inv_detail_id}}</h4></div>
              <div class="modal-body">
                  <div class="row">
                      <div class="col-md-12">
                        <div class="mbm">
                            <label>UOM :</label>
                            <select name="uom_id" class="form-control required">
                                  <option value="{{$data->uom_id}}" selected>{{$data->uom_desc}}</option>
                               @foreach($uoms as $uom)
                                  <option value="{{$uom->uom_id}}">{{$uom->uom_desc}}</option>
                               @endforeach
                            </select>
                        </div>
                        <div class="mbm">
                          <label>UOM  conversion:</label>
                          <input type="number" placeholder="Please type the product conversion" name="uom_conversion" class="form-control required" value="{{number_format($data->uom_conversion)}}"/>
                        </div>
                        <div class="mbm">
                          <label>Qty :</label>
                          <input type="number" placeholder="Please type the product quantity" name="qty_inv" class="form-control required" value="{{number_format($data->qty_inv)}}" />
                        </div>
                        <div class="mbm">
                           <label>Harga:</label>
                           <input id="retail_price_format{{$data->inv_detail_id}}" type="text" placeholder="Please type the product price" name="retail_price" class="form-control required" value="{{$data->price_inv}}"  data-a-sep="." data-a-dec="," data-a-form="false" data-a-pad="false"/>
                           <script>
                             $('#retail_price_format{{$data->inv_detail_id}}').autoNumeric('init');
                           </script>
                        </div>
                      </div>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                  <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
              </div>
          </div>
        </form>
      </div>
  </div>

 <!--Modal delete-->
  <div id="delete{{$data->inv_detail_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
      <div class="modal-dialog">
        <form role="form" method="post" action="/detail-pi-delete/transactionPurchasing/delete-detail-pi">
          {{csrf_field()}}
          <input type="hidden" name="inv_detail_id" value="{{$data->inv_detail_id}}">
          <input type="hidden" name="inv_id" value="{{$pi_id->inv_id}}">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                  <h4 id="modal-responsive-label" class="modal-title">Delete detail pi {{$data->inv_detail_id}}</h4></div>
              <div class="modal-body">
                  <div class="row">
                        <h3 style="text-align:center">Are you sure want to delete this detail pi ?</h3>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                  <button type="submit" class="btn btn-primary">Yes</button>
              </div>
          </div>
        </form>
      </div>
  </div>
  <script>
      $(document).ready(function() {
        $('form').each(function () {
           $(this).validate();
        });
      });
  </script>
