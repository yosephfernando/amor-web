@if($privilaged->canEdit != 0)
       <button type="button" class="btn btn-default btn-xs" data-target="#pi{{$data->inv_id}}" data-toggle="modal" >
         <i class="fa fa-pencil"></i>&nbsp;
           Edit
       </button>
@endif
<a href="/add-detail-pi/transactionPurchasing/piDetailPi/{{$data->inv_id}}">
 <button type="button" class="btn btn-primary btn-xs">
   <i class="fa fa-plus"></i>&nbsp;
     Detail pi
 </button>
</a>
@if($privilaged->canDelete != 0)
       <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->inv_id}}" data-toggle="modal">
         <i class="fa fa-trash"></i>&nbsp;
           Delete
       </button>
@endif

<?php
   $dates = strtotime($data->inv_date);
   $dateEdit = date("d-m-Y", $dates);
?>


<!--Modal edit-->
 <div id="pi{{$data->inv_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade text-left">
     <div class="modal-dialog">
       <form role="form" method="post" action="/pi-edit/transactionPurchasing/edit-pi">
         {{csrf_field()}}
         <input type="hidden" name="inv_id" value="{{$data->inv_id}}">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                 <h4 id="modal-responsive-label" class="modal-title">Edit pi {{$data->inv_no}}</h4></div>
             <div class="modal-body">
                 <div class="row">
                     <div class="col-md-12">
                     @if($privilaged->canApprove == 1)
                       <div class="mbm">
                          <label>Status :</label>
                          <select name="inv_status" class="form-control">
                            @if($data->inv_status == 'open')
                              <option value="{{$data->inv_status}}" selected>{{$data->inv_status}}</option>
                              <option value="approve">approve</option>
                            @else
                              <option value="{{$data->inv_status}}" selected>{{$data->inv_status}}</option>
                              <option value="open">open</option>
                            @endif
                          </select>
                       </div>
                      @endif
                       <div class="mbm">
                          <label>Inv date :</label>
                          <input type="text" placeholder="dd-mm-yy" name="inv_date" data-date-format="dd-mm-yyyy" value="{{date('d-m-Y', strtotime($data->inv_date))}}" class="datepicker-default form-control required"/>
                       </div>
                       <div class="mbm">
                         <label>Gudang : </label>
                             <input type="hidden"  id="wh_id{{$data->inv_id}}" name="wh_id"  class="form-control" value="{{$data->wh_id}}" />
                             <input type="text"  id="autocompletewh{{$data->inv_id}}" placeholder="Please type purchase invoice warehouse" name="wh_desc" class="form-control required" value="{{$data->wh_desc}}" />

                             <!-- warehouse catalog -->
                              <script>
                                   $('#autocompletewh{{$data->inv_id}}').devbridgeAutocomplete({
                                           serviceUrl : '/lookupSugestion/warehouse/',
                                           minChars : 3,
                                           onSelect: function (suggestion) {
                                             $('#wh_id{{$data->inv_id}}').val(suggestion.data);
                                             $('#autocompletewh{{$data->inv_id}}').val(suggestion.value);
                                           }
                                   });
                              </script>
                       </div>
                       <div class="mbm">
                         <label>Supplier : </label>
                             <input type="hidden"  id="supp_id{{$data->inv_id}}" name="supp_id"  class="form-control" value="{{$data->supp_id}}" />
                             <input type="text"  id="autocompletesupp{{$data->inv_id}}" placeholder="Please type purchase invoice supplier" name="supp_desc" class="form-control required" value="{{$data->supp_desc}}" />

                             <!-- warehouse catalog -->
                              <script>
                                   $('#autocompletesupp{{$data->inv_id}}').devbridgeAutocomplete({
                                           serviceUrl : '/lookupSugestion/supplier',
                                           minChars : 3,
                                           onSelect: function (suggestion) {
                                             $('#supp_id{{$data->inv_id}}').val(suggestion.data);
                                             $('#autocompletesupp{{$data->inv_id}}').val(suggestion.value);
                                           }
                                   });
                              </script>
                       </div>
                       <div class="mbm">
                          <label>No Invoice Supplier :</label>
                          <input type="text" placeholder="Please type purchase invoice supplier no" name="no_inv_supp" class="form-control required" value="{{$data->no_inv_supp}}" />
                       </div>
                       <div class="mbm">
                          <label>No Invoice Pajak :</label>
                          <input type="text" placeholder="Please type purchase invoice pajax no" name="no_inv_pajak" class="form-control required" value="{{$data->no_inv_pajak}}"/>
                       </div>
                       <div class="mbm">
                          <label>Reference :</label>
                          <input type="text" placeholder="Please type purchase invoice reference" name="reference" class="form-control required" value="{{$data->reference}}" />
                       </div>
                       <div class="mbm">
                          <label>Notes :</label>
                          <textarea placeholder="Please type purchase invoice note" name="notes" class="form-control required">{{$data->notes}}</textarea>
                       </div>
                       <div class="mbm">
                        @if($data->ppn > 0)
                          <input type="checkbox" name="ppn" value="{{$data->ppn}}" checked><span> PPN ( 10% )</span>
                        @else
                          <input type="checkbox" name="ppn" value="10"><span> PPN ( 10% )</span>
                        @endif
                       </div>
                     </div>
                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                 <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
             </div>
         </div>
       </form>
     </div>
 </div>

 <!--Modal delete-->
  <div id="delete{{$data->inv_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
      <div class="modal-dialog">
        <form role="form" method="post" action="/pi-delete/transactionPurchasing/delete-pi">
          {{csrf_field()}}
          <input type="hidden" name="inv_id" value="{{$data->inv_id}}">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                  <h4 id="modal-responsive-label" class="modal-title">Delete pi {{$data->inv_no}}</h4></div>
              <div class="modal-body">
                  <div class="row">
                        <h3 style="text-align:center">Are you sure want to delete this pi ?</h3>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                  <button type="submit" class="btn btn-primary">Yes</button>
              </div>
          </div>
        </form>
      </div>
  </div>
  <script>
      $(".datepicker-default").datepicker();

      $(document).ready(function() {
        $('form').each(function () {
           $(this).validate();
        });
      });
  </script>
