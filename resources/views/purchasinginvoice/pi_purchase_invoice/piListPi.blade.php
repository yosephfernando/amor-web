@extends('layout.layout')
@section('content')
<!--BEGIN TITLE & BREADCRUMB PAGE-->
            <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                <div class="page-header pull-left">
                    <div class="page-title">{{$title->moduleLabel}}</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a href="#">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li><a href="#">Transaction purchasing</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li class="active">{{$title->moduleLabel}}</li>
                </ol>
                <div class="clearfix"></div>
            </div>
            <!--END TITLE & BREADCRUMB PAGE-->
           <div class="page-content">
              <div class="row">
                <div class="col-md-12">
                <div class="panel panel-green">
                        <div class="panel-body">
                          @if($errors->any())
                              <script>
                                     toastr.error("{{$errors->first()}}");
                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                          @if(session('status'))
                              <script>
                                 @if(session('status') == 'Pi added' || session('status') == 'Pi updated' || session('status') == 'Pi deleted')
                                   toastr.success("{{ session('status') }}");
                                 @else
                                   toastr.error("{{ session('status') }}");
                                 @endif

                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                            <div class="row">
                              <div class="col-md-3">
                                <div class="dropdown">
                                   @if($privilaged->canAdd != 0)
                                      <button type="button" class="btn btn-primary btn-md" data-target="#piAdd" data-toggle="modal">
                                        <i class="fa fa-plus"></i>&nbsp;
                                          Add
                                      </button>
                                   @endif
                                   @if($privilaged->canExport != 0)
                                      <a href="/exportPi/transactionPurchasing/exportPiToExcel" class="btn btn-success btn-md"><i class="fa fa-download"></i>&nbsp;Export to xls</a>
                                   @endif
                                   <div class="clear" style="height:20px"></div>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                <form method="POST" id="date-range" class="form-inline" role="form">
                                    <div class="form-group">
                                        <div class="col-md-1" style="padding-right: 0px;padding-left: 0px;">
                                          <h5>Date: </h5>
                                        </div>
                                        <div class="col-md-8" style="padding-right: 0px;padding-left: 0px;">
                                            <div class="input-group input-daterange">
                                                <input type="text" name="start_date" id="start_date"  class="form-control" />
                                                <span class="input-group-addon">to</span>
                                                <input type="text" name="end_date" id="end_date" class="form-control"/>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <button type="submit" class="btn btn-primary">Search</button>
                                        </div>
                                    </div>
                                    <div class="clear" style="height:10px"></div>
                                </form>
                              </div>
                            </div>
                            <div class="col-md-12">
                              <div class="table-responsive">
                                <table class="table table-bordered" id="pi-table">
                                      <thead>
                                          <tr>
                                            <th><input type="checkbox"  id="bulkDelete"/></th>
                                            <th>No inv</th>
                                            <th>Date</th>
                                            <th>Total price</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                          </tr>
                                      </thead>
                                </table>
                                <script>
                                  $(function() {
                                    var table = $('#pi-table').DataTable({
                                         processing: true,
                                         serverSide: true,
                                         ajax: {
                                            url: '/list-pi-data/transactionPurchasing/piListPiData',
                                            data: function (d) {
                                                d.start_date = $('input[name=start_date]').val();
                                                d.end_date = $('input[name=end_date]').val();
                                            }
                                        },
                                         columns: [
                                             {data: 'delete_bulk', name: 'delete_bulk', orderable: false, searchable: false},
                                             { data: 'inv_no', name: 'inv_no' },
                                             { data: 'inv_date', name: 'inv_date' },
                                             { data: 'total_nett', name: 'total_nett' },
                                             { data: 'inv_status', name: 'inv_status' },
                                             {data: 'action', name: 'action', className: 'text-center', orderable: false, searchable: false}
                                         ]
                                     });
                                      $('div.dataTables_filter input').addClass('form-control');

                                      $("#deleteTriger").click(function(){
                                         var allValue = [];
                                         $("input[name='inv_id[]']:checked").each( function () {
                                             allValue.push($(this).val());
                                         });
                                         $.post( "/delete-bulk-pi/transactionPurchasing/deleteBulkPi",  {'inv_id': allValue, '_token' : '{{csrf_token()}}' })
                                           .done(function( data ) {
                                             if(data == "success"){
                                                location.reload();
                                             }else{
                                                alert(data);
                                             }
                                             console.log( "Data Loaded: " + data );
                                           });
                                       });

                                       $("#bulkDelete").click(function(){
                                          $("input[name='inv_id[]']").prop("checked", $(this).prop('checked'));
                                          if($("input[name='inv_id[]']:checked").length > 0){
                                            $("#deleteTriger").show();
                                          }else{
                                            $("#deleteTriger").hide();
                                          }
                                       });

                                      $("#pi-table").on('click', ':checkbox', function(){
                                        if($("input[name='inv_id[]']:checked").length > 0){
                                          $("#deleteTriger").show();
                                        }else{
                                          $("#deleteTriger").hide();
                                        }
                                      });

                                       $('#date-range').on('submit', function(e) {
                                           table.draw();
                                           e.preventDefault();
                                       });

                                  });
                               </script>
                              </div>
                              <button class='btn btn-danger' id="deleteTriger" style="display:none">Delete checked</button>
                            </div>
                        </div>
                    </div>
                  </div>
              </div>
          </div>

          <!--Modal add user-->
           <div id="piAdd" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
               <div class="modal-dialog">
                 <form role="form" method="post" action="/pi-add/transactionPurchasing/add-pi">
                    {{csrf_field()}}
                   <div class="modal-content">
                       <div class="modal-header">
                           <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                           <h4 id="modal-responsive-label" class="modal-title">Add pi</h4></div>
                       <div class="modal-body">
                           <div class="row">
                               <div class="col-md-12">
                                   <div class="mbm">
                                      <label>Inv date :</label>
                                      <input type="text" placeholder="dd-mm-yy" name="inv_date" data-date-format="dd-mm-yyyy" value="{{date('d-m-Y')}}" class="datepicker-default form-control required"/>
                                   </div>
                                   <div class="mbm">
                                     <label>Gudang : </label>
                                         <input type="hidden"  id="wh_id" name="wh_id"  class="form-control">
                                         <input type="text"  id="autocompletewh" placeholder="Please type purchase invoice warehouse" name="wh_desc"  class="form-control required">

                                         <!-- warehouse catalog -->
                                          <script>
                                               $('#autocompletewh').devbridgeAutocomplete({
                                                       serviceUrl : '/lookupSugestion/warehouse/',
                                                       minChars : 3,
                                                       onSelect: function (suggestion) {
                                                         $('#wh_id').val(suggestion.data);
                                                         $('#autocompletewh').val(suggestion.value);
                                                       }
                                               });
                                          </script>
                                   </div>
                                   <div class="mbm">
                                     <label>Supplier :</label>
                                         <input type="hidden"  id="supp_id" name="supp_id"  class="form-control">
                                         <input type="text" id="autocompletesupp" placeholder="Please type purchase invoice supplier" name="supp_desc" class="form-control required">

                                         <!-- Barang catalog -->
                                          <script>
                                              $('#autocompletesupp').devbridgeAutocomplete({
                                                      serviceUrl : '/lookupSugestion/supplier',
                                                      minChars : 3,
                                                      onSelect: function (suggestion) {
                                                        $('#supp_id').val(suggestion.data);
                                                        $('#autocompletesupp').val(suggestion.value);
                                                      }
                                              });
                                          </script>
                                   </div>
                                   <div class="mbm">
                                      <label>No Invoice Supplier :</label>
                                      <input type="text" placeholder="Please type purchase invoice supplier no" name="no_inv_supp" class="form-control required"/>
                                   </div>
                                   <div class="mbm">
                                      <label>No Invoice Pajak :</label>
                                      <input type="text" placeholder="Please type purchase invoice pajax no" name="no_inv_pajak" class="form-control required"/>
                                   </div>
                                   <div class="mbm">
                                      <label>Reference :</label>
                                      <input type="text" placeholder="Please type purchase invoice reference" name="reference" class="form-control required"/>
                                   </div>
                                   <div class="mbm">
                                      <label>Notes :</label>
                                      <textarea class="form-control required" placeholder="Please type purchase invoice note" name="notes"></textarea>
                                   </div>
                                   <div class="mbm">
                                      <input type="checkbox" name="ppn" value="10"><span> PPN ( 10% )</span>
                                   </div>
                                 </div>
                               </div>
                           </div>
                       <div class="modal-footer">
                           <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                           <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                       </div>
                     </div>
                    </form>
                   </div>
               </div>
               <script>
                     $(document).ready(function() {
                       $('form').each(function () {
                          $(this).validate();
                       });
                     });
               </script>
@endsection
