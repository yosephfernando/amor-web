@if($privilaged->canEdit != 0)
       <button type="button" class="btn btn-default btn-xs" data-target="#cc{{$data->cost_center_id}}" data-toggle="modal" >
         <i class="fa fa-pencil"></i>&nbsp;
           Edit
       </button>
@endif
@if($privilaged->canDelete != 0)
     <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->cost_center_id}}" data-toggle="modal">
       <i class="fa fa-trash"></i>&nbsp;
         Delete
     </button>
@endif
<!--Modal edit-->
 <div id="cc{{$data->cost_center_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
     <div class="modal-dialog">
       <form role="form" method="post" action="/cost-center-edit/masterGL/edit-cost-center">
         {{csrf_field()}}
         <input type="hidden" name="cost_center_id" value="{{$data->cost_center_id}}" required>
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                 <h4 id="modal-responsive-label" class="modal-title">Edit cost center {{$data->cost_center_code}}</h4></div>
             <div class="modal-body">
                 <div class="row">
                   <div class="col-md-12">
                     <div class="mbm">
                         <label>Cost center code :</label>
                         <input type="text" placeholder="Please type the cost center code" name="cost_center_code"  class="form-control required" value="{{$data->cost_center_code}}" maxlength="6"/>
                     </div>
                     <div class="mbm">
                         <label>Cost center description :</label>
                         <input type="text" placeholder="Please type the cost center description" name="cost_center_desc"  class="form-control required" value="{{$data->cost_center_desc}}"/>
                     </div>
                     <div class="mbm">
                       <label>Cost center parent :</label>
                       <input type="text" placeholder="Optional" name="cost_center_parent" class="form-control" value="{{$data->cost_center_parent}}"/>
                     </div>
                     <div class="mbm">
                       @if($data->cost_center_active_flag == 1)
                        <input type="checkbox" name="cost_center_active_flag" value="1" checked/><span> Active</span>
                       @else
                        <input type="checkbox" name="cost_center_active_flag" value="1" /><span> Active</span>
                       @endif
                     </div>
                   </div>
                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                 <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
             </div>
         </div>
       </form>
     </div>
 </div>


 <!--Modal delete-->
  <div id="delete{{$data->cost_center_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
      <div class="modal-dialog">
        <form role="form" method="post" action="/cost-center-delete/masterGL/delete-cost-center">
          {{csrf_field()}}
          <input type="hidden" name="cost_center_id" value="{{$data->cost_center_id}}">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                  <h4 id="modal-responsive-label" class="modal-title">Delete cost center {{$data->cost_center_code}}</h4></div>
              <div class="modal-body">
                  <div class="row">
                        <h3 style="text-align:center">Are you sure want to delete this cost center ?</h3>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                  <button type="submit" class="btn btn-primary">Yes</button>
              </div>
          </div>
        </form>
      </div>
  </div>
  <script>
      $(document).ready(function() {
        $('form').each(function () {
           $(this).validate();
        });
      });
  </script>
