@if($privilaged->canEdit != 0)
       <button type="button" class="btn btn-default btn-xs" data-target="#cf{{$data->cash_flow_id}}" data-toggle="modal" >
         <i class="fa fa-pencil"></i>&nbsp;
           Edit
       </button>
@endif
@if($privilaged->canDelete != 0)
     <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->cash_flow_id}}" data-toggle="modal">
       <i class="fa fa-trash"></i>&nbsp;
         Delete
     </button>
@endif
<!--Modal edit-->
 <div id="cf{{$data->cash_flow_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
     <div class="modal-dialog">
       <form role="form" method="post" action="/cash-flow-edit/masterGL/edit-cash-flow">
         {{csrf_field()}}
         <input type="hidden" name="cash_flow_id" value="{{$data->cash_flow_id}}" required>
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                 <h4 id="modal-responsive-label" class="modal-title">Edit cash flow {{$data->cash_flow_code}}</h4></div>
             <div class="modal-body">
                 <div class="row">
                   <div class="col-md-12">
                     <div class="mbm">
                         <label>Cash flow code :</label>
                         <input type="text" placeholder="Please type the cash flow code" name="cash_flow_code"  class="form-control required" value="{{$data->cash_flow_code}}"/>
                     </div>
                     <div class="mbm">
                         <label>Cash flow description :</label>
                         <input type="text" placeholder="Please type the cash flow description" name="cash_flow_desc"  class="form-control required" value="{{$data->cash_flow_desc}}"/>
                     </div>
                     <div class="mbm">
                       <label>Cash flow parent :</label>
                       <input type="text" placeholder="Optional" name="cash_flow_parent" class="form-control" value="{{$data->cash_flow_parent}}"/>
                     </div>
                     <div class="mbm">
                       <label>Cash flow alias :</label>
                       <input type="text" placeholder="Please type the cash flow alias" name="cash_flow_alias" class="form-control required" value="{{$data->cash_flow_alias}}"/>
                     </div>
                     <div class="mbm">
                        <label>Cash flow group :</label>
                        <select name="cash_flow_group_id" class="form-control required">
                          @foreach($cfGroup as $cfgs)
                           @if($data->cash_flow_group_id == $cfgs->cash_flow_group_id)
                            <option value="{{$cfgs->cash_flow_group_id}}" selected>{{$cfgs->cash_flow_group_desc}}</option>
                           @else
                            <option value="{{$cfgs->cash_flow_group_id}}">{{$cfgs->cash_flow_group_desc}}</option>
                           @endif
                          @endforeach
                        </select>
                     </div>
                     <div class="mbm">
                        <label>Cash flow position :</label>
                        <select name="cash_flow_position" class="form-control required">
                          @if($data->cash_flow_position == 0)
                            <option value="header" selected>Header</option>
                            <option value="detail">Detail</option>
                          @else
                          <option value="header">Header</option>
                          <option value="detail" selected>Detail</option>
                          @endif
                        </select>
                     </div>
                   </div>
                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                 <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
             </div>
         </div>
       </form>
     </div>
 </div>


 <!--Modal delete-->
  <div id="delete{{$data->cash_flow_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
      <div class="modal-dialog">
        <form role="form" method="post" action="/cash-flow-delete/masterGL/delete-cash-flow">
          {{csrf_field()}}
          <input type="hidden" name="cash_flow_id" value="{{$data->cash_flow_id}}">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                  <h4 id="modal-responsive-label" class="modal-title">Delete cfount {{$data->cash_flow_code}}</h4></div>
              <div class="modal-body">
                  <div class="row">
                        <h3 style="text-align:center">Are you sure want to delete this cash flow ?</h3>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                  <button type="submit" class="btn btn-primary">Yes</button>
              </div>
          </div>
        </form>
      </div>
  </div>
  <script>
      $(document).ready(function() {
        $('form').each(function () {
           $(this).validate();
        });
      });
  </script>
