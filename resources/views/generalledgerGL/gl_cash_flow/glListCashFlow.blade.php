@extends('layout.layout')
@section('content')
<!--BEGIN TITLE & BREADCRUMB PAGE-->
            <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
                <div class="page-header pull-left">
                    <div class="page-title">{{$title->moduleLabel}}</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a href="#">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li><a href="#">General ledger management</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
                    <li class="active">{{$title->moduleLabel}}</li>
                </ol>
                <div class="clearfix"></div>
            </div>
            <!--END TITLE & BREADCRUMB PAGE-->
           <div class="page-content">
              <div class="row">
                <div class="col-md-12">
                <div class="panel panel-green">
                        <div class="panel-body">
                          @if($errors->any())
                              <script>
                                     toastr.error("{{$errors->first()}}");
                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                          @if(session('status'))
                              <script>
                                 @if(session('status') == 'Cash flow added' || session('status') == 'Cash flow updated' || session('status') == 'Cash flow deleted')
                                   toastr.success("{{ session('status') }}");
                                 @else
                                   toastr.error("{{ session('status') }}");
                                 @endif

                                     toastr.options = {
                                      "closeButton": false,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "300",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    };

                              </script>
                          @endif
                          <div class="row">
                            <div class="col-md-7">
                                @if($privilaged->canAdd != 0)
                                   <button type="button" class="btn btn-primary btn-md" data-target="#cfAdd" data-toggle="modal">
                                     <i class="fa fa-plus"></i>&nbsp;
                                       Add
                                   </button>
                                @endif
                                @if($privilaged->canExport != 0)
                                   <a href="/exportCf/masterGL/exportCfToExcel" class="btn btn-success btn-md"><i class="fa fa-download"></i>&nbsp;Export to xls</a>
                                @endif
                                <div class="clear" style="height:20px"></div>
                            </div>
                          </div>

                              <div class="col-md-12">
                                  <div class="table-responsive">
                                    <table class="table table-bordered" id="cf-table">
                                        <thead>
                                          <tr>
                                            <th><input type="checkbox"  id="bulkDelete"  /></th>
                                            <th>No cash flow</th>
                                            <th>description</th>
                                            <th>Action</th>
                                          </tr>
                                        </thead>
                                      </table>
                                      <script>
                                        $(function() {
                                          var table = $('#cf-table').DataTable({
                                               processing: true,
                                               serverSide: true,
                                               ajax: {
                                                  url: '/listCfData/masterGL/glListCashFlowDataTable',
                                               },
                                               columns: [
                                                   {data: 'delete_bulk', name: 'delete_bulk', orderable: false, searchable: false},
                                                   { data: 'cash_flow_code', name: 'cash_flow_code' },
                                                   { data: 'cash_flow_desc', name: 'cash_flow_desc' },
                                                   {data: 'action', name: 'action', className: 'text-center', orderable: false, searchable: false}
                                               ]
                                           });
                                            $('div.dataTables_filter input').addClass('form-control');

                                            $("#deleteTriger").click(function(){
                                               var allValue = [];
                                               $("input[name='cash_flow_id[]']:checked").each( function () {
                                                   allValue.push($(this).val());
                                               });
                                               $.post( "/delete-bulk-cf/masterGL/deleteBulkCf",  {'cash_flow_id': allValue, '_token' : '{{csrf_token()}}' })
                                                 .done(function( data ) {
                                                   if(data == "success"){
                                                      location.reload();
                                                   }else{
                                                      alert(data.text);
                                                   }
                                                   console.log( "Data Loaded: " + data );
                                                 }).error(function(data){
                                                   console.log( "Data Loaded: " + data.text );
                                                 });
                                             });

                                             $("#bulkDelete").click(function(){
                                                $("input[name='cash_flow_id[]']").prop("checked", $(this).prop('checked'))
                                                if($("input[name='cash_flow_id[]']:checked").length > 0){
                                                  $("#deleteTriger").show();
                                                }else{
                                                  $("#deleteTriger").hide();
                                                }
                                             });

                                             $("#cf-table").on('click', ':checkbox', function(){
                                               if($("input[name='cash_flow_id[]']:checked").length > 0){
                                                 $("#deleteTriger").show();
                                               }else{
                                                 $("#deleteTriger").hide();
                                               }
                                             });

                                             $('#search-group').on('submit', function(e) {
                                                 table.draw();
                                                 e.preventDefault();
                                             });

                                        });
                                     </script>
                                  </div>
                                  <button class='btn btn-danger' id="deleteTriger" style="display:none">Delete cheked</button>
                                  <div class="clear" style="height:20px"></div>
                              </div>
                            </div>
                        </div>
                    </div>
                  </div>
              </div>
          </div>

          <!--Modal add user-->
           <div id="cfAdd" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
               <div class="modal-dialog">
                 <form role="form" method="post" action="/cf-add/masterGL/add-cf">
                    {{csrf_field()}}
                   <div class="modal-content">
                       <div class="modal-header">
                           <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                           <h4 id="modal-responsive-label" class="modal-title">Add cash flow</h4></div>
                       <div class="modal-body">
                           <div class="row">
                               <div class="col-md-12">
                                 <div class="mbm">
                                     <label>Cash flow code :</label>
                                     <input type="text" placeholder="Please type the cash flow code" name="cash_flow_code"  class="form-control required"/>
                                 </div>
                                 <div class="mbm">
                                     <label>Cash flow description :</label>
                                     <input type="text" placeholder="Please type the cash flow description" name="cash_flow_desc"  class="form-control required"/>
                                 </div>
                                 <div class="mbm">
                                   <label>Cash flow parent :</label>
                                   <input id="cash_flow_parent" type="text" placeholder="Optional" class="form-control"/>
                                   <input id="cash_flow_parent_id" name="cash_flow_parent" type="hidden" name="cash_flow_parent"/>
                                   <!-- Barang catalog -->
                                    <script>
                                        $('#cash_flow_parent').devbridgeAutocomplete({
                                                serviceUrl : '/lookupSugestion/cash-flow',
                                                minChars : 3,
                                                onSelect: function (suggestion) {
                                                  $('#cash_flow_parent_id').val(suggestion.data);
                                                  $('#cash_flow_parent').val(suggestion.value);
                                                }
                                        });
                                    </script>
                                 </div>
                                 <div class="mbm">
                                   <label>Cash flow alias :</label>
                                   <input type="text" placeholder="Please type the cash flow alias" name="cash_flow_alias" class="form-control required"/>
                                 </div>
                                 <div class="mbm">
                                    <label>Cash flow group :</label>
                                    <select name="cash_flow_group_id" class="form-control required">
                                      @foreach($cfGroup as $data)
                                        <option value="{{$data->cash_flow_group_id}}">{{$data->cash_flow_group_desc}}</option>
                                      @endforeach
                                    </select>
                                 </div>
                                 <div class="mbm">
                                    <label>Cash flow position :</label>
                                    <select name="cash_flow_position" class="form-control required">
                                        <option value="header">Header</option>
                                        <option value="detail">Detail</option>
                                    </select>
                                 </div>
                               </div>
                           </div>
                       </div>
                       <div class="modal-footer">
                           <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                           <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save</button>
                       </div>
                   </div>
                  </form>
               </div>
           </div>
           <script>
               $(document).ready(function() {
                 $('form').each(function () {
                    $(this).validate();
                 });
               });
           </script>
@endsection
