@if($data->pr_status != "approve")
      @if($privilaged->canEdit != 0)
             <button type="button" class="btn btn-default btn-xs" data-target="#pr{{$data->pr_id}}" data-toggle="modal" >
               <i class="fa fa-pencil"></i>&nbsp;
                 Edit
             </button>
      @endif
      <a href="/add-detail-pr/transactionPurchasing/prDetailPr/{{$data->pr_id}}">
       <button type="button" class="btn btn-primary btn-xs">
         <i class="fa fa-plus"></i>&nbsp;
           Detail pr
       </button>
      </a>
      @if($privilaged->canDelete != 0)
             <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->pr_id}}" data-toggle="modal">
               <i class="fa fa-trash"></i>&nbsp;
                 Delete
             </button>
      @endif
@else
      @if($privilaged->canEdit != 0)
             <button type="button" class="btn btn-default btn-xs" style="cursor:not-allowed">
               <i class="fa fa-pencil"></i>&nbsp;
                 Edit
             </button>
      @endif
      <a href="/add-detail-pr/transactionPurchasing/prDetailPr/{{$data->pr_id}}">
       <button type="button" class="btn btn-primary btn-xs">
         <i class="fa fa-plus"></i>&nbsp;
           Detail pr
       </button>
      </a>
      @if($privilaged->canDelete != 0)
             <button type="button" class="btn btn-danger btn-xs" style="cursor:not-allowed">
               <i class="fa fa-trash"></i>&nbsp;
                 Delete
             </button>
      @endif
@endif
<?php
   $dates = strtotime($data->pr_date);
   $dateEdit = date("d-m-Y", $dates);
?>


<!--Modal edit-->
 <div id="pr{{$data->pr_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade text-left">
     <div class="modal-dialog">
       <form role="form" method="post" action="/pr-edit/transactionPurchasing/edit-pr" id="edit-form-pr{{$data->pr_id}}">
         {{csrf_field()}}
         <input type="hidden" name="pr_id" value="{{$data->pr_id}}">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                 <h4 id="modal-responsive-label" class="modal-title">Edit pr {{$data->pr_no}}</h4></div>
             <div class="modal-body">
                 <div class="row">
                     <div class="col-md-12">
                       <div class="mbm">
                          <label>Pr no :</label>
                          <input name="pr_no" type="text" class="form-control required" placeholder="please enter purchase request number" value="{{$data->pr_no}}" />
                       </div>
                       <div class="mbm">
                          <label>Notes :</label>
                          <textarea name="notes" class="form-control required" placeholder="please enter purchase request note">{{$data->notes}}</textarea>
                       </div>
                       <div class="mbm">
                          <label>Status :</label>
                          <select name="pr_status" class="form-control required">
                              <option value="{{$data->pr_status}}">{{$data->pr_status}}</option>
                              @if($privilaged->canApprove != 0)
                                <option value="approve">Approve</option>
                              @endif
                              <option value="partial">Partial</option>
                              <option value="finish">Finish</option>
                          </select>
                       </div>
                       <div class="mbm">
                          <label>Pr date :</label>
                          <input type="text" placeholder="dd-mm-yyyy" name="pr_date" data-date-format="dd-mm-yyyy" class="datepicker-default form-control required" value="{{$dateEdit}}" />
                       </div>
                     </div>
                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                 <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
             </div>
         </div>
       </form>
       <script>
           $("#edit-form-pr{{$data->pr_id}}").validate();
       </script>
     </div>
 </div>


 <!--Modal delete-->
  <div id="delete{{$data->pr_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
      <div class="modal-dialog">
        <form role="form" method="post" action="/pr-delete/transactionPurchasing/delete-pr">
          {{csrf_field()}}
          <input type="hidden" name="pr_id" value="{{$data->pr_id}}">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                  <h4 id="modal-responsive-label" class="modal-title">Delete pr {{$data->pr_no}}</h4></div>
              <div class="modal-body">
                  <div class="row">
                        <h3 style="text-align:center">Are you sure want to delete this pr ?</h3>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                  <button type="submit" class="btn btn-primary">Yes</button>
              </div>
          </div>
        </form>
      </div>
  </div>
  <script>
      $(".datepicker-default").datepicker();
  </script>
