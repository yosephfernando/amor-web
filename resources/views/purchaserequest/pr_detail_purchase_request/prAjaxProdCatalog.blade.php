@if($keyword != "null")
    {{
      buildSearch::searchMaster(
          'select ipm.prd_id, ipm.prd_desc, ips.size_code from "IM_PRD_MASTER" ipm join
           "IM_PRD_SIZE" ips on ipm.size_id = ips.size_id
           where CAST(ipm.prd_id as varchar) LIKE '."'".'%'.$keyword.'%'."'".' OR CAST(ipm.prd_desc as  varchar) LIKE '."'".'%'.$keyword.'%'."'".'',
          "Product catalog", "prodIdVal", ["prd_id", "prd_desc"]
          )
    }}
@else
    {{buildSearch::searchMaster(
          'select ipm.prd_id, ipm.prd_desc, ips.size_code from "IM_PRD_MASTER" ipm join
           "IM_PRD_SIZE" ips on ipm.size_id = ips.size_id  ORDER BY ipm.prd_id DESC LIMIT 10',
          "Product catalog", "prodIdVal", ["prd_id", "prd_desc"]
          )
    }}
@endif
