@extends('layout.layout')
@section('content')

<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb n-print">
    <div class="page-header pull-left">
        <div class="page-title">{{$title->moduleLabel}}</div>
    </div>
    <ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i>&nbsp;<a href="#">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li><a href="#">Transaction purchasing</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li><a href="#">Purchase request</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
        <li class="active">{{$title->moduleLabel}}</li>
    </ol>
    <div class="clearfix"></div>
</div>

<div class="page-content">
   <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-green">
            <div class="panel-body pan">
                    <div class="form-body pal">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        @if($errors->any())
                            <script>
                                   toastr.error("{{$errors->first()}}");
                                   toastr.options = {
                                    "closeButton": false,
                                    "debug": false,
                                    "positionClass": "toast-top-right",
                                    "onclick": null,
                                    "showDuration": "300",
                                    "hideDuration": "1000",
                                    "timeOut": "5000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                  };

                            </script>
                        @endif
                        @if(session('status'))
                            <script>
                               @if(session('status') == 'Detail pr added' || session('status') == 'Detail pr updated' || session('status') == 'Detail pr deleted')
                                 toastr.success("{{ session('status') }}");
                               @else
                                 toastr.error("{{ session('status') }}");
                               @endif

                                   toastr.options = {
                                    "closeButton": false,
                                    "debug": false,
                                    "positionClass": "toast-top-right",
                                    "onclick": null,
                                    "showDuration": "300",
                                    "hideDuration": "1000",
                                    "timeOut": "5000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                  };

                            </script>
                        @endif
                      </div>
                  @if($prId->pr_status != "approve")
                    <form action="/detailpr-add-action/transactionPurchasing/addDetailPrAction"  method="post" class="form-group">
                      {{csrf_field()}}
                       <input type="hidden" name="pr_id" value="{{$prId->pr_id}}" />
                        <div class="col-md-4 col-sm-12 col-xs-12">
                          <label>Product :</label>
                              <input type="hidden"  id="prd_id" name="prd_id"  class="form-control">
                              <input type="text" id="autocompleteprd" name="prd_desc" class="form-control required" placeholder="Please type the product article" />
                              <!-- Barang catalog -->
                               <script>
                                   $('#autocompleteprd').devbridgeAutocomplete({
                                           serviceUrl : '/lookupSugestion/product',
                                           minChars : 3,
                                           onSelect: function (suggestion) {
                                               $('#prd_id').val(suggestion.data);
                                               $('#autocompleteprd').val(suggestion.value);
                                               $('#uom_id').val(suggestion.uom_id);
                                               $('#uom_conv').val(suggestion.uom_conversion);
                                               $('#uomdesc').val(suggestion.uomdesc);
                                               $('#retail_price').val(Math.round(suggestion.retail_price));
                                           }
                                   });
                               </script>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <label>Qty :</label>
                            <input type="number" name="qty_pr" class="form-control required number" placeholder="Please type the product quantity" />
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <label>Uom desc :</label>
                            <input id="uomdesc" type="text" class="form-control required" readonly>
                        </div>
                        <input id="uom_id" type="hidden" name="uom_id" />
                        <input id="uom_conv" type="hidden" name="uom_conversion">
                        <div class="col-md-2 col-sm-12 col-xs-12">
                           <div class="clear" style="height:20px"></div>
                            <button type="submit" class="btn btn-primary form-control"><i class="fa fa-plus"></i>&nbsp;Add</button>
                            <div class="clear" style="height:20px"></div>
                        </div>
                   </form>
                @endif
                        <div class="row">
                            @if($detailPrId != null)
                              <div class="col-md-12">
                                <div class="col-md-6">
                                    <h3 style="margin-top:0px"># {{$prId->pr_no}}</h3>
                                </div>
                                <div class="col-md-6 text-right">
                                      @if($privilaged->canPrint != 0)
                                            <button class="btn btn-success" onclick="window.print();"><i class="fa fa-print"></i>&nbsp;Print</button>
                                      @endif
                                      <a href="/listPr/transactionPurchasing/prListPr">
                                        <button type="button" class="btn btn-default">Back</button>
                                      </a>
                                        <div class="clear" style="height:10px"></div>
                                </div>
                              </div>

                              <div class="col-md-12">
                                  <div class="table-responsive">
                                    <table class="table table-bordered" id="prD-table">
                                          <thead>
                                              <tr>
                                                <th><input type="checkbox"  id="bulkDelete"  /></th>
                                                <th>Nama produk</th>
                                                <th>Produk size</th>
                                                <th>Qty</th>
                                                <th>UOM</th>
                                                <th>UOM conversion</th>
                                              @if($prId->pr_status != "approve")
                                                <th>Action</th>
                                              @endif
                                              </tr>
                                          </thead>
                                    </table>
                                    <script>
                                      $(function() {
                                        var table = $('#prD-table').DataTable({
                                          dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
                                             "<'row'<'col-xs-12't>>"+
                                             "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
                                             processing: true,
                                             serverSide: true,
                                             ajax: {
                                                url: '/listDetailPrData/transactionPurchasing/prListDetailPrData/{{$prId->pr_id}}',
                                             },
                                             columns: [
                                                 {data: 'delete_bulk', name: 'delete_bulk', orderable: false, searchable: false},
                                                 { data: 'prd_desc', name: 'prd_desc' },
                                                 { data: 'size_desc', name: 'size_desc' },
                                                 { data: 'qty_pr', name: 'qty_pr' },
                                                 { data: 'uom_desc', name: 'uom_desc' },
                                                 { data: 'uom_conversion', name: 'uom_conversion' },
                                              @if($prId->pr_status != "approve")
                                                 {data: 'action', name: 'action', className: 'text-center', orderable: false, searchable: false}
                                              @endif
                                             ],
                                             paginate : false
                                         });
                                          $('div.dataTables_filter input').addClass('form-control');

                                          $("#deleteTriger").click(function(){
                                             var allValue = [];
                                             $("input[name='pr_detail_id[]']:checked").each( function () {
                                                 allValue.push($(this).val());
                                             });
                                             $.post( "/detailPr-delete-action/transactionPurchasing/deleteDetailprAction",  {'pr_detail_id': allValue, '_token' : '{{csrf_token()}}' })
                                               .done(function( data ) {
                                                 if(data == "success"){
                                                    location.reload();
                                                 }else{
                                                    alert(data);
                                                 }
                                                 console.log( "Data Loaded: " + data );
                                               });
                                           });

                                           $("#bulkDelete").click(function(){
                                              $("input[name='pr_detail_id[]']").prop("checked", $(this).prop('checked'))
                                              if($("input[name='pr_detail_id[]']:checked").length > 0){
                                                $("#deleteTriger").show();
                                              }else{
                                                $("#deleteTriger").hide();
                                              }
                                           });

                                           $("#prD-table").on('click', ':checkbox', function(){
                                             if($("input[name='pr_detail_id[]']:checked").length > 0){
                                               $("#deleteTriger").show();
                                             }else{
                                               $("#deleteTriger").hide();
                                             }
                                           });

                                           $('#search-form').on('submit', function(e) {
                                               table.draw();
                                               e.preventDefault();
                                           });

                                      });
                                   </script>
                                  </div>
                                  <button class='btn btn-danger' id="deleteTriger" style="display:none">Delete cheked</button>
                                  <div class="clear" style="height:20px"></div>
                              </div>
                            @endif
                        </div>
                    </div>
            </div>
        </div>
      </div>
     </div>
</div>
<script>
      $(document).ready(function() {
        $('form').each(function () {
           $(this).validate();
        });
      });
</script>
@include('purchaserequest/pr_detail_purchase_request/prPrintTemplateDetailPr');
@endsection
