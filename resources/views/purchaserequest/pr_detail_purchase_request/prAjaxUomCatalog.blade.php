<label>UOM :</label>
<select id="uom_id" name="uom_id" class="form-control">
  <option>-- choose --</option>
  <option value="{{$uomFromPrd->uom_id1}}">{{$uomFromPrd->desc1}}</option>
  <option value="{{$uomFromPrd->uom_id2}}">{{$uomFromPrd->desc2}}</option>
</select>
<script>
    $("#uom_id").change(function(){
      var conv = "";
      if($("#uom_id").val() == "{{$uomFromPrd->uom_id2}}"){
          conv = "{{number_format($uomFromPrd->uom_conversion)}}";
      }else{
          conv = 1;
      }
      $("#uom_conv").val(conv);
    });
</script>
