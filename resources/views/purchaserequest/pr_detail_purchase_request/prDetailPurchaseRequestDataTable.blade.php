@if($pr_status->pr_status != "approve")
    @if($privilaged->canEdit != 0)
         <button type="button" class="btn btn-default btn-xs" data-target="#dp{{$data->pr_detail_id}}" data-toggle="modal" >
           <i class="fa fa-pencil"></i>&nbsp;
             Edit
         </button>
     @endif
     @if($privilaged->canDelete != 0)
         <button type="button" class="btn btn-danger btn-xs" data-target="#delete{{$data->pr_detail_id}}" data-toggle="modal">
           <i class="fa fa-trash"></i>&nbsp;
             Delete
         </button>
     @endif
@endif
 <!--Modal edit-->
  <div id="dp{{$data->pr_detail_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
      <div class="modal-dialog">
        <form role="form" method="post" action="/detail-pr-edit/transactionPurchasing/edit-dpr">
          {{csrf_field()}}
          <input type="hidden" name="pr_detail_id" value="{{$data->pr_detail_id}}">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                  <h4 id="modal-responsive-label" class="modal-title">Edit detail pr {{$data->pr_detail_id}}</h4></div>
              <div class="modal-body">
                  <div class="row">
                      <div class="col-md-12">
                        <div class="mbm">
                            <label>UOM :</label>
                            <select name="uom_id" class="form-control required">
                                  <option value="{{$data->uom_id}}" selected>{{$data->uom_desc}}</option>
                               @foreach($uoms as $uom)
                                  <option value="{{$uom->uom_id}}">{{$uom->uom_desc}}</option>
                               @endforeach
                            </select>
                        </div>
                        <div class="mbm">
                          <label>UOM  conversion:</label>
                          <input type="number" name="uom_conversion" class="form-control required" value="{{number_format($data->uom_conversion)}}" placeholder="Please type the product uom conversion" />
                        </div>
                        <div class="mbm">
                          <label>Qty :</label>
                          <input type="number" name="qty_pr" class="form-control required" value="{{number_format($data->qty_pr)}}" placeholder="Please type the product quantity"  />
                        </div>
                      </div>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                  <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;Save changes</button>
              </div>
          </div>
        </form>
      </div>
  </div>

 <!--Modal delete-->
  <div id="delete{{$data->pr_detail_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade  text-left">
      <div class="modal-dialog">
        <form role="form" method="post" action="/detail-pr-delete/transactionPurchasing/delete-detail-pr">
          {{csrf_field()}}
          <input type="hidden" name="pr_detail_id" value="{{$data->pr_detail_id}}">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                  <h4 id="modal-responsive-label" class="modal-title">Delete detail pr {{$data->pr_detail_id}}</h4></div>
              <div class="modal-body">
                  <div class="row">
                        <h3 style="text-align:center">Are you sure want to delete this detail pr ?</h3>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                  <button type="submit" class="btn btn-primary">Yes</button>
              </div>
          </div>
        </form>
      </div>
  </div>

  <script>
        $(document).ready(function() {
          $('form').each(function () {
             $(this).validate();
          });
        });
  </script>
