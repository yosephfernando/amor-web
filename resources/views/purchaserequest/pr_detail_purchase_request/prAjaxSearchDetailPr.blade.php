<h3>Search result of '{{$keyword}}'</h3>
<table id="beforeDelete" class="table table-striped table-bordered table-hover">
  <div class="clear" style="height:10px"></div>
    <thead>
    <tr>
      <th>#</th>
      <th>Nama produk</th>
      <th>Produk size</th>
      <th>Qty</th>
      <th>UOM</th>
      <th>UOM conversion</th>
      <th>Action</th>
    </tr>
    </thead>
    <tbody>
      <?php $i = 1;?>
      @foreach($detailPr as $data)
              <tr>
                <td>{{$i}}</td>
                <td>{{$data->prd_desc}}</td>
                <td>{{$data->size_desc}}</td>
                <td>{{number_format($data->qty_pr)}}</td>
                <td>{{$data->uom_desc}}</td>
                <td>{{number_format($data->uom_conversion)}}</td>
                <td>
                        @if($privilaged->canEdit != 0)
                               <button type="button" class="btn btn-default btn-md" data-target="#dp{{$data->pr_detail_id}}" data-toggle="modal" >
                                 <i class="fa fa-edit"></i>&nbsp;
                                   Edit
                               </button>
                       @endif
                       @if($privilaged->canDelete != 0)
                             <button type="button" class="btn btn-danger btn-md" data-target="#delete{{$data->pr_detail_id}}" data-toggle="modal">
                                 Delete
                             </button>
                       @endif
              </tr>

              <!--Modal edit-->
               <div id="dp{{$data->pr_detail_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
                   <div class="modal-dialog">
                       <input type="hidden" id="pr_detail_id{{$data->pr_detail_id}}" value="{{$data->pr_detail_id}}" />
                       <div class="modal-content">
                           <div class="modal-header">
                               <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                               <h4 id="modal-responsive-label" class="modal-title">Edit detail pr {{$data->pr_detail_id}}</h4>
                           </div>
                           <div class="modal-body">
                               <div class="row">
                                   <div class="col-md-12">
                                       <h4 id="message{{$data->pr_detail_id}}" style="display:none">Data updated</h4>
                                       <h4 id="error{{$data->pr_detail_id}}" style="display:none">Error updating</h4>
                                           <div class="mbm">
                                               <label>UOM :</label>
                                               <select id="uom_id{{$data->pr_detail_id}}" class="form-control">
                                                     <option value="{{$data->uom_id}}" selected>{{$data->uom_desc}}</option>
                                                  @foreach($uoms as $uom)
                                                     <option value="{{$uom->uom_id}}">{{$uom->uom_desc}}</option>
                                                  @endforeach
                                               </select>
                                           </div>
                                           <div class="mbm">
                                             <label>UOM  conversion:</label>
                                             <input type="number" id="uom_conversion{{$data->pr_detail_id}}" class="form-control" value="{{number_format($data->uom_conversion)}}"/>
                                           </div>
                                           <div class="mbm">
                                             <label>Qty :</label>
                                             <input type="number" id="qty_pr{{$data->pr_detail_id}}" class="form-control" value="{{number_format($data->qty_pr)}}" />
                                           </div>
                                     </div>
                                   </div>
                               </div>
                           <div class="modal-footer">
                               <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                               <button id="submit{{$data->pr_detail_id}}" class="btn btn-primary">Save changes</button>
                           </div>
                        </div>
                       </div>

                       <!-- ajax post data -->
                          <script>
                                $("#message{{$data->pr_detail_id}}").hide();
                                $("#error{{$data->pr_detail_id}}").hide();
                                $("#submit{{$data->pr_detail_id}}").click(function(){
                                  var id = $("#pr_detail_id{{$data->pr_detail_id}}").val();
                                  var uom_id = $("#uom_id{{$data->pr_detail_id}}").val();
                                  var uom_conversion = $("#uom_conversion{{$data->pr_detail_id}}").val();
                                  var qty_pr = $("#qty_pr{{$data->pr_detail_id}}").val();
                                  if(id != "" || uom_id != "" || uom_conversion != "" || qty_pr != ""){
                                    $.post('/detail-pr-edit/purchaseRequest/edit-dpr', {'pr_detail_id' : id, 'uom_id' : uom_id, 'uom_conversion' : uom_conversion, 'qty_pr' : qty_pr, '_token' : '{{csrf_token()}}' })
                                    .done(function(data){
                                        $("#message{{$data->pr_detail_id}}").show();
                                     })
                                     .error(function(xhr, status, error){
                                        $("#error{{$data->pr_detail_id}}").show();
                                     });
                                  }else{
                                    console.log("failed");
                                  }
                                });

                                $("#dp{{$data->pr_detail_id}}").on('hidden.bs.modal', function(e){
                                  $("#message{{$data->pr_detail_id}}").hide();
                                  $("#error{{$data->pr_detail_id}}").hide();
                                });
                          </script>
                       <!-- end of ajax post data -->

                   </div>
               </div>


               <!--Modal delete-->
                <div id="delete{{$data->pr_detail_id}}" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
                    <div class="modal-dialog">
                        <input type="hidden" id="pr_detail_id{{$data->pr_detail_id}}" value="{{$data->pr_detail_id}}">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close">&times;</button>
                                <h4 id="modal-responsive-label" class="modal-title">Delete detail pr {{$data->pr_detail_id}}</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                      <h3 id="messageDelete{{$data->pr_detail_id}}" style="text-align:center">Are you sure want to delete this detail pr ?</h3>
                                </div>
                            </div>
                            <div id="beforeDel{{$data->pr_detail_id}}" class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-default">No</button>
                                <button id="deleteSubmit{{$data->pr_detail_id}}" type="submit" class="btn btn-primary">Yes</button>
                            </div>
                            <div id="afterDel{{$data->pr_detail_id}}" class="modal-footer" style="display:none">
                                <button type="button" data-dismiss="modal" class="btn btn-default">Ok</button>
                            </div>
                        </div>

                      <!-- ajax post data -->
                         <script>
                               $("#deleteSubmit{{$data->pr_detail_id}}").click(function(){
                                 var pr_detail_id = $("#pr_detail_id{{$data->pr_detail_id}}").val();
                                 if(pr_detail_id != ""){
                                   $.post('/detail-pr-delete/purchaseRequest/delete-detail-pr', {'pr_detail_id' : pr_detail_id, '_token' : '{{csrf_token()}}' })
                                   .done(function(data){
                                       $("#beforeDel{{$data->pr_detail_id}}").hide();
                                       $("#afterDel{{$data->pr_detail_id}}").show();
                                       $("#messageDelete{{$data->pr_detail_id}}").text('Data deleted');
                                    })
                                    .error(function(xhr, status, error){
                                       $("#beforeDel{{$data->pr_detail_id}}").hide();
                                       $("#afterDel{{$data->pr_detail_id}}").show();
                                       $("#messageDelete{{$data->pr_detail_id}}").text('Error deleting');
                                    });
                                 }else{
                                   console.log("failed");
                                 }
                               });

                               $("#dp{{$data->pr_detail_id}}").on('hidden.bs.modal', function(e){
                                 $("#messageDelete{{$data->pr_detail_id}}").text('Are you sure want to delete this detail pr ?');
                                 $("#beforeDel{{$data->pr_detail_id}}").show();
                                 $("#afterDel{{$data->pr_detail_id}}").hide();
                               });
                         </script>
                      <!-- end of ajax post data -->

                    </div>
                </div>
            <?php $i++ ?>
       @endforeach
    </tbody>
</table>
