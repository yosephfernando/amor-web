<style>
    #print{display:none}
    @media print{
      @page {
        size: landscape;
        margin-left: 10mm;
        margin-right: 0px;
        margin-top: 10mm;
        margin-bottom: 0px;
      }
      .page-content {display:none}
      #print {display:block}
      #page-wrapper{border : 0px !important}
    }
</style>

<div id="print" class="container-fluid">
  @if($privilaged->canPrint != 0)
  <div class="col-md-12">
       <h3>COMPANY NAME</h3>
       <div class="col-sm-6 row">
         <p style="text-align:left;font-size:large">no. pr : {{$prId->pr_no}}</p>
         <p style="text-align:left;font-size:large">tanggal transaksi :
           <?php
             $created_date = $prId->created_date;
             $created_date = strToTime($created_date);
             $created_date = date("d M Y", $created_date);
             echo $created_date;
            ?>
         </p>
       </div>
       <div class="col-sm-6">
             <p style="text-align:right;font-size:large">tanggal cetak : {{date("M d Y")}}</p>
             <p style="text-align:right;font-size:large">dibuat oleh : {{$prId->name}}</p>
       </div>
       <div class="col-sm-12 row">
             <p style="text-align:left;font-size:large">keterangan : {{$prId->notes}}</p>
       </div>
   </div>
   <div class="col-md-12">
       <div class="clear" style="height:50px"></div>
       <table class="table table-striped table-bordered table-hover">
         <thead>
           <tr>
             <th>#</th>
             <th>Kode barang</th>
             <th>Nama barang</th>
             <th>Qty</th>
             <th>Satuan</th>
           </tr>
         </thead>
         <?php $i = 1 ?>
         @foreach($detailPr as $data)
           <tr>
             <td>{{$i}}</td>
             <td>{{$data->prd_code}}</td>
             <td>{{$data->prd_desc}}</td>
             <td>{{number_format($data->qty_pr)}}</td>
             <td>{{$data->uom_desc}}</td>
           </tr>
          <?php $i++ ?>
         @endforeach
            <tr>
              <td colspan=2></td>
              <th>Total</th>
              <td>{{number_format($sumPr)}}</td>
              <td ></td>
            </tr>
       </table>
   </div>
   <div class="col-md-12 row">
      <div class="col-sm-4">
          <span>Dibuat/diminta oleh, </span>
          <div class="clear" style="height:100px"></div>
          ................................
      </div>
      <div class="col-sm-4">
          <span>Disetujui oleh, </span>
          <div class="clear" style="height:100px"></div>
          ................................
      </div>
      <div class="col-sm-4">
          <span>Diketahui oleh, </span>
          <div class="clear" style="height:100px"></div>
          ................................
      </div>
   </div>
  @else
    <h3 class="text-center">Not Permitted</h3>
  @endif

</div>
