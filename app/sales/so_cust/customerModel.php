<?php

namespace App\sales\so_cust;

use Illuminate\Database\Eloquent\Model;

class customerModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'SO_CUST_MASTER';
    protected $guarded = [''];
    protected $primaryKey = 'cust_id';
}
