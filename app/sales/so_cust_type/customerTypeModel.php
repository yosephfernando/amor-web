<?php

namespace App\sales\so_cust_type;

use Illuminate\Database\Eloquent\Model;

class customerTypeModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'SO_CUST_TYPE';
    protected $guarded = [''];
    protected $primaryKey = 'cust_type_id';
}
