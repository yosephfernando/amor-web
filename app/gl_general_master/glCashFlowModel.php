<?php

namespace App\gl_general_master;

use Illuminate\Database\Eloquent\Model;

class glCashFlowModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'GL_CASH_FLOW';
    protected $guarded = [''];
    protected $primaryKey = 'cash_flow_id';
}
