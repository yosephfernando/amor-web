<?php

namespace App\gl_general_master;

use Illuminate\Database\Eloquent\Model;

class glCostCenterModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'GL_COST_CENTER';
    protected $guarded = [''];
    protected $primaryKey = 'cost_center_id';
}
