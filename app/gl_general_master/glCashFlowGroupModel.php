<?php

namespace App\gl_general_master;

use Illuminate\Database\Eloquent\Model;

class glCashFlowGroupModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'GL_CASH_FLOW_GROUP';
    protected $guarded = [''];
    protected $primaryKey = 'cash_flow_group_id';
}
