<?php

namespace App\usermanagement\um_user_group;

use Illuminate\Database\Eloquent\Model;

class umUserGroupModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'GS_USER_GROUP';
    protected $fillable = ['groupName', 'created_at', 'updated_at'];
    protected $primaryKey = 'userGroupId';
}
