<?php

namespace App\usermanagement\um_user;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class umUserModel extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   const CREATED_AT = 'created_date';
   const UPDATED_AT  = 'updated_date';

    protected $table = 'GS_USERS';
    protected $fillable = [
        'name', 'email', 'password', 'userGroupId'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'userGroupId'
    ];
}
