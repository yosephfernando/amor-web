<?php

namespace App\usermanagement\um_module;

use Illuminate\Database\Eloquent\Model;

class umModuleModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'GS_MODULES';
    protected $fillable = ['moduleName', 'moduleView', 'moduleMethod', 'moduleLink'];
    protected $primaryKey = 'moduleId';
}
