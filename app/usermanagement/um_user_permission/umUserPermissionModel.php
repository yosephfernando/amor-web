<?php

namespace App\usermanagement\um_user_permission;

use Illuminate\Database\Eloquent\Model;

class umUserPermissionModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'GS_PERMISSIONS';
    protected $fillable = ['permissionModule', 'canAdd', 'canEdit', 'canDelete', 'canPrint', 'canExport', 'canApprove'];
    protected $primaryKey = 'permissionId';
}
