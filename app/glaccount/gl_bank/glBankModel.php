<?php

namespace App\glaccount\gl_bank;

use Illuminate\Database\Eloquent\Model;

class glBankModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'GL_BANK_MASTER';
    protected $guarded = [''];
    protected $primaryKey = 'bank_id';
}
