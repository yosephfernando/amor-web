<?php

namespace App\glaccount\gl_account;

use Illuminate\Database\Eloquent\Model;

class glAccountModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'GL_ACCOUNT';
    protected $guarded = [''];
    protected $primaryKey = 'coa_id';
}
