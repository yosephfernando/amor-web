<?php

namespace App\glaccount\gl_currency;

use Illuminate\Database\Eloquent\Model;

class glCurrencyModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'GL_CURR_MASTER';
    protected $guarded = [''];
    protected $primaryKey = 'curr_id';
}
