<?php

namespace App\glaccount\gl_account_group;

use Illuminate\Database\Eloquent\Model;

class glAccountGroupModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'GL_ACCOUNT_GROUP';
    protected $guarded = [''];
    protected $primaryKey = 'coa_group_id';
}
