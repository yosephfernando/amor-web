<?php

namespace App\glaccount\gl_currency_rate;

use Illuminate\Database\Eloquent\Model;

class glCurrencyRateModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'GL_CURR_RATE_MASTER';
    protected $guarded = [''];
    protected $primaryKey = 'curr_rate_id';
}
