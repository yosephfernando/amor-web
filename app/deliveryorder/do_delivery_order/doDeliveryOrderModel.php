<?php

namespace App\deliveryorder\do_delivery_order;

use Illuminate\Database\Eloquent\Model;

class doDeliveryOrderModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'DO_HEADER';
    protected $guarded = [''];
    protected $primaryKey = 'do_id';
}
