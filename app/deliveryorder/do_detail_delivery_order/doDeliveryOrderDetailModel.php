<?php

namespace App\deliveryorder\do_detail_delivery_order;

use Illuminate\Database\Eloquent\Model;

class doDeliveryOrderDetailModel extends Model
{
    protected $table = 'DO_DETAIL';
    protected $guarded = [''];
    protected $primaryKey = 'do_detail_id';

    public $timestamps = false;
}
