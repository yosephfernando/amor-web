<?php

namespace App\Http\Controllers\usermanagement;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\usermanagement\um_module\umModuleModel;
use App\usermanagement\um_user_group\umUserGroupModel;
use App\usermanagement\um_user_permission\umUserPermissionModel;
use App\usermanagement\um_user\umUserModel;
use Illuminate\Support\Facades\Auth;
use App\Traits\privilaged;

class umUserGroup extends Controller
{
    use privilaged;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listUserGroup($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;
        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        $userGroups = umUserGroupModel::select('groupName', 'userGroupId', 'groupDashboard')
                                ->paginate(15);
        $groupsOption = umUserGroupModel::select('userGroupId', 'groupName')->get();
        $usersOption = umUserModel::select('id', 'email')->get();

        return view('usermanagement.um_user_group.'.$view, [
            'userGroupId' => $userGroupId,
            'listModules' => $listModules,
            'title' => $userModule,
            'userGroupData' => $userGroups,
            'groupsOption' => $groupsOption,
            'usersOption' => $usersOption,
            'privilaged' => $priv
        ]);
    }

    public function seacrhUsergroupAjax($view, $keyword)
    {
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      $userGroups = umUserGroupModel::select('groupName', 'userGroupId', 'groupDashboard')
                                     ->where('groupName', 'like', '%'.$keyword.'%')
                                     ->get();

      if($priv != null && $priv->canView == 1){
        return view('usermanagement.um_user_group.'.$view, [
          'title' => $userModule,
          'userGroupData' => $userGroups,
          'privilaged' => $priv,
          'keyword' => $keyword
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function addGroup(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
         try {
            $userDataGroup = new umUserGroupModel();
            $userDataGroup->groupName = $request->groupName;
            $userDataGroup->groupDashboard = $request->groupDashboard;
            $userDataGroup->created_date = \Carbon\Carbon::now();
            $userDataGroup->updated_date = \Carbon\Carbon::now();
            $userDataGroup->created_by = $userId;
            $insert =  $userDataGroup->save();

            return Redirect::back()->with('status', 'Group added');
          } catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error inserting');
          }
      }

    }

    public function editGroup(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);
      $groupId = $request->id;
      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
         try {
            $userDataGroup = new umUserGroupModel();
            $userDataGroup::where('userGroupId', $groupId)
                       ->update([
                              'groupName'=>$request->groupName,
                              'groupDashboard'=>$request->groupDashboard,
                              'updated_date' => \Carbon\Carbon::now(),
                              'updated_by' => $userId
                        ]);

            return Redirect::back()->with('status', 'Group updated');
         } catch(\Illuminate\Database\QueryException $ex){
           return Redirect::back()->with('status', 'Error updating');
         }
      }

    }

    public function assignUserToGroup(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);
      $groupId = $request->group;
      $userIds = $request->user;

      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
         try {
            $userAssignGroup = new umUserModel();
            $userAssignGroup::whereIn('id', $userIds)
                       ->update([
                              'userGroupId'=>$groupId,
                              'updated_date' => \Carbon\Carbon::now(),
                              'updated_by' => $userId
                        ]);

            return Redirect::back()->with('status', 'Group updated');
         } catch(\Illuminate\Database\QueryException $ex){
           return Redirect::back()->with('status', 'Error updating');
         }
      }

    }

    public function deleteGroup(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idGroup = $request->userGroupId;

      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
            $userDataGroup = new umUserGroupModel();
            $userDataGroup::where('userGroupId', $idGroup)->delete();

            $permissionData = new umUserPermissionModel();
            $permissionData::where('userGroupId', $idGroup)->delete();

            return Redirect::back()->with('status', 'Group deleted');
        } catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error deleting');
        }
      }

    }

    public function deleteBulkUserGroup(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $id = $request->userGroupId;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $userData = new umUserGroupModel();
        $userData::whereIn('userGroupId', $id)->delete();

        $permissionData = new umUserPermissionModel();
        $permissionData::whereIn('userGroupId', $id)->delete();
        $status = "success";

      }
      return $status;
    }
}
