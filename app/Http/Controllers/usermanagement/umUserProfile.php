<?php

namespace App\Http\Controllers\usermanagement;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\usermanagement\um_module\umModuleModel;
use App\usermanagement\um_user\umUserModel;
use App\usermanagement\um_user_group\umUserGroupModel;
use Illuminate\Support\Facades\Storage;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Traits\privilaged;
use App\Traits\excelExport;

class umUserProfile extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getImage($view, $userId){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $listModules = $this->listModules($userGroupId);

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      $data = umUserModel::select('profpic')->where('id',$userId)->first();
      $contents = Storage::get($data->profpic);

      if($priv != null && $priv->canView == 1){
        return response($contents, 200)->header('Content-Type', 'image/jpeg');
      }

    }

    public function listUser($view, $userId)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);
        $users = umUserModel::join('GS_USER_GROUP', 'GS_USER_GROUP.userGroupId', '=', 'GS_USERS.userGroupId')
                       ->select('GS_USERS.id', 'GS_USERS.name', 'GS_USERS.email', 'GS_USER_GROUP.groupName','GS_USERS.userGroupId', 'GS_USERS.profpic')
                       ->where('GS_USERS.id', $userId)
                       ->first();

        if($priv != null && $priv->canView == 1){
          return view('usermanagement.um_user_profile.'.$view, [
              'userGroupId' => $userGroupId,
              'listModules' => $listModules,
              'title' => $userModule,
              'userData' => $users,
              'privilaged' => $priv
          ]);
        }else{
           return Redirect::back()->withErrors(['Not permitted']);
        }
    }

    public function postProfPic(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      if($priv->canEdit != 0){
        $rules = array(
            'picture' => 'required | mimes:jpeg,jpg,png | max:3000'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return $validator->errors();
        }else{
          $path = $request->file('picture')->storeAs('picture', $userId.".".$request->picture->extension(), 'local');
          $updateUser = umUserModel::where('id', $userId)->update(['profpic' => $path]);
          return "sukses";
        }

      }else{
        return "not permitted";
      }
    }

    public function editUser(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idUser = $request->id;

      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
            $userData = new umUserModel();
            $userData::where('id', $idUser)
                       ->update([
                              'name'=>$request->name,
                              'email'=>$request->email,
                              'updated_date' => \Carbon\Carbon::now(),
                              'updated_by' => $userId
                        ]);

          }catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error updating');
          }
          return Redirect::back()->with('status', 'Profile updated');
      }

    }

    public function changePassword(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idUser = $request->id;
      $olPassword =  $request->ol_password;
      $newPassword = Hash::make($request->new_password);
      $password = umUserModel::select('password')->where('id', $idUser)->first();

      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
          if($olPassword != "" && Hash::check($olPassword, $password->password)){
            try {
                $userData = new umUserModel();
                $userData::where('id', $idUser)
                           ->update([
                                  'password'=>$newPassword,
                                  'updated_date' => \Carbon\Carbon::now(),
                                  'updated_by' => $userId
                            ]);

              }catch(\Illuminate\Database\QueryException $ex){
                return Redirect::back()->with('status', 'Error changing password');
              }
              return Redirect::back()->with('status', 'Password changed');
          }else{
            return Redirect::back()->with('status', 'Your old password is invalid');
          }
      }
    }
}
