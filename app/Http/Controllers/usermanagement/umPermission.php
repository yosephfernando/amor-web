<?php

namespace App\Http\Controllers\usermanagement;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\usermanagement\um_module\umModuleModel;
use App\usermanagement\um_user_group\umUserGroupModel;
use App\usermanagement\um_user_permission\umUserPermissionModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Traits\privilaged;

class umPermission extends Controller
{
    use privilaged;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function seacrhPermissionAjax($view, $keyword)
    {
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      $permission = umUserPermissionModel::join('GS_MODULES', 'GS_MODULES.moduleId', '=', 'GS_PERMISSIONS.moduleId')
                                 ->select('GS_MODULES.moduleMethod', 'GS_MODULES.moduleName', 'GS_MODULES.moduleParent','GS_PERMISSIONS.canAdd', 'GS_PERMISSIONS.canDelete', 'GS_PERMISSIONS.canEdit', 'GS_PERMISSIONS.canView', 'GS_PERMISSIONS.canPrint', 'GS_PERMISSIONS.canExport', 'GS_PERMISSIONS.canApprove', 'GS_PERMISSIONS.permissionId')
                                 ->where('GS_MODULES.moduleName', 'LIKE','%'.$keyword.'%')
                                 ->where('GS_MODULES.moduleShowInSidebar', '!=', null)
                                 ->where('GS_MODULES.moduleParent', '!=', null)
                                 ->orWhere(function($query) use ($keyword){
                                   $query->where('GS_MODULES.moduleMethod', 'LIKE','%'.$keyword.'%')
                                         ->where('GS_MODULES.moduleShowInSidebar', '!=', null)
                                         ->where('GS_MODULES.moduleParent', '!=', null);
                                 })
                                 ->orderBy('GS_MODULES.moduleParent', 'ASC')
                                 ->get();

      if($priv != null && $priv->canView == 1){
        return view('usermanagement.um_user_permission.'.$view, [
          'title' => $userModule,
          'permission' => $permission,
          'privilaged' => $priv,
          'keyword' => $keyword
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function addPermissionAction(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $modIdRequest = $request->moduleId;
      $groupIdRequest = $request->groupId;
      $moduleGroup = umModuleModel::select('moduleGroup')
                                    ->where('moduleId',$modIdRequest)
                                    ->first();

      $moduleBulkInsert = null;
      if($moduleGroup->moduleGroup != ""){
        $moduleBulkInsert = umModuleModel::select('moduleId')->where('moduleGroup', $moduleGroup->moduleGroup)->get();
      }


      $cekPermission = umUserPermissionModel::select('moduleId')->where(['moduleId'=>$modIdRequest, 'userGroupId'=>$groupIdRequest])->first();

      $priv = $this->checkPrivilaged($module_id, $userGroupId);
      if($priv != null && $priv->canAdd == 1){
        if($cekPermission == null){
          if($moduleBulkInsert != null){
            foreach($moduleBulkInsert as $insert){
                 $permissionData = new umUserPermissionModel();
                 $permissionData->moduleId = $insert->moduleId;
                 $permissionData->userGroupId = $groupIdRequest;
                 $permissionData->canAdd = $request->input('canAdd1', 0);
                 $permissionData->canEdit = $request->input('canEdit1', 0);
                 $permissionData->canDelete =  $request->input('canDelete1', 0);
                 $permissionData->canView =   $request->input('canView1', 0);
                 $permissionData->canPrint =   $request->input('canPrint1', 0);
                 $permissionData->canExport =   $request->input('canExport1', 0);
                 $permissionData->canApprove =   $request->input('canApprove1', 0);
                 $permissionData->created_date = \Carbon\Carbon::now();
                 $permissionData->updated_date = \Carbon\Carbon::now();
                 $permissionData->created_by = $userId;
                 $insert =  $permissionData->save();
             }
          }else{
            $permissionData = new umUserPermissionModel();
            $permissionData->moduleId = $modIdRequest;
            $permissionData->userGroupId = $groupIdRequest;
            $permissionData->canAdd = $request->input('canAdd1', 0);
            $permissionData->canEdit = $request->input('canEdit1', 0);
            $permissionData->canDelete =  $request->input('canDelete1', 0);
            $permissionData->canView =   $request->input('canView1', 0);
            $permissionData->canPrint =   $request->input('canPrint1', 0);
            $permissionData->canExport =   $request->input('canExport1', 0);
            $permissionData->canApprove =   $request->input('canApprove1', 0);
            $permissionData->created_date = \Carbon\Carbon::now();
            $permissionData->updated_date = \Carbon\Carbon::now();
            $permissionData->created_by = $userId;
            $insert =  $permissionData->save();
          }
          return Redirect::back()->with('status', 'Permission added');
         }else{
           return Redirect::back()->with('status', 'Permission already exist');
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function addPermission($view, $groupId){
      $userGroupId = Auth::user()->userGroupId;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $moduleAll = umModuleModel::select(DB::raw('CAST("moduleId" AS varchar) AS data'), DB::raw('(CASE WHEN "moduleParent" IS NULL THEN "moduleName"||'."'-'".'||'."'( Parent )'".' ELSE "moduleMethod"||'."'-'".'||"moduleParent" END) AS value'))
                                ->where('moduleShowInSidebar', 1)
                                ->groupBy('moduleGroup', 'moduleParent', 'moduleMethod', 'moduleId', 'moduleName')
                                ->get();

     $permisison = umUserPermissionModel::join('GS_MODULES', 'GS_MODULES.moduleId', '=', 'GS_PERMISSIONS.moduleId')
                                ->select( 'GS_MODULES.moduleLabel','GS_MODULES.moduleMethod', 'GS_MODULES.moduleName', 'GS_MODULES.moduleParent','GS_PERMISSIONS.canAdd', 'GS_PERMISSIONS.canDelete', 'GS_PERMISSIONS.canEdit', 'GS_PERMISSIONS.canView', 'GS_PERMISSIONS.canPrint', 'GS_PERMISSIONS.canExport', 'GS_PERMISSIONS.canApprove', 'GS_PERMISSIONS.permissionId')
                                ->where('GS_PERMISSIONS.userGroupId', $groupId)
                                ->where('GS_MODULES.moduleShowInSidebar', 1)
                                ->where('GS_MODULES.moduleParent', '!=', null)
                                ->where('GS_MODULES.moduleGroup', '!=', null)
                                ->orderBy('GS_MODULES.moduleParent', 'ASC')
                                ->paginate(20);

      $groupLabel = umUserGroupModel::select('groupName')->where('userGroupId', $groupId)->first();

      $listModules = $this->listModules($userGroupId);

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('usermanagement.um_user_permission.'.$view, [
            'userGroupId' => $userGroupId,
            'listModules' => $listModules,
            'title' => $userModule,
            'privilaged' => $priv,
            'moduleAll' => $moduleAll,
            'permission' => $permisison,
            'groupLabel' => $groupLabel,
            'groupId' => $groupId
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function updatePermission(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);
      $permissionId = $request->id;

      $moduleId = umUserPermissionModel::select('moduleId', 'userGroupId')->where('permissionId', $permissionId)->first();
      $userGroupIds = $moduleId->userGroupId;
      $moduleGroup = umModuleModel::select('moduleGroup')
                                    ->where('moduleId', $moduleId->moduleId)
                                    ->where('moduleGroup' , '!=', null)
                                    ->first();
      if($moduleGroup == null){
        try {
          $permissionData = new umUserPermissionModel();
          $permissionData::where(['userGroupId' => $userGroupIds, 'permissionId' => $permissionId])
                     ->update([
                           'canAdd'=>$request->input('canAdd', 0),
                           'canEdit'=>$request->input('canEdit', 0),
                           'canDelete'=>$request->input('canDelete', 0),
                           'canView'=>$request->input('canView', 0),
                           'canPrint'=>$request->input('canPrint', 0),
                           'canExport'=>$request->input('canExport', 0),
                           'canApprove'=>$request->input('canApprove', 0),
                           'updated_date' => \Carbon\Carbon::now(),
                           'updated_by' => $userId
                      ]);

            return "success";
        } catch(\Illuminate\Database\QueryException $ex){
            return "error";
        }
      }else{
        $modules = umModuleModel::select('moduleId')->where('moduleGroup', $moduleGroup->moduleGroup)->get();
      }

      if($priv->canEdit != 1){
        return 'Not permitted';
      }else{
        foreach($modules as $module){
            $id = $module->moduleId;
            try {
              $permissionData = new umUserPermissionModel();
              $permissionData::where(['userGroupId' => $userGroupIds, 'moduleId' => $id])
                         ->update([
                               'canAdd'=>$request->input('canAdd', 0),
                               'canEdit'=>$request->input('canEdit', 0),
                               'canDelete'=>$request->input('canDelete', 0),
                               'canView'=>$request->input('canView', 0),
                               'canPrint'=>$request->input('canPrint', 0),
                               'canExport'=>$request->input('canExport', 0),
                               'canApprove'=>$request->input('canApprove', 0),
                               'updated_date' => \Carbon\Carbon::now(),
                               'updated_by' => $userId
                          ]);
            } catch(\Illuminate\Database\QueryException $ex){
                return "error";
            }
        }
        return "success";
      }

    }

    public function deletePermissionAction(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idPermission = $request->permissionId;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $permissionData = new umUserPermissionModel();
        $permissionData::whereIn('permissionId', $idPermission)->delete();
        $status = "success";

      }
      return $status;
    }
}
