<?php

namespace App\Http\Controllers\usermanagement;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\usermanagement\um_module\umModuleModel;
use App\usermanagement\um_user_permission\umUserPermissionModel;
use Illuminate\Support\Facades\Auth;
use App\Traits\privilaged;

class umModule extends Controller
{
    use privilaged;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function seacrhModuleAjax($view, $keyword)
    {
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      $moduleParent = umModuleModel::select('moduleName', 'moduleParent')
                                    ->where('moduleParent', null)
                                    ->get();

      $modulesAllA = "";
      $modulesAllP = "";
      $modulesAll = umModuleModel::select('moduleMethod')
                                  ->where('moduleParent', '!=', null)
                                  ->get();

      foreach($modulesAll as $dat){
          $modulesAllA .= "'".$dat['moduleMethod']."'".",";
      }

      foreach($moduleParent as $dat){
          $modulesAllP .= "'".$dat['moduleName']."'".",";
      }

      $modules = umModuleModel::select('moduleName','moduleMethod', 'moduleView', 'moduleParent', 'moduleShowInSidebar', 'moduleId', 'moduleLink', 'moduleHttpMethod', 'moduleLabel', 'moduleParams', 'moduleOrder', 'moduleGroup', 'moduleIcon')
                                ->where('moduleName', 'LIKE', '%'.$keyword.'%')
                                ->orWhere('moduleParent', 'LIKE', '%'.$keyword.'%')
                                ->orWhere('moduleMethod', 'LIKE', '%'.$keyword.'%')
                                ->get();

      if($priv != null && $priv->canView == 1){
        return view('usermanagement.um_module.'.$view, [
          'title' => $userModule,
          'modulesData' => $modules,
          'modulesAllA' => $modulesAllA,
          'modulesAllP' => $modulesAllP,
          'privilaged' => $priv,
          'keyword' => $keyword
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function listModulesWeb($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $moduleParent = umModuleModel::select('moduleName', 'moduleParent')
                                      ->where('moduleParent', null)
                                      ->get();

        $listModules = $this->listModules($userGroupId);
        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        $modules = umModuleModel::select('moduleName','moduleMethod', 'moduleView', 'moduleParent', 'moduleShowInSidebar', 'moduleId', 'moduleLink', 'moduleHttpMethod', 'moduleLabel', 'moduleParams', 'moduleOrder', 'moduleGroup', 'moduleIcon')
                                  ->paginate(10);

        $modulesAllA = "";
        $modulesAllP = "";
        $modulesAll = umModuleModel::select('moduleMethod')
                                    ->where('moduleParent', '!=', null)
                                    ->get();

        foreach($modulesAll as $dat){
            $modulesAllA .= "'".$dat['moduleMethod']."'".",";
        }

        foreach($moduleParent as $dat){
            $modulesAllP .= "'".$dat['moduleName']."'".",";
        }
        return view('usermanagement.um_module.'.$view, [
            'userGroupId' => $userGroupId,
            'listModules' => $listModules,
            'modulesAllA' => $modulesAllA,
            'modulesAllP' => $modulesAllP,
            'modulesData' => $modules,
            'title' => $userModule,
            'parent' => $moduleParent,
            'privilaged' => $priv
        ]);
    }

    public function addModule(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $moduleData = new umModuleModel();
        $moduleData->moduleName = $request->moduleName;
        $moduleData->moduleView = $request->moduleView;
        $moduleData->moduleMethod = $request->moduleMethod;
        $moduleData->moduleLink = $request->moduleLink;
        $moduleData->moduleParams = $request->moduleParams;
        $moduleData->moduleLabel = $request->moduleLabel;
        $moduleData->moduleParent = $request->input("moduleParent", null);
        $moduleData->moduleShowInSidebar = $request->moduleShowInSidebar;
        $moduleData->moduleOrder = $request->input("moduleOrder", 0);
        $moduleData->moduleGroup = $request->moduleGroup;
        $moduleData->moduleIcon = $request->moduleIcon;
        $moduleData->moduleHttpMethod = $request->moduleHttpMethod;
        $moduleData->created_date = \Carbon\Carbon::now();
        $moduleData->updated_date = \Carbon\Carbon::now();
        $moduleData->created_by = $userId;
        $insert =  $moduleData->save();

        return Redirect::back()->with('status', 'Module added');
      }

    }

    public function editModule(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);
      $moduleId = $request->moduleId;
      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $moduleData = new umModuleModel();
        $moduleData::where('moduleId', $moduleId)
                   ->update([
                         'moduleName' => $request->moduleName,
                         'moduleView' => $request->moduleView,
                         'moduleMethod' => $request->moduleMethod,
                         'moduleLink' => $request->moduleLink,
                         'moduleParams' => $request->moduleParams,
                         'moduleLabel' => $request->moduleLabel,
                         'moduleParent' => $request->moduleParent,
                         'moduleShowInSidebar' => $request->moduleShowInSidebar,
                         'moduleOrder' => $request->input("moduleOrder", 0),
                         'moduleGroup' => $request->moduleGroup,
                         'moduleIcon' => $request->moduleIcon,
                         'moduleHttpMethod' => $request->moduleHttpMethod,
                         'updated_date'=> \Carbon\Carbon::now(),
                         'updated_by'=> $userId
                    ]);

        return Redirect::back()->with('status', 'Module updated');
      }

    }

    public function moduleDelete(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idModule = $request->moduleId;

      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $moduleData = new umModuleModel();
        $moduleData::where('moduleId', $idModule)->delete();

        $permissionData = new umUserPermissionModel();
        $permissionData::where('moduleId', $idModule)->delete();

        return Redirect::back()->with('status', 'Module deleted');
      }

    }

    public function moduleDeleteBulk(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idModule = $request->moduleId;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $moduleData = new umModuleModel();
        $moduleData::whereIn('moduleId', $idModule)->delete();

        $permissionData = new umUserPermissionModel();
        $permissionData::whereIn('moduleId', $idModule)->delete();
        $status = "success";

      }
      return $status;
    }

}
