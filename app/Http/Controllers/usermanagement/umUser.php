<?php

namespace App\Http\Controllers\usermanagement;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\usermanagement\um_module\umModuleModel;
use App\usermanagement\um_user\umUserModel;
use App\usermanagement\um_user_group\umUserGroupModel;
use Illuminate\Support\Facades\Auth;
use App\Traits\privilaged;
use App\Traits\excelExport;

class umUser extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listUser($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        $users = umUserModel::join('GS_USER_GROUP', 'GS_USER_GROUP.userGroupId', '=', 'GS_USERS.userGroupId')
                       ->select('GS_USERS.id', 'GS_USERS.name', 'GS_USERS.email', 'GS_USER_GROUP.groupName','GS_USERS.userGroupId')
                       ->paginate(15);

       /* CONTOH EXPORT DATA KE EXCEL
       $tes = umUserModel::join('GS_USER_GROUP', 'GS_USER_GROUP.userGroupId', '=', 'GS_USERS.userGroupId')
                      ->select('GS_USERS.id', 'GS_USERS.name', 'GS_USERS.email', 'GS_USER_GROUP.groupName','GS_USERS.userGroupId')
                      ->get();

        foreach($tes as $test){
            $data[] = array(
              $test->id,
              $test->name,
              $test->email,
              $test->groupName,
              $test->userGroupId
            );
        }

        $column = array('ID USER', 'NAME USER','EMAIL USER', 'GROUP USER', 'GROUP ID');
        $this->excelDownload($data, "Data user", $column);*/



        $userGroupsAll = umUserGroupModel::all();
        if($priv != null && $priv->canView == 1){
          return view('usermanagement.um_user.'.$view, [
              'userGroupId' => $userGroupId,
              'listModules' => $listModules,
              'title' => $userModule,
              'userData' => $users,
              'userGroupAll' => $userGroupsAll,
              'privilaged' => $priv
          ]);
        }else{
           return Redirect::back()->withErrors(['Not permitted']);
        }
    }

    public function seacrhUserAjax($view, $keyword)
    {
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      $users = umUserModel::join('GS_USER_GROUP', 'GS_USER_GROUP.userGroupId', '=', 'GS_USERS.userGroupId')
                     ->select('GS_USERS.id', 'GS_USERS.name', 'GS_USERS.email', 'GS_USER_GROUP.groupName','GS_USERS.userGroupId')
                     ->where('GS_USERS.name', 'like', '%'.$keyword.'%')
                     ->orWhere('GS_USERS.email', 'like', '%'.$keyword.'%')
                     ->get();

      $userGroupsAll = umUserGroupModel::all();

      if($priv != null && $priv->canView == 1){
        return view('usermanagement.um_user.'.$view, [
          'title' => $userModule,
          'userData' => $users,
          'userGroupAll' => $userGroupsAll,
          'privilaged' => $priv,
          'keyword' => $keyword
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function addUser(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
              $userData = new umUserModel();
              $userData->name = $request->name;
              $userData->email = $request->email;
              $userData->userGroupId = 4;
              $userData->created_date = \Carbon\Carbon::now();
              $userData->updated_date = \Carbon\Carbon::now();
              $userData->created_by = $userId;
              $userData->password =  Hash::make($request->password);
              $insert =  $userData->save();

              return Redirect::back()->with('status', 'User added');
       } catch(\Illuminate\Database\QueryException $ex){
         return Redirect::back()->with('status', 'Error inserting');
       }
      }

    }


    public function editUser(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idUser = $request->id;

      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
            $userData = new umUserModel();
            $userData::where('id', $idUser)
                       ->update([
                              'name'=>$request->name,
                              'email'=>$request->email,
                              'updated_date' => \Carbon\Carbon::now(),
                              'updated_by' => $userId
                        ]);

            return Redirect::back()->with('status', 'User updated');
        } catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error updating');
        }
      }

    }

    public function deleteUser(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idUser = $request->id;

      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
            $userData = new umUserModel();
            $userData::where('id', $idUser)->delete();

            return Redirect::back()->with('status', 'User deleted');
        } catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error deleting');
        }
      }

    }

    public function deleteBulkUser(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $id = $request->id;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $userData = new umUserModel();
        $userData::whereIn('id', $id)->delete();
        $status = "success";

      }
      return $status;
    }
}
