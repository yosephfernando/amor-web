<?php

namespace App\Http\Controllers\salesreturn;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\inventory\im_prd_master\imPrdMasterModel;
use App\inventory\im_prd_unit\imPrdUnitModel;
use App\salesinvoice\si_sales_invoice\siSalesInvoiceModel;
use App\salesinvoice\si_detail_sales_invoice\siSalesInvoiceDetailModel;
use App\stocktxn\im_stock_txn\imStockTxnModel;
use App\usermanagement\um_module\umModuleModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Illuminate\Support\Facades\DB;
use Datatables;

class srtDetailSalesReturn extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    private function updateTotalSrt($srtid, $price, $qty){
      $ppn = siSalesInvoiceModel::select('ppn')->where('inv_id', $srtid)->first();
      if($ppn->ppn > 0){
        $percentPPN = $ppn->ppn / 100;
        $total_ppn = $percentPPN * $price;
        $total_gross = $price + $total_ppn;
      }else{
        $total_ppn = 0;
        $total_gross = $price;
      }

      $updatePrt = new siSalesInvoiceModel();
      $updatePrt::where('inv_id', $srtid)
                 ->update([
                    "total_gross" => $total_gross,
                    "total_nett" => $total_gross,
                    "total_ppn" => $total_ppn,
                 ]);

       return true;
    }

    public function exportToExcel($view, $srtId){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv->canExport)){
        $detailSrt = siSalesInvoiceDetailModel::join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'AR_INVOICE_DETAIL.uom_id')
                                  ->join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'AR_INVOICE_DETAIL.prd_id')
                                  ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IPM.size_id')
                                  ->select('AR_INVOICE_DETAIL.inv_detail_id', 'IPU.uom_desc', 'AR_INVOICE_DETAIL.uom_conversion', 'IPM.prd_desc', 'AR_INVOICE_DETAIL.qty_inv', 'AR_INVOICE_DETAIL.price_inv', 'AR_INVOICE_DETAIL.sub_total', 'IPS.size_code')
                                  ->where('AR_INVOICE_DETAIL.inv_id', $srtId)
                                  ->get();
        $data = null;
         foreach($detailSrt as $datas){
             $data[] = array(
               $datas->inv_detail_id,
               $datas->prd_desc,
               $datas->qty_inv,
               $datas->price_inv,
               $datas->sub_total,
               $datas->uom_desc,
               $datas->uom_conversion,
               $datas->size_code,
             );
         }
         if($data != null){
           $column = array('ID DETAIL INV','PRODUCT', 'QTY', 'UOM', 'UOM CONVERSION','SIZE');
           $this->excelDownload($data, "Data sales return", $column);
         }else{
           return Redirect::back()->withErrors(['No data to export']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    /*public function ajaxPrice($view, $prd_id){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);
      $retail_price = imPrdMasterModel::select('retail_price')->where('prd_id', $prd_id)->first();

      if($priv != null && $priv->canView == 1){
        return view('salesreturn.srt_detail_sales_return.'.$view, [
            'retail_price' => $retail_price,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    } */

    /* public function ajaxPiCatalog($view, $keyword){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('salesreturn.srt_detail_sales_return.'.$view, [
            'keyword' => $keyword,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    } */

    public function ajaxSiDetailCatalog($view, $id){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);
      $invDetail = siSalesInvoiceDetailModel::select('IPM.prd_code', 'IPM.prd_desc','IPS.size_desc', 'IPM.retail_price', 'AR_INVOICE_DETAIL.qty_inv', 'AR_INVOICE_DETAIL.prd_id', 'AR_INVOICE_DETAIL.inv_detail_id')
                                                ->join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'AR_INVOICE_DETAIL.prd_id')
                                                ->join('IM_PRD_SIZE AS IPS', 'IPM.size_id', '=', 'IPS.size_id')
                                                ->where('AR_INVOICE_DETAIL.inv_id', $id)
                                                ->get();

      if($priv != null && $priv->canView == 1){
        return view('salesreturn.srt_detail_sales_return.'.$view, [
            'invDetail' => $invDetail,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function addDetailSrtAction(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);
      $retail_price =  imPrdMasterModel::select('retail_price')->where('prd_id', $request->prd_id)->first();
      $inv_id = $request->inv_id;

      $inv_no = siSalesInvoiceModel::select('inv_no', 'inv_date', 'wh_id')->where('inv_id', $inv_id)->first();

      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $sub_total = str_replace(".", "", $request->price_inv) *  $request->qty_inv;
        try {
          $detailSrtData = new siSalesInvoiceDetailModel();
          $detailSrtData->inv_id = $inv_id;
          $detailSrtData->prd_id = $request->prd_id;
          $detailSrtData->qty_inv =  $request->qty_inv;
          $detailSrtData->price_inv =  str_replace(".", "", $request->price_inv);
          $detailSrtData->disc =  0;
          $detailSrtData->disc_value =  0;
          $detailSrtData->uom_id = $request->uom_id;
          $detailSrtData->uom_conversion = $request->uom_conversion;
          $detailSrtData->sub_total = $sub_total;
          $insert =  $detailSrtData->save();
        } catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error inserting detail invoice');
        }

        /* insert TXN */
        try {
          $insertStockTxn = new imStockTxnModel();
          $insertStockTxn->txn_id = $inv_id;
          $insertStockTxn->txn_no = ($inv_no != null ? $inv_no->inv_no:0);
          $insertStockTxn->txn_date = ($inv_no != null ? $inv_no->inv_date:null);
          $insertStockTxn->txn_type_id = 1;
          $insertStockTxn->wh_id = $inv_no->wh_id;
          $insertStockTxn->prd_id = $request->prd_id;
          $insertStockTxn->qty = $request->qty_inv;
          $insertStockTxn->cost_value = $sub_total;
          $insertStockTxn->txn_index = 1;
          $insertStockTxn->created_date = \Carbon\Carbon::now();
          $insertStockTxn->created_by = Auth::user()->id;
          $insertStockTxn->save();
        } catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error inserting txn');
        }

        $sum_qty = siSalesInvoiceDetailModel::where('inv_id', $inv_id)->sum('qty_inv');
        $sum_subtotal = siSalesInvoiceDetailModel::select(DB::raw("SUM(sub_total::money::numeric::float8) AS sub_total"))->where('inv_id', $inv_id)->first();
        $this->updateTotalSrt($inv_id, $sum_subtotal->sub_total, $sum_qty);
        return Redirect::back()->with('status', 'Detail sales return added');
      }
    }

    public function loadSi(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      $inv_detail_id = $request->inv_detail_id;
      //inv id type 2
      $inv_id_ = $request->inv_id_;

      if($inv_detail_id != null){
        $inv_details = siSalesInvoiceDetailModel::join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'AR_INVOICE_DETAIL.prd_id')
                                                    ->select('IPM.retail_price', 'AR_INVOICE_DETAIL.*')
                                                    ->whereIn('AR_INVOICE_DETAIL.inv_detail_id', $inv_detail_id)->get();
        //inv id type 1
        $inv_id = siSalesInvoiceDetailModel::select('inv_id')->where('inv_detail_id', $inv_detail_id)->first();
        $inv_no = siSalesInvoiceModel::select('inv_no', 'inv_date', 'wh_id')->where('inv_id', $inv_id->inv_id)->first();
      }else{
        return Redirect::back()->with('status', 'error : choose one of the detail(s)');
      }


      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $i = 0;
        foreach($inv_details as $inv_detail){
              $inv_det_id = $inv_detail->inv_detail_id;
              $price_inv = preg_replace("/[^A-Za-z0-9 ]/", '',$request->input('price_inv'.$inv_det_id));
              $price_inv = str_replace('Rp', '', $price_inv);
              $qty_inv = $request->input('qty_inv'.$inv_det_id);
              $sub_total = $price_inv * $qty_inv ;

              try {
                $detailSiData = new siSalesInvoiceDetailModel();
                $detailSiData->inv_id = $inv_id_;
                $detailSiData->prd_id = $inv_detail->prd_id;
                $detailSiData->qty_inv =  $qty_inv;
                $detailSiData->price_inv =  $price_inv;
                $detailSiData->disc =  0;
                $detailSiData->disc_value =  0;
                $detailSiData->uom_id = $inv_detail->uom_id;
                $detailSiData->uom_conversion = $inv_detail->uom_conversion;
                $detailSiData->sub_total = $sub_total;
                $insert =  $detailSiData->save();
              } catch(\Illuminate\Database\QueryException $ex){
                  return Redirect::back()->with('status', 'Error inserting invoice detail');
              }

              /* insert TXN */
              try {
                $insertStockTxn = new imStockTxnModel();
                $insertStockTxn->txn_id = $inv_id_;
                $insertStockTxn->txn_no = ($inv_no != null ? $inv_no->inv_no:0);
                $insertStockTxn->txn_date = ($inv_no != null ? $inv_no->inv_date:null);
                $insertStockTxn->txn_type_id = 1;
                $insertStockTxn->wh_id =($inv_no != null ? $inv_no->wh_id:0);
                $insertStockTxn->prd_id = $inv_detail->prd_id;
                $insertStockTxn->qty = $qty_inv;
                $insertStockTxn->cost_value = $sub_total;
                $insertStockTxn->txn_index = $i;
                $insertStockTxn->created_date = \Carbon\Carbon::now();
                $insertStockTxn->created_by = Auth::user()->id;
                $insertStockTxn->save();
              } catch(\Illuminate\Database\QueryException $ex){
                  return Redirect::back()->with('status', 'Error inserting txn');
              }

              $sum_qty = siSalesInvoiceDetailModel::where('inv_id', $inv_id_)->sum('qty_inv');
              $sum_subtotal = siSalesInvoiceDetailModel::select(DB::raw("SUM(sub_total::money::numeric::float8) AS sub_total"))->where('inv_id', $inv_id_)->first();
              $this->updateTotalSrt($inv_id_, $sum_subtotal->sub_total, $sum_qty);
        $i++;}
        return Redirect::back()->with('status', 'Detail sales return added');
      }
    }

    public function listDetailSrtView($view, $srtId){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $detailSrt = siSalesInvoiceDetailModel::join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'AR_INVOICE_DETAIL.prd_id')
                                ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IPM.size_id')
                                ->join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'AR_INVOICE_DETAIL.uom_id')
                                ->select('AR_INVOICE_DETAIL.inv_detail_id', 'AR_INVOICE_DETAIL.qty_inv', 'AR_INVOICE_DETAIL.sub_total', 'IPM.prd_id', 'AR_INVOICE_DETAIL.price_inv', 'IPM.prd_desc', 'IPS.size_desc', 'AR_INVOICE_DETAIL.uom_id', 'IPU.uom_desc', 'AR_INVOICE_DETAIL.uom_conversion')
                                ->where('AR_INVOICE_DETAIL.inv_id', $srtId)
                                ->get();

      $uoms = imPrdUnitModel::select('uom_id', 'uom_desc')->get();

      $srt_detail_id = siSalesInvoiceDetailModel::select('inv_detail_id')->where('inv_id', $srtId)->first();

      $srt_detail_sub_sum = siSalesInvoiceDetailModel::select(DB::raw('SUM(sub_total) as sub_total'), DB::raw('SUM(qty_inv) as qty_inv'))->where('inv_id', $srtId)->first();
      $srt_detail_retail_price_sum = siSalesInvoiceDetailModel::join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'AR_INVOICE_DETAIL.prd_id')
                                                                ->select(DB::raw('SUM("IPM"."retail_price") as "retail_price"'))
                                                                ->where('inv_id', $srtId)
                                                                ->first();

      $ppn = siSalesInvoiceModel::select('ppn')->where('inv_id', $srtId)->first();
      $price = preg_replace("/[^A-Za-z0-9 ]/", '',$srt_detail_sub_sum->sub_total);
      $price = str_replace('Rp', '', $price);
      $total = $price;
      $total_ppn = 0;
      if($ppn->ppn > 0){
        $percentPPN = $ppn->ppn / 100;
        $total_ppn = $percentPPN * $price;
        $total = $price + $total_ppn;
      }

      $srt_id = siSalesInvoiceModel::select('inv_id', 'inv_no', 'inv_status', 'created_date', 'si_flag')->where('inv_id', $srtId)->first();
      $siSugest = siSalesInvoiceModel::select(DB::raw('CAST(inv_id AS varchar) AS data'), 'inv_no AS value')->where('inv_status', '=', 'open')->where('inv_type', 1)->get();
      $listModules = $this->listModules($userGroupId);
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('salesreturn.srt_detail_sales_return.'.$view, [
          'userGroupId' => $userGroupId,
          'listModules' => $listModules,
          'title' => $userModule,
          'privilaged' => $priv,
          'detailPrt' => $detailSrt,
          'uoms' => $uoms,
          'srt_id' => $srt_id,
          'srt_detail_id' => $srt_detail_id,
          'srt_detail_sub_sum' => $srt_detail_sub_sum,
          'total' => $total,
          'total_bef_ppn' => $price,
          'total_ppn' => $total_ppn,
          'srt_detail_retail_price_sum' => $srt_detail_retail_price_sum,
          'siSugest' => $siSugest
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function listDetailSrtData(Request $request, $view, $srtId){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $detailSrt = siSalesInvoiceDetailModel::join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'AR_INVOICE_DETAIL.prd_id')
                                ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IPM.size_id')
                                ->join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'AR_INVOICE_DETAIL.uom_id')
                                ->select('AR_INVOICE_DETAIL.inv_detail_id', 'AR_INVOICE_DETAIL.qty_inv', 'AR_INVOICE_DETAIL.sub_total', 'IPM.prd_id', 'AR_INVOICE_DETAIL.price_inv', 'IPM.prd_desc', 'IPS.size_desc', 'AR_INVOICE_DETAIL.uom_id', 'IPU.uom_desc', 'AR_INVOICE_DETAIL.uom_conversion')
                                ->where('AR_INVOICE_DETAIL.inv_id', $srtId);

      $uoms = imPrdUnitModel::select('uom_id', 'uom_desc')->get();
      $srt_id = siSalesInvoiceModel::select('inv_id', 'inv_no', 'inv_status', 'created_date')->where('inv_id', $srtId)->first();

      $listModules = $this->listModules($userGroupId);
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($srt_id->inv_status != "approve"){
            return Datatables::of($detailSrt)
            ->addColumn('delete_bulk', function($data){
                return "
                  <input type='checkbox' name='inv_detail_id[]' value='".$data->inv_detail_id."' />
                ";
            })
            ->editColumn('qty_inv', function ($data) {
                   return number_format($data->qty_inv);
            })
            ->addColumn('action', function ($data) use ($priv, $uoms, $srt_id, $view) {
                return view('salesreturn.srt_detail_sales_return.'.$view, ['privilaged' => $priv, 'data' => $data, 'uoms' => $uoms, 'srt_id' => $srt_id]);
            })
            ->make(true);
      }else{
            return Datatables::of($detailSrt)
            ->addColumn('delete_bulk', function($data){
                return "
                  <input type='checkbox' name='inv_detail_id[]' value='".$data->inv_detail_id."' />
                ";
            })
            ->editColumn('qty_inv', function ($data) {
                   return number_format($data->qty_inv);
            })
            ->make(true);
      }
    }

    public function editDetailSrt(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $inv_detail_id = $request->inv_detail_id;
      $inv_id = $request->inv_id;
      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
          $retail_price = preg_replace("/[^A-Za-z0-9 ]/", '',$request->retail_price);
          $retail_price = str_replace('Rp', '', $retail_price);
          $subtotal = $retail_price * $request->qty_inv;

          $detailSrtData = new siSalesInvoiceDetailModel();
          $detailSrtData::where('inv_detail_id', $inv_detail_id)
                     ->update([
                            'uom_id'=>$request->uom_id,
                            'uom_conversion'=>$request->uom_conversion,
                            'qty_inv'=>$request->qty_inv,
                            'price_inv'=>$retail_price,
                            'sub_total'=>$subtotal
                      ]);
       } catch(\Illuminate\Database\QueryException $ex){
           return Redirect::back()->with('status', 'Error updating');
       }
       $sum_qty = siSalesInvoiceDetailModel::where('inv_id', $inv_id)->sum('qty_inv');
       $sum_subtotal = siSalesInvoiceDetailModel::select(DB::raw("SUM(sub_total::money::numeric::float8) AS sub_total"))->where('inv_id', $inv_id)->first();
       $this->updateTotalSrt($inv_id, $sum_subtotal->sub_total, $sum_qty);

       return Redirect::back()->with('status', 'Detail sales return updated');
      }

    }

    public function deleteDetailSrt(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $inv_detail_id = $request->inv_detail_id;
      $inv_id = $request->inv_id;

      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $detailSrtData = siSalesInvoiceDetailModel::where('inv_detail_id', $inv_detail_id)->delete();

        $sum_qty = siSalesInvoiceDetailModel::where('inv_id', $inv_id)->sum('qty_inv');
        $sum_subtotal = siSalesInvoiceDetailModel::select(DB::raw("SUM(sub_total::money::numeric::float8) AS sub_total"))->where('inv_id', $inv_id)->first();
        $this->updateTotalSrt($inv_id, $sum_subtotal->sub_total, $sum_qty);
        return Redirect::back()->with('status', 'Detail sales return deleted');
      }

    }

    public function deleteBulkDetailSrt(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $inv_detail_id = $request->inv_detail_id;
      $inv_id = $request->inv_id;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $detailSrtData = siSalesInvoiceDetailModel::whereIn('inv_detail_id', $inv_detail_id)->delete();
        $sum_qty = siSalesInvoiceDetailModel::where('inv_id', $inv_id)->sum('qty_inv');
        $sum_subtotal = siSalesInvoiceDetailModel::select(DB::raw("SUM(sub_total::money::numeric::float8) AS sub_total"))->where('inv_id', $inv_id)->first();
        $this->updateTotalSrt($inv_id, $sum_subtotal->sub_total, $sum_qty);
        $status = "success";
      }
      return $status;
    }
}
