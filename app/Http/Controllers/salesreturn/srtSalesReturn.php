<?php

namespace App\Http\Controllers\salesreturn;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\salesinvoice\si_sales_invoice\siSalesInvoiceModel;
use App\salesinvoice\si_detail_sales_invoice\siSalesInvoiceDetailModel;
use App\sales\so_cust\customerModel;
use App\usermanagement\um_module\umModuleModel;
use App\warehouse\im_wh_loc\imWhLocModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Datatables;
use Carbon\Carbon;

class srtSalesReturn extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /* public function ajaxWarehouseCatalog($view, $keyword){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('purchasingreturn.prt_purchase_return.'.$view, [
            'keyword' => $keyword,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    } */

    public function exportToExcel($view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv->canExport)){
        $srt = siSalesInvoiceModel::select('inv_no', 'inv_date', 'notes', 'inv_status', 'total_gross', 'total_ppn')->where('inv_type', 2)->get();
         $data = null;
         foreach($srt as $datas){
           $date = date("d M Y", strtotime($datas->inv_date));
             $data[] = array(
               $datas->inv_no,
               $date,
               $datas->notes,
               $datas->inv_status,
               $datas->total_gross,
               $datas->total_ppn,
             );
         }
        if($data != null){
           $column = array('INV NO','DATE', 'NOTES', 'STATUS', 'TOTAL GROSS', 'TOTAL PPN');
           $this->excelDownload($data, "Data sales return", $column);
         }else{
           return Redirect::back()->withErrors(['No data to export']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function listSrtView($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $listModules = $this->listModules($userGroupId);
        $priv = $this->checkPrivilaged($moduleId, $userGroupId);
        if($priv != null && $priv->canView == 1){
          return view('salesreturn.srt_sales_return.'.$view, [
              'userGroupId' => $userGroupId,
              'listModules' => $listModules,
              'title' => $userModule,
              'privilaged' => $priv
          ]);
        }else{
           return Redirect::back()->withErrors(['Not permitted']);
        }
    }

    public function listSrtData(Request $request, $view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $listModules = $this->listModules($userGroupId);
        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        $srt = siSalesInvoiceModel::join('SO_CUST_MASTER AS SCM', 'SCM.cust_id', '=', 'AR_INVOICE_HEADER.cust_id')
                                      ->join('IM_WH_LOC AS WH', 'WH.wh_id', '=', 'AR_INVOICE_HEADER.wh_id')
                                      ->select(['AR_INVOICE_HEADER.inv_id', 'AR_INVOICE_HEADER.inv_no', 'AR_INVOICE_HEADER.inv_date', 'AR_INVOICE_HEADER.inv_status',
                                      'AR_INVOICE_HEADER.notes',  'AR_INVOICE_HEADER.no_inv_pajak',
                                      'AR_INVOICE_HEADER.reference', 'AR_INVOICE_HEADER.cust_id', 'AR_INVOICE_HEADER.ppn',
                                      'AR_INVOICE_HEADER.total_nett', 'SCM.cust_desc', 'WH.wh_id', 'WH.wh_desc'])
                                       ->where('inv_type', 2);
        if ($request->has('start_date') && $request->has('end_date')) {
            $start = strtotime($request->get('start_date'));
            $end = strtotime($request->get('end_date'));
            $pi->whereBetween('inv_date', [date('Y-m-d', $start), date('Y-m-d', $end)]);
        }

        return Datatables::of($srt)
        ->addColumn('delete_bulk', function($data){
            return "
              <input type='checkbox' name='inv_id[]' value='".$data->inv_id."' />
            ";
        })
        ->editColumn('inv_date', function ($data) {
               return $data->inv_date ? with(new Carbon($data->inv_date))->format('d M y') : '';
        })
        ->addColumn('action', function ($data) use ($priv, $view) {
            return view('salesreturn.srt_sales_return.'.$view, ['privilaged' => $priv, 'data' => $data]);
       })
       ->make(true);
    }

    private function srtCodeGenerate($srtDate, $maxSrt){
      $code = "RS";
      $srtDate = strtotime($srtDate);
      $yy = date("y", $srtDate);
      $mm = date("m", $srtDate);

      if($maxSrt != ""){
        $maxSrt = substr($maxSrt, -6);
        $srt_no = $maxSrt + 1;
        $srt_no = str_pad($srt_no, 6, '0',STR_PAD_LEFT);
      }else{
        $srt_no = "000001";
      }

      $srt_no_final = $code.$yy.$mm.$srt_no;
      return $srt_no_final;
    }

    public function addSrt(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);
      $maxsrt = siSalesInvoiceModel::select(DB::raw('MAX(RIGHT(inv_no, 6)) as inv_no'))->where('inv_type', 2)->first();
      $srtDate = strtotime($request->inv_date);
      $srt_no = $this->srtCodeGenerate($request->inv_date, $maxsrt->inv_no);
      $cust_term = customerModel::select('cust_term')->where('cust_id', $request->cust_id)->first();
      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
            $srtData = new siSalesInvoiceModel();
            $srtData->inv_no = $srt_no;
            $srtData->notes = $request->notes;
            $srtData->inv_status = "open";
            $srtData->inv_type = "2";
            $srtData->cust_id = $request->cust_id;
            $srtData->wh_id = $request->wh_id;
            $srtData->term = ($cust_term != null ? $cust_term->cust_term:0);
            $srtData->no_inv_pajak = $request->no_inv_pajak;
            $srtData->reference = $request->reference;
            $srtData->ppn = $request->input('ppn', 0);
            $srtData->si_flag = $request->input('si_flag', 0);
            $srtData->inv_date = date("Y-m-d", $srtDate);
            $srtData->created_date = \Carbon\Carbon::now();
            $srtData->updated_date = \Carbon\Carbon::now();
            $srtData->created_by = $userId;
            $insert =  $srtData->save();
        } catch(\Illuminate\Database\QueryException $ex){
            dd($ex->getMessage());
            return Redirect::back()->with('status', 'Error inserting');
        }
        return Redirect::back()->with('status', 'Srt added');
      }

    }

    public function editSrt(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);
      $srtId = $request->inv_id;
      $srtDate = strtotime($request->inv_date);
      $cust_term = customerModel::select('cust_term')->where('cust_id', $request->cust_id)->first();

      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
          $srtData = new siSalesInvoiceModel();
          $srtData::where('inv_id', $srtId)
                     ->update([
                            'inv_date'=>date("Y-m-d", $srtDate),
                            'cust_id'=> $request->cust_id,
                            'wh_id'=> $request->wh_id,
                            'term'=> ($cust_term != null ? $cust_term->cust_term:0),
                            'inv_type'=>2,
                            'no_inv_pajak'=>$request->no_inv_pajak,
                            'reference'=>$request->reference,
                            'ppn'=>$request->input('ppn', 0),
                            'notes'=>$request->notes,
                            'updated_date' => \Carbon\Carbon::now(),
                            'updated_by' => $userId
                      ]);
          } catch(\Illuminate\Database\QueryException $ex){
              return Redirect::back()->with('status', 'Error updating');
          }
        return Redirect::back()->with('status', 'Srt updated');
      }

    }

    public function deleteSrt(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $srtId = $request->inv_id;
      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $srtData = new siSalesInvoiceModel();
        $srtData::where('inv_id', $srtId)->delete();

        $srtDetailData = new siSalesInvoiceDetailModel();
        $srtDetailData::where('inv_id', $srtId)->delete();
        return Redirect::back()->with('status', 'Srt deleted');
      }

    }

    public function deleteBulkSrt(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $srtId = $request->inv_id;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $srtData = new siSalesInvoiceModel();
        $srtData::whereIn('inv_id', $srtId)->delete();

        $srtDetailData = new siSalesInvoiceDetailModel();
        $srtDetailData::whereIn('inv_id', $srtId)->delete();

        $status = "success";

      }
      return $status;
    }
}
