<?php

namespace App\Http\Controllers\goodsreceive;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\purchaseorder\po_purchase_order\poPurchaseOrderModel;
use App\purchaseorder\po_detail_purchase_order\poPurchaseOrderDetailModel;
use App\goodsreceive\gr_goods_receive\grGoodsReceiveModel;
use App\goodsreceive\gr_detail_goods_receive\grGoodsReceiveDetailModel;
use App\stocktxn\im_stock_txn\imStockTxnModel;
use App\usermanagement\um_module\umModuleModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Datatables;
use Carbon\Carbon;

class grGoodsReceive extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function exportToExcel($view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv->canExport)){
        $gr = grGoodsReceiveModel::select('gr_no', 'po_no', 'gr_date', 'gr_status', 'total_qty', 'total_gross', 'total_ppn')->get();

        $data = null;
        foreach($gr as $datas){
           $date = date("d M Y", strtotime($datas->gr_date));
             $data[] = array(
               $datas->gr_no,
               $datas->po_no,
               $date,
               $datas->gr_status,
               $datas->total_qty,
               $datas->total_gross,
               $datas->total_ppn,
             );
         }
         if($data != null){
             $column = array('GR NO', 'PO NO','DATE', 'STATUS', 'TOTAL QTY', 'TOTAL GROSS', 'TOTAL PPN');
             $this->excelDownload($data, "Data gr", $column);
         }else{
           return Redirect::back()->withErrors(['No data to export']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    private function updateTotalGr($grid, $price, $qty){
      $ppn = grGoodsReceiveModel::select('ppn')->where('gr_id', $grid)->first();
      if($ppn->ppn >= 0){
        $percentPPN = $ppn->ppn / 100;
        $total_ppn = $percentPPN * $price;
        $total_gross = $price + $total_ppn;
      }else{
        $total_gross = $price;
        $total_ppn = 0;
      }

      $updateGr = new grGoodsReceiveModel();
      $updateGr::where('gr_id', $grid)
                 ->update([
                    "total_qty" => $qty,
                    "total_gross_product" => $total_gross,
                    "total_gross" => $total_gross,
                    "total_nett" => $total_gross,
                    "total_ppn" => $total_ppn,
                 ]);

       return true;
    }

    public function loadDetailPo($grId, $poId){
      $detailpodata = poPurchaseOrderDetailModel::select('po_detail_id', 'po_id', 'prd_id', 'qty_receive', 'uom_id', 'uom_conversion', 'sub_total', 'order_price', 'qty_order')
                                                  ->where('po_id', $poId)
                                                  ->get();
      $grNo = grGoodsReceiveModel::select('gr_no', 'gr_date')->where('gr_id', $grId)->first();
      $whId = poPurchaseOrderModel::select('wh_id')->where('po_id', $poId)->first();
     $i = 1;
     foreach($detailpodata as $data){
       $subtotal = preg_replace("/[^A-Za-z0-9 ]/", '', $data->sub_total);
       $subtotal = str_replace('Rp', '', $subtotal);

         try {
              $inserDetailGr = new grGoodsReceiveDetailModel();
              $inserDetailGr->gr_id = $grId;
              $inserDetailGr->po_detail_id = $data->po_detail_id;
              $inserDetailGr->prd_id = $data->prd_id;
              $inserDetailGr->qty_receive = $data->qty_receive;
              $inserDetailGr->uom_id = $data->uom_id;
              $inserDetailGr->uom_conversion = $data->uom_conversion;
              $inserDetailGr->order_price = $data->order_price;
              $inserDetailGr->disc = 0;
              $inserDetailGr->disc_value = 0;
              $inserDetailGr->sub_total = $subtotal;
              $inserDetailGr->dpp_product = 0;
              $inserDetailGr->save();
        } catch(\Illuminate\Database\QueryException $ex){
            return false;die();
        }
      /* insert TXN */
        try {
            $insertStockTxn = new imStockTxnModel();
            $insertStockTxn->txn_id = $grId;
            $insertStockTxn->txn_no = ($grNo != null ? $grNo->gr_no:0);
            $insertStockTxn->txn_date = ($grNo != null ? $grNo->gr_date:null);
            $insertStockTxn->txn_type_id = 1;
            $insertStockTxn->wh_id = ($whId != null ? $whId->wh_id:0);
            $insertStockTxn->prd_id = $data->prd_id;
            $insertStockTxn->qty = $data->qty_order;
            $insertStockTxn->cost_value = $subtotal;
            $insertStockTxn->txn_index = $i;
            $insertStockTxn->created_date = \Carbon\Carbon::now();
            $insertStockTxn->created_by = Auth::user()->id;
            $insertStockTxn->save();
          } catch(\Illuminate\Database\QueryException $ex){
              return false;die();
          }
      $i++;}

      $sum_qty = grGoodsReceiveDetailModel::where('gr_id', $grId)->sum('qty_receive');
      $sum_subtotal = grGoodsReceiveDetailModel::where('gr_id', $grId)->sum('sub_total');
      $sum_subtotal = preg_replace("/[^A-Za-z0-9 ]/", '', $sum_subtotal);
      $sum_subtotal = str_replace('Rp', '', $sum_subtotal);
      $this->updateTotalGr($grId, $sum_subtotal, $sum_qty);

      $updateWhId = grGoodsReceiveModel::where('gr_id', $grId)->update(['wh_id' => $whId->wh_id]);
      $updatePo = poPurchaseOrderModel::where('po_id', $poId)->update(["po_status" => "finish"]);

      return true;
    }

    /* public function ajaxPoCatalog($view, $keyword){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('goodsreceive.gr_goods_receive.'.$view, [
            'keyword' => $keyword,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    } */

    public function listGr($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $listModules = $this->listModules($userGroupId);
        $priv = $this->checkPrivilaged($moduleId, $userGroupId);
        return view('goodsreceive.gr_goods_receive.'.$view, [
            'userGroupId' => $userGroupId,
            'listModules' => $listModules,
            'title' => $userModule,
            'privilaged' => $priv
        ]);
    }

    public function listGrData(Request $request, $view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $listModules = $this->listModules($userGroupId);
        $priv = $this->checkPrivilaged($moduleId, $userGroupId);
        $gr = grGoodsReceiveModel::select('*');

        if ($request->has('start_date') && $request->has('end_date')) {
            $start = strtotime($request->get('start_date'));
            $end = strtotime($request->get('end_date'));
            $gr->whereBetween('gr_date', [date('Y-m-d', $start), date('Y-m-d', $end)]);
        }

        return Datatables::of($gr)
        ->addColumn('delete_bulk', function($data){
            return "
              <input type='checkbox' name='gr_id[]' value='".$data->gr_id."' />
            ";
        })
        ->editColumn('gr_date', function ($data) {
               return $data->gr_date ? with(new Carbon($data->gr_date))->format('d M y') : '';
        })
        ->editColumn('total_qty', function ($data) {
               return number_format($data->total_qty);
        })
        ->addColumn('action', function ($data) use ($priv) {
            return view('goodsreceive.gr_goods_receive.grDataTable', ['privilaged' => $priv, 'data' => $data]);
        })
        ->make(true);
    }

    private function grCodeGenerate($grDate, $maxGr){
      $code = "GR";
      $grDate = strtotime($grDate);
      $yy = date("y", $grDate);
      $mm = date("m", $grDate);

      if($maxGr != ""){
        $maxGr = substr($maxGr, -6);
        $gr_no = $maxGr + 1;
        $gr_no = str_pad($gr_no, 6, '0',STR_PAD_LEFT);
      }else{
        $gr_no = "000001";
      }

      $gr_no_final = $code.$yy.$mm.$gr_no;
      return $gr_no_final;
    }

    public function addGr(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);
      $gr_date = strtotime($request->gr_date);
      $gr_date = date("Y-m-d", $gr_date);

      $maxgr = grGoodsReceiveModel::max('gr_no');
      $gr_no = $this->grCodeGenerate($request->gr_date, $maxgr);
      $whId = poPurchaseOrderModel::select('wh_id', 'ppn')->where('po_no', $request->po_no)->first();

      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
            $grData = new grGoodsReceiveModel();
            $grData->gr_no = $gr_no;
            $grData->po_no = $request->po_no;
            $grData->gr_date = $gr_date;
            $grData->supp_id = 1;
            $grData->ppn_type = $request->ppn_type;
            $grData->notes = $request->notes;
            $grData->gr_status = $request->gr_status;
            $grData->total_qty = 0;
            $grData->total_gross_product = 0;
            $grData->total_gross = 0;
            $grData->disc = 0;
            $grData->total_disc = 0;
            $grData->total_dpp = 0;
            $grData->ppn = $ppn = ($whId != null ? $whId->ppn:0);
            $grData->total_ppn = 0;
            $grData->total_nett = 0;
            $grData->wh_id = ($whId != null ? $whId->wh_id:0);
            $grData->created_date = \Carbon\Carbon::now();
            $grData->updated_date = \Carbon\Carbon::now();
            $grData->created_by = $userId;
            $insert = $grData->save();
        } catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error inserting');
        }

        $max_gr_id = grGoodsReceiveModel::max('gr_id');
        $po_id = poPurchaseOrderModel::select('po_id')
                                       ->where('po_no', $request->po_no)
                                       ->first();

        $this->loadDetailPo($max_gr_id, $po_id->po_id);
        return Redirect::back()->with('status', 'Gr added');
      }

    }

    public function editGr(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $gr_id = $request->gr_id;
      $gr_date = strtotime($request->gr_date);
      $gr_date = date("Y-m-d", $gr_date);

      $req_date = strtotime($request->req_date);
      $req_date = date("Y-m-d", $req_date);
      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        if($priv->canApprove != 1){
          if($request->gr_status != "approve"){
                      try {
                        $grData = new grGoodsReceiveModel();
                        $grData::where('po_id', $po_id)
                                   ->update([
                                          'po_no'=>$request->po_no,
                                          'gr_date'=>$gr_date,
                                          'ppn_type'=>$request->ppn_type,
                                          'notes'=>$request->notes,
                                          'gr_status'=>$request->gr_status,
                                          'updated_date' => \Carbon\Carbon::now(),
                                          'updated_by' => $userId
                                    ]);
                        } catch(\Illuminate\Database\QueryException $ex){
                          if($ex->getCode() == 23000){
                            return Redirect::back()->with('status', 'error : duplicate entry'.$ex->getMessage());
                          }else{
                            return Redirect::back()->with('status', 'Error inserting');
                          }
                        }
            }else{
              return Redirect::back()->with('status', 'Not permitted');
            }
      }else{
          try {
            $grData = new grGoodsReceiveModel();
            $grData::where('gr_id', $gr_id)
                       ->update([
                              'gr_no'=>$request->gr_no,
                              'po_no'=>$request->po_no,
                              'gr_date'=>$gr_date,
                              'ppn_type'=>$request->ppn_type,
                              'notes'=>$request->notes,
                              'gr_status'=>$request->gr_status,
                              'updated_date' => \Carbon\Carbon::now(),
                              'updated_by' => $userId
                        ]);
            } catch(\Illuminate\Database\QueryException $ex){
              if($ex->getCode() == 23000){
                return Redirect::back()->with('status', 'error : duplicate entry');
              }else{
                return Redirect::back()->with('status', 'Error inserting');
              }
            }
      }
        $sum_qty = grGoodsReceiveDetailModel::where('gr_id', $gr_id)->sum('qty_receive');
        $sum_subtotal = grGoodsReceiveDetailModel::where('gr_id', $gr_id)->sum('sub_total');
        $this->updateTotalGr($gr_id, $sum_subtotal, $sum_qty);

        return Redirect::back()->with('status', 'Gr updated');
      }

    }

    public function deleteGr(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $gr_id = $request->gr_id;

      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $grData = new grGoodsReceiveModel();
        $grData::where('gr_id', $gr_id)->delete();

        $grDetailData = new grGoodsReceiveDetailModel();
        $grDetailData::where('gr_id', $gr_id)->delete();
        return Redirect::back()->with('status', 'Gr deleted');
      }
    }

      public function deleteBulkGr(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $gr_id = $request->gr_id;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $grData = new grGoodsReceiveModel();
        $grData::whereIn('gr_id', $gr_id)->delete();

        $grDetailData = new grGoodsReceiveDetailModel();
        $grDetailData::whereIn('gr_id', $gr_id)->delete();

        $status = "success";

      }
      return $status;
    }
}
