<?php

namespace App\Http\Controllers\goodsreceive;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\purchaseorder\po_purchase_order\poPurchaseOrderModel;
use App\purchaseorder\po_detail_purchase_order\poPurchaseOrderDetailModel;
use App\goodsreceive\gr_goods_receive\grGoodsReceiveModel;
use App\goodsreceive\gr_detail_goods_receive\grGoodsReceiveDetailModel;
use App\stocktxn\im_stock_txn\imStockTxnModel;
use App\inventory\im_prd_master\imPrdMasterModel;
use App\inventory\im_prd_unit\imPrdUnitModel;
use App\usermanagement\um_module\umModuleModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Datatables;

class grDetailGoodsReceive extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    private function updateTotalGr($grid, $price, $qty, $status){
      $ppn = grGoodsReceiveModel::select('ppn')->where('gr_id', $grid)->first();
      if($ppn->ppn > 0){
        $percentPPN = $ppn->ppn / 100;
        $total_ppn = $percentPPN * $price;
        $total_gross = $price + $total_ppn;
      }else{
          $total_gross = $price;
          $total_ppn = 0;
      }
      try {
            $updateGr = new grGoodsReceiveModel();
            $updateGr::where('gr_id', $grid)
                       ->update([
                          "total_qty" => $qty,
                          "total_gross_product" => $total_gross,
                          "total_gross" => $total_gross,
                          "total_nett" => $total_gross,
                          "total_ppn" => $total_ppn,
                          "gr_status" => $status
                       ]);
     } catch(\Illuminate\Database\QueryException $ex){
         return false;
     }
     return true;

    }

    public function exportToExcel($view, $poId){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv->canExport)){
        $detailPo = poPurchaseOrderDetailModel::join('GS_MASTER_BARANG', 'GS_DETAIL_PO.barangCode', '=', 'GS_MASTER_BARANG.barangCode')
                                  ->select('GS_DETAIL_PO.detailPoId', 'GS_DETAIL_PO.detailPoCode', 'GS_DETAIL_PO.qty', 'GS_MASTER_BARANG.barangName')
                                  ->where('GS_DETAIL_PO.poCode', $poId)
                                  ->get();
         $data = null;
         foreach($detailPo as $datas){
             $data[] = array(
               $datas->detailPoId,
               $datas->detailPoCode,
               $datas->barangName,
               $datas->qty
             );
         }
         if($data != null){
           $column = array('ID DETAIL PO', 'DETAIL PO CODE','NAMA BARANG', 'QTY');
           $this->excelDownload($data, "Data po", $column);
         }else{
           return Redirect::back()->withErrors(['No data']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function addDetailGr(Request $request, $view, $grId){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $uoms = imPrdUnitModel::select('uom_id', 'uom_desc')->get();

      $detailGr = grGoodsReceiveDetailModel::join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'GR_DETAIL.prd_id')
                                ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IPM.size_id')
                                ->join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'GR_DETAIL.uom_id')
                                ->select('GR_DETAIL.gr_detail_id', 'GR_DETAIL.qty_receive', 'IPM.prd_id', 'IPM.prd_desc', 'IPS.size_desc', 'GR_DETAIL.uom_id', 'IPU.uom_desc', 'GR_DETAIL.uom_conversion')
                                ->where('GR_DETAIL.gr_id', $grId)
                                ->get();

      $gr_detail_id = grGoodsReceiveDetailModel::select('gr_detail_id')->where('gr_id', $grId)->first();
      $gr_id = grGoodsReceiveModel::select('gr_id', 'gr_no', 'gr_status', 'created_date')->where('gr_id', $grId)->first();

      $listModules = $this->listModules($userGroupId);
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('goodsreceive.gr_detail_goods_receive.'.$view, [
            'userGroupId' => $userGroupId,
            'listModules' => $listModules,
            'title' => $userModule,
            'privilaged' => $priv,
            'uoms' => $uoms,
            'detailGr' => $detailGr,
            'gr_id' => $gr_id,
            'gr_detail_id' => $gr_detail_id
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }


    public function addDetailGrData(Request $request, $view, $grId){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $detailGr = grGoodsReceiveDetailModel::join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'GR_DETAIL.prd_id')
                                ->join('IM_PRD_COLOR AS IPC', 'IPC.color_id', '=', 'IPM.color_id')
                                ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IPM.size_id')
                                ->join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'GR_DETAIL.uom_id')
                                ->select('GR_DETAIL.gr_detail_id', 'GR_DETAIL.qty_receive', 'GR_DETAIL.sub_total', 'IPM.prd_id', 'IPM.prd_desc', 'IPS.size_desc', 'GR_DETAIL.uom_id', 'IPU.uom_desc', 'GR_DETAIL.uom_conversion', 'IPC.color_desc', 'GR_DETAIL.po_detail_id')
                                ->where('GR_DETAIL.gr_id', $grId);

      $uoms = imPrdUnitModel::select('uom_id', 'uom_desc')->get();
      $gr_id = grGoodsReceiveModel::select('gr_id', 'gr_no', 'gr_status')->where('gr_id', $grId)->first();
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return Datatables::of($detailGr)
        ->addColumn('delete_bulk', function($data){
            return "
              <input type='checkbox' name='gr_detail_id[]' value='".$data->gr_Detail_id."' />
            ";
        })
        ->editColumn('qty_receive', function ($data) {
               return number_format($data->qty_receive);
        })
        ->addColumn('action', function ($data) use ($priv, $gr_id, $uoms) {
            return view('goodsreceive.gr_detail_goods_receive.grDetailDataTable',
            [
               'privilaged' => $priv,
               'data' => $data,
               'gr_id' => $gr_id,
               'uoms' => $uoms
            ]);
        })
        ->make(true);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }


    public function editDetailGr(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $gr_detail_id = $request->gr_detail_id;
      $po_detail_id = $request->po_detail_id;
      $grId = $request->gr_id;
      $grNo = grGoodsReceiveModel::join('PO_HEADER AS PH', 'PH.po_no', '=', 'GR_HEADER.po_no')
                                   ->join('PO_DETAIL AS PD', 'PD.po_id', '=', 'PH.po_id')
                                   ->select('GR_HEADER.gr_no', 'GR_HEADER.gr_date', 'GR_HEADER.wh_id', 'GR_HEADER.po_no',
                                   'PH.total_qty')
                                   ->where('GR_HEADER.gr_id', $grId)
                                   ->groupBy('GR_HEADER.gr_no', 'GR_HEADER.gr_date', 'GR_HEADER.wh_id', 'GR_HEADER.po_no', 'PH.total_qty')
                                   ->first();

      $poTotalQty = poPurchaseOrderDetailModel::join('PO_HEADER AS PH', 'PH.po_id', '=', 'PO_DETAIL.po_id')
                                                ->where('PO_DETAIL.po_detail_id', $po_detail_id)
                                                ->sum('PH.total_qty');

      $poDetailData = poPurchaseOrderDetailModel::select('qty_order')->where('po_detail_id', $po_detail_id)->first();

      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        if($request->qty_receive <= $poDetailData->qty_order){
          try {
            $detailGrData = new grGoodsReceiveDetailModel();
            $detailGrData::where('gr_detail_id', $gr_detail_id)
                       ->update([
                              'prd_id'=>$request->prd_id,
                              'qty_receive'=>$request->qty_receive,
                              'uom_id'=>$request->uom_id,
                              'uom_conversion'=>$request->uom_conversion,
                        ]);
            } catch(\Illuminate\Database\QueryException $ex){
                return Redirect::back()->with('status', 'Error updating');
            }

          $sum_qty = grGoodsReceiveDetailModel::where('gr_id', $grId)->sum('qty_receive');
          $sum_subtotal = grGoodsReceiveDetailModel::where('gr_id', $grId)->sum('sub_total');
          $sum_subtotal = preg_replace("/[^A-Za-z0-9 ]/", '', $sum_subtotal);
          $sum_subtotal = str_replace('Rp', '', $sum_subtotal);
          /* insert TXN */
            try {
                $insertStockTxn = new imStockTxnModel();
                $insertStockTxn->txn_id = $grId;
                $insertStockTxn->txn_no = $grNo->gr_no;
                $insertStockTxn->txn_date = $grNo->gr_date;
                $insertStockTxn->txn_type_id = 1;
                $insertStockTxn->wh_id = ($grNo != null ? $grNo->wh_id:0);
                $insertStockTxn->prd_id = $request->prd_id;
                $insertStockTxn->qty = $sum_qty;
                $insertStockTxn->cost_value = $sum_subtotal;
                $insertStockTxn->txn_index = 1;
                $insertStockTxn->created_date = \Carbon\Carbon::now();
                $insertStockTxn->created_by = Auth::user()->id;
                $insertStockTxn->save();
              } catch(\Illuminate\Database\QueryException $ex){
                  return Redirect::back()->with('status', 'Error inserting txn');
              }

          if($sum_qty == $poTotalQty){
            $this->updateTotalGr($grId, $sum_subtotal, $sum_qty, "finish");
          }else{
            $this->updateTotalGr($grId, $sum_subtotal, $sum_qty, "partial");
          }

          return Redirect::back()->with('status', 'Detail gr updated');
        }else if($request->qty_receive > $poDetailData->qty_order){
          return Redirect::back()->with('status', 'Qty receive > qty order');
        }
      }

    }

    public function deleteDetailGr(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $gr_detail_id = $request->gr_detail_id;
      $grId = $request->gr_id;
      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $detailGrData = new grGoodsReceiveDetailModel();
        $detailGrData::where('gr_detail_id', $gr_detail_id)->delete();

        $sum_qty = grGoodsReceiveDetailModel::where('gr_id', $grId)->sum('qty_receive');
        $sum_subtotal = grGoodsReceiveDetailModel::where('gr_id', $grId)->sum('sub_total');
        $sum_subtotal = preg_replace("/[^A-Za-z0-9 ]/", '', $sum_subtotal);
        $sum_subtotal = str_replace('Rp', '', $sum_subtotal);
        $this->updateTotalGr($grId, $sum_subtotal, $sum_qty);

        return Redirect::back()->with('status', 'Detail gr deleted');
      }

    }

    public function deleteBulkDetailGr(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $gr_detail_id = $request->gr_detail_id;
      $grId = $request->gr_id;
      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $detailGrData = new grGoodsReceiveDetailModel();
        $detailGrData::whereIn('gr_detail_id', $gr_detail_id)->delete();

        $sum_qty = grGoodsReceiveDetailModel::where('gr_id', $grId)->sum('qty_receive');
        $sum_subtotal = grGoodsReceiveDetailModel::where('gr_id', $grId)->sum('sub_total');
        $sum_subtotal = preg_replace("/[^A-Za-z0-9 ]/", '', $sum_subtotal);
        $sum_subtotal = str_replace('Rp', '', $sum_subtotal);
        $this->updateTotalGr($grId, $sum_subtotal, $sum_qty);

        $status = "success";

      }
      return $status;
    }
}
