<?php

namespace App\Http\Controllers\inventory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\usermanagement\um_module\umModuleModel;
use App\inventory\im_prd_sub_group\imPrdSubGroupModel;
use Illuminate\Support\Facades\Auth;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Ramsey\Uuid\Uuid;
use Datatables;

class imPrdSubGroup extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listView($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        return view('inventory.im_prd_sub_group.'.$view, [
            'userGroupId' => $userGroupId,
            'listModules' => $listModules,
            'title' => $userModule,
            'privilaged' => $priv
        ]);
    }

    public function listData($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        $subGroups = imPrdSubGroupModel::select('sgroup_id', 'sgroup_code', 'sgroup_desc');

        return Datatables::of($subGroups)
        ->addColumn('delete_bulk', function($data){
            return "
              <input type='checkbox' name='sgroup_id[]' value='".$data->sgroup_id."' />
            ";
        })
        ->addColumn('action', function ($data) use ($priv) {
            return view('inventory.im_prd_sub_group.imPrdSubGroupDataTable', ['privilaged' => $priv, 'data' => $data]);
       })
        ->make(true);
    }

    public function add(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      /* $maxSgroupCode = imPrdSubGroupModel::max('sgroup_code');

      if(is_null($maxSgroupCode)){
         $sGroupCode = "sgrp-0001";
      }else{
        $sGroupCode = $maxSgroupCode;
        $sGroupCode = explode("-", $sGroupCode);
        $sGroupCode = $sGroupCode[1] + 1;
        $sGroupCode = "sgrp-".str_pad($sGroupCode, 4, '0',STR_PAD_LEFT);
      } */

      if(!is_null($priv) && $priv->canAdd != 0){
        try{
          $sGroupData = new imPrdSubGroupModel();
          //$sGroupData->sgroup_id = Uuid::uuid5(Uuid::NAMESPACE_DNS, 'php.net')->getHex();
          $sGroupData->sgroup_code = $request->sgroup_code;
          $sGroupData->sgroup_desc = $request->sgroup_desc;
          $sGroupData->created_date = \Carbon\Carbon::now();
          $sGroupData->updated_date = \Carbon\Carbon::now();
          $sGroupData->created_by = $userId;
          $sGroupData->updated_by = $userId;
          $insert =  $sGroupData->save();

          return Redirect::back()->with('status', 'Subgroup added');
        } catch(\Illuminate\Database\QueryException $ex){
            dd($ex->getMessage());
            return Redirect::back()->with('status', 'Error inserting');
        }
      }else{
        return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function edit(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idSgroup = $request->sgroup_id;

      if(!is_null($priv) && $priv->canEdit != 0){
        try{
          $sGroupData = new imPrdSubGroupModel();
          $sGroupData::where('sgroup_id', $idSgroup)
                     ->update([
                            'sgroup_code'=>$request->sgroup_code,
                            'sgroup_desc'=>$request->sgroup_desc,
                            'updated_date' => \Carbon\Carbon::now(),
                            'updated_by' => $userId
                      ]);
           return Redirect::back()->with('status', 'Subgroup updated');
         } catch(\Illuminate\Database\QueryException $ex){
           if($ex->getCode() == 23000){
             return Redirect::back()->with('status', 'error : duplicate entry');
           }else{
             return Redirect::back()->with('status', 'Error updating');
           }
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function delete(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idSgroup = $request->sgroup_id;

      if(!is_null($priv) && $priv->canDelete != 0){
        $sGroupData = new imPrdSubGroupModel();
        $sGroupData::where('sgroup_id', $idSgroup)->delete();
        return Redirect::back()->with('status', 'Subgroup deleted');
      }else{
        return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function deleteBulk(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idSgroup = $request->sgroup_id;
      if(!is_null($priv) && $priv->canDelete != 0){
          $sGroupData = new imPrdSubGroupModel();
          $sGroupData::whereIn('sgroup_id', $idSgroup)->delete();
          $status = "success";
      }else{
          $status = 'Not permitted';
      }
      return $status;
    }

    public function exportToExcel($view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv) && $priv->canExport != 0){
        $datamodel = imPrdSubGroupModel::select('sgroup_code', 'sgroup_desc')
                                        ->get();
         $data = null;
         foreach($datamodel as $datas){
             $data[] = array(
               $datas->sgroup_code,
               $datas->sgroup_desc,
             );
         }
         if($data != null){
           $column = array('SUB GROUP CODE', 'SUB GROUP DESC');
           $this->excelDownload($data, "Data Sub Group", $column);
         }else{
           return Redirect::back()->withErrors(['No data']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }
}
