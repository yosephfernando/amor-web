<?php

namespace App\Http\Controllers\inventory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\usermanagement\um_module\umModuleModel;
use App\inventory\im_prd_brand\imPrdBrandModel;
use Illuminate\Support\Facades\Auth;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Datatables;

class imPrdBrand extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listView($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        return view('inventory.im_prd_brand.'.$view, [
            'userGroupId' => $userGroupId,
            'listModules' => $listModules,
            'title' => $userModule,
            'privilaged' => $priv
        ]);
    }

    public function listData($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        $brands = imPrdBrandModel::select('brand_id', 'brand_code', 'brand_desc');

        return Datatables::of($brands)
        ->addColumn('delete_bulk', function($data){
            return "
              <input type='checkbox' name='brand_id[]' value='".$data->brand_id."' />
            ";
        })
        ->addColumn('action', function ($data) use ($priv) {
            return view('inventory.im_prd_brand.imPrdBrandDataTable', ['privilaged' => $priv, 'data' => $data]);
       })
        ->make(true);
    }

    public function add(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
         try {
            $brandData = new imPrdBrandModel();
            $brandData->brand_code = $request->brand_code;
            $brandData->brand_desc = $request->brand_desc;
            $brandData->created_date = \Carbon\Carbon::now();
            $brandData->updated_date = \Carbon\Carbon::now();
            $brandData->created_by = $userId;
            $brandData->updated_by = $userId;
            $insert =  $brandData->save();

            return Redirect::back()->with('status', 'Brand added');
          } catch(\Illuminate\Database\QueryException $ex){
              return Redirect::back()->with('status', 'Error inserting');
          }
      }

    }

    public function edit(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idBrand = $request->brand_id;

      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
          $brandData = new imPrdBrandModel();
          $brandData::where('brand_id', $idBrand)
                     ->update([
                            'brand_code'=>$request->brand_code,
                            'brand_desc'=>$request->brand_desc,
                            'updated_date' => \Carbon\Carbon::now(),
                            'updated_by' => $userId
                      ]);
          return Redirect::back()->with('status', 'Brand updated');
        } catch(\Illuminate\Database\QueryException $ex){
          if($ex->getCode() == 23000){
            return Redirect::back()->with('status', 'error : duplicate entry');
          }else{
            return Redirect::back()->with('status', 'Error updating');
          }
        }
      }

    }

    public function delete(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idBrand = $request->brand_id;

      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $brandData = new imPrdBrandModel();

        $brandData::where('brand_id', $idBrand)->delete();

        return Redirect::back()->with('status', 'Brand deleted');
      }

    }

    public function deleteBulk(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idBrand = $request->brand_id;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $brandData = new imPrdBrandModel();
        $brandData::whereIn('brand_id', $idBrand)->delete();
        $status = "success";

      }
      return $status;
    }

    public function exportToExcel($view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv->canExport)){
        $datamodel = imPrdBrandModel::select('brand_code', 'brand_desc')
                                        ->get();
        $data = null;
        foreach($datamodel as $datas){
             $data[] = array(
               $datas->brand_code,
               $datas->brand_desc,
             );
         }
         if($data != null){
           $column = array('BRAND CODE', 'BRAND DESC');
           $this->excelDownload($data, "Data Brand", $column);
         }else{
           return Redirect::back()->withErrors(['No data to export']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }
}
