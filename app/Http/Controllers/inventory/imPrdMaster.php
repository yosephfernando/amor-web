<?php

namespace App\Http\Controllers\inventory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\usermanagement\um_module\umModuleModel;
use App\inventory\im_prd_brand\imPrdBrandModel;
use App\inventory\im_prd_master\imPrdMasterModel;
use App\inventory\im_prd_master_dim\imPrdMasterDimModel;
use App\inventory\im_prd_group\imPrdGroupModel;
use App\inventory\im_prd_sub_group\imPrdSubGroupModel;
use App\inventory\im_prd_color\imPrdColorModel;
use App\inventory\im_prd_size_group\imPrdSizeGroupModel;
use App\inventory\im_prd_unit\imPrdUnitModel;
use App\inventory\im_prd_field_type\imPrdFieldTypeModel;
use App\inventory\im_prd_field\imPrdFieldModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Traits\privilaged;
use Datatables;

class imPrdMaster extends Controller
{
    use privilaged;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function ajaxBrandCatalog($view, $keyword){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('inventory.im_prd_master.'.$view, [
            'keyword' => $keyword,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function ajaxGroupCatalog($view, $keyword){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('inventory.im_prd_master.'.$view, [
            'keyword' => $keyword,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function ajaxSgroupCatalog($view, $keyword){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('inventory.im_prd_master.'.$view, [
            'keyword' => $keyword,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function ajaxColorCatalog($view, $keyword){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('inventory.im_prd_master.'.$view, [
            'keyword' => $keyword,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function ajaxSgroup($view, $sizeGroupCode){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $sizeGroupCode = sprintf("%'.03d", $sizeGroupCode);
      $sizeGroup = imPrdSizeGroupModel::join('IM_PRD_SIZE', 'IM_PRD_SIZE.size_id', '=', 'IM_PRD_SIZE_GROUP.size_id')
                                        ->select('IM_PRD_SIZE.size_id', 'IM_PRD_SIZE.size_desc', 'IM_PRD_SIZE_GROUP.size_group_code')
                                        ->where('IM_PRD_SIZE_GROUP.size_group_code', $sizeGroupCode)
                                        ->get();

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('inventory.im_prd_master.'.$view, [
            'sizeGroup' => $sizeGroup,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function ajaxSgroupEdit($view, $productId){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $prd = imPrdMasterModel::join('IM_PRD_SIZE AS IPZ', 'IPZ.size_id', '=', 'IM_PRD_MASTER.size_id')
                                ->select('IPZ.size_id', 'IPZ.size_desc', 'IM_PRD_MASTER.order_price', 'IM_PRD_MASTER.retail_price', 'IM_PRD_MASTER.barcode', 'IM_PRD_MASTER.prd_id')
                                ->where('IM_PRD_MASTER.prd_id', $productId)
                                ->get();

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('inventory.im_prd_master.'.$view, [
            'prd' => $prd,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function ajaxAttrEdit($view, $prdId){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $fieldType = imPrdFieldTypeModel::select('prd_field_type_id', 'prd_field_type_code', 'prd_field_type_desc')
                                        ->get();

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('inventory.im_prd_master.'.$view, [
            'fieldType' => $fieldType,
            'prdId' => $prdId
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function listView($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        $sizeGroup = imPrdSizeGroupModel::select('size_group_code')
                                          ->groupBy('size_group_code')
                                          ->get();

        $fieldType = imPrdFieldTypeModel::select('prd_field_type_id', 'prd_field_type_code', 'prd_field_type_desc')
                                      ->get();

        $unit = imPrdUnitModel::select('uom_id', 'uom_code', 'uom_desc')->get();
        return view('inventory.im_prd_master.'.$view, [
            'userGroupId' => $userGroupId,
            'listModules' => $listModules,
            'title' => $userModule,
            'fieldType' => $fieldType,
            'unit' => $unit,
            'sizeGroup' => $sizeGroup,
            'privilaged' => $priv
        ]);
    }

    public function listData($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        $products = imPrdMasterModel::leftJoin('IM_PRD_BRAND AS IPB', 'IPB.brand_id', '=', 'IM_PRD_MASTER.brand_id')
                                      ->leftJoin('IM_PRD_GROUP AS IPG', 'IPG.group_id', '=', 'IM_PRD_MASTER.group_id')
                                      ->leftJoin('IM_PRD_SUB_GROUP AS IPSG', 'IPSG.sgroup_id', '=', 'IM_PRD_MASTER.sub_group_id')
                                      ->leftJoin('IM_PRD_COLOR AS IPC', 'IPC.color_id', '=', 'IM_PRD_MASTER.color_id')
                                      ->leftJoin('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IM_PRD_MASTER.size_id')
                                      ->leftJoin('IM_PRD_SIZE_GROUP AS IPZG', 'IPZG.size_group_code', '=', 'IM_PRD_MASTER.size_group_code')
                                      ->leftJoin('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'IM_PRD_MASTER.uom_id1')
                                      ->leftJoin('IM_PRD_UNIT AS IPU2', 'IPU2.uom_id', '=', 'IM_PRD_MASTER.uom_id2')
                                      ->leftJoin('IM_PRD_MASTER_DIM AS IPMD', 'IPMD.prd_id', '=', 'IM_PRD_MASTER.prd_id')
                                      ->select(
                                          'IM_PRD_MASTER.prd_id', 'IM_PRD_MASTER.prd_code', 'IM_PRD_MASTER.prd_desc','IM_PRD_MASTER.active_flag',
                                          'IM_PRD_MASTER.prd_short_desc', 'IM_PRD_MASTER.brand_id','IM_PRD_MASTER.group_id', 'IM_PRD_MASTER.sub_group_id',
                                          'IM_PRD_MASTER.color_id', 'IM_PRD_MASTER.size_id', 'IM_PRD_MASTER.size_group_code',
                                          'IM_PRD_MASTER.uom_id1', 'IM_PRD_MASTER.uom_id2', 'IM_PRD_MASTER.uom_conversion', 'IPB.brand_code',
                                          'IPB.brand_desc', 'IPG.group_code', 'IPG.group_desc', 'IPSG.sgroup_code',
                                          'IPSG.sgroup_desc', 'IPC.color_code', 'IPC.color_desc', 'IPS.size_code',
                                          'IPS.size_desc', 'IPU.uom_code as uom_code1', 'IPU.uom_desc as uom_desc1',
                                          'IPU2.uom_code as uom_code2', 'IPU2.uom_desc as uom_desc2'
                                       )->groupBy('IM_PRD_MASTER.prd_id', 'IM_PRD_MASTER.prd_code', 'IM_PRD_MASTER.prd_desc','IM_PRD_MASTER.active_flag',
                                       'IM_PRD_MASTER.prd_short_desc', 'IM_PRD_MASTER.brand_id','IM_PRD_MASTER.group_id', 'IM_PRD_MASTER.sub_group_id',
                                       'IM_PRD_MASTER.color_id', 'IM_PRD_MASTER.size_id', 'IM_PRD_MASTER.size_group_code',
                                       'IM_PRD_MASTER.uom_id1', 'IM_PRD_MASTER.uom_id2', 'IM_PRD_MASTER.uom_conversion', 'IPB.brand_code',
                                       'IPB.brand_desc', 'IPG.group_code', 'IPG.group_desc', 'IPSG.sgroup_code',
                                       'IPSG.sgroup_desc', 'IPC.color_code', 'IPC.color_desc', 'IPS.size_code',
                                       'IPS.size_desc', 'IPU.uom_code', 'IPU.uom_desc',
                                       'IPU2.uom_code', 'IPU2.uom_desc');

        $fieldType = imPrdFieldTypeModel::select('prd_field_type_id', 'prd_field_type_code', 'prd_field_type_desc')
                                      ->get();

        $unit = imPrdUnitModel::select('uom_id', 'uom_code', 'uom_desc')->get();
        return Datatables::of($products)
          ->addColumn('delete_bulk', function($data){
              return "
                <input type='checkbox' name='prd_id[]' value='".$data->prd_id."' />
              ";
          })
          ->addColumn('action', function ($data) use ($priv, $fieldType, $unit) {
              return view('inventory.im_prd_master.tesLoop', ['privilaged' => $priv, 'data' => $data, 'fieldType' => $fieldType, 'unit' => $unit]);
            })
          ->make(true);
    }

    public static function getFieldCode($idFieldType){
      $fields = imPrdFieldModel::select('prd_field_id','prd_field_desc')
                                 ->where('prd_field_type_id', $idFieldType)
                                 ->get();

      echo "<select id='attr".$idFieldType."' name='attr".$idFieldType."' class='form-control required'>";
          echo "<option value='0'>-- choose --</option>";
        foreach($fields as $field){
          echo "<option value=".$field->prd_field_id.">".$field->prd_field_desc."</option>";
        }
      echo "</select>";

    }

    public static function getFieldCodeEdit($idFieldType, $prdId){
      $fieldsSelected = imPrdFieldModel::join('IM_PRD_MASTER_DIM AS IPMD', 'IPMD.prd_field_id', '=', 'IM_PRD_FIELD.prd_field_id')
                                 ->select('IPMD.prd_dim_id', 'IM_PRD_FIELD.prd_field_id','IM_PRD_FIELD.prd_field_desc')
                                 ->where('IPMD.prd_id', $prdId)
                                 ->where('IM_PRD_FIELD.prd_field_type_id', $idFieldType)
                                 ->get();

       $dims = imPrdFieldModel::join('IM_PRD_MASTER_DIM AS IPMD', 'IPMD.prd_field_id', '=', 'IM_PRD_FIELD.prd_field_id')
                                  ->select('IPMD.prd_dim_id')
                                  ->where('IPMD.prd_id', $prdId)
                                  ->where('IM_PRD_FIELD.prd_field_type_id', $idFieldType)
                                  ->get();

      $fields = imPrdFieldModel::select('prd_field_id','prd_field_desc')
                                ->where('prd_field_type_id', $idFieldType)
                                ->get();
      foreach($dims as $dim){
        echo "<input type='hidden' id='prd_dim_id".$idFieldType.$prdId."' name='prd_dim_id".$idFieldType."' value='$dim->prd_dim_id'>";
      }
      echo "<select id='attrS".$idFieldType.$prdId."' name='attr".$idFieldType."' class='form-control required' >";
          echo "<option value='0'>-- choose --</option>";
        foreach($fieldsSelected as $field){
          echo "<option value=".$field->prd_field_id." selected>".$field->prd_field_desc."</option>";
        }
        foreach($fields as $field){
          echo "<option value=".$field->prd_field_id.">".$field->prd_field_desc."</option>";
        }
      echo "</select>";
    }

    public function add(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
         $sizeIds = $request->size_id;
         $countSizeIds = count($sizeIds);
         $fieldTypes = imPrdFieldTypeModel::select('prd_field_type_id')->get();

         foreach($fieldTypes as $fieldType){
           $attr = "attr".$fieldType->prd_field_type_id;
          if($request->$attr == 0){
              return Redirect::back()->with('status', 'error : Please complete the form');
           }
         }

         for($i = 0; $i <= $countSizeIds - 1; $i++){
           $odPrice = "order_price".$sizeIds[$i];
           $rtPrice = "retail_price".$sizeIds[$i];
           $barCodee = "barcode".$sizeIds[$i];

           $invPrice = $request->$odPrice / $request->uom_conversion;
            try {
              $prodData = new imPrdMasterModel();
              $prodData->prd_type_id = 1;//$request->prd_type_id;
              $prodData->brand_id = $request->brand_id;
              $prodData->group_id = $request->group_id;
              $prodData->sub_group_id = $request->sgroup_id;
              $prodData->production_code = 1;//$request->production_code;
              $prodData->prd_code = $request->prd_code;
              $prodData->color_id = $request->color_id;
              $prodData->size_id = $sizeIds[$i];
              $prodData->size_group_code = $request->size_group_code;
              $prodData->prd_desc = $request->prd_desc;
              $prodData->prd_short_desc = $request->prd_short_desc;
              $prodData->barcode = $request->$barCodee;
              $prodData->supp_id = 1;//$request->supp_id;
              $prodData->uom_id1 = $request->uom_id1;
              $prodData->uom_id2 = $request->uom_id2;
              $prodData->uom_conversion = $request->uom_conversion;
              $prodData->inv_price = str_replace(".","",$invPrice);
              $prodData->order_price = str_replace(".","",$request->$odPrice);
              $prodData->retail_price = str_replace(".","",$request->$rtPrice);
              $prodData->active_flag = $request->active_flag;
              $prodData->created_date = \Carbon\Carbon::now();
              $prodData->updated_date = \Carbon\Carbon::now();
              $prodData->created_by = $userId;
              $insert =  $prodData->save();
            } catch(\Illuminate\Database\QueryException $ex){
                return Redirect::back()->with('status', 'Error inserting product');
            }


            foreach($fieldTypes as $fieldType){
              $prdMaxId = imPrdMasterModel::max('prd_id');
              if(is_null($prdMaxId)){
                 $prdMaxId = 1;
              }

              $attr = "attr".$fieldType->prd_field_type_id;
              try {
                 $prodDimData = new imPrdMasterDimModel();
                 $prodDimData->prd_field_id = $request->$attr;
                 $prodDimData->prd_id = $prdMaxId;

                 $insert =  $prodDimData->save();
              } catch(\Illuminate\Database\QueryException $ex){
                   return Redirect::back()->with('status', 'Error inserting product DIM');
              }
           }

        }
           return Redirect::back()->with('status', 'Product added');
       }
      }

    public function edit(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idProduct = $request->prd_id;
      $fieldTypes = imPrdFieldTypeModel::select('prd_field_type_id')->get();

      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
          $invPrice = $request->order_price / $request->uom_conversion;
          $prodData = new imPrdMasterModel();
          $prodData::where('prd_id', $idProduct)
                     ->update([
                            'prd_type_id'=>1,//$request->prd_type_id,
                            'brand_id'=>$request->brand_id,
                            'group_id'=>$request->group_id,
                            'sub_group_id'=>$request->sgroup_id,
                            'color_id'=>$request->color_id,
                            'prd_code'=>$request->product_code,
                            'production_code'=>1,//$request->production_code,
                            'size_id'=>$request->size_id,
                            'prd_desc'=>$request->product_desc,
                            'prd_short_desc'=>$request->product_short_desc,
                            'barcode'=>$request->barcode,
                            'supp_id'=>1,//$request->supp_id,
                            'uom_id1'=>$request->uom1,
                            'uom_id2'=>$request->uom2,
                            'uom_conversion'=>$request->uom_conversion,
                            'inv_price'=>str_replace(".","",$invPrice),
                            'order_price'=>str_replace(".","",$request->order_price),
                            'retail_price'=>str_replace(".","",$request->retail_price),
                            'active_flag'=>$request->active_flag,
                            'updated_date' => \Carbon\Carbon::now(),
                            'updated_by' => $userId
                      ]);

                      return "success";
        } catch(\Illuminate\Database\QueryException $ex){
            return "failed";
        }

        foreach($fieldTypes as $fieldType){
            try {
               $prodDimData = new imPrdMasterDimModel();
               $attr = "attrS".$fieldType->prd_field_type_id.$idProduct;
               $dim = "prd_dim_id".$fieldType->prd_field_type_id.$idProduct;
               $prodDimData = $prodDimData::where('prd_dim_id', $request->$dim)
                                            ->update([
                                              'prd_field_id' => $request->$attr
                                            ]);
             } catch(\Illuminate\Database\QueryException $ex){
                return "failed";
             }
          }
          return "success";

      }

    }

    public function delete(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idProduct = $request->prd_id;

      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $productData = new imPrdMasterModel();
        $productData::where('prd_id', $idProduct)->delete();

        $productDimData = new imPrdMasterDimModel();
        $productDimData::where('prd_id', $idProduct)->delete();

        return Redirect::back()->with('status', 'Product deleted');
      }

    }

    public function deleteBulk(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idProduct = $request->prd_id;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $productData = new imPrdMasterModel();
        $productData::whereIn('prd_id', $idProduct)->delete();

        $productDimData = new imPrdMasterDimModel();
        $productDimData::whereIn('prd_id', $idProduct)->delete();
        $status = "success";

      }
      return $status;
    }
}
