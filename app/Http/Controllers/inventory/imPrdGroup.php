<?php

namespace App\Http\Controllers\inventory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\usermanagement\um_module\umModuleModel;
use App\inventory\im_prd_group\imPrdGroupModel;
use Illuminate\Support\Facades\Auth;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Datatables;

class imPrdGroup extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listView($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        return view('inventory.im_prd_group.'.$view, [
            'userGroupId' => $userGroupId,
            'listModules' => $listModules,
            'title' => $userModule,
            'privilaged' => $priv
        ]);
    }

    public function listData($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        $groups = imPrdGroupModel::select('group_id', 'group_code', 'group_desc');

        return Datatables::of($groups)
        ->addColumn('delete_bulk', function($data){
            return "
              <input type='checkbox' name='group_id[]' value='".$data->group_id."' />
            ";
        })
        ->addColumn('action', function ($data) use ($priv) {
            return view('inventory.im_prd_group.imPrdGroupDataTable', ['privilaged' => $priv, 'data' => $data]);
       })
        ->make(true);
    }

    public function add(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      if(!is_null($priv) && $priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try{
          $groupData = new imPrdGroupModel();
          $groupData->group_code = $request->group_code;
          $groupData->group_desc = $request->group_desc;
          $groupData->created_date = \Carbon\Carbon::now();
          $groupData->updated_date = \Carbon\Carbon::now();
          $groupData->created_by = $userId;
          $groupData->updated_by = $userId;
          $insert =  $groupData->save();

          return Redirect::back()->with('status', 'Group added');
        } catch(\Illuminate\Database\QueryException $ex){
          dd($ex->getMessage());
            return Redirect::back()->with('status', 'Error inserting');
        }
      }

    }

    public function edit(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idGroup = $request->group_id;

      if(!is_null($priv) && $priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try{
          $groupData = new imPrdGroupModel();
          $groupData::where('group_id', $idGroup)
                     ->update([
                            'group_code'=>$request->group_code,
                            'group_desc'=>$request->group_desc,
                            'updated_date' => \Carbon\Carbon::now(),
                            'updated_by' => $userId,
                      ]);

          return Redirect::back()->with('status', 'Group updated');
        } catch(\Illuminate\Database\QueryException $ex){
          if($ex->getCode() == 23000){
            return Redirect::back()->with('status', 'error : duplicate entry');
          }else{
            return Redirect::back()->with('status', 'Error updating');
          }
        }
      }

    }

    public function delete(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idGroup = $request->group_id;

      if(!is_null($priv) && $priv->canDelete != 0){
        $groupData = new imPrdGroupModel();
        $groupData::where('group_id', $idGroup)->delete();
        return Redirect::back()->with('status', 'Group deleted');
      }else{
        return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function deleteBulk(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idGroup = $request->group_id;

      if(!is_null($priv) && $priv->canDelete != 0){
        $groupData = new imPrdGroupModel();
        $groupData::whereIn('group_id', $idGroup)->delete();
        $status = "success";
      }else{
        $status = 'Not permitted';
      }
      return $status;
    }

    public function exportToExcel($view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv) && !is_null($priv->canExport)){
        $datamodel = imPrdGroupModel::select('group_code', 'group_desc')
                                        ->get();
        $data = null;
        foreach($datamodel as $datas){
             $data[] = array(
               $datas->group_code,
               $datas->group_desc,
             );
         }
         if($data != null){
           $column = array('GROUP CODE', 'GROUP DESC');
           $this->excelDownload($data, "Data Group", $column);
         }else{
           return Redirect::back()->withErrors(['No data']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }
}
