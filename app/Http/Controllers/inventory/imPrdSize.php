<?php

namespace App\Http\Controllers\inventory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\usermanagement\um_module\umModuleModel;
use App\inventory\im_prd_size\imPrdSizeModel;
use Illuminate\Support\Facades\Auth;
use App\Traits\privilaged;

class imPrdSize extends Controller
{
    use privilaged;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listSize($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        $sizes = imPrdSizeModel::select('size_id', 'size_code', 'size_desc')
                              ->paginate(15);

        return view('inventory.im_prd_size.'.$view, [
            'userGroupId' => $userGroupId,
            'listModules' => $listModules,
            'title' => $userModule,
            'sizes' => $sizes,
            'privilaged' => $priv
        ]);
    }

    public function addSize(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      //if($priv->canAdd != 1){
        //return Redirect::back()->withErrors(['Not permitted']);
      //}else{
         try {
            $sizeData = new imPrdSizeModel();
            $sizeData->size_code = $request->size_code;
            $sizeData->size_desc = $request->size_desc;
            $sizeData->created_date = \Carbon\Carbon::now();
            $sizeData->updated_date = \Carbon\Carbon::now();
            $sizeData->created_by = $userId;
            $insert =  $sizeData->save();

            return Redirect::back()->with('status', 'Size added');
          } catch(\Illuminate\Database\QueryException $ex){
            if($ex->getCode() == 23000){
              return Redirect::back()->with('status', 'error : duplicate entry');
            }else{
              return Redirect::back()->with('status', 'Error inserting');
            }
          }
      //}

    }

    public function editSize(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idSize = $request->size_id;

      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
          $sizeData = new imPrdSizeModel();
          $sizeData::where('size_id', $idSize)
                     ->update([
                            'size_code'=>$request->size_code,
                            'size_desc'=>$request->size_desc,
                            'updated_date' => \Carbon\Carbon::now(),
                            'updated_by' => $userId
                      ]);
          return Redirect::back()->with('status', 'Size desc updated');
        } catch(\Illuminate\Database\QueryException $ex){
          if($ex->getCode() == 23000){
            return Redirect::back()->with('status', 'error : duplicate entry');
          }else{
            return Redirect::back()->with('status', 'Error updating');
          }
        }
      }

    }

    public function deleteSize(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idSize = $request->size_id;

      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $sizeData = new imPrdSizeModel();

        $sizeData::where('size_id', $idSize)->delete();

        return Redirect::back()->with('status', 'Size deleted');
      }

    }

    public function deleteBulkSize(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idSize = $request->size_id;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $sizeData = new imPrdSizeModel();
        $sizeData::whereIn('size_id', $idSize)->delete();
        $status = "success";

      }
      return $status;
    }

    public function ajaxSearchSize($view, $keyword)
    {
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      $sizeData = imPrdSizeModel::select('size_code', 'size_id', 'size_desc')
                               ->where('size_code', 'like', '%'.$keyword.'%')
                               ->orWhere('size_desc', 'like', '%'.$keyword.'%')
                               ->get();

      if($priv != null && $priv->canView == 1){
        return view('inventory.im_prd_size..'.$view, [
          'title' => $userModule,
          'sizeData' => $sizeData,
          'privilaged' => $priv,
          'keyword' => $keyword
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }
}
