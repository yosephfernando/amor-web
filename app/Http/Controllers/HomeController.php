<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\modulesModel;
use Illuminate\Support\Facades\Auth;
use App\Traits\privilaged;
use App\usermanagement\um_user_group\umUserGroupModel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    use privilaged;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function home()
    {
        $userGroupId = Auth::user()->userGroupId;

        $listModules = $this->listModules($userGroupId);
        $groupDashboard = umUserGroupModel::select('groupDashboard')->where('userGroupId', $userGroupId)->first();
        return view('dashboard.'.$groupDashboard->groupDashboard, [
          'userGroupId' => $userGroupId,
          'listModules' => $listModules,
        ]);
    }
}
