<?php

namespace App\Http\Controllers\generalledgerGL\master;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\usermanagement\um_module\umModuleModel;
use App\gl_general_master\glCostCenterModel;
use Illuminate\Support\Facades\Auth;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Datatables;
use DB;

class costCenterMaster extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listCcView($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);
        $cc = glCostCenterModel::select('cost_center_code', 'cost_center_desc', 'cost_center_parent', 'cost_center_active_flag')->get();
        return view('generalledgerGL.gl_cost_center.'.$view, [
            'userGroupId' => $userGroupId,
            'listModules' => $listModules,
            'title' => $userModule,
            'privilaged' => $priv,
            'cc' => $cc
        ]);
    }

    public function listCcData(Request $request, $view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        $cc = glCostCenterModel::select('cost_center_id', 'cost_center_code', 'cost_center_desc', 'cost_center_parent', 'cost_center_active_flag');

        return Datatables::of($cc)
        ->addColumn('delete_bulk', function($data){
            return "
              <input type='checkbox' name='cost_center_id[]' value='".$data->cost_center_id."' />
            ";
        })
        ->addColumn('action', function ($data) use ($priv, $view) {
            return view('generalledgerGL.gl_cost_center.'.$view, ['privilaged' => $priv, 'data' => $data]);
       })
         ->editColumn('cost_center_active_flag', function($data){
           if($data->cost_center_active_flag == 1){
             return "
             <span class='label label-md label-success'>Active</span>
             ";
           }else{
             return "
               <span class='label label-md label-danger'>Non active</span>
             ";
           }

         })
        ->make(true);
    }

    public function add(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);
      $parent = substr($request->cost_center_code,0,1);
      $maxLevel = glCostCenterModel::select(DB::raw('MAX(cost_level) AS cost_level'))->where('cost_center_parent',$parent)->first();
      if($priv != null && $priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
         try {
            $ccData = new glCostCenterModel();
            $ccData->cost_center_code = $request->cost_center_code;
            $ccData->cost_center_desc = $request->cost_center_desc;
            $ccData->cost_center_parent = substr($request->cost_center_code,0,1);
            $ccData->cost_center_active_flag = $request->input('cost_center_active_flag', 0);

            if($maxLevel->cost_level != null){
              $ccData->cost_level = $maxLevel->cost_level + 1;
            }else{
              $ccData->cost_level = 1;
            }

            $ccData->created_date = \Carbon\Carbon::now();
            $ccData->updated_date = \Carbon\Carbon::now();
            $ccData->created_by = $userId;
            $insert =  $ccData->save();

            return Redirect::back()->with('status', 'Cost center added');
          } catch(\Illuminate\Database\QueryException $ex){
              return Redirect::back()->with('status', 'Error inserting');
          }
      }

    }

    public function edit(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);
      $idCc = $request->cost_center_id;

      if($priv != null && $priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
          $ccData = new glCostCenterModel();
          $ccData::where('cost_center_id', $idCc)
                     ->update([
                            'cost_center_code'=>$request->cost_center_code,
                            'cost_center_desc'=>$request->cost_center_desc,
                            'cost_center_active_flag'=> $request->input('cost_center_active_flag', 0),
                            'cost_center_parent'=>substr($request->cost_center_code,0,1),
                            'updated_date' => \Carbon\Carbon::now(),
                            'updated_by' => $userId
                      ]);
          return Redirect::back()->with('status', 'Cost center updated');
        } catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error updating');
        }
      }

    }

    public function delete(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idCc = $request->cost_center_id;

      if($priv != null && $priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $ccData = new glCostCenterModel();

        $ccData::where('cost_center_id', $idCc)->delete();

        return Redirect::back()->with('status', 'Cost center deleted');
      }

    }

    public function deleteBulk(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idCc = $request->cost_center_id;

      if($priv != null && $priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $ccData = new glCostCenterModel();
        $ccData::whereIn('cost_center_id', $idCc)->delete();
        $status = "success";

      }
      return $status;
    }

    public function exportToExcel($view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && !is_null($priv->canExport)){
        $datamodel = glCostCenterModel::select('cost_center_code', 'cost_center_desc', 'cost_center_parent')
                                        ->get();
         $data = null;
         foreach($datamodel as $datas){
             $data[] = array(
               $datas->cost_center_code,
               $datas->cost_center_desc,
               $datas->cost_center_parent
             );
         }
         if($data != null){
           $column = array('COST CENTER CODE', 'COST CENTER DESC', 'COST CENTER PARENT');
           $this->excelDownload($data, "Data Cost Center", $column);
         }else{
           return Redirect::back()->withErrors(['No data']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }
}
