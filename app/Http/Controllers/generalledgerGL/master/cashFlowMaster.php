<?php

namespace App\Http\Controllers\generalledgerGL\master;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\usermanagement\um_module\umModuleModel;
use App\gl_general_master\glCashFlowModel;
use App\gl_general_master\glCashFlowGroupModel;
use Illuminate\Support\Facades\Auth;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Datatables;
use DB;

class cashFlowMaster extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listCfView($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);
        $cf = glCashFlowModel::join('GL_CASH_FLOW_GROUP AS CFG', 'CFG.cash_flow_group_id', '=', 'GL_CASH_FLOW.cash_flow_group_id')
                               ->select('GL_CASH_FLOW.cash_flow_id', 'GL_CASH_FLOW.cash_flow_code', 'GL_CASH_FLOW.cash_flow_desc', 'GL_CASH_FLOW.cash_flow_alias', 'CFG.cash_flow_group_desc')
                               ->get();
        $cfGroup = glCashFlowGroupModel::select('cash_flow_group_id', 'cash_flow_group_code', 'cash_flow_group_desc')->get();
        return view('generalledgerGL.gl_cash_flow.'.$view, [
            'userGroupId' => $userGroupId,
            'listModules' => $listModules,
            'title' => $userModule,
            'privilaged' => $priv,
            'cf' => $cf,
            'cfGroup' => $cfGroup
        ]);
    }

    public function listCfData(Request $request, $view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);
        $cf = glCashFlowModel::join('GL_CASH_FLOW_GROUP AS CFG', 'CFG.cash_flow_group_id', '=', 'GL_CASH_FLOW.cash_flow_group_id')
                               ->select('GL_CASH_FLOW.cash_flow_id', 'GL_CASH_FLOW.cash_flow_code', 'GL_CASH_FLOW.cash_flow_desc', 'GL_CASH_FLOW.cash_flow_alias', 'CFG.cash_flow_group_desc');
        $cfGroup = glCashFlowGroupModel::select('cash_flow_group_id', 'cash_flow_group_code', 'cash_flow_group_desc')->get();
        return Datatables::of($cf)
        ->addColumn('delete_bulk', function($data){
            return "
              <input type='checkbox' name='cash_flow_id[]' value='".$data->cash_flow_id."' />
            ";
        })
        ->addColumn('action', function ($data) use ($priv, $view, $cfGroup) {
            return view('generalledgerGL.gl_cash_flow.'.$view, ['privilaged' => $priv, 'data' => $data, 'cfGroup' => $cfGroup]);
       })
        ->make(true);
    }

    public function add(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $parent = $request->cash_flow_parent;
      $maxLevel = glCashFlowModel::select(DB::raw('MAX(cash_flow_level) AS cash_flow_level'))->where('cash_flow_parent',$parent)->first();
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      if(!is_null($priv) && $priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
         try {
            $cfData = new glCashFlowModel();
            $cfData->cash_flow_code = $request->cash_flow_code;
            $cfData->cash_flow_desc = $request->cash_flow_desc;
            $cfData->cash_flow_parent = $request->cash_flow_parent;
            $cfData->cash_flow_position = $request->cash_flow_position;
            $cfData->cash_flow_group_id = $request->cash_flow_group_id;

            if($maxLevel->cash_flow_level != null){
              $cfData->cash_flow_level = $maxLevel->cash_flow_level + 1;
            }else{
              $cfData->cash_flow_level = 1;
            }

            if($request->cash_flow_position == "header"){
              $cfData->cash_flow_position = 0;
            }else{
              $cfData->cash_flow_position = 1;
            }

            $cfData->cash_flow_alias = $request->cash_flow_alias;
            $cfData->created_date = \Carbon\Carbon::now();
            $cfData->updated_date = \Carbon\Carbon::now();
            $cfData->created_by = $userId;
            $insert =  $cfData->save();

            return Redirect::back()->with('status', 'Cash flow added');
          } catch(\Illuminate\Database\QueryException $ex){
              return Redirect::back()->with('status', 'Error inserting');
          }
      }

    }

    public function edit(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idCf = $request->cash_flow_id;

      if(!is_null($priv) && $priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
          $cfData = new glCashFlowModel();
          $cfData::where('cash_flow_id', $idCf)
                     ->update([
                            'cash_flow_code'=>$request->cash_flow_code,
                            'cash_flow_desc'=>$request->cash_flow_desc,
                            'cash_flow_parent'=>$request->cash_flow_parent,
                            'cash_flow_alias'=>$request->cash_flow_alias,
                            'cash_flow_group_id'=>$request->cash_flow_group_id,
                            'updated_date' => \Carbon\Carbon::now(),
                            'updated_by' => $userId
                      ]);
          return Redirect::back()->with('status', 'Cash flow updated');
        } catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error updating');
        }
      }

    }

    public function delete(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idCf = $request->cash_flow_id;

      if(!is_null($priv) && $priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $cfData = new glCashFlowModel();

        $cfData::where('cash_flow_id', $idCf)->delete();

        return Redirect::back()->with('status', 'Cash flow deleted');
      }

    }

    public function deleteBulk(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idCf = $request->cash_flow_id;

      if(!is_null($priv) && $priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $cfData = new glCashFlowModel();
        $cfData::whereIn('cash_flow_id', $idCf)->delete();
        $status = "success";

      }
      return $status;
    }

    public function exportToExcel($view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv) && !is_null($priv->canExport)){
        $datamodel = glCashFlowModel::select('cash_flow_code', 'cash_flow_desc')
                                    ->get();
         $data = null;
         foreach($datamodel as $datas){
             $data[] = array(
               $datas->cash_flow_code,
               $datas->cash_flow_desc
             );
         }
         if($data != null){
           $column = array('CASH FLOW CODE','CASH FLOW DESC');
           $this->excelDownload($data, "Data Cash Flow", $column);
         }else{
           return Redirect::back()->withErrors(['No data']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }
}
