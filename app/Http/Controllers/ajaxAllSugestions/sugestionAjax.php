<?php
namespace App\Http\Controllers\ajaxAllSugestions;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\glaccount\gl_account\glAccountModel;
use App\inventory\im_prd_master\imPrdMasterModel;
use App\purchaseorder\po_supp\supplierModel;
use App\warehouse\im_wh_loc\imWhLocModel;
use App\purchaseorder\po_purchase_order\poPurchaseOrderModel;
use App\goodsreceive\gr_goods_receive\grGoodsReceiveModel;
use App\purchasinginvoice\pi_purchase_invoice\piPurchaseInvoiceModel;
use App\inventory\im_prd_brand\imPrdBrandModel;
use App\inventory\im_prd_group\imPrdGroupModel;
use App\inventory\im_prd_sub_group\imPrdSubGroupModel;
use App\inventory\im_prd_color\imPrdColorModel;
use App\usermanagement\um_module\umModuleModel;
use App\sales\so_cust\customerModel;
use App\glaccount\gl_currency\glCurrencyModel;
use App\gl_general_master\glCashFlowModel;
use Illuminate\Support\Facades\DB;

class sugestionAjax extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function lookupCatalog($label, Request $request)
    {
      $keyword = strtolower($request->get('query'));
      if($label == "coa"){
        $dataSugest = glAccountModel::select(DB::raw('CAST(coa_id AS varchar) AS data'),
        DB::raw('coa_code ||'."'-'".'|| coa_desc AS value'))
        ->where(DB::raw('LOWER(coa_code ||'."'-'".'|| coa_desc)'), 'LIKE', '%'.$keyword.'%')
        ->get();
        if(!$dataSugest->isEmpty()){
          $sugestion = "";
          $i = 0;
                foreach($dataSugest as $data){
                     if($i < count($dataSugest) - 1){
                        $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'"},';
                     }else{
                        $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'"}';
                     }
                $i++;}
          return '{"suggestions" :['.$sugestion.']}';
        }else{
          return "no data";
        }
      }else if($label == "product"){
        $dataSugest = imPrdMasterModel::join('IM_PRD_COLOR AS IPC', 'IPC.color_id', '=', 'IM_PRD_MASTER.color_id')
                                       ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IM_PRD_MASTER.size_id')
                                       ->join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'IM_PRD_MASTER.uom_id1')
                                       ->select(DB::raw('CAST(prd_id AS varchar) AS data'),
                                       DB::raw('prd_desc ||'."'-'".'||"IPC"."color_desc" ||'."'-'".'||"IPS"."size_desc" AS value'),
                                       'uom_id', 'IPU.uom_desc AS uomdesc', 'uom_conversion', 'retail_price')
                                       ->where(DB::raw('LOWER(prd_desc ||'."'-'".'||"IPC"."color_desc" ||'."'-'".'||"IPS"."size_desc")'), "LIKE", '%'.$keyword.'%')
                                       ->get();

         if(!$dataSugest->isEmpty()){
           $sugestion = "";
           $i = 0;
                 foreach($dataSugest as $data){
                      if($i < count($dataSugest) - 1){
                         $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'", "uomdesc": "'.$data['uomdesc'].'", "uom_conversion": "'.$data['uom_conversion'].'", "uom_id": "'.$data['uom_id'].'", "retail_price": "'.number_format($data['retail_price'], 0, "", "").'"},';
                      }else{
                         $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'", "uomdesc": "'.$data['uomdesc'].'", "uom_conversion": "'.$data['uom_conversion'].'", "uom_id": "'.$data['uom_id'].'", "retail_price": "'.number_format($data['retail_price'], 0, "", "").'"}';
                      }
                 $i++;}
           return '{"suggestions" :['.$sugestion.']}';
         }else{
           return "no data";
         }
     }else if($label == "supplier"){
        $dataSugest = supplierModel::select(DB::raw('CAST(supp_id AS varchar) AS data'),
        'supp_desc AS value')
        ->where(DB::raw('LOWER(supp_desc)'), 'LIKE', '%'.$keyword.'%')
        ->where('supp_active_flag',1)
        ->get();

        if(!$dataSugest->isEmpty()){
          $sugestion = "";
          $i = 0;
                foreach($dataSugest as $data){
                     if($i < count($dataSugest) - 1){
                        $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'"},';
                     }else{
                        $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'"}';
                     }
                $i++;}
          return '{"suggestions" :['.$sugestion.']}';
        }else{
          return "no data";
        }
     }else if($label == "warehouse"){
       $dataSugest = imWhLocModel::select(DB::raw('CAST(wh_id AS varchar) AS data'), 'wh_desc AS value')
       ->where(DB::raw('LOWER(wh_desc)'), 'LIKE', '%'.$keyword.'%')
       ->where('active_flag', 1)
       ->get();

       if(!$dataSugest->isEmpty()){
         $sugestion = "";
         $i = 0;
               foreach($dataSugest as $data){
                    if($i < count($dataSugest) - 1){
                       $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'"},';
                    }else{
                       $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'"}';
                    }
               $i++;}
         return '{"suggestions" :['.$sugestion.']}';
       }else{
         return "no data";
       }
     }else if($label == "po"){
       $dataSugest = poPurchaseOrderModel::select(DB::raw('CAST(po_id AS varchar) as data'), 'po_no AS value')
       ->where(DB::raw('LOWER(po_no)'), 'LIKE', '%'.$keyword.'%')
       ->where('po_status', 'open')
       ->get();

       if(!$dataSugest->isEmpty()){
         $sugestion = "";
         $i = 0;
               foreach($dataSugest as $data){
                    if($i < count($dataSugest) - 1){
                       $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'"},';
                    }else{
                       $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'"}';
                    }
               $i++;}
         return '{"suggestions" :['.$sugestion.']}';
       }else{
         return "no data";
       }
     }else if($label == "goods"){
       $dataSugest = grGoodsReceiveModel::select(DB::raw('CAST(gr_id AS varchar) AS data'), 'gr_no AS value', 'gr_status')
       ->where(DB::raw('LOWER(gr_no)'), 'LIKE', '%'.$keyword.'%')
       ->where('gr_status', '!=','invoice')
       ->get();

       if(!$dataSugest->isEmpty()){
         $sugestion = "";
         $i = 0;
               foreach($dataSugest as $data){
                    if($i < count($dataSugest) - 1){
                       $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'", "gr_status":"'.$data['gr_status'].'"},';
                    }else{
                       $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'", "gr_status":"'.$data['gr_status'].'"}';
                    }
               $i++;}
         return '{"suggestions" :['.$sugestion.']}';
       }else{
         return "no data";
       }
     }else if($label == "brand"){
       $dataSugest = imPrdBrandModel::select(DB::raw('CAST(brand_id AS varchar) AS data'), DB::raw('brand_code ||'."'-'".' || brand_desc AS value'))
       ->where(DB::raw('LOWER(brand_code ||'."'-'".' || brand_desc)'), 'LIKE', '%'.$keyword.'%')
       ->get();

       if(!$dataSugest->isEmpty()){
         $sugestion = "";
         $i = 0;
               foreach($dataSugest as $data){
                    if($i < count($dataSugest) - 1){
                       $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'"},';
                    }else{
                       $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'"}';
                    }
               $i++;}
         return '{"suggestions" :['.$sugestion.']}';
       }else{
         return "no data";
       }
     }else if($label == "group"){
       $dataSugest = imPrdGroupModel::select(DB::raw('CAST(group_id AS varchar) AS data'), DB::raw('group_code ||'."'-'".' || group_desc AS value'))
       ->where(DB::raw('LOWER(group_code ||'."'-'".' || group_desc)'), 'LIKE', '%'.$keyword.'%')
       ->get();

       if(!$dataSugest->isEmpty()){
         $sugestion = "";
         $i = 0;
               foreach($dataSugest as $data){
                    if($i < count($dataSugest) - 1){
                       $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'"},';
                    }else{
                       $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'"}';
                    }
               $i++;}
         return '{"suggestions" :['.$sugestion.']}';
       }else{
         return "no data";
       }
     }else if($label == "subgroup"){
       $dataSugest = imPrdSubGroupModel::select(DB::raw('CAST(sgroup_id AS varchar) AS data'), DB::raw('sgroup_code ||'."'-'".' || sgroup_desc AS value'))
       ->where(DB::raw('LOWER(sgroup_code ||'."'-'".' || sgroup_desc)'), 'LIKE', '%'.$keyword.'%')
       ->get();

       if(!$dataSugest->isEmpty()){
         $sugestion = "";
         $i = 0;
               foreach($dataSugest as $data){
                    if($i < count($dataSugest) - 1){
                       $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'"},';
                    }else{
                       $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'"}';
                    }
               $i++;}
         return '{"suggestions" :['.$sugestion.']}';
       }else{
         return "no data";
       }
     }else if($label == "color"){
       $dataSugest = imPrdColorModel::select(DB::raw('CAST(color_id AS varchar) AS data'), DB::raw('color_code ||'."'-'".' || color_desc AS value'))
       ->where(DB::raw('LOWER(color_code ||'."'-'".' || color_desc)'), 'LIKE', '%'.$keyword.'%')
       ->get();

       if(!$dataSugest->isEmpty()){
         $sugestion = "";
         $i = 0;
               foreach($dataSugest as $data){
                    if($i < count($dataSugest) - 1){
                       $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'"},';
                    }else{
                       $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'"}';
                    }
               $i++;}
         return '{"suggestions" :['.$sugestion.']}';
       }else{
         return "no data";
       }
     }else if($label == "module"){
       $dataSugest = umModuleModel::select(DB::raw('CAST("moduleId" AS varchar) AS data'), DB::raw('(CASE WHEN "moduleParent" IS NULL THEN "moduleName"||'."'-'".'||'."'( Parent )'".' ELSE "moduleMethod"||'."'-'".'||"moduleParent" END) AS value'))
                                 ->where('moduleShowInSidebar', 1)
                                 ->where(DB::raw('(CASE WHEN "moduleParent" IS NULL THEN LOWER("moduleName"||'."'-'".'||'."'( Parent )')".' ELSE LOWER("moduleMethod"||'."'-'".'||"moduleParent") END) '), 'LIKE', '%'.$keyword.'%')
                                 ->groupBy('moduleGroup', 'moduleParent', 'moduleMethod', 'moduleId', 'moduleName')
                                 ->get();

       if(!$dataSugest->isEmpty()){
         $sugestion = "";
         $i = 0;
               foreach($dataSugest as $data){
                    if($i < count($dataSugest) - 1){
                       $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'"},';
                    }else{
                       $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'"}';
                    }
               $i++;}
         return '{"suggestions" :['.$sugestion.']}';
       }else{
         return "no data";
       }
     }else if($label == "customer"){
        $dataSugest = customerModel::select(DB::raw('CAST(cust_id AS varchar) AS data'),
        'cust_desc AS value')
        ->where(DB::raw('LOWER(cust_desc)'), 'LIKE', '%'.$keyword.'%')
        ->where('cust_active_flag',1)
        ->get();

        if(!$dataSugest->isEmpty()){
          $sugestion = "";
          $i = 0;
                foreach($dataSugest as $data){
                     if($i < count($dataSugest) - 1){
                        $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'"},';
                     }else{
                        $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'"}';
                     }
                $i++;}
          return '{"suggestions" :['.$sugestion.']}';
        }else{
          return "no data";
        }
     }else if($label == "curr"){
        $dataSugest = glCurrencyModel::select(DB::raw('CAST(curr_id AS varchar) AS data'),
        'curr_code AS value')
        ->where(DB::raw('LOWER(curr_code)'), 'LIKE', '%'.$keyword.'%')
        ->get();

        if(!$dataSugest->isEmpty()){
          $sugestion = "";
          $i = 0;
                foreach($dataSugest as $data){
                     if($i < count($dataSugest) - 1){
                        $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'"},';
                     }else{
                        $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'"}';
                     }
                $i++;}
          return '{"suggestions" :['.$sugestion.']}';
        }else{
          return "no data";
        }
     }else if($label == "pi"){
        $dataSugest = piPurchaseInvoiceModel::select(DB::raw('CAST(inv_id AS varchar) AS data'),
        'inv_no AS value')
        ->where(DB::raw('LOWER(inv_no)'), 'LIKE', '%'.$keyword.'%')
        ->where('inv_status', '=', 'approve')
        ->get();

        if(!$dataSugest->isEmpty()){
          $sugestion = "";
          $i = 0;
                foreach($dataSugest as $data){
                     if($i < count($dataSugest) - 1){
                        $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'"},';
                     }else{
                        $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'"}';
                     }
                $i++;}
          return '{"suggestions" :['.$sugestion.']}';
        }else{
          return "no data";
        }
     }else if($label == "cash-flow"){
        $dataSugest = glCashFlowModel::select(DB::raw('CAST(cash_flow_id AS varchar) AS data'),
        'cash_flow_code AS value')
        ->where(DB::raw('LOWER(cash_flow_code)'), 'LIKE', '%'.$keyword.'%')
        ->get();

        if(!$dataSugest->isEmpty()){
          $sugestion = "";
          $i = 0;
                foreach($dataSugest as $data){
                     if($i < count($dataSugest) - 1){
                        $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'"},';
                     }else{
                        $sugestion .= '{"data":"'.$data['data'].'", "value": "'.$data['value'].'"}';
                     }
                $i++;}
          return '{"suggestions" :['.$sugestion.']}';
        }else{
          return "no data";
        }
     }
  }
}
