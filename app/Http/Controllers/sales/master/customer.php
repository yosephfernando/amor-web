<?php

namespace App\Http\Controllers\sales\master;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\usermanagement\um_module\umModuleModel;
use App\sales\so_cust_type\customerTypeModel;
use App\term\termModel;
use App\sales\so_cust\customerModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Datatables;

class customer extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listView($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);
        $custs = customerTypeModel::select(DB::raw('CAST(cust_type_id AS varchar) AS data'), 'cust_type_desc AS value')->get();
        $term = termModel::select('term_id AS data', 'pptc_term_desc AS value')->get();
        return view('sales.master.so_cust.'.$view, [
            'userGroupId' => $userGroupId,
            'listModules' => $listModules,
            'title' => $userModule,
            'privilaged' => $priv,
            'custs' => $custs,
            'term' => $term
        ]);
    }

    public function listData($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        $custs = customerModel::join('GS_TERM AS GT', 'GT.term_id', '=', 'SO_CUST_MASTER.cust_term')
                                ->join('SO_CUST_TYPE AS SCT', 'SCT.cust_type_id', '=', 'SO_CUST_MASTER.cust_type_id')
                                ->select('SO_CUST_MASTER.*', 'GT.pptc_term_desc', 'SCT.cust_type_desc');

        $custType = customerTypeModel::join('GL_ACCOUNT AS GAAR', 'GAAR.coa_id', '=', 'SO_CUST_TYPE.coa_id_ar')
                                    ->join('GL_ACCOUNT AS GADP', 'GADP.coa_id', '=', 'SO_CUST_TYPE.coa_id_dp')
                                    ->join('GL_ACCOUNT AS GAPPN', 'GAPPN.coa_id', '=', 'SO_CUST_TYPE.coa_id_ppn')
                                    ->join('GL_ACCOUNT AS GASA', 'GASA.coa_id', '=', 'SO_CUST_TYPE.coa_id_sales')
                                    ->join('GL_ACCOUNT AS GASAR', 'GASAR.coa_id', '=', 'SO_CUST_TYPE.coa_id_sales_return')
                                    ->join('GL_ACCOUNT AS GADISC', 'GADISC.coa_id', '=', 'SO_CUST_TYPE.coa_id_disc')
                                    ->select(DB::raw('CAST("SO_CUST_TYPE".cust_type_id AS varchar) AS data'), 'SO_CUST_TYPE.cust_type_desc AS value')
                                    ->get();

        $term = termModel::select('term_id AS data', 'pptc_term_desc AS value')->get();

        return Datatables::of($custs)
        ->addColumn('delete_bulk', function($data){
            return "
              <input type='checkbox' name='cust_id[]' value='".$data->cust_id."' />
            ";
        })
        ->editColumn('cust_active_flag', function($data){
          if($data->cust_active_flag == 1){
            return "
            <span class='label label-md label-success'>Active</span>
            ";
          }else{
            return "
              <span class='label label-md label-danger'>Non active</span>
            ";
          }

        })
        ->addColumn('action', function ($data) use ($priv, $view, $custType, $term) {
            return view('sales.master.so_cust.'.$view, ['privilaged' => $priv, 'data' => $data, 'custType' => $custType, 'term' => $term]);
       })
        ->make(true);
    }

    public function add(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
         try {
            $custData = new customerModel();
            $custData->cust_code = $request->cust_code;
            $custData->cust_desc = $request->cust_desc;
            $custData->cust_type_id = $request->cust_type_id;
            $custData->cust_address1 = $request->cust_address1;
            $custData->cust_address2 = $request->cust_address2;
            $custData->cust_address3 = $request->cust_address3;
            $custData->cust_city = $request->cust_city;
            $custData->cust_country = $request->cust_country;
            $custData->cust_phone = $request->cust_phone;
            $custData->cust_fax = $request->cust_fax;
            $custData->cust_email = $request->cust_email;
            $custData->cust_post_code = $request->cust_post_code;
            $custData->cust_contact_person = $request->cust_contact_person;
            $custData->cust_term = $request->cust_term;
            $custData->cust_pkp_flag = $request->input('cust_pkp_flag', 0);
            $custData->cust_active_flag = $request->input('cust_active_flag', 0);
            $custData->created_date = \Carbon\Carbon::now();
            $custData->updated_date = \Carbon\Carbon::now();
            $custData->created_by = $userId;
            $insert =  $custData->save();

            return Redirect::back()->with('status', 'Customer added');
          } catch(\Illuminate\Database\QueryException $ex){
            dd($ex->getMessage());
              return Redirect::back()->with('status', 'Error inserting');
          }
      }

    }

    public function edit(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idCust = $request->cust_id;

      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
          $custData = new customerModel();
          $custData::where('cust_id', $idCust)
                     ->update([
                            'cust_code'=>$request->cust_code,
                            'cust_desc'=>$request->cust_desc,
                            'cust_type_id'=>$request->cust_type_id,
                            'cust_address1'=>$request->cust_address1,
                            'cust_address2'=>$request->cust_address2,
                            'cust_address3'=>$request->cust_address3,
                            'cust_city'=>$request->cust_city,
                            'cust_country'=>$request->cust_country,
                            'cust_phone'=>$request->cust_phone,
                            'cust_fax'=>$request->cust_fax,
                            'cust_email'=>$request->cust_email,
                            'cust_post_code'=>$request->cust_post_code,
                            'cust_contact_person'=>$request->cust_contact_person,
                            'cust_term'=>$request->cust_term,
                            'cust_pkp_flag'=>$request->input('cust_pkp_flag', 0),
                            'cust_active_flag'=>$request->input('cust_active_flag', 0),
                            'updated_date' => \Carbon\Carbon::now(),
                            'updated_by' => $userId
                      ]);
          return Redirect::back()->with('status', 'Customer updated');
        } catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error updating');
        }
      }

    }

    public function delete(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idCust = $request->cust_id;

      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $custData = new customerModel();

        $custData::where('cust_id', $idCust)->delete();

        return Redirect::back()->with('status', 'Customer deleted');
      }

    }

    public function deleteBulk(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idCust = $request->cust_id;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $custData = new customerModel();
        $custData::whereIn('cust_id', $idCust)->delete();
        $status = "success";

      }
      return $status;
    }

    public function exportToExcel($view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv->canExport)){
        $datamodel = customerModel::select('cust_code', 'cust_desc')
                                        ->get();
         $data = null;
         foreach($datamodel as $datas){
             $data[] = array(
               $datas->cust_code,
               $datas->cust_desc,
             );
         }
         if($data != null){
           $column = array('CUSTOMER CODE', 'CUSTOMER DESC');
           $this->excelDownload($data, "Data Customer", $column);
         }else{
           return Redirect::back()->withErrors(['No data to export']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }
}
