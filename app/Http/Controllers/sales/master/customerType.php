<?php

namespace App\Http\Controllers\sales\master;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\usermanagement\um_module\umModuleModel;
use App\sales\so_cust_type\customerTypeModel;
use App\glaccount\gl_account\glAccountModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Datatables;

class customerType extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function ajaxCoaCatalog($view, $keyword){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('sales.master.so_cust_type.'.$view, [
            'keyword' => $keyword,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function listCustTypeView($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);
        $coaSugest = glAccountModel::select(DB::raw('CAST(coa_id AS varchar) AS data'), DB::raw('coa_code ||'."'-'".'||coa_desc AS value'))->get();

        return view('sales.master.so_cust_type.'.$view, [
            'userGroupId' => $userGroupId,
            'listModules' => $listModules,
            'title' => $userModule,
            'coaSugest' => $coaSugest,
            'privilaged' => $priv
        ]);
    }

    public function listCustTypeData($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        $custType = customerTypeModel::join('GL_ACCOUNT AS GA', 'GA.coa_id', '=', 'SO_CUST_TYPE.coa_id_ar')
                                       ->join('GL_ACCOUNT AS GP', 'GP.coa_id', '=', 'SO_CUST_TYPE.coa_id_ppn')
                                       ->join('GL_ACCOUNT AS GD', 'GD.coa_id', '=', 'SO_CUST_TYPE.coa_id_dp')
                                       ->join('GL_ACCOUNT AS GS', 'GS.coa_id', '=', 'SO_CUST_TYPE.coa_id_sales')
                                       ->join('GL_ACCOUNT AS GSR', 'GSR.coa_id', '=', 'SO_CUST_TYPE.coa_id_sales_return')
                                       ->join('GL_ACCOUNT AS GDS', 'GDS.coa_id', '=', 'SO_CUST_TYPE.coa_id_disc')
                                       ->select('SO_CUST_TYPE.cust_type_id', 'SO_CUST_TYPE.cust_type_code', 'SO_CUST_TYPE.cust_type_desc',
                                       'SO_CUST_TYPE.coa_id_ar AS coa_id_ar', 'GA.coa_desc AS coa_desc_ar', 'GA.coa_code AS coa_code_ar',
                                       'SO_CUST_TYPE.coa_id_dp AS coa_id_dp', 'GD.coa_desc AS coa_desc_dp', 'GD.coa_code AS coa_code_dp',
                                       'SO_CUST_TYPE.coa_id_ppn AS coa_id_ppn', 'GP.coa_desc AS coa_desc_ppn', 'GP.coa_code AS coa_code_ppn',
                                       'SO_CUST_TYPE.coa_id_sales AS coa_id_sales', 'GS.coa_desc AS coa_desc_sales', 'GS.coa_code AS coa_code_sales',
                                       'SO_CUST_TYPE.coa_id_sales_return AS coa_id_sales_return', 'GSR.coa_desc AS coa_desc_sales_return', 'GSR.coa_code AS coa_code_sales_return',
                                       'SO_CUST_TYPE.coa_id_disc AS coa_id_disc', 'GDS.coa_desc AS coa_desc_disc', 'GDS.coa_code AS coa_code_disc'
                                      );
        $coaSugest = glAccountModel::select(DB::raw('CAST(coa_id AS varchar) AS data'), DB::raw('coa_code ||'."'-'".'||coa_desc AS value'))->get();
        return Datatables::of($custType)
        ->addColumn('delete_bulk', function($data){
            return "
              <input type='checkbox' name='cust_type_id[]' value='".$data->cust_type_id."' />
            ";
        })
        ->addColumn('action', function ($data) use ($priv, $view, $coaSugest) {
            return view('sales.master.so_cust_type.'.$view, ['privilaged' => $priv, 'data' => $data, 'coaSugest' => $coaSugest]);
       })
        ->make(true);
    }

    public function add(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
         try {
            $custTypeData = new customerTypeModel();
            $custTypeData->cust_type_code = $request->cust_type_code;
            $custTypeData->cust_type_desc = $request->cust_type_desc;
            $custTypeData->coa_id_ar = $request->coa_id_ar;
            $custTypeData->coa_id_dp = $request->coa_id_dp;
            $custTypeData->coa_id_ppn = $request->coa_id_ppn;
            $custTypeData->coa_id_sales = $request->coa_id_sales;
            $custTypeData->coa_id_sales_return = $request->coa_id_sales_return;
            $custTypeData->coa_id_disc = $request->coa_id_disc;
            $custTypeData->created_date = \Carbon\Carbon::now();
            $custTypeData->updated_date = \Carbon\Carbon::now();
            $custTypeData->created_by = $userId;
            $insert =  $custTypeData->save();

            return Redirect::back()->with('status', 'Customer type added');
          } catch(\Illuminate\Database\QueryException $ex){
              return Redirect::back()->with('status', 'Error inserting');
          }
      }

    }

    public function edit(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idCustType = $request->cust_type_id;

      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
          $custTypeData = new customerTypeModel();
          $custTypeData::where('cust_type_id', $idCustType)
                     ->update([
                            'cust_type_code'=>$request->cust_type_code,
                            'cust_type_desc'=>$request->cust_type_desc,
                            'coa_id_ar'=>$request->coa_id_ar,
                            'coa_id_dp'=>$request->coa_id_dp,
                            'coa_id_ppn'=>$request->coa_id_ppn,
                            'coa_id_sales'=>$request->coa_id_sales,
                            'coa_id_sales_return'=>$request->coa_id_sales_return,
                            'coa_id_disc'=>$request->coa_id_disc,
                            'updated_date' => \Carbon\Carbon::now(),
                            'updated_by' => $userId
                      ]);
          return Redirect::back()->with('status', 'Customer type updated');
        } catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error updating');
        }
      }

    }

    public function delete(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idCustType = $request->cust_type_id;

      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $custTypeData = new customerTypeModel();

        $custTypeData::where('cust_type_id', $idCustType)->delete();

        return Redirect::back()->with('status', 'Customer type deleted');
      }

    }

    public function deleteBulk(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idCustType = $request->cust_type_id;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $custTypeData = new customerTypeModel();
        $custTypeData::whereIn('cust_type_id', $idCustType)->delete();
        $status = "success";

      }
      return $status;
    }

    public function exportToExcel($view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv->canExport)){
        $datamodel = customerTypeModel::select('cust_type_code', 'cust_type_desc')
                                        ->get();
        $data = null;
        foreach($datamodel as $datas){
             $data[] = array(
               $datas->cust_type_code,
               $datas->cust_type_desc,
             );
         }
         if($data != null){
            $column = array('CUSTOMER TYPE CODE', 'CUSTOMER TYPE DESC');
            $this->excelDownload($data, "Data Customer type", $column);
          }else{
            return Redirect::back()->withErrors(['No data to export']);
          }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }
}
