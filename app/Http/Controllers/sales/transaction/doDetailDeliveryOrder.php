<?php
namespace App\Http\Controllers\sales\transaction;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\deliveryorder\do_delivery_order\doDeliveryOrderModel;
use App\deliveryorder\do_detail_delivery_order\doDeliveryOrderDetailModel;
use App\salesorder\so_sales_order\soSalesOrderModel;
use App\inventory\im_prd_master\imPrdMasterModel;
use App\inventory\im_prd_unit\imPrdUnitModel;
use App\usermanagement\um_module\umModuleModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Illuminate\Support\Facades\DB;
use Datatables;

class doDetailDeliveryOrder extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    private function updateTotalDo($doid, $qty, $status){
      $updateDo = new doDeliveryOrderModel();
      $updateDo::where('do_id', $doid)
                 ->update([
                    "total_qty" => $qty,
                    "do_status" => $status,
                 ]);

       return true;
    }

    public function exportToExcel($view, $doId){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv->canExport)){
        $detailDo = doDeliveryOrderDetailModel::join('IM_PRD_MASTER AS IPM', 'DO_DETAIL.prd_id', '=', 'IPM.prd_id')
                                  ->join('IM_PRD_SIZE AS IPZ', 'IPM.size_id', '=', 'IPZ.size_id')
                                  ->join('IM_PRD_COLOR AS IPC', 'IPM.color_id', '=', 'IPC.color_id')
                                  ->select('DO_DETAIL.do_detail_id', 'DO_DETAIL.do_id', 'IPZ.size_desc', 'IPC.color_desc', 'DO_DETAIL.qty_delivery', 'IPM.prd_desc')
                                  ->where('DO_DETAIL.do_id', $doId)
                                  ->get();
         $data = null;
         foreach($detailDo as $datas){
             $data[] = array(
               $datas->do_id,
               $datas->do_detail_id,
               $datas->prd_desc,
               $datas->size_desc,
               $datas->color_desc,
               $datas->qty_delivery
             );
         }
         if($data != null){
           $column = array('ID SO', 'ID DETAIL SO','NAMA BARANG', 'SIZE', 'COLOR', 'QTY');
           $this->excelDownload($data, "Data detail do", $column);
         }else{
            return Redirect::back()->withErrors(['No data']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function addDetailDo(Request $request, $view, $doId){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $detailDo = doDeliveryOrderDetailModel::join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'DO_DETAIL.prd_id')
                                ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IPM.size_id')
                                ->join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'DO_DETAIL.uom_id')
                                ->select('DO_DETAIL.do_detail_id', 'DO_DETAIL.qty_delivery', 'DO_DETAIL.sub_total', 'IPM.prd_id', 'IPM.retail_price', 'IPM.prd_desc', 'IPS.size_desc', 'DO_DETAIL.uom_id', 'IPU.uom_desc', 'DO_DETAIL.uom_conversion', 'DO_DETAIL.cogs_price')
                                ->where('DO_DETAIL.do_id', $doId)
                                ->get();

      $uoms = imPrdUnitModel::select('uom_id', 'uom_desc')->get();

      $do_detail_id = doDeliveryOrderDetailModel::select('do_detail_id')->where('do_id', $doId)->first();

      $do_detail_sub_sum = doDeliveryOrderDetailModel::select(DB::raw('SUM(sub_total) as sub_total'), DB::raw('SUM(qty_delivery) as qty_delivery'))->where('do_id', $doId)->first();
      $do_detail_retail_price_sum = doDeliveryOrderDetailModel::join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'DO_DETAIL.prd_id')
                                                                ->select(DB::raw('SUM("IPM"."retail_price") as "retail_price"'))
                                                                ->where('do_id', $doId)
                                                                ->first();

      $prdSugest = imPrdMasterModel::join('IM_PRD_COLOR AS IPC', 'IPC.color_id', '=', 'IM_PRD_MASTER.color_id')
                                     ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IM_PRD_MASTER.size_id')
                                     ->join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'IM_PRD_MASTER.uom_id1')
                                     ->select(DB::raw('CAST(prd_id AS varchar) AS data'), DB::raw('prd_desc ||'."'-'".'||"IPC"."color_desc" ||'."'-'".'||"IPS"."size_desc" AS value'), 'uom_id', 'IPU.uom_desc AS uomdesc', 'uom_conversion', 'retail_price')->get();


      $price = preg_replace("/[^A-Za-z0-9 ]/", '',  $do_detail_sub_sum->sub_total);
      $price = str_replace('Rp', '', $price);

      $do_id = doDeliveryOrderModel::select('do_id', 'do_no', 'do_status', 'created_date')->where('do_id', $doId)->first();

      $listModules = $this->listModules($userGroupId);
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('deliveryorder.do_detail_delivery_order.'.$view, [
            'userGroupId' => $userGroupId,
            'listModules' => $listModules,
            'title' => $userModule,
            'privilaged' => $priv,
            'detailDo' => $detailDo,
            'uoms' => $uoms,
            'do_id' => $do_id,
            'total' => $price,
            'do_detail_id' => $do_detail_id,
            'do_detail_sub_sum' => $do_detail_sub_sum,
            'do_detail_retail_price_sum' => $do_detail_retail_price_sum,
            'prdSugest' => $prdSugest
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function listDetailDo(Request $request, $view, $doId){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $detailDo = doDeliveryOrderDetailModel::join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'DO_DETAIL.prd_id')
                                ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IPM.size_id')
                                ->join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'DO_DETAIL.uom_id')
                                ->join('IM_PRD_COLOR AS IPC', 'IPC.color_id', '=', 'IPM.color_id')
                                ->select('DO_DETAIL.do_detail_id', 'DO_DETAIL.qty_delivery', 'DO_DETAIL.sub_total', 'IPM.prd_id', 'IPM.retail_price', 'IPM.prd_desc',
                                'IPS.size_desc', 'DO_DETAIL.uom_id', 'IPU.uom_desc', 'DO_DETAIL.uom_conversion', 'DO_DETAIL.cogs_price',
                                'IPM.prd_code', 'IPC.color_desc')
                                ->where('DO_DETAIL.do_id', $doId);

      $uoms = imPrdUnitModel::select('uom_id', 'uom_desc')->get();
      $do_id = doDeliveryOrderModel::select('do_id', 'do_no', 'do_status', 'created_date')->where('do_id', $doId)->first();
      $prdSugest = imPrdMasterModel::join('IM_PRD_COLOR AS IPC', 'IPC.color_id', '=', 'IM_PRD_MASTER.color_id')
                                     ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IM_PRD_MASTER.size_id')
                                     ->join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'IM_PRD_MASTER.uom_id1')
                                     ->select(DB::raw('CAST(prd_id AS varchar) AS data'), DB::raw('prd_desc ||'."'-'".'||"IPC"."color_desc" ||'."'-'".'||"IPS"."size_desc" AS value'), 'uom_id', 'IPU.uom_desc AS uomdesc', 'uom_conversion', 'retail_price')->get();
      $listModules = $this->listModules($userGroupId);
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($do_id->do_status != "approve"){
            return Datatables::of($detailDo)
            ->addColumn('delete_bulk', function($data){
                return "
                  <input type='checkbox' name='do_detail_id[]' value='".$data->do_detail_id."' />
                ";
            })
            ->editColumn('sub_total', function ($data) {
                   return $data->sub_total;
            })
            ->editColumn('qty_delivery', function ($data) {
                   return number_format($data->qty_delivery);
            })
            ->addColumn('action', function ($data) use ($priv, $uoms, $do_id, $prdSugest,$view) {
                return view('deliveryorder.do_detail_delivery_order.'.$view, ['privilaged' => $priv, 'data' => $data, 'uoms' => $uoms, 'do_id' => $do_id, 'prdSugest' => $prdSugest]);
            })
            ->filter(function ($detailDo) use ($request) {
                     if ($request->has('prd_desc')) {
                         $detailSo->where('IPM.prd_desc', 'LIKE', '%'.$request->prd_desc.'%');
                     }
             })
            ->make(true);
      }else{
            return Datatables::of($detailDo)
            ->addColumn('delete_bulk', function($data){
                return "
                  <input type='checkbox' name='do_detail_id[]' value='".$data->do_detail_id."' />
                ";
            })
            ->editColumn('sub_total', function ($data) {
                   return $data->sub_total;
            })
            ->editColumn('qty_delivery', function ($data) {
                   return number_format($data->qty_delivery);
            })
            ->filter(function ($detailDo) use ($request) {
                   if ($request->has('prd_desc')) {
                       $detailDo->where('IPM.prd_desc', 'LIKE', '%'.$request->prd_desc.'%');
                   }
             })
            ->make(true);
      }
    }

    public function addDetailDoAction(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);
      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $qty_so = soSalesOrderModel::join('DO_HEADER AS DH', 'DH.so_no', '=', 'SO_HEADER.so_no')
                                         ->where('DH.do_id', $request->do_id)
                                         ->select(DB::raw('"SO_HEADER".total_qty AS total_qty'), 'SO_HEADER.so_no AS so_no')
                                         ->first();
        $sum_qty = doDeliveryOrderDetailModel::where('do_id', $request->do_id)->sum('qty_delivery');
        $sum_qty = ($sum_qty == null) ? 0:$sum_qty;
        if($sum_qty <= $qty_so->total_qty){

          $prd_price = preg_replace("/[^A-Za-z0-9 ]/", '', $request->cogs_price);
          $sub_total = $prd_price * $request->qty_delivery;
        try {
          $detailDoData = new doDeliveryOrderDetailModel();
          $detailDoData->do_id = $request->do_id;
          $detailDoData->prd_id = $request->prd_id;
          $detailDoData->qty_delivery =  $request->qty_delivery;
          $detailDoData->cogs_price =  $prd_price;
          $detailDoData->uom_id = $request->uom_id;
          $detailDoData->uom_conversion = $request->uom_conversion;
          $detailDoData->sub_total = $sub_total;
          $insert =  $detailDoData->save();
        } catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error inserting');
        }
        $sum_qty = doDeliveryOrderDetailModel::where('do_id', $request->do_id)->sum('qty_delivery');
        $sum_qty = ($sum_qty == null) ? 0:$sum_qty;
        $sum_subtotal = doDeliveryOrderDetailModel::where('do_id', $request->do_id)->sum('sub_total');

          if($sum_qty == $qty_so->total_qty){
            $this->updateTotalDo($request->do_id, $sum_qty, "approve");
          }else{
            $this->updateTotalDo($request->do_id, $sum_qty, "partial");
          }
          return Redirect::back()->with('status', 'Detail do added');
        }else{
          return Redirect::back()->with('status', 'Qty do > qty so');
          die();
        }
      }

    }

    public function editDetailDo(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $do_detail_id = $request->do_detail_id;
      $prd_price = imPrdMasterModel::select('retail_price')->where('prd_id', $request->prd_id)->first();

      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $price = preg_replace("/[^A-Za-z0-9 ]/", '',  $request->cogs_price);
        $price = str_replace('Rp', '', $price);
        $sub_total = $price * $request->qty_delivery;

        try {
          $detailDoData = new doDeliveryOrderDetailModel();
          $detailDoData::where('do_detail_id', $do_detail_id)
                     ->update([
                            'do_id'=>$request->do_id,
                            'prd_id'=>$request->prd_id,
                            'qty_delivery'=>$request->qty_delivery,
                            'cogs_price' => $price,
                            'uom_id'=>$request->uom_id,
                            'uom_conversion'=>$request->uom_conversion,
                            'sub_total'=>$sub_total,
                      ]);
          } catch(\Illuminate\Database\QueryException $ex){
              return Redirect::back()->with('status', 'Error updating');
          }

          $sum_qty = doDeliveryOrderDetailModel::where('do_id', $request->do_id)->sum('qty_delivery');
          $sum_subtotal = doDeliveryOrderDetailModel::where('do_id', $request->do_id)->sum('sub_total');
          $this->updateTotalDo($request->do_id, $sum_qty);

        return Redirect::back()->with('status', 'Detail do updated');
      }

    }

    public function deleteDetailDo(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);


      $do_detail_id = $request->do_detail_id;

      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $detailDoData = new doDeliveryOrderDetailModel();
        $detailDoData::where('do_detail_id', $do_detail_id)->delete();

        $sum_qty = doDeliveryOrderDetailModel::where('do_id', $request->do_id)->sum('qty_delivery');
        $this->updateTotalDo($request->do_id, $sum_qty);

        return Redirect::back()->with('status', 'Detail do deleted');
      }

    }

    public function deleteBulkDetailDo(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $do_detail_id = $request->do_detail_id;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $detailDoData = new doDeliveryOrderDetailModel();
        $detailDoData::whereIn('do_detail_id', $do_detail_id)->delete();

        $sum_qty = doDeliveryOrderDetailModel::where('do_id', $request->do_id)->sum('qty_delivery');
        $this->updateTotalDo($request->do_id, $sum_qty);

        $status = "success";

      }
      return $status;
    }
}
