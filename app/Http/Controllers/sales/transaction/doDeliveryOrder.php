<?php

namespace App\Http\Controllers\sales\transaction;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\deliveryorder\do_delivery_order\doDeliveryOrderModel;
use App\deliveryorder\do_detail_delivery_order\doDeliveryOrderDetailModel;
use App\salesorder\so_sales_order\soSalesOrderModel;
use App\sales\so_cust\customerModel;
use App\usermanagement\um_module\umModuleModel;
use App\warehouse\im_wh_loc\imWhLocModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Datatables;
use Carbon\Carbon;

class doDeliveryOrder extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function exportToExcel($view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv->canExport)){
        $do = doDeliveryOrderModel::select('do_no', 'do_date', 'total_qty', 'notes', 'do_status')->get();
        $data = null;
        foreach($do as $datas){
           $date = date("d M Y", strtotime($datas->do_date));
             $data[] = array(
               $datas->do_no,
               $date,
               $datas->total_qty,
               $datas->notes,
               $datas->do_status,
             );
         }
         if($data != null){
           $column = array('DO NO','DATE', 'TOTAL QTY', 'NOTES', 'STATUS');
           $this->excelDownload($data, "Data do", $column);
         }else{
           return Redirect::back()->withErrors(['No data to export']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function listDo($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $listModules = $this->listModules($userGroupId);
        $priv = $this->checkPrivilaged($moduleId, $userGroupId);
        $cust = customerModel::select(DB::raw('CAST(cust_id AS varchar) AS data'), 'cust_desc AS value')->where('cust_active_flag',1)->get();
        $wh = imWhLocModel::select(DB::raw('CAST(wh_id AS varchar) AS data'), 'wh_desc AS value')->get();
        $so = soSalesOrderModel::select(DB::raw('CAST(so_id AS varchar) AS data'), 'so_no AS value')->get();
      if($priv != null && $priv->canView == 1){
        return view('deliveryorder.do_delivery_order.'.$view, [
            'userGroupId' => $userGroupId,
            'listModules' => $listModules,
            'title' => $userModule,
            'cust' => $cust,
            'wh' => $wh,
            'so' => $so,
            'privilaged' => $priv
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function listDoData(Request $request, $view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $listModules = $this->listModules($userGroupId);
        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        $do = doDeliveryOrderModel::join('IM_WH_LOC AS WH', 'WH.wh_id', '=', 'DO_HEADER.wh_id')
                                    ->join('SO_CUST_MASTER AS SCM', 'SCM.cust_id', '=', 'DO_HEADER.cust_id')
                                    ->select('DO_HEADER.*', 'WH.wh_id', 'WH.wh_desc', 'SCM.cust_id', 'SCM.cust_desc');
        $cust = customerModel::select(DB::raw('CAST(cust_id AS varchar) AS data'), 'cust_desc AS value')->where('cust_active_flag',1)->get();
        $wh = imWhLocModel::select(DB::raw('CAST(wh_id AS varchar) AS data'), 'wh_desc AS value')->get();
        $so = soSalesOrderModel::select(DB::raw('CAST(so_id AS varchar) AS data'), 'so_no AS value')->get();
        if ($request->has('start_date') && $request->has('end_date')) {
            $start = strtotime($request->get('start_date'));
            $end = strtotime($request->get('end_date'));
            $do->whereBetween('DO_HEADER.do_date', [date('Y-m-d', $start), date('Y-m-d', $end)]);
        }

        return Datatables::of($do)
        ->addColumn('delete_bulk', function($data){
            return "
              <input type='checkbox' name='do_id[]' value='".$data->do_id."' />
            ";
        })
        ->editColumn('do_date', function ($data) {
               return $data->do_date ? with(new Carbon($data->do_date))->format('d M y') : '';
        })
        ->editColumn('total_qty', function ($data) {
               return number_format($data->total_qty);
        })
        ->addColumn('action', function ($data) use ($priv, $cust, $wh, $so, $view) {
            return view('deliveryorder.do_delivery_order.'.$view, ['privilaged' => $priv, 'data' => $data, 'cust' => $cust, 'so' => $so, 'wh' => $wh]);
       })
        ->make(true);
    }

    private function doCodeGenerate($doDate, $maxDo){
      $code = "DO";
      $doDate = strtotime($doDate);
      $yy = date("y", $doDate);
      $mm = date("m", $doDate);

      if($maxDo != ""){
        $maxDo = substr($maxDo, -6);
        $do_no = $maxDo + 1;
        $do_no = str_pad($do_no, 6, '0',STR_PAD_LEFT);
      }else{
        $do_no = "000001";
      }

      $do_no_final = $code.$yy.$mm.$do_no;
      return $do_no_final;
    }

    public function addDo(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);
      $do_date = strtotime($request->do_date);
      $do_final_date = date("Y-m-d", $do_date);

      $req_date = strtotime($request->req_date);
      $req_date = date("Y-m-d", $req_date);

      $maxdo = doDeliveryOrderModel::select(DB::raw('MAX(RIGHT(do_no, 6)) as do_no'))->first();

      $do_no = $this->doCodeGenerate($request->do_date, $maxdo->do_no);
      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
            $doData = new doDeliveryOrderModel();
            $doData->do_no = $do_no;
            $doData->so_no = $request->so_no;
            $doData->do_date = $do_final_date;
            $doData->wh_id = $request->wh_id;
            $doData->cust_id = $request->cust_id;
            $doData->notes = $request->notes;
            $doData->do_status = $request->do_status;
            $doData->total_qty = 0;
            $doData->created_date = \Carbon\Carbon::now();
            $doData->updated_date = \Carbon\Carbon::now();
            $doData->created_by = $userId;
            $insert =  $doData->save();
        } catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error inserting');
        }
        $update_so_status = soSalesOrderModel::where('so_no', $request->so_no)->update(['so_status' => 'approve']);
        return Redirect::back()->with('status', 'Do added');
      }

    }

    public function editDo(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $do_id = $request->do_id;
      $do_date = strtotime($request->do_date);
      $do_date = date("Y-m-d", $do_date);

      $req_date = strtotime($request->req_date);
      $req_date = date("Y-m-d", $req_date);
      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        if($priv->canApprove != 1){
          if($request->do_status != "approve"){
                      try {
                        $doData = new doDeliveryOrderModel();
                        $doData::where('do_id', $do_id)
                                   ->update([
                                          'do_no'=>$request->do_no,
                                          'so_no'=>$request->so_no,
                                          'do_date'=>$do_date,
                                          'wh_id'=>$request->wh_id,
                                          'cust_id' =>  $request->cust_id,
                                          'notes'=>$request->notes,
                                          'do_status'=>$request->do_status,
                                          'updated_date' => \Carbon\Carbon::now(),
                                          'updated_by' => $userId
                                    ]);
                        } catch(\Illuminate\Database\QueryException $ex){
                          return Redirect::back()->with('status', 'Error updating');
                        }
                        $update_so_status = soSalesOrderModel::where('so_no', $request->so_no)->update(['so_status' => 'approve']);
            }else{
              return Redirect::back()->with('status', 'Not permitted');
            }
      }else{
          try {
            $doData = new doDeliveryOrderModel();
            $doData::where('do_id', $do_id)
                       ->update([
                              'do_no'=>$request->do_no,
                              'so_no'=>$request->so_no,
                              'do_date'=>$do_date,
                              'wh_id'=>$request->wh_id,
                              'cust_id' =>  $request->cust_id,
                              'notes'=>$request->notes,
                              'do_status'=>$request->do_status,
                              'updated_date' => \Carbon\Carbon::now(),
                              'updated_by' => $userId
                        ]);
            } catch(\Illuminate\Database\QueryException $ex){
                return Redirect::back()->with('status', 'Error updating');
            }
            $update_so_status = soSalesOrderModel::where('so_no', $request->so_no)->update(['so_status' => 'approve']);
      }

        return Redirect::back()->with('status', 'Do updated');
      }

    }

    public function deleteDo(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $do_id = $request->do_id;

      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $doData = new doDeliveryOrderModel();
        $doData::where('do_id', $do_id)->delete();

        $doDetailData = new doDeliveryOrderDetailModel();
        $doDetailData::where('do_id', $do_id)->delete();
        return Redirect::back()->with('status', 'Do deleted');
      }

    }

    public function deleteBulkDo(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $do_id = $request->do_id;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $doData = new doDeliveryOrderModel();
        $doData::whereIn('do_id', $do_id)->delete();

        $doDetailData = new doDeliveryOrderDetailModel();
        $doDetailData::whereIn('do_id', $do_id)->delete();

        $status = "success";

      }
      return $status;
    }
}
