<?php

namespace App\Http\Controllers\sales\transaction;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\salesorder\so_sales_order\soSalesOrderModel;
use App\salesorder\so_detail_sales_order\soSalesOrderDetailModel;
use App\sales\so_cust\customerModel;
use App\usermanagement\um_module\umModuleModel;
use App\warehouse\im_wh_loc\imWhLocModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Datatables;
use Carbon\Carbon;

class soSalesOrder extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function exportToExcel($view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv->canExport)){
        $so = soSalesOrderModel::select('so_no', 'so_date', 'total_qty', 'total_gross', 'total_ppn', 'notes', 'so_status')->get();
        $data = null;
        foreach($so as $datas){
           $date = date("d M Y", strtotime($datas->so_date));
             $data[] = array(
               $datas->so_no,
               $date,
               $datas->total_qty,
               $datas->total_gross,
               $datas->total_ppn,
               $datas->notes,
               $datas->so_status,
             );
         }
         if($data != null){
           $column = array('SO NO','DATE', 'TOTAL QTY', 'TOTAL GROSS', 'TOTAL PPN', 'NOTES', 'STATUS');
           $this->excelDownload($data, "Data so", $column);
         }else{
           return Redirect::back()->withErrors(['No data to export']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function listSo($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $listModules = $this->listModules($userGroupId);
        $priv = $this->checkPrivilaged($moduleId, $userGroupId);
        $cust = customerModel::select(DB::raw('CAST(cust_id AS varchar) AS data'), 'cust_desc AS value')->where('cust_active_flag',1)->get();
        $wh = imWhLocModel::select(DB::raw('CAST(wh_id AS varchar) AS data'), 'wh_desc AS value')->get();
      if($priv != null && $priv->canView == 1){
        return view('salesorder.so_sales_order.'.$view, [
            'userGroupId' => $userGroupId,
            'listModules' => $listModules,
            'title' => $userModule,
            'cust' => $cust,
            'wh' => $wh,
            'privilaged' => $priv
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function listSoData(Request $request, $view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $listModules = $this->listModules($userGroupId);
        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        $so = soSalesOrderModel::join('IM_WH_LOC AS WH', 'WH.wh_id', '=', 'SO_HEADER.wh_id')
                                 ->join('SO_CUST_MASTER AS SCM', 'SCM.cust_id', '=', 'SO_HEADER.cust_id')
                                 ->select('SO_HEADER.*', 'WH.wh_id', 'WH.wh_desc', 'SCM.cust_desc');

        $cust = customerModel::select(DB::raw('CAST(cust_id AS varchar) AS data'), 'cust_desc AS value')->where('cust_active_flag',1)->get();
        $wh = imWhLocModel::select(DB::raw('CAST(wh_id AS varchar) AS data'), 'wh_desc AS value')->get();

        if ($request->has('start_date') && $request->has('end_date')) {
            $start = strtotime($request->get('start_date'));
            $end = strtotime($request->get('end_date'));
            $so->whereBetween('so_date', [date('Y-m-d', $start), date('Y-m-d', $end)]);
        }

        return Datatables::of($so)
        ->addColumn('delete_bulk', function($data){
            return "
              <input type='checkbox' name='so_id[]' value='".$data->so_id."' />
            ";
        })
        ->editColumn('so_date', function ($data) {
               return $data->so_date ? with(new Carbon($data->so_date))->format('d M y') : '';
        })
        ->editColumn('total_qty', function ($data) {
               return number_format($data->total_qty);
        })
        ->addColumn('action', function ($data) use ($priv, $cust, $wh, $view) {
            return view('salesorder.so_sales_order.'.$view, ['privilaged' => $priv, 'data' => $data, 'cust' => $cust, 'wh' => $wh]);
       })
        ->make(true);
    }

    private function soCodeGenerate($soDate, $maxSo){
      $code = "SO";
      $soDate = strtotime($soDate);
      $yy = date("y", $soDate);
      $mm = date("m", $soDate);

      if($maxSo != ""){
        $maxSo = substr($maxSo, -6);
        $so_no = $maxSo + 1;
        $so_no = str_pad($so_no, 6, '0',STR_PAD_LEFT);
      }else{
        $so_no = "000001";
      }

      $so_no_final = $code.$yy.$mm.$so_no;
      return $so_no_final;
    }

    public function addSo(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);
      $so_date = strtotime($request->so_date);
      $so_final_date = date("Y-m-d", $so_date);

      $req_date = strtotime($request->req_date);
      $req_date = date("Y-m-d", $req_date);

      $maxso = soSalesOrderModel::select(DB::raw('MAX(RIGHT(so_no, 6)) as so_no'))->first();

      $so_no = $this->soCodeGenerate($request->so_date, $maxso->so_no);
      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
            $soData = new soSalesOrderModel();
            $soData->so_no = $so_no;
            $soData->so_date = $so_final_date;
            $soData->req_date = $req_date;
            $soData->wh_id = $request->wh_id;
            $soData->cust_id = $request->cust_id;
            $soData->ppn_type = $request->ppn_type;
            $soData->notes = $request->notes;
            $soData->so_status = $request->so_status;
            $soData->total_qty = 0;
            $soData->total_gross_product = 0;
            $soData->total_gross = 0;
            $soData->disc = 0;
            $soData->total_disc = 0;
            $soData->total_dpp = 0;

            if($request->ppn == 1){
                $soData->ppn = 10;
            }else{
                $soData->ppn = 0;
            }

            $soData->total_ppn = 0;
            $soData->total_nett = 0;
            $soData->created_date = \Carbon\Carbon::now();
            $soData->updated_date = \Carbon\Carbon::now();
            $soData->created_by = $userId;
            $insert =  $soData->save();
        } catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error inserting');
        }
        return Redirect::back()->with('status', 'So added');
      }

    }

    public function editSo(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $so_id = $request->so_id;
      $so_date = strtotime($request->so_date);
      $so_date = date("Y-m-d", $so_date);

      $req_date = strtotime($request->req_date);
      $req_date = date("Y-m-d", $req_date);
      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        if($priv->canApprove != 1){
          if($request->so_status != "approve"){
                      try {
                        $soData = new soSalesOrderModel();
                        $soData::where('so_id', $so_id)
                                   ->update([
                                          'so_no'=>$request->so_no,
                                          'so_date'=>$so_date,
                                          'req_date'=>$req_date,
                                          'wh_id'=>$request->wh_id,
                                          'cust_id' =>  $request->cust_id,
                                          'ppn_type'=>$request->ppn_type,
                                          'notes'=>$request->notes,
                                          'so_status'=>$request->so_status,
                                          'updated_date' => \Carbon\Carbon::now(),
                                          'updated_by' => $userId
                                    ]);
                        } catch(\Illuminate\Database\QueryException $ex){
                          return Redirect::back()->with('status', 'Error updating');
                        }
            }else{
              return Redirect::back()->with('status', 'Not permitted');
            }
      }else{
          try {
            $soData = new soSalesOrderModel();
            $soData::where('so_id', $so_id)
                       ->update([
                              'so_no'=>$request->so_no,
                              'so_date'=>$so_date,
                              'req_date'=>$req_date,
                              'ppn_type'=>$request->ppn_type,
                              'wh_id'=>$request->wh_id,
                              'cust_id' =>  $request->cust_id,
                              'notes'=>$request->notes,
                              'so_status'=>$request->so_status,
                              'updated_date' => \Carbon\Carbon::now(),
                              'updated_by' => $userId
                        ]);
            } catch(\Illuminate\Database\QueryException $ex){
                return Redirect::back()->with('status', 'Error updating');
            }
      }

        return Redirect::back()->with('status', 'So updated');
      }

    }

    public function deleteSo(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $so_id = $request->so_id;

      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $soData = new soSalesOrderModel();
        $soData::where('so_id', $so_id)->delete();

        $soDetailData = new soSalesOrderDetailModel();
        $soDetailData::where('so_id', $so_id)->delete();
        return Redirect::back()->with('status', 'So deleted');
      }

    }

    public function deleteBulkSo(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $so_id = $request->so_id;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $soData = new soSalesOrderModel();
        $soData::whereIn('so_id', $so_id)->delete();

        $soDetailData = new soSalesOrderDetailModel();
        $soDetailData::whereIn('so_id', $so_id)->delete();

        $status = "success";

      }
      return $status;
    }
}
