<?php

namespace App\Http\Controllers\sales\transaction;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\salesorder\so_sales_order\soSalesOrderModel;
use App\salesorder\so_detail_sales_order\soSalesOrderDetailModel;
use App\inventory\im_prd_master\imPrdMasterModel;
use App\inventory\im_prd_unit\imPrdUnitModel;
use App\usermanagement\um_module\umModuleModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Illuminate\Support\Facades\DB;
use Datatables;

class soDetailSalesOrder extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    private function updateTotalSo($soid, $price, $qty, $pr_flag){
      $ppn = soSalesOrderModel::select('ppn')->where('so_id', $soid)->first();
      if($ppn->ppn > 0){
        $percentPPN = $ppn->ppn / 100;
        $total_ppn = $percentPPN * $price;
        $total_gross = $price + $total_ppn;
      }else{
        $total_ppn = 0;
        $total_gross = $price;
      }

      $updateSo = new soSalesOrderModel();
      $updateSo::where('so_id', $soid)
                 ->update([
                    "total_qty" => $qty,
                    "total_gross_product" => $total_gross,
                    "total_gross" => $total_gross,
                    "total_nett" => $total_gross,
                    "total_ppn" => $total_ppn
                 ]);

       return true;
    }

    /* private function poPrInsert($poid, $pr_detail_id, $prd_id, $qty_pr){
      $poPrInsert = new poPrModel();
      $poPrInsert->so_id = $poid;
      $poPrInsert->pr_detail_id = $pr_detail_id;
      $poPrInsert->prd_id = $prd_id;
      $poPrInsert->qty_pr = $qty_pr;
      $insert =  $poPrInsert->save();

      return true;
    }

    private function updateDetailPr($poid, $pr_detail_id){
      $updatePrDet = new prPurchaseRequestDetailModel();
      $updatePrDet::where('pr_detail_id', $pr_detail_id)
                 ->update([
                    "so_id" => $poid
                 ]);

      return true;
    }

    private function updatePrStatus($pr_id, $status){
      $updatePrStatus = prPurchaseRequestModel::where('pr_id', $pr_id)
                 ->update([
                    "pr_status" => $status
                 ]);

      return true;
    } */

    public function ajaxPrdCatalog($view, $keyword){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('salesorder.so_detail_sales_order.'.$view, [
            'keyword' => $keyword,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    /* public function ajaxPrCatalog($view, $keyword){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('purchaseorder.po_detail_purchase_order.'.$view, [
            'keyword' => $keyword,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function ajaxPrDetailCatalog($view, $id){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);
      $prDetail = prPurchaseRequestDetailModel::select('IPM.prd_code', 'IPM.prd_desc','IPS.size_desc', 'IPM.retail_price', 'PR_DETAIL.qty_pr', 'PR_DETAIL.prd_id', 'PR_DETAIL.pr_detail_id')
                                                ->join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'PR_DETAIL.prd_id')
                                                ->join('IM_PRD_SIZE AS IPS', 'IPM.size_id', '=', 'IPS.size_id')
                                                ->where('PR_DETAIL.pr_id', $id)
                                                ->get();

      if($priv != null && $priv->canView == 1){
        return view('purchaseorder.po_detail_purchase_order.'.$view, [
            'prDetail' => $prDetail,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function loadPr(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      $pr_detail_id = $request->pr_detail_id;
      $pr_id = $request->pr_id;

      if($pr_detail_id != null){
        $pr_details =  prPurchaseRequestDetailModel::whereIn('pr_detail_id', $pr_detail_id)->get();
      }else{
        return Redirect::back()->with('status', 'error : choose one of the detail(s)');
      }

      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $i = 0;
        foreach($pr_details as $pr_detail){
              $pr_det_id = $pr_detail->pr_detail_id;
              $prd_price = preg_replace("/[^A-Za-z0-9 ]/", '', $request->input('retail_price'.$pr_det_id));
              $prd_price = str_replace('Rp', '', $prd_price);
              $qty = $pr_detail->qty_pr;
              $sub_total = $prd_price * $qty;

              try {
                $detailPoData = new poPurchaseOrderDetailModel();
                $detailPoData->so_id = $request->so_id;
                $detailPoData->prd_id = $pr_detail->prd_id;
                $detailPoData->qty_pr =  $qty;
                $detailPoData->qty_min =  0;
                $detailPoData->qty_other =  0;
                $detailPoData->qty_order =  0;
                $detailPoData->qty_receive =  0;
                $detailPoData->order_price =  $prd_price;
                $detailPoData->disc =  0;
                $detailPoData->disc_value =  0;
                $detailPoData->so_status_detail =  0;
                $detailPoData->uom_id = $pr_detail->uom_id;
                $detailPoData->uom_conversion = $pr_detail->uom_conversion;
                $detailPoData->sub_total = $sub_total;
                $insert =  $detailPoData->save();
              } catch(\Illuminate\Database\QueryException $ex){
                  return false;
              }

              $sum_qty = poPurchaseOrderDetailModel::where('so_id', $request->so_id)->sum('qty_pr');
              $sum_subtotal = poPurchaseOrderDetailModel::where('so_id', $request->so_id)->sum('sub_total');
              $this->updateTotalPo($request->so_id, $sum_subtotal, $sum_qty, 1);
              $this->poPrInsert($request->so_id, $pr_detail->pr_detail_id, $pr_detail->prd_id, $pr_detail->qty_pr);
              $this->updateDetailPr($request->so_id, $pr_detail->pr_detail_id);
        $i++;}

        $cekPartial = prPurchaseRequestDetailModel::where('so_id', $request->so_id)->get();
        $statusPr = "";

        if($cekPartial->isEmpty()){
            $statusPr = "partial";
        }else{
            $statusPr = "finish";
        }

        $this->updatePrStatus($pr_id, $statusPr);

        return Redirect::back()->with('status', 'Detail po added');
      }
    } */

    public function exportToExcel($view, $soId){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv->canExport)){
        $detailSo = soSalesOrderDetailModel::join('IM_PRD_MASTER AS IPM', 'SO_DETAIL.prd_id', '=', 'IPM.prd_id')
                                  ->join('IM_PRD_SIZE AS IPZ', 'IPM.size_id', '=', 'IPZ.size_id')
                                  ->join('IM_PRD_COLOR AS IPC', 'IPM.color_id', '=', 'IPC.color_id')
                                  ->select('SO_DETAIL.so_detail_id', 'SO_DETAIL.so_id', 'IPZ.size_desc', 'IPC.color_desc', 'SO_DETAIL.qty_order', 'IPM.prd_desc')
                                  ->where('SO_DETAIL.so_id', $soId)
                                  ->get();
         $data = null;
         foreach($detailSo as $datas){
             $data[] = array(
               $datas->so_id,
               $datas->so_detail_id,
               $datas->prd_desc,
               $datas->size_desc,
               $datas->color_desc,
               $datas->qty_order
             );
         }
         if($data != null){
           $column = array('ID SO', 'ID DETAIL SO','NAMA BARANG', 'SIZE', 'COLOR', 'QTY');
           $this->excelDownload($data, "Data detail so", $column);
         }else{
            return Redirect::back()->withErrors(['No data']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function addDetailSo(Request $request, $view, $soId){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $detailSo = soSalesOrderDetailModel::join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'SO_DETAIL.prd_id')
                                ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IPM.size_id')
                                ->join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'SO_DETAIL.uom_id')
                                ->select('SO_DETAIL.so_detail_id', 'SO_DETAIL.qty_order', 'SO_DETAIL.sub_total', 'IPM.prd_id', 'IPM.retail_price', 'IPM.prd_desc', 'IPS.size_desc', 'SO_DETAIL.uom_id', 'IPU.uom_desc', 'SO_DETAIL.uom_conversion', 'SO_DETAIL.order_price')
                                ->where('SO_DETAIL.so_id', $soId)
                                ->get();

      $uoms = imPrdUnitModel::select('uom_id', 'uom_desc')->get();

      $so_detail_id = soSalesOrderDetailModel::select('so_detail_id')->where('so_id', $soId)->first();

      $so_detail_sub_sum = soSalesOrderDetailModel::select(DB::raw('SUM(sub_total) as sub_total'), DB::raw('SUM(qty_order) as qty_order'))->where('so_id', $soId)->first();
      $so_detail_retail_price_sum = soSalesOrderDetailModel::join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'SO_DETAIL.prd_id')
                                                                ->select(DB::raw('SUM("IPM"."retail_price") as "retail_price"'))
                                                                ->where('so_id', $soId)
                                                                ->first();

      $prdSugest = imPrdMasterModel::join('IM_PRD_COLOR AS IPC', 'IPC.color_id', '=', 'IM_PRD_MASTER.color_id')
                                     ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IM_PRD_MASTER.size_id')
                                     ->join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'IM_PRD_MASTER.uom_id1')
                                     ->select(DB::raw('CAST(prd_id AS varchar) AS data'), DB::raw('prd_desc ||'."'-'".'||"IPC"."color_desc" ||'."'-'".'||"IPS"."size_desc" AS value'), 'uom_id', 'IPU.uom_desc AS uomdesc', 'uom_conversion', 'retail_price')->get();

      $ppn = soSalesOrderModel::select('ppn')->where('so_id', $soId)->first();
      $price = preg_replace("/[^A-Za-z0-9 ]/", '',  $so_detail_sub_sum->sub_total);
      $price = str_replace('Rp', '', $price);

      if($ppn->ppn > 0){
        $percentPPN = $ppn->ppn / 100;
        $total_ppn = $percentPPN * $price;
        $total = $price + $total_ppn;
      }else{
        $total_ppn = 0;
        $total = $price;
      }

      $so_id = soSalesOrderModel::select('so_id', 'so_no', 'so_status', 'created_date')->where('so_id', $soId)->first();

      $listModules = $this->listModules($userGroupId);
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('salesorder.so_detail_sales_order.'.$view, [
            'userGroupId' => $userGroupId,
            'listModules' => $listModules,
            'title' => $userModule,
            'privilaged' => $priv,
            'detailSo' => $detailSo,
            'uoms' => $uoms,
            'so_id' => $so_id,
            'so_detail_id' => $so_detail_id,
            'so_detail_sub_sum' => $so_detail_sub_sum,
            'total' => $total,
            'total_bef_ppn' => $price,
            'total_ppn' => $total_ppn,
            'so_detail_retail_price_sum' => $so_detail_retail_price_sum,
            'prdSugest' => $prdSugest
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function listDetailSo(Request $request, $view, $soId){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $detailSo = soSalesOrderDetailModel::join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'SO_DETAIL.prd_id')
                                ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IPM.size_id')
                                ->join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'SO_DETAIL.uom_id')
                                ->join('IM_PRD_COLOR AS IPC', 'IPC.color_id', '=', 'IPM.color_id')
                                ->select('SO_DETAIL.so_detail_id', 'SO_DETAIL.qty_order', 'SO_DETAIL.sub_total', 'IPM.prd_id', 'IPM.retail_price', 'IPM.prd_desc',
                                'IPS.size_desc', 'SO_DETAIL.uom_id', 'IPU.uom_desc', 'SO_DETAIL.uom_conversion', 'SO_DETAIL.order_price',
                                'IPM.prd_code', 'IPC.color_desc')
                                ->where('SO_DETAIL.so_id', $soId);

      $uoms = imPrdUnitModel::select('uom_id', 'uom_desc')->get();
      $so_id = soSalesOrderModel::select('so_id', 'so_no', 'so_status', 'created_date')->where('so_id', $soId)->first();
      $prdSugest = imPrdMasterModel::join('IM_PRD_COLOR AS IPC', 'IPC.color_id', '=', 'IM_PRD_MASTER.color_id')
                                     ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IM_PRD_MASTER.size_id')
                                     ->join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'IM_PRD_MASTER.uom_id1')
                                     ->select(DB::raw('CAST(prd_id AS varchar) AS data'), DB::raw('prd_desc ||'."'-'".'||"IPC"."color_desc" ||'."'-'".'||"IPS"."size_desc" AS value'), 'uom_id', 'IPU.uom_desc AS uomdesc', 'uom_conversion', 'retail_price')->get();
      $listModules = $this->listModules($userGroupId);
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($so_id->so_status != "approve"){
            return Datatables::of($detailSo)
            ->addColumn('delete_bulk', function($data){
                return "
                  <input type='checkbox' name='so_detail_id[]' value='".$data->so_detail_id."' />
                ";
            })
            ->editColumn('sub_total', function ($data) {
                   return $data->sub_total;
            })
            ->editColumn('qty_order', function ($data) {
                   return number_format($data->qty_order);
            })
            ->addColumn('action', function ($data) use ($priv, $uoms, $so_id, $prdSugest,$view) {
                return view('salesorder.so_detail_sales_order.'.$view, ['privilaged' => $priv, 'data' => $data, 'uoms' => $uoms, 'so_id' => $so_id, 'prdSugest' => $prdSugest]);
            })
            ->filter(function ($detailSo) use ($request) {
                     if ($request->has('prd_desc')) {
                         $detailSo->where('IPM.prd_desc', 'LIKE', '%'.$request->prd_desc.'%');
                     }
             })
            ->make(true);
      }else{
            return Datatables::of($detailSo)
            ->addColumn('delete_bulk', function($data){
                return "
                  <input type='checkbox' name='so_detail_id[]' value='".$data->so_detail_id."' />
                ";
            })
            ->editColumn('sub_total', function ($data) {
                   return $data->sub_total;
            })
            ->editColumn('qty_order', function ($data) {
                   return number_format($data->qty_order);
            })
            ->filter(function ($detailSo) use ($request) {
                   if ($request->has('prd_desc')) {
                       $detailSo->where('IPM.prd_desc', 'LIKE', '%'.$request->prd_desc.'%');
                   }
             })
            ->make(true);
      }
    }

    public function addDetailSoAction(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);
      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
          $prd_price = preg_replace("/[^A-Za-z0-9 ]/", '', $request->retail_price);
          $sub_total = $prd_price * $request->qty_order;
        try {
          $detailSoData = new soSalesOrderDetailModel();
          $detailSoData->so_id = $request->so_id;
          $detailSoData->prd_id = $request->prd_id;
          $detailSoData->qty_delivery =  0;
          $detailSoData->qty_order =  $request->qty_order;
          $detailSoData->order_price =  $prd_price;
          $detailSoData->disc =  0;
          $detailSoData->disc_value =  0;
          $detailSoData->uom_id = $request->uom_id;
          $detailSoData->uom_conversion = $request->uom_conversion;
          $detailSoData->sub_total = $sub_total;
          $insert =  $detailSoData->save();
        } catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error inserting');
        }

        $sum_qty = soSalesOrderDetailModel::where('so_id', $request->so_id)->sum('qty_order');
        $sum_subtotal = soSalesOrderDetailModel::where('so_id', $request->so_id)->sum('sub_total');
        $this->updateTotalSo($request->so_id, $sum_subtotal, $sum_qty, 0);

        return Redirect::back()->with('status', 'Detail so added');
      }

    }

    public function editDetailSo(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $so_detail_id = $request->so_detail_id;
      $prd_price = imPrdMasterModel::select('retail_price')->where('prd_id', $request->prd_id)->first();

      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $price = preg_replace("/[^A-Za-z0-9 ]/", '',  $request->retail_price);
        $price = str_replace('Rp', '', $price);
        $sub_total = $price * $request->qty_order;

        try {
          $detailSoData = new soSalesOrderDetailModel();
          $detailSoData::where('so_detail_id', $so_detail_id)
                     ->update([
                            'so_id'=>$request->so_id,
                            'prd_id'=>$request->prd_id,
                            'qty_order'=>$request->qty_order,
                            'order_price' => $price,
                            'uom_id'=>$request->uom_id,
                            'uom_conversion'=>$request->uom_conversion,
                            'sub_total'=>$sub_total,

                      ]);
          } catch(\Illuminate\Database\QueryException $ex){
              return Redirect::back()->with('status', 'Error updating');
          }

          $sum_qty = soSalesOrderDetailModel::where('so_id', $request->so_id)->sum('qty_order');
          $sum_subtotal = soSalesOrderDetailModel::where('so_id', $request->so_id)->sum('sub_total');
          $this->updateTotalSo($request->so_id, $sum_subtotal, $sum_qty, 0);

        return Redirect::back()->with('status', 'Detail so updated');
      }

    }

    public function deleteDetailSo(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $so_detail_id = $request->so_detail_id;

      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $detailSoData = new soSalesOrderDetailModel();
        $detailSoData::where('so_detail_id', $so_detail_id)->delete();

        $sum_qty = soSalesOrderDetailModel::where('so_id', $request->so_id)->sum('qty_order');
        $sum_subtotal = soSalesOrderDetailModel::where('so_id', $request->so_id)->sum('sub_total');

        $this->updateTotalSo($request->so_id, $sum_subtotal, $sum_qty, 0);

        return Redirect::back()->with('status', 'Detail so deleted');
      }

    }

    public function deleteBulkDetailSo(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $so_detail_id = $request->so_detail_id;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $detailSoData = new soSalesOrderDetailModel();
        $detailSoData::whereIn('so_detail_id', $so_detail_id)->delete();

        $sum_qty = soSalesOrderDetailModel::where('so_id', $request->so_id)->sum('qty_order');
        $sum_subtotal = soSalesOrderDetailModel::where('so_id', $request->so_id)->sum('sub_total');
        $this->updateTotalSo($request->so_id, $sum_subtotal, $sum_qty, 0);

        $status = "success";

      }
      return $status;
    }
}
