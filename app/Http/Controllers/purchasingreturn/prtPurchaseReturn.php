<?php

namespace App\Http\Controllers\purchasingreturn;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\purchasinginvoice\pi_purchase_invoice\piPurchaseInvoiceModel;
use App\purchasinginvoice\pi_detail_purchase_invoice\piPurchaseInvoiceDetailModel;
use App\purchaseorder\po_supp\supplierModel;
use App\usermanagement\um_module\umModuleModel;
use App\warehouse\im_wh_loc\imWhLocModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Datatables;
use Carbon\Carbon;

class prtPurchaseReturn extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function ajaxWarehouseCatalog($view, $keyword){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('purchasingreturn.prt_purchase_return.'.$view, [
            'keyword' => $keyword,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function exportToExcel($view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv->canExport)){
        $prt = piPurchaseInvoiceModel::select('inv_no', 'inv_date', 'notes', 'inv_status', 'total_gross', 'total_ppn')->where('inv_type', 2)->get();
         $data = null;
         foreach($prt as $datas){
           $date = date("d M Y", strtotime($datas->inv_date));
             $data[] = array(
               $datas->inv_no,
               $date,
               $datas->notes,
               $datas->inv_status,
               $datas->total_gross,
               $datas->total_ppn,
             );
         }
        if($data != null){
           $column = array('INV NO','DATE', 'NOTES', 'STATUS', 'TOTAL GROSS', 'TOTAL PPN');
           $this->excelDownload($data, "Data purchase return", $column);
         }else{
           return Redirect::back()->withErrors(['No data to export']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function listPrtView($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $listModules = $this->listModules($userGroupId);
        $priv = $this->checkPrivilaged($moduleId, $userGroupId);
        if($priv != null && $priv->canView == 1){
          return view('purchasingreturn.prt_purchase_return.'.$view, [
              'userGroupId' => $userGroupId,
              'listModules' => $listModules,
              'title' => $userModule,
              'privilaged' => $priv
          ]);
        }else{
           return Redirect::back()->withErrors(['Not permitted']);
        }
    }

    public function listPrtData(Request $request, $view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $listModules = $this->listModules($userGroupId);
        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        $prt = piPurchaseInvoiceModel::join('PO_SUPP_MASTER AS PSM', 'PSM.supp_id', '=', 'AP_INVOICE_HEADER.supp_id')
                                      ->join('IM_WH_LOC AS WH', 'WH.wh_id', '=', 'AP_INVOICE_HEADER.wh_id')
                                      ->select(['AP_INVOICE_HEADER.inv_id', 'AP_INVOICE_HEADER.inv_no', 'AP_INVOICE_HEADER.inv_date', 'AP_INVOICE_HEADER.inv_status',
                                      'AP_INVOICE_HEADER.notes', 'AP_INVOICE_HEADER.no_inv_supp', 'AP_INVOICE_HEADER.no_inv_pajak',
                                      'AP_INVOICE_HEADER.reference', 'AP_INVOICE_HEADER.supp_id', 'AP_INVOICE_HEADER.ppn',
                                      'AP_INVOICE_HEADER.total_nett', 'PSM.supp_desc', 'WH.wh_id', 'WH.wh_desc'])
                                       ->where('inv_type', 2);
        if ($request->has('start_date') && $request->has('end_date')) {
            $start = strtotime($request->get('start_date'));
            $end = strtotime($request->get('end_date'));
            $pi->whereBetween('inv_date', [date('Y-m-d', $start), date('Y-m-d', $end)]);
        }

        return Datatables::of($prt)
        ->addColumn('delete_bulk', function($data){
            return "
              <input type='checkbox' name='inv_id[]' value='".$data->inv_id."' />
            ";
        })
        ->editColumn('inv_date', function ($data) {
               return $data->inv_date ? with(new Carbon($data->inv_date))->format('d M y') : '';
        })
        ->addColumn('action', function ($data) use ($priv) {
            return view('purchasingreturn.prt_purchase_return.prtPurchaseReturnDataTable', ['privilaged' => $priv, 'data' => $data]);
       })
       ->make(true);
    }

    private function prtCodeGenerate($prtDate, $maxPrt){
      $code = "RO";
      $prtDate = strtotime($prtDate);
      $yy = date("y", $prtDate);
      $mm = date("m", $prtDate);

      if($maxPrt != ""){
        $maxPrt = substr($maxPrt, -6);
        $prt_no = $maxPrt + 1;
        $prt_no = str_pad($prt_no, 6, '0',STR_PAD_LEFT);
      }else{
        $prt_no = "000001";
      }

      $prt_no_final = $code.$yy.$mm.$prt_no;
      return $prt_no_final;
    }

    public function addPrt(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);
      $maxprt = piPurchaseInvoiceModel::select(DB::raw('MAX(RIGHT(inv_no, 6)) as inv_no'))->where('inv_type', 2)->first();
      $prtDate = strtotime($request->inv_date);
      $prt_no = $this->prtCodeGenerate($request->inv_date, $maxprt->inv_no);
      $supp_term = supplierModel::select('supp_term')->where('supp_id', $request->supp_id)->first();
      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
            $prtData = new piPurchaseInvoiceModel();
            $prtData->inv_no = $prt_no;
            $prtData->notes = $request->notes;
            $prtData->inv_status = "open";
            $prtData->inv_type = "2";
            $prtData->supp_id = $request->supp_id;
            $prtData->wh_id = $request->wh_id;
            $prtData->term = ($supp_term != null ? $supp_term->supp_term:0);
            $prtData->no_inv_supp = $request->no_inv_supp;
            $prtData->no_inv_pajak = $request->no_inv_pajak;
            $prtData->reference = $request->reference;
            $prtData->ppn = $request->input('ppn', 0);
            $prtData->pi_flag = $request->input('pi_flag', 0);
            $prtData->inv_date = date("Y-m-d", $prtDate);
            $prtData->created_date = \Carbon\Carbon::now();
            $prtData->updated_date = \Carbon\Carbon::now();
            $prtData->created_by = $userId;
            $insert =  $prtData->save();
        } catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error inserting');
        }
        return Redirect::back()->with('status', 'Prt added');
      }

    }

    public function editPrt(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);
      $prtId = $request->inv_id;
      $prtDate = strtotime($request->inv_date);
      $supp_term = supplierModel::select('supp_term')->where('supp_id', $request->supp_id)->first();

      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
          $prtData = new piPurchaseInvoiceModel();
          $prtData::where('inv_id', $prtId)
                     ->update([
                            'inv_status'=>$request->input('inv_status', 'open'),
                            'inv_date'=>date("Y-m-d", $prtDate),
                            'supp_id'=> $request->supp_id,
                            'wh_id'=> $request->wh_id,
                            'term'=> ($supp_term != null ? $supp_term->supp_term:0),
                            'inv_type'=>2,
                            'no_inv_supp'=>$request->no_inv_supp,
                            'no_inv_pajak'=>$request->no_inv_pajak,
                            'reference'=>$request->reference,
                            'ppn'=>$request->input('ppn', 0),
                            'notes'=>$request->notes,
                            'updated_date' => \Carbon\Carbon::now(),
                            'updated_by' => $userId
                      ]);
          } catch(\Illuminate\Database\QueryException $ex){
              return Redirect::back()->with('status', 'Error updating');
          }
        return Redirect::back()->with('status', 'Prt updated');
      }

    }

    public function deletePrt(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $prtId = $request->inv_id;
      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $prtData = new piPurchaseInvoiceModel();
        $prtData::where('inv_id', $prtId)->delete();

        $prtDetailData = new piPurchaseInvoiceDetailModel();
        $prtDetailData::where('inv_id', $prtId)->delete();
        return Redirect::back()->with('status', 'Prt deleted');
      }

    }

    public function deleteBulkPrt(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $prtId = $request->inv_id;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $prtData = new piPurchaseInvoiceModel();
        $prtData::whereIn('inv_id', $prtId)->delete();

        $prtDetailData = new piPurchaseInvoiceDetailModel();
        $prtDetailData::whereIn('inv_id', $prtId)->delete();

        $status = "success";

      }
      return $status;
    }
}
