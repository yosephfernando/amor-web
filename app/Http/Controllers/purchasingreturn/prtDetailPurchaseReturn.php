<?php

namespace App\Http\Controllers\purchasingreturn;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\inventory\im_prd_master\imPrdMasterModel;
use App\inventory\im_prd_unit\imPrdUnitModel;
use App\purchaseorder\po_pr\poPrModel;
use App\purchasinginvoice\pi_purchase_invoice\piPurchaseInvoiceModel;
use App\purchasinginvoice\pi_detail_purchase_invoice\piPurchaseInvoiceDetailModel;
use App\stocktxn\im_stock_txn\imStockTxnModel;
use App\purchasinginvoice\pi_gr\piGrModel;
use App\usermanagement\um_module\umModuleModel;
use App\goodsreceive\gr_detail_goods_receive\grGoodsReceiveDetailModel;
use App\goodsreceive\gr_goods_receive\grGoodsReceiveModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Illuminate\Support\Facades\DB;
use Datatables;

class prtDetailPurchaseReturn extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    private function updateTotalPrt($prtid, $price, $qty){
      $ppn = piPurchaseInvoiceModel::select('ppn')->where('inv_id', $prtid)->first();
      if($ppn->ppn > 0){
        $percentPPN = $ppn->ppn / 100;
        $total_ppn = $percentPPN * $price;

        $total_gross = $price + $total_ppn;
      }else{
        $total_ppn = 0;
        $total_gross = $price;
      }

      $updatePrt = new piPurchaseInvoiceModel();
      $updatePrt::where('inv_id', $prtid)
                 ->update([
                    "total_gross" => $total_gross,
                    "total_nett" => $total_gross,
                    "total_ppn" => $total_ppn,
                 ]);

       return true;
    }

    public function exportToExcel($view, $prtId){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv->canExport)){
        $detailPrt = piPurchaseInvoiceDetailModel::join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'AP_INVOICE_DETAIL.uom_id')
                                  ->join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'AP_INVOICE_DETAIL.prd_id')
                                  ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IPM.size_id')
                                  ->select('AP_INVOICE_DETAIL.inv_detail_id', 'IPU.uom_desc', 'AP_INVOICE_DETAIL.uom_conversion', 'IPM.prd_desc', 'AP_INVOICE_DETAIL.qty_inv', 'AP_INVOICE_DETAIL.price_inv', 'AP_INVOICE_DETAIL.sub_total', 'IPS.size_code')
                                  ->where('AP_INVOICE_DETAIL.inv_id', $prtId)
                                  ->get();
        $data = null;
         foreach($detailPrt as $datas){
             $data[] = array(
               $datas->inv_detail_id,
               $datas->prd_desc,
               $datas->qty_inv,
               $datas->price_inv,
               $datas->sub_total,
               $datas->uom_desc,
               $datas->uom_conversion,
               $datas->size_code,
             );
         }
         if($data != null){
           $column = array('ID DETAIL INV','PRODUCT', 'QTY', 'UOM', 'UOM CONVERSION','SIZE');
           $this->excelDownload($data, "Data purchase return", $column);
         }else{
           return Redirect::back()->withErrors(['No data to export']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function ajaxPrice($view, $prd_id){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);
      $retail_price = imPrdMasterModel::select('retail_price')->where('prd_id', $prd_id)->first();

      if($priv != null && $priv->canView == 1){
        return view('purchasingreturn.prt_detail_purchase_return.'.$view, [
            'retail_price' => $retail_price,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function ajaxPiCatalog($view, $keyword){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('purchasingreturn.prt_detail_purchase_return.'.$view, [
            'keyword' => $keyword,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function ajaxPiDetailCatalog($view, $id){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);
      $invDetail = piPurchaseInvoiceDetailModel::select('IPM.prd_code', 'IPM.prd_desc','IPS.size_desc', 'IPM.retail_price', 'AP_INVOICE_DETAIL.qty_inv', 'AP_INVOICE_DETAIL.prd_id', 'AP_INVOICE_DETAIL.inv_detail_id')
                                                ->join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'AP_INVOICE_DETAIL.prd_id')
                                                ->join('IM_PRD_SIZE AS IPS', 'IPM.size_id', '=', 'IPS.size_id')
                                                ->where('AP_INVOICE_DETAIL.inv_id', $id)
                                                ->get();

      if($priv != null && $priv->canView == 1){
        return view('purchasingreturn.prt_detail_purchase_return.'.$view, [
            'invDetail' => $invDetail,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function addDetailPrtAction(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);
      $retail_price =  imPrdMasterModel::select('retail_price')->where('prd_id', $request->prd_id)->first();
      $inv_id = $request->inv_id;

      $inv_no = piPurchaseInvoiceModel::select('inv_no', 'inv_date', 'wh_id')->where('inv_id', $inv_id)->first();

      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $sub_total = str_replace(".", "", $request->price_inv) *  $request->qty_inv;
        try {
          $detailPrtData = new piPurchaseInvoiceDetailModel();
          $detailPrtData->inv_id = $inv_id;
          $detailPrtData->prd_id = $request->prd_id;
          $detailPrtData->qty_inv =  $request->qty_inv;
          $detailPrtData->price_inv =  str_replace(".", "", $request->price_inv);
          $detailPrtData->disc =  0;
          $detailPrtData->disc_value =  0;
          $detailPrtData->uom_id = $request->uom_id;
          $detailPrtData->uom_conversion = $request->uom_conversion;
          $detailPrtData->sub_total = $sub_total;
          $insert =  $detailPrtData->save();
        } catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error inserting detail invoice');
        }

        /* insert TXN */
        try {
          $insertStockTxn = new imStockTxnModel();
          $insertStockTxn->txn_id = $inv_id;
          $insertStockTxn->txn_no = ($inv_no != null ? $inv_no->inv_no:0);
          $insertStockTxn->txn_date = ($inv_no != null ? $inv_no->inv_date:null);
          $insertStockTxn->txn_type_id = 1;
          $insertStockTxn->wh_id = $inv_no->wh_id;
          $insertStockTxn->prd_id = $request->prd_id;
          $insertStockTxn->qty = "-".$request->qty_inv;
          $insertStockTxn->cost_value = $sub_total;
          $insertStockTxn->txn_index = 1;
          $insertStockTxn->created_date = \Carbon\Carbon::now();
          $insertStockTxn->created_by = Auth::user()->id;
          $insertStockTxn->save();
        } catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error inserting txn');
        }

        $sum_qty = piPurchaseInvoiceDetailModel::where('inv_id', $inv_id)->sum('qty_inv');
        $sum_subtotal = piPurchaseInvoiceDetailModel::select(DB::raw("SUM(sub_total::money::numeric::float8) AS sub_total"))->where('inv_id', $inv_id)->first();
        $this->updateTotalPrt($inv_id, $sum_subtotal->sub_total, $sum_qty);
        return Redirect::back()->with('status', 'Detail purchase return added');
      }
    }

    public function loadPi(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      $inv_detail_id = $request->inv_detail_id;
      //inv id type 2
      $inv_id_ = $request->inv_id_;

      if($inv_detail_id != null){
        $inv_details = piPurchaseInvoiceDetailModel::join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'AP_INVOICE_DETAIL.prd_id')
                                                    ->select('IPM.retail_price', 'AP_INVOICE_DETAIL.*')
                                                    ->whereIn('AP_INVOICE_DETAIL.inv_detail_id', $inv_detail_id)->get();
        //inv id type 1
        $inv_id = piPurchaseInvoiceDetailModel::select('inv_id')->where('inv_detail_id', $inv_detail_id)->first();
        $inv_no = piPurchaseInvoiceModel::select('inv_no', 'inv_date', 'wh_id')->where('inv_id', $inv_id->inv_id)->first();
      }else{
        return Redirect::back()->with('status', 'error : choose one of the detail(s)');
      }


      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $i = 0;
        foreach($inv_details as $inv_detail){
              $inv_det_id = $inv_detail->inv_detail_id;
              $price_inv = preg_replace("/[^A-Za-z0-9 ]/", '',$request->input('price_inv'.$inv_det_id));
              $price_inv = str_replace('Rp', '', $price_inv);
              $qty_inv = $request->input('qty_inv'.$inv_det_id);
              $sub_total = $price_inv * $qty_inv ;

              try {
                $detailPiData = new piPurchaseInvoiceDetailModel();
                $detailPiData->inv_id = $inv_id_;
                $detailPiData->prd_id = $inv_detail->prd_id;
                $detailPiData->qty_inv =  $qty_inv;
                $detailPiData->price_inv =  $price_inv;
                $detailPiData->disc =  0;
                $detailPiData->disc_value =  0;
                $detailPiData->uom_id = $inv_detail->uom_id;
                $detailPiData->uom_conversion = $inv_detail->uom_conversion;
                $detailPiData->sub_total = $sub_total;
                $insert =  $detailPiData->save();
              } catch(\Illuminate\Database\QueryException $ex){
                  return Redirect::back()->with('status', 'Error inserting invoice detail');
              }

              /* insert TXN */
              try {
                $insertStockTxn = new imStockTxnModel();
                $insertStockTxn->txn_id = $inv_id_;
                $insertStockTxn->txn_no = ($inv_no != null ? $inv_no->inv_no:0);
                $insertStockTxn->txn_date = ($inv_no != null ? $inv_no->inv_date:null);
                $insertStockTxn->txn_type_id = 1;
                $insertStockTxn->wh_id =($inv_no != null ? $inv_no->wh_id:0);
                $insertStockTxn->prd_id = $inv_detail->prd_id;
                $insertStockTxn->qty = "-".$qty_inv;
                $insertStockTxn->cost_value = $sub_total;
                $insertStockTxn->txn_index = $i;
                $insertStockTxn->created_date = \Carbon\Carbon::now();
                $insertStockTxn->created_by = Auth::user()->id;
                $insertStockTxn->save();
              } catch(\Illuminate\Database\QueryException $ex){
                  return Redirect::back()->with('status', 'Error inserting txn');
              }

              $sum_qty = piPurchaseInvoiceDetailModel::where('inv_id', $inv_id_)->sum('qty_inv');
              $sum_subtotal = piPurchaseInvoiceDetailModel::select(DB::raw("SUM(sub_total::money::numeric::float8) AS sub_total"))->where('inv_id', $inv_id_)->first();
              $this->updateTotalPrt($inv_id_, $sum_subtotal->sub_total, $sum_qty);
        $i++;}
        return Redirect::back()->with('status', 'Detail purchase return added');
      }
    }

    public function listDetailPrtView($view, $prtId){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $detailPrt = piPurchaseInvoiceDetailModel::join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'AP_INVOICE_DETAIL.prd_id')
                                ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IPM.size_id')
                                ->join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'AP_INVOICE_DETAIL.uom_id')
                                ->select('AP_INVOICE_DETAIL.inv_detail_id', 'AP_INVOICE_DETAIL.qty_inv', 'AP_INVOICE_DETAIL.sub_total', 'IPM.prd_id', 'AP_INVOICE_DETAIL.price_inv', 'IPM.prd_desc', 'IPS.size_desc', 'AP_INVOICE_DETAIL.uom_id', 'IPU.uom_desc', 'AP_INVOICE_DETAIL.uom_conversion')
                                ->where('AP_INVOICE_DETAIL.inv_id', $prtId)
                                ->get();

      $uoms = imPrdUnitModel::select('uom_id', 'uom_desc')->get();

      $prt_detail_id = piPurchaseInvoiceDetailModel::select('inv_detail_id')->where('inv_id', $prtId)->first();

      $prt_detail_sub_sum = piPurchaseInvoiceDetailModel::select(DB::raw('SUM(sub_total) as sub_total'), DB::raw('SUM(qty_inv) as qty_inv'))->where('inv_id', $prtId)->first();
      $prt_detail_retail_price_sum = piPurchaseInvoiceDetailModel::join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'AP_INVOICE_DETAIL.prd_id')
                                                                ->select(DB::raw('SUM("IPM"."retail_price") as "retail_price"'))
                                                                ->where('inv_id', $prtId)
                                                                ->first();

      $ppn = piPurchaseInvoiceModel::select('ppn')->where('inv_id', $prtId)->first();
      $price = preg_replace("/[^A-Za-z0-9 ]/", '',$prt_detail_sub_sum->sub_total);
      $price = str_replace('Rp', '', $price);
      $total = $price;
      $total_ppn = 0;
      if($ppn->ppn > 0){
        $percentPPN = $ppn->ppn / 100;
        $total_ppn = $percentPPN * $price;
        $total = $price + $total_ppn;
      }

      $prt_id = piPurchaseInvoiceModel::select('inv_id', 'inv_no', 'inv_status', 'created_date', 'pi_flag')->where('inv_id', $prtId)->first();
      $listModules = $this->listModules($userGroupId);
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('purchasingreturn.prt_detail_purchase_return.'.$view, [
          'userGroupId' => $userGroupId,
          'listModules' => $listModules,
          'title' => $userModule,
          'privilaged' => $priv,
          'detailPrt' => $detailPrt,
          'uoms' => $uoms,
          'prt_id' => $prt_id,
          'prt_detail_id' => $prt_detail_id,
          'prt_detail_sub_sum' => $prt_detail_sub_sum,
          'total' => $total,
          'total_bef_ppn' => $price,
          'total_ppn' => $total_ppn,
          'prt_detail_retail_price_sum' => $prt_detail_retail_price_sum
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function listDetailPrtData(Request $request, $view, $prtId){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $detailPrt = piPurchaseInvoiceDetailModel::join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'AP_INVOICE_DETAIL.prd_id')
                                ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IPM.size_id')
                                ->join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'AP_INVOICE_DETAIL.uom_id')
                                ->select('AP_INVOICE_DETAIL.inv_detail_id', 'AP_INVOICE_DETAIL.qty_inv', 'AP_INVOICE_DETAIL.sub_total', 'IPM.prd_id', 'AP_INVOICE_DETAIL.price_inv', 'IPM.prd_desc', 'IPS.size_desc', 'AP_INVOICE_DETAIL.uom_id', 'IPU.uom_desc', 'AP_INVOICE_DETAIL.uom_conversion')
                                ->where('AP_INVOICE_DETAIL.inv_id', $prtId);

      $uoms = imPrdUnitModel::select('uom_id', 'uom_desc')->get();
      $prt_id = piPurchaseInvoiceModel::select('inv_id', 'inv_no', 'inv_status', 'created_date')->where('inv_id', $prtId)->first();

      $listModules = $this->listModules($userGroupId);
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($prt_id->po_status != "approve"){
            return Datatables::of($detailPrt)
            ->addColumn('delete_bulk', function($data){
                return "
                  <input type='checkbox' name='inv_detail_id[]' value='".$data->inv_detail_id."' />
                ";
            })
            ->editColumn('qty_inv', function ($data) {
                   return number_format($data->qty_inv);
            })
            ->addColumn('action', function ($data) use ($priv, $uoms, $prt_id) {
                return view('purchasingreturn.prt_detail_purchase_return.prtDetailPurchaseReturnDataTable', ['privilaged' => $priv, 'data' => $data, 'uoms' => $uoms, 'prt_id' => $prt_id]);
            })
            ->filter(function ($detailPo) use ($request) {
                     if ($request->has('prd_desc')) {
                         $detailPo->where('IPM.prd_desc', 'LIKE', '%'.$request->prd_desc.'%');
                     }
             })
            ->make(true);
      }else{
            return Datatables::of($detailPrt)
            ->addColumn('delete_bulk', function($data){
                return "
                  <input type='checkbox' name='inv_detail_id[]' value='".$data->inv_detail_id."' />
                ";
            })
            ->editColumn('qty_inv', function ($data) {
                   return number_format($data->qty_inv);
            })
            ->filter(function ($detailPrt) use ($request) {
                   if ($request->has('prd_desc')) {
                       $detailPo->where('IPM.prd_desc', 'LIKE', '%'.$request->prd_desc.'%');
                   }
             })
            ->make(true);
      }
    }

    public function editDetailPrt(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $inv_detail_id = $request->inv_detail_id;
      $inv_id = $request->inv_id;
      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
          $retail_price = preg_replace("/[^A-Za-z0-9 ]/", '',$request->retail_price);
          $retail_price = str_replace('Rp', '', $retail_price);
          $subtotal = $retail_price * $request->qty_inv;

          $detailPrtData = new piPurchaseInvoiceDetailModel();
          $detailPrtData::where('inv_detail_id', $inv_detail_id)
                     ->update([
                            'uom_id'=>$request->uom_id,
                            'uom_conversion'=>$request->uom_conversion,
                            'qty_inv'=>$request->qty_inv,
                            'price_inv'=>$retail_price,
                            'sub_total'=>$subtotal
                      ]);
       } catch(\Illuminate\Database\QueryException $ex){
           return Redirect::back()->with('status', 'Error updating');
       }
       $sum_qty = piPurchaseInvoiceDetailModel::where('inv_id', $inv_id)->sum('qty_inv');
       $sum_subtotal = piPurchaseInvoiceDetailModel::select(DB::raw("SUM(sub_total::money::numeric::float8) AS sub_total"))->where('inv_id', $inv_id)->first();
       $this->updateTotalPrt($inv_id, $sum_subtotal->sub_total, $sum_qty);

       return Redirect::back()->with('status', 'Detail purchase return updated');
      }

    }

    public function deleteDetailPrt(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $inv_detail_id = $request->inv_detail_id;
      $inv_id = $request->inv_id;

      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $detailPrtData = piPurchaseInvoiceDetailModel::where('inv_detail_id', $inv_detail_id)->delete();
        $prtGrData = piGrModel::where('inv_id', $inv_id)->delete();

        $sum_qty = piPurchaseInvoiceDetailModel::where('inv_id', $inv_id)->sum('qty_inv');
        $sum_subtotal = piPurchaseInvoiceDetailModel::select(DB::raw("SUM(sub_total::money::numeric::float8) AS sub_total"))->where('inv_id', $inv_id)->first();
        $this->updateTotalPrt($inv_id, $sum_subtotal->sub_total, $sum_qty);
        return Redirect::back()->with('status', 'Detail purchase return deleted');
      }

    }

    public function deleteBulkDetailPrt(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $inv_detail_id = $request->inv_detail_id;
      $inv_id = $request->inv_id;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $detailPrtData = piPurchaseInvoiceDetailModel::whereIn('inv_detail_id', $inv_detail_id)->delete();
        $prtGrData = piGrModel::select('inv_id')->where('inv_id', $inv_id)->get();
        if(!$prtGrData->isEmpty()){
            $prtGrDataDel = piGrModel::whereIn('inv_id', $inv_id)->delete();
        }
        $sum_qty = piPurchaseInvoiceDetailModel::where('inv_id', $inv_id)->sum('qty_inv');
        $sum_subtotal = piPurchaseInvoiceDetailModel::select(DB::raw("SUM(sub_total::money::numeric::float8) AS sub_total"))->where('inv_id', $inv_id)->first();
        $this->updateTotalPrt($inv_id, $sum_subtotal->sub_total, $sum_qty);
        $status = "success";
      }
      return $status;
    }
}
