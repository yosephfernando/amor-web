<?php

namespace App\Http\Controllers\salesinvoice;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\inventory\im_prd_master\imPrdMasterModel;
use App\inventory\im_prd_unit\imPrdUnitModel;
use App\salesinvoice\si_sales_invoice\siSalesInvoiceModel;
use App\salesinvoice\si_detail_sales_invoice\siSalesInvoiceDetailModel;
use App\usermanagement\um_module\umModuleModel;
use App\deliveryorder\do_delivery_order\doDeliveryOrderModel;
use App\deliveryorder\do_detail_delivery_order\doDeliveryOrderDetailModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Illuminate\Support\Facades\DB;
use Datatables;

class siDetailSalesInvoice extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    private function updateTotalSi($siid, $price, $qty){
      $ppn = siSalesInvoiceModel::select('ppn')->where('inv_id', $siid)->first();
      if($ppn->ppn > 0){
        $percentPPN = $ppn->ppn / 100;
        $total_ppn = $percentPPN * $price;
        $total_gross = $price + $total_ppn;
      }else{
        $total_gross = $price;
        $total_ppn = 0;
      }

      $updateSi = new siSalesInvoiceModel();
      $updateSi::where('inv_id', $siid)
                 ->update([
                    "total_gross" => $total_gross,
                    "total_nett" => $total_gross,
                    "total_ppn" => $total_ppn,
                 ]);

       return true;
    }

    /* private function piGrInsert($siid, $do_id){
      $piGrInsert = new piGrModel();
      $piGrInsert->inv_id = $siid;
      $piGrInsert->do_id = $do_id;
      $insert =  $piGrInsert->save();

      return true;
    } */

    private function updateDetailDo($siid, $do_detail_id){
      $updateDoDet = new doDeliveryOrderDetailModel();
      $updateDoDet::where('do_detail_id', $do_detail_id)
                 ->update([
                    "inv_id" => $siid
                 ]);

      return true;
    }

    private function updateStatusDo($doid){
      $updateDo = new doDeliveryOrderModel();
      $updateDo::where('do_id', $doid)
                 ->update([
                    "do_status" => "invoice"
                 ]);

      return true;
    }

    public function exportToExcel($view, $siId){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv->canExport)){
        $detailSi = siSalesInvoiceDetailModel::join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'AR_INVOICE_DETAIL.uom_id')
                                  ->join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'AR_INVOICE_DETAIL.prd_id')
                                  ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IPM.size_id')
                                  ->select('AR_INVOICE_DETAIL.inv_detail_id', 'IPU.uom_desc', 'AR_INVOICE_DETAIL.uom_conversion', 'IPM.prd_desc', 'AR_INVOICE_DETAIL.qty_inv', 'AR_INVOICE_DETAIL.price_inv', 'AR_INVOICE_DETAIL.sub_total', 'IPS.size_code')
                                  ->where('AR_INVOICE_DETAIL.inv_id', $siId)
                                  ->get();
        $data = null;
         foreach($detailSi as $datas){
             $data[] = array(
               $datas->inv_detail_id,
               $datas->prd_desc,
               $datas->qty_inv,
               $datas->uom_desc,
               $datas->uom_conversion,
               $datas->size_code,
               $datas->price_inv,
               $datas->sub_total,
             );
         }
         if($data != null){
           $column = array('ID DETAIL INV','PRODUCT', 'QTY', 'UOM', 'UOM CONVERSION','SIZE', 'PRICE', 'TOTAL PRICE');
           $this->excelDownload($data, "Data si", $column);
         }else{
           return Redirect::back()->withErrors(['No data to export']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    /* public function ajaxGrCatalog($view, $keyword){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('salesinvoice.si_detail_sales_invoice.'.$view, [
            'keyword' => $keyword,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    } */

    public function ajaxDoDetailCatalog($view, $id){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);
      $doDetail = doDeliveryOrderDetailModel::select('IPM.prd_code', 'IPM.prd_desc','IPS.size_desc', 'IPM.retail_price', 'DO_DETAIL.qty_delivery', 'DO_DETAIL.prd_id', 'DO_DETAIL.do_detail_id')
                                                ->join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'DO_DETAIL.prd_id')
                                                ->join('IM_PRD_SIZE AS IPS', 'IPM.size_id', '=', 'IPS.size_id')
                                                ->where('DO_DETAIL.do_id', $id)
                                                ->get();

      if($priv != null && $priv->canView == 1){
        return view('salesinvoice.si_detail_sales_invoice.'.$view, [
            'doDetail' => $doDetail,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function loadDo(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      $do_detail_id = $request->do_detail_id;
      $do_id = $request->do_id;

      if($do_detail_id != null){
        $do_details =  doDeliveryOrderDetailModel::whereIn('do_detail_id', $do_detail_id)->get();
      }else{
        return Redirect::back()->with('status', 'error : choose one on the detail(s)');
      }

      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $i = 0;
        foreach($do_details as $do_details){
              $do_detail_id = $do_details->do_detail_id;
              $prd_price = preg_replace("/[^A-Za-z0-9 ]/", '', $request->input('retail_price'.$do_detail_id));
              $prd_price = str_replace('Rp', '', $prd_price);

              $qty = $do_details->qty_delivery;
              $sub_total = $prd_price * $qty;

              try {
                $detailSiData = new siSalesInvoiceDetailModel();
                $detailSiData->inv_id = $request->inv_id;
                $detailSiData->prd_id = $do_details->prd_id;
                $detailSiData->qty_inv =  $do_details->qty_delivery;
                $detailSiData->price_inv =  $prd_price;
                $detailSiData->disc =  0;
                $detailSiData->disc_value =  0;
                $detailSiData->uom_id = $do_details->uom_id;
                $detailSiData->uom_conversion = $do_details->uom_conversion;
                $detailSiData->sub_total = $sub_total;
                $insert =  $detailSiData->save();
              } catch(\Illuminate\Database\QueryException $ex){
                  return Redirect::back()->with('status', 'Error inserting');
              }

              $sum_qty = siSalesInvoiceDetailModel::where('inv_id', $request->inv_id)->sum('qty_inv');
              $sum_subtotal = siSalesInvoiceDetailModel::select(DB::raw("SUM(sub_total::money::numeric::float8) AS sub_total"))->where('inv_id', $request->inv_id)->first();
              $this->updateTotalSi($request->inv_id, $sum_subtotal->sub_total, $sum_qty);
              //$this->piGrInsert($request->inv_id, $do_details->do_detail_id);
        $i++;}
        $this->updateStatusDo($do_id);
        return Redirect::back()->with('status', 'Detail si added');
      }
    }

    public function listDetailSiView(Request $request, $view, $siId){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $detailSi = siSalesInvoiceDetailModel::join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'AR_INVOICE_DETAIL.prd_id')
                                ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IPM.size_id')
                                ->join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'AR_INVOICE_DETAIL.uom_id')
                                ->select('AR_INVOICE_DETAIL.inv_detail_id', 'AR_INVOICE_DETAIL.qty_inv', 'AR_INVOICE_DETAIL.sub_total', 'AR_INVOICE_DETAIL.price_inv', 'IPM.prd_id', 'IPM.retail_price', 'IPM.prd_desc', 'IPS.size_desc', 'AR_INVOICE_DETAIL.uom_id', 'IPU.uom_desc', 'AR_INVOICE_DETAIL.uom_conversion')
                                ->where('AR_INVOICE_DETAIL.inv_id', $siId)
                                ->get();

      $uoms = imPrdUnitModel::select('uom_id', 'uom_desc')->get();

      $si_detail_id = siSalesInvoiceDetailModel::select('inv_detail_id')->where('inv_id', $siId)->first();

      $si_detail_sub_sum = siSalesInvoiceDetailModel::select(DB::raw('SUM(sub_total) as sub_total'), DB::raw('SUM(qty_inv) as qty_inv'))->where('inv_id', $siId)->first();
      $si_detail_retail_price_sum = siSalesInvoiceDetailModel::join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'AR_INVOICE_DETAIL.prd_id')
                                                                ->select(DB::raw('SUM("IPM"."retail_price") as "retail_price"'))
                                                                ->where('inv_id', $siId)
                                                                ->first();

      $ppn = siSalesInvoiceModel::select('ppn')->where('inv_id', $siId)->first();
      $price = $si_detail_sub_sum->sub_total;
      $total = $price;
      $total_ppn = 0;
      if($ppn->ppn > 0){
        $percentPPN = $ppn->ppn / 100;
        $total_ppn = $percentPPN * $price;
        $total = $price + $total_ppn;
      }

      $si_id = siSalesInvoiceModel::select('inv_id', 'inv_no', 'inv_status', 'created_date')->where('inv_id', $siId)->first();
      $doSugest = doDeliveryOrderModel::select(DB::raw('CAST(do_id AS varchar) AS data'), 'do_no AS value', 'do_status')->where('do_status', 'open')->get();
      $listModules = $this->listModules($userGroupId);
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('salesinvoice.si_detail_sales_invoice.'.$view, [
          'userGroupId' => $userGroupId,
          'listModules' => $listModules,
          'title' => $userModule,
          'privilaged' => $priv,
          'detailSi' => $detailSi,
          'uoms' => $uoms,
          'si_id' => $si_id,
          'si_detail_id' => $si_detail_id,
          'si_detail_sub_sum' => $si_detail_sub_sum,
          'total' => $total,
          'total_bef_ppn' => $price,
          'total_ppn' => $total_ppn,
          'si_detail_retail_price_sum' => $si_detail_retail_price_sum,
          'doSugest' => $doSugest
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function listDetailSiData(Request $request, $view, $siId){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $detailSi = siSalesInvoiceDetailModel::join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'AR_INVOICE_DETAIL.prd_id')
                                ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IPM.size_id')
                                ->join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'AR_INVOICE_DETAIL.uom_id')
                                ->select('AR_INVOICE_DETAIL.inv_detail_id', 'AR_INVOICE_DETAIL.qty_inv', 'AR_INVOICE_DETAIL.sub_total', 'AR_INVOICE_DETAIL.price_inv', 'IPM.prd_id', 'IPM.retail_price', 'IPM.prd_desc', 'IPS.size_desc', 'AR_INVOICE_DETAIL.uom_id', 'IPU.uom_desc', 'AR_INVOICE_DETAIL.uom_conversion')
                                ->where('AR_INVOICE_DETAIL.inv_id', $siId);

      $uoms = imPrdUnitModel::select('uom_id', 'uom_desc')->get();
      $si_id = siSalesInvoiceModel::select('inv_id', 'inv_no', 'inv_status', 'created_date')->where('inv_id', $siId)->first();

      $listModules = $this->listModules($userGroupId);
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($si_id->inv_status != "approve"){
            return Datatables::of($detailSi)
            ->addColumn('delete_bulk', function($data){
                return "
                  <input type='checkbox' name='inv_detail_id[]' value='".$data->inv_detail_id."' />
                ";
            })
            ->editColumn('qty_inv', function ($data) {
                   return number_format($data->qty_inv);
            })
            ->addColumn('action', function ($data) use ($priv, $uoms, $si_id, $view) {
                return view('salesinvoice.si_detail_sales_invoice.'.$view, ['privilaged' => $priv, 'data' => $data, 'uoms' => $uoms, 'si_id' => $si_id]);
            })
            ->make(true);
      }else{
            return Datatables::of($detailSi)
            ->addColumn('delete_bulk', function($data){
                return "
                  <input type='checkbox' name='inv_detail_id[]' value='".$data->inv_detail_id."' />
                ";
            })
            ->editColumn('qty_inv', function ($data) {
                   return number_format($data->qty_inv);
            })
            ->make(true);
      }
    }

    public function editDetailSi(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $inv_detail_id = $request->inv_detail_id;

      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
          $retail_price = preg_replace("/[^A-Za-z0-9 ]/", '', $request->retail_price);
          $retail_price = str_replace('Rp', '', $retail_price);
          $subtotal = $retail_price * $request->qty_inv;

          $detailSiData = new siSalesInvoiceDetailModel();
          $detailSiData::where('inv_detail_id', $inv_detail_id)
                     ->update([
                            'uom_id'=>$request->uom_id,
                            'uom_conversion'=>$request->uom_conversion,
                            'qty_inv'=>$request->qty_inv,
                            'price_inv'=>$retail_price,
                            'sub_total'=>$subtotal
                      ]);
       } catch(\Illuminate\Database\QueryException $ex){
         if($ex->getCode() == 23000){
           return Redirect::back()->with('status', 'error : duplicate entry');
         }else{
           return Redirect::back()->with('status', 'Error updating');
         }
       }

       $sum_qty = siSalesInvoiceDetailModel::where('inv_id', $request->inv_id)->sum('qty_inv');
       $sum_subtotal = siSalesInvoiceDetailModel::select(DB::raw("SUM(sub_total::money::numeric::float8) AS sub_total"))->where('inv_id', $request->inv_id)->first();
       $this->updateTotalSi($request->inv_id, $sum_subtotal->sub_total, $sum_qty);

       return Redirect::back()->with('status', 'Detail si updated');
      }

    }

    public function deleteDetailSi(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $inv_detail_id = $request->inv_detail_id;
      $inv_id = $request->inv_id;

      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        //$piGrData = piGrModel::where('inv_id', $inv_id)->delete();
        $detailSiData = siSalesInvoiceDetailModel::where('inv_detail_id', $inv_detail_id)->delete();

        $sum_qty = siSalesInvoiceDetailModel::where('inv_id', $inv_id)->sum('qty_inv');
        $sum_subtotal = siSalesInvoiceDetailModel::select(DB::raw("SUM(sub_total::money::numeric::float8) AS sub_total"))->where('inv_id', $request->inv_id)->first();
        $this->updateTotalSi($request->inv_id, $sum_subtotal->sub_total, $sum_qty);
        return Redirect::back()->with('status', 'Detail si deleted');
      }

    }

    public function deleteBulkDetailSi(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $inv_detail_id = $request->inv_detail_id;
      $inv_id = $request->inv_id;
      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        /*$piGrData = piGrModel::select('inv_do_id')->where('inv_id', $inv_id)->get();

        foreach($piGrData as $data){
          $inv_do_id = $data->inv_do_id;
          $piGrDataDel = piGrModel::where('inv_do_id', $inv_do_id)->delete();
        }*/

        $detailSiData = siSalesInvoiceDetailModel::whereIn('inv_detail_id', $inv_detail_id)->delete();
        $sum_qty = siSalesInvoiceDetailModel::where('inv_id', $inv_id)->sum('qty_inv');
        $sum_subtotal = siSalesInvoiceDetailModel::select(DB::raw("SUM(sub_total::money::numeric::float8) AS sub_total"))->where('inv_id', $request->inv_id)->first();
        $this->updateTotalSi($inv_id, $sum_subtotal->sub_total, $sum_qty);
        $status = "success";
      }
      return $status;
    }
}
