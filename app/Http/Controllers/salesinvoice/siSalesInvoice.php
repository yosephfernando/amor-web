<?php

namespace App\Http\Controllers\salesinvoice;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\salesinvoice\si_sales_invoice\siSalesInvoiceModel;
use App\sales\so_cust\customerModel;
use App\usermanagement\um_module\umModuleModel;
use App\warehouse\im_wh_loc\imWhLocModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Datatables;
use Carbon\Carbon;

class siSalesInvoice extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function exportToExcel($view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv->canExport)){
        $si = siSalesInvoiceModel::select('inv_no', 'inv_date', 'notes', 'inv_status', 'total_gross', 'total_ppn')->where('inv_type', 1)->get();
        $data = null;
         foreach($si as $datas){
           $date = date("d M Y", strtotime($datas->inv_date));
             $data[] = array(
               $datas->inv_no,
               $date,
               $datas->notes,
               $datas->inv_status,
               $datas->total_gross,
               $datas->total_ppn,
             );
         }
         if($data != null){
             $column = array('INV NO','DATE', 'NOTES', 'STATUS', 'TOTAL GROSS', 'TOTAL PPN');
             $this->excelDownload($data, "Data si", $column);
         }else{
           return Redirect::back()->withErrors(['No data to export']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function listSiView($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $listModules = $this->listModules($userGroupId);
        $priv = $this->checkPrivilaged($moduleId, $userGroupId);
        $cust = customerModel::select(DB::raw('CAST(cust_id AS varchar) AS data'), 'cust_desc AS value')->where('cust_active_flag',1)->get();
        $wh = imWhLocModel::select(DB::raw('CAST(wh_id AS varchar) AS data'), 'wh_desc AS value')->get();
        if($priv != null && $priv->canView == 1){
          return view('salesinvoice.si_sales_invoice.'.$view, [
              'userGroupId' => $userGroupId,
              'listModules' => $listModules,
              'title' => $userModule,
              'cust' => $cust,
              'wh' => $wh,
              'privilaged' => $priv
          ]);
        }else{
           return Redirect::back()->withErrors(['Not permitted']);
        }
    }

    public function listSiData(Request $request, $view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $listModules = $this->listModules($userGroupId);
        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        $pi = siSalesInvoiceModel::join('SO_CUST_MASTER AS SCM', 'SCM.cust_id', '=', 'AR_INVOICE_HEADER.cust_id')
                                      ->join('IM_WH_LOC AS IWL', 'IWL.wh_id', '=', 'AR_INVOICE_HEADER.wh_id')
                                      ->select(['AR_INVOICE_HEADER.inv_id', 'AR_INVOICE_HEADER.inv_no', 'AR_INVOICE_HEADER.inv_date', 'AR_INVOICE_HEADER.inv_status',
                                      'AR_INVOICE_HEADER.notes', 'AR_INVOICE_HEADER.no_inv_pajak',
                                      'AR_INVOICE_HEADER.reference', 'AR_INVOICE_HEADER.cust_id', 'AR_INVOICE_HEADER.ppn',
                                      'AR_INVOICE_HEADER.total_nett', 'SCM.cust_desc', 'IWL.wh_id', 'IWL.wh_desc'])
                                      ->where('inv_type', 1);
        $cust = customerModel::select(DB::raw('CAST(cust_id AS varchar) AS data'), 'cust_desc AS value')->where('cust_active_flag',1)->get();
        $wh = imWhLocModel::select(DB::raw('CAST(wh_id AS varchar) AS data'), 'wh_desc AS value')->get();
        if ($request->has('start_date') && $request->has('end_date')) {
            $start = strtotime($request->get('start_date'));
            $end = strtotime($request->get('end_date'));
            $pi->whereBetween('inv_date', [date('Y-m-d', $start), date('Y-m-d', $end)]);
        }

        return Datatables::of($pi)
        ->addColumn('delete_bulk', function($data){
            return "
              <input type='checkbox' name='inv_id[]' value='".$data->inv_id."' />
            ";
        })
        ->editColumn('inv_date', function ($data) {
               return $data->inv_date ? with(new Carbon($data->inv_date))->format('d M y') : '';
        })
        ->addColumn('action', function ($data) use ($priv, $cust, $wh, $view) {
            return view('salesinvoice.si_sales_invoice.'.$view, ['privilaged' => $priv, 'data' => $data, 'cust' => $cust, 'wh' => $wh]);
       })
       ->make(true);
    }

    private function siCodeGenerate($siDate, $maxSi){
      $code = "SI";
      $siDate = strtotime($siDate);
      $yy = date("y", $siDate);
      $mm = date("m", $siDate);

      if($maxSi != ""){
        $maxSi = substr($maxSi, -6);
        $si_no = $maxSi + 1;
        $si_no = str_pad($si_no, 6, '0',STR_PAD_LEFT);
      }else{
        $si_no = "000001";
      }

      $si_no_final = $code.$yy.$mm.$si_no;
      return $si_no_final;
    }

    public function addSi(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);
      $maxsi = siSalesInvoiceModel::select(DB::raw('MAX(RIGHT(inv_no, 6)) as inv_no'))->where('inv_type', 1)->first();
      $siDate = strtotime($request->inv_date);
      $si_no = $this->siCodeGenerate($request->inv_date, $maxsi->inv_no);
      //$cust_term = customerModel::select('cust_term')->where('cust_id', $request->cust_id)->first();
      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
            $siData = new siSalesInvoiceModel();
            $siData->inv_no = $si_no;
            $siData->notes = $request->notes;
            $siData->inv_status = "open";
            $siData->inv_type = "1";
            $siData->cust_id = $request->cust_id;
            $siData->wh_id = $request->wh_id;
            $siData->term = 1;
            $siData->no_inv_pajak = $request->no_inv_pajak;
            $siData->reference = $request->reference;
            $siData->ppn = $request->input('ppn', 0);
            $siData->inv_date = date("Y-m-d", $siDate);
            $siData->created_date = \Carbon\Carbon::now();
            $siData->updated_date = \Carbon\Carbon::now();
            $siData->created_by = $userId;
            $insert =  $siData->save();
        } catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error inserting');
        }
        return Redirect::back()->with('status', 'Si added');
      }

    }

    public function editSi(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);
      $siId = $request->inv_id;
      $siDate = strtotime($request->inv_date);
      //$cust_term = customerModel::select('cust_term')->where('cust_id', $request->cust_id)->first();
      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
          $siData = new siSalesInvoiceModel();
          $siData::where('inv_id', $siId)
                     ->update([
                            'inv_date'=>date("Y-m-d", $siDate),
                            'cust_id'=> $request->cust_id,
                            'wh_id'=> $request->wh_id,
                            'term'=> 1,//$request->term,
                            'inv_type'=>1,//$request->inv_type,
                            'no_inv_pajak'=>$request->no_inv_pajak,
                            'reference'=>$request->reference,
                            'ppn'=>$request->input('ppn', 0),
                            'notes'=>$request->notes,
                            'updated_date' => \Carbon\Carbon::now(),
                            'updated_by' => $userId
                      ]);
          } catch(\Illuminate\Database\QueryException $ex){
              return Redirect::back()->with('status', 'Error updating');
          }
        return Redirect::back()->with('status', 'Si updated');
      }

    }

    public function deleteSi(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $siId = $request->inv_id;

      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $siData = new siSalesInvoiceModel();
        $siData::where('inv_id', $siId)->delete();

        $siDetailData = new siSalesInvoiceDetailModel();
        $siDetailData::where('inv_id', $siId)->delete();
        return Redirect::back()->with('status', 'Si deleted');
      }

    }

    public function deleteBulkSi(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $siId = $request->inv_id;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $siData = new siSalesInvoiceModel();
        $siData::whereIn('inv_id', $siId)->delete();

        $siDetailData = new siSalesInvoiceDetailModel();
        $siDetailData::whereIn('inv_id', $siId)->delete();

        $status = "success";

      }
      return $status;
    }
}
