<?php

namespace App\Http\Controllers\purchasinginvoice;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\inventory\im_prd_master\imPrdMasterModel;
use App\inventory\im_prd_unit\imPrdUnitModel;
use App\purchaseorder\po_pr\poPrModel;
use App\purchasinginvoice\pi_purchase_invoice\piPurchaseInvoiceModel;
use App\purchasinginvoice\pi_detail_purchase_invoice\piPurchaseInvoiceDetailModel;
use App\purchasinginvoice\pi_gr\piGrModel;
use App\usermanagement\um_module\umModuleModel;
use App\goodsreceive\gr_detail_goods_receive\grGoodsReceiveDetailModel;
use App\goodsreceive\gr_goods_receive\grGoodsReceiveModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Illuminate\Support\Facades\DB;
use Datatables;

class piDetailPurchaseInvoice extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    private function updateTotalPi($piid, $price, $qty){
      $ppn = piPurchaseInvoiceModel::select('ppn')->where('inv_id', $piid)->first();
      if($ppn->ppn > 0){
        $percentPPN = $ppn->ppn / 100;
        $total_ppn = $percentPPN * $price;
        $total_gross = $price + $total_ppn;
      }else{
        $total_gross = $price;
        $total_ppn = 0;
      }

      $updatePi = new piPurchaseInvoiceModel();
      $updatePi::where('inv_id', $piid)
                 ->update([
                    "total_gross" => $total_gross,
                    "total_nett" => $total_gross,
                    "total_ppn" => $total_ppn,
                 ]);

       return true;
    }

    private function piGrInsert($piid, $gr_id){
      $piGrInsert = new piGrModel();
      $piGrInsert->inv_id = $piid;
      $piGrInsert->gr_id = $gr_id;
      $insert =  $piGrInsert->save();

      return true;
    }

    private function updateDetailGr($piid, $gr_detail_id){
      $updateGrDet = new grGoodsReceiveDetailModel();
      $updateGrDet::where('gr_detail_id', $gr_detail_id)
                 ->update([
                    "inv_id" => $piid
                 ]);

      return true;
    }

    private function updateStatusGr($grid){
      $updateGr = new grGoodsReceiveModel();
      $updateGr::where('gr_id', $grid)
                 ->update([
                    "gr_status" => "invoice"
                 ]);

      return true;
    }

    public function exportToExcel($view, $piId){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv->canExport)){
        $detailPi = piPurchaseInvoiceDetailModel::join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'AP_INVOICE_DETAIL.uom_id')
                                  ->join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'AP_INVOICE_DETAIL.prd_id')
                                  ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IPM.size_id')
                                  ->select('AP_INVOICE_DETAIL.inv_detail_id', 'IPU.uom_desc', 'AP_INVOICE_DETAIL.uom_conversion', 'IPM.prd_desc', 'AP_INVOICE_DETAIL.qty_inv', 'AP_INVOICE_DETAIL.price_inv', 'AP_INVOICE_DETAIL.sub_total', 'IPS.size_code')
                                  ->where('AP_INVOICE_DETAIL.inv_id', $piId)
                                  ->get();
        $data = null;
         foreach($detailPi as $datas){
             $data[] = array(
               $datas->inv_detail_id,
               $datas->prd_desc,
               $datas->qty_inv,
               $datas->uom_desc,
               $datas->uom_conversion,
               $datas->size_code,
               $datas->price_inv,
               $datas->sub_total,
             );
         }
         if($data != null){
           $column = array('ID DETAIL INV','PRODUCT', 'QTY', 'UOM', 'UOM CONVERSION','SIZE', 'PRICE', 'TOTAL PRICE');
           $this->excelDownload($data, "Data pi", $column);
         }else{
           return Redirect::back()->withErrors(['No data to export']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function ajaxGrCatalog($view, $keyword){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('purchasinginvoice.pi_detail_purchase_invoice.'.$view, [
            'keyword' => $keyword,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function ajaxGrDetailCatalog($view, $id){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);
      $grDetail = grGoodsReceiveDetailModel::select('IPM.prd_code', 'IPM.prd_desc','IPS.size_desc', 'IPM.retail_price', 'GR_DETAIL.qty_receive', 'GR_DETAIL.prd_id', 'GR_DETAIL.gr_detail_id')
                                                ->join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'GR_DETAIL.prd_id')
                                                ->join('IM_PRD_SIZE AS IPS', 'IPM.size_id', '=', 'IPS.size_id')
                                                ->where('GR_DETAIL.gr_id', $id)
                                                ->get();

      if($priv != null && $priv->canView == 1){
        return view('purchasinginvoice.pi_detail_purchase_invoice.'.$view, [
            'grDetail' => $grDetail,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function loadGr(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      $gr_detail_id = $request->gr_detail_id;
      $gr_id = $request->gr_id;

      if($gr_detail_id != null){
        $gr_details =  grGoodsReceiveDetailModel::whereIn('gr_detail_id', $gr_detail_id)->get();
      }else{
        return Redirect::back()->with('status', 'error : choose one on the detail(s)');
      }

      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $i = 0;
        foreach($gr_details as $gr_detail){
              $gr_detail_id = $gr_detail->gr_detail_id;
              $prd_price = preg_replace("/[^A-Za-z0-9 ]/", '', $request->input('retail_price'.$gr_detail_id));
              $prd_price = str_replace('Rp', '', $prd_price);

              $qty = $gr_detail->qty_receive;
              $sub_total = $prd_price * $qty;

              try {
                $detailPiData = new piPurchaseInvoiceDetailModel();
                $detailPiData->inv_id = $request->inv_id;
                $detailPiData->prd_id = $gr_detail->prd_id;
                $detailPiData->qty_inv =  $gr_detail->qty_receive;
                $detailPiData->price_inv =  $prd_price;
                $detailPiData->disc =  0;
                $detailPiData->disc_value =  0;
                $detailPiData->uom_id = $gr_detail->uom_id;
                $detailPiData->uom_conversion = $gr_detail->uom_conversion;
                $detailPiData->sub_total = $sub_total;
                $insert =  $detailPiData->save();
              } catch(\Illuminate\Database\QueryException $ex){
                  return Redirect::back()->with('status', 'Error inserting');
              }

              $sum_qty = piPurchaseInvoiceDetailModel::where('inv_id', $request->inv_id)->sum('qty_inv');
              $sum_subtotal = piPurchaseInvoiceDetailModel::select(DB::raw("SUM(sub_total::money::numeric::float8) AS sub_total"))->where('inv_id', $request->inv_id)->first();
              $this->updateTotalPi($request->inv_id, $sum_subtotal->sub_total, $sum_qty);
              $this->piGrInsert($request->inv_id, $gr_detail->gr_detail_id);
        $i++;}
        $this->updateStatusGr($gr_id);
        return Redirect::back()->with('status', 'Detail pi added');
      }
    }

    public function listDetailPiView(Request $request, $view, $piId){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $detailPi = piPurchaseInvoiceDetailModel::join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'AP_INVOICE_DETAIL.prd_id')
                                ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IPM.size_id')
                                ->join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'AP_INVOICE_DETAIL.uom_id')
                                ->select('AP_INVOICE_DETAIL.inv_detail_id', 'AP_INVOICE_DETAIL.qty_inv', 'AP_INVOICE_DETAIL.sub_total', 'AP_INVOICE_DETAIL.price_inv', 'IPM.prd_id', 'IPM.retail_price', 'IPM.prd_desc', 'IPS.size_desc', 'AP_INVOICE_DETAIL.uom_id', 'IPU.uom_desc', 'AP_INVOICE_DETAIL.uom_conversion')
                                ->where('AP_INVOICE_DETAIL.inv_id', $piId)
                                ->get();

      $uoms = imPrdUnitModel::select('uom_id', 'uom_desc')->get();

      $pi_detail_id = piPurchaseInvoiceDetailModel::select('inv_detail_id')->where('inv_id', $piId)->first();

      $pi_detail_sub_sum = piPurchaseInvoiceDetailModel::select(DB::raw('SUM(sub_total) as sub_total'), DB::raw('SUM(qty_inv) as qty_inv'))->where('inv_id', $piId)->first();
      $pi_detail_retail_price_sum = piPurchaseInvoiceDetailModel::join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'AP_INVOICE_DETAIL.prd_id')
                                                                ->select(DB::raw('SUM("IPM"."retail_price") as "retail_price"'))
                                                                ->where('inv_id', $piId)
                                                                ->first();

      $ppn = piPurchaseInvoiceModel::select('ppn')->where('inv_id', $piId)->first();
      $price = $pi_detail_sub_sum->sub_total;
      $total = $price;
      $total_ppn = 0;
      if($ppn->ppn > 0){
        $percentPPN = $ppn->ppn / 100;
        $total_ppn = $percentPPN * $price;
        $total = $price + $total_ppn;
      }

      $pi_id = piPurchaseInvoiceModel::select('inv_id', 'inv_no', 'inv_status', 'created_date')->where('inv_id', $piId)->first();
      $listModules = $this->listModules($userGroupId);
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('purchasinginvoice.pi_detail_purchase_invoice.'.$view, [
          'userGroupId' => $userGroupId,
          'listModules' => $listModules,
          'title' => $userModule,
          'privilaged' => $priv,
          'detailPi' => $detailPi,
          'uoms' => $uoms,
          'pi_id' => $pi_id,
          'pi_detail_id' => $pi_detail_id,
          'pi_detail_sub_sum' => $pi_detail_sub_sum,
          'total' => $total,
          'total_bef_ppn' => $price,
          'total_ppn' => $total_ppn,
          'pi_detail_retail_price_sum' => $pi_detail_retail_price_sum
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function listDetailPiData(Request $request, $view, $piId){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $detailPi = piPurchaseInvoiceDetailModel::join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'AP_INVOICE_DETAIL.prd_id')
                                ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IPM.size_id')
                                ->join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'AP_INVOICE_DETAIL.uom_id')
                                ->select('AP_INVOICE_DETAIL.inv_detail_id', 'AP_INVOICE_DETAIL.qty_inv', 'AP_INVOICE_DETAIL.sub_total', 'AP_INVOICE_DETAIL.price_inv', 'IPM.prd_id', 'IPM.retail_price', 'IPM.prd_desc', 'IPS.size_desc', 'AP_INVOICE_DETAIL.uom_id', 'IPU.uom_desc', 'AP_INVOICE_DETAIL.uom_conversion')
                                ->where('AP_INVOICE_DETAIL.inv_id', $piId);

      $uoms = imPrdUnitModel::select('uom_id', 'uom_desc')->get();
      $pi_id = piPurchaseInvoiceModel::select('inv_id', 'inv_no', 'inv_status', 'created_date')->where('inv_id', $piId)->first();

      $listModules = $this->listModules($userGroupId);
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($pi_id->inv_status != "approve"){
            return Datatables::of($detailPi)
            ->addColumn('delete_bulk', function($data){
                return "
                  <input type='checkbox' name='inv_detail_id[]' value='".$data->inv_detail_id."' />
                ";
            })
            ->editColumn('qty_inv', function ($data) {
                   return number_format($data->qty_inv);
            })
            ->addColumn('action', function ($data) use ($priv, $uoms, $pi_id) {
                return view('purchasinginvoice.pi_detail_purchase_invoice.piDetailPurchaseInvoiceDataTable', ['privilaged' => $priv, 'data' => $data, 'uoms' => $uoms, 'pi_id' => $pi_id]);
            })
            ->make(true);
      }else{
            return Datatables::of($detailPi)
            ->addColumn('delete_bulk', function($data){
                return "
                  <input type='checkbox' name='inv_detail_id[]' value='".$data->inv_detail_id."' />
                ";
            })
            ->editColumn('qty_inv', function ($data) {
                   return number_format($data->qty_inv);
            })
            ->make(true);
      }
    }

    public function editDetailPi(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $inv_detail_id = $request->inv_detail_id;

      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
          $retail_price = preg_replace("/[^A-Za-z0-9 ]/", '', $request->retail_price);
          $retail_price = str_replace('Rp', '', $retail_price);
          $subtotal = $retail_price * $request->qty_inv;

          $detailPiData = new piPurchaseInvoiceDetailModel();
          $detailPiData::where('inv_detail_id', $inv_detail_id)
                     ->update([
                            'uom_id'=>$request->uom_id,
                            'uom_conversion'=>$request->uom_conversion,
                            'qty_inv'=>$request->qty_inv,
                            'price_inv'=>$retail_price,
                            'sub_total'=>$subtotal
                      ]);
       } catch(\Illuminate\Database\QueryException $ex){
         if($ex->getCode() == 23000){
           return Redirect::back()->with('status', 'error : duplicate entry');
         }else{
           return Redirect::back()->with('status', 'Error updating');
         }
       }

       $sum_qty = piPurchaseInvoiceDetailModel::where('inv_id', $request->inv_id)->sum('qty_inv');
       $sum_subtotal = piPurchaseInvoiceDetailModel::select(DB::raw("SUM(sub_total::money::numeric::float8) AS sub_total"))->where('inv_id', $request->inv_id)->first();
       $this->updateTotalPi($request->inv_id, $sum_subtotal->sub_total, $sum_qty);

       return Redirect::back()->with('status', 'Detail pi updated');
      }

    }

    public function deleteDetailPi(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $inv_detail_id = $request->inv_detail_id;
      $inv_id = $request->inv_id;

      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $piGrData = piGrModel::where('inv_id', $inv_id)->delete();
        $detailPiData = piPurchaseInvoiceDetailModel::where('inv_detail_id', $inv_detail_id)->delete();

        $sum_qty = piPurchaseInvoiceDetailModel::where('inv_id', $inv_id)->sum('qty_inv');
        $sum_subtotal = piPurchaseInvoiceDetailModel::select(DB::raw("SUM(sub_total::money::numeric::float8) AS sub_total"))->where('inv_id', $request->inv_id)->first();
        $this->updateTotalPi($request->inv_id, $sum_subtotal->sub_total, $sum_qty);
        return Redirect::back()->with('status', 'Detail pi deleted');
      }

    }

    public function deleteBulkDetailPi(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $inv_detail_id = $request->inv_detail_id;
      $inv_id = $request->inv_id;
      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $piGrData = piGrModel::select('inv_gr_id')->where('inv_id', $inv_id)->get();

        foreach($piGrData as $data){
          $inv_gr_id = $data->inv_gr_id;
          $piGrDataDel = piGrModel::where('inv_gr_id', $inv_gr_id)->delete();
        }

        $detailPiData = piPurchaseInvoiceDetailModel::whereIn('inv_detail_id', $inv_detail_id)->delete();
        $sum_qty = piPurchaseInvoiceDetailModel::where('inv_id', $inv_id)->sum('qty_inv');
        $sum_subtotal = piPurchaseInvoiceDetailModel::select(DB::raw("SUM(sub_total::money::numeric::float8) AS sub_total"))->where('inv_id', $request->inv_id)->first();
        $this->updateTotalPi($inv_id, $sum_subtotal->sub_total, $sum_qty);
        $status = "success";
      }
      return $status;
    }
}
