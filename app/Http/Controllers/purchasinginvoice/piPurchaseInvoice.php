<?php

namespace App\Http\Controllers\purchasinginvoice;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\purchasinginvoice\pi_purchase_invoice\piPurchaseInvoiceModel;
use App\purchasinginvoice\pi_detail_purchase_invoice\piPurchaseInvoiceDetailModel;
use App\purchaseorder\po_supp\supplierModel;
use App\usermanagement\um_module\umModuleModel;
use App\warehouse\im_wh_loc\imWhLocModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Datatables;
use Carbon\Carbon;

class piPurchaseInvoice extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function exportToExcel($view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv->canExport)){
        $pi = piPurchaseInvoiceModel::select('inv_no', 'inv_date', 'notes', 'inv_status', 'total_gross', 'total_ppn')->where('inv_type', 1)->get();
        $data = null;
         foreach($pi as $datas){
           $date = date("d M Y", strtotime($datas->inv_date));
             $data[] = array(
               $datas->inv_no,
               $date,
               $datas->notes,
               $datas->inv_status,
               $datas->total_gross,
               $datas->total_ppn,
             );
         }
         if($data != null){
             $column = array('INV NO','DATE', 'NOTES', 'STATUS', 'TOTAL GROSS', 'TOTAL PPN');
             $this->excelDownload($data, "Data pi", $column);
         }else{
           return Redirect::back()->withErrors(['No data to export']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function listPiView($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $listModules = $this->listModules($userGroupId);
        $priv = $this->checkPrivilaged($moduleId, $userGroupId);
        $supp = supplierModel::select(DB::raw('CAST(supp_id AS varchar) AS data'), 'supp_desc AS value')->where('supp_active_flag',1)->get();
        $wh = imWhLocModel::select(DB::raw('CAST(wh_id AS varchar) AS data'), 'wh_desc AS value')->get();
        if($priv != null && $priv->canView == 1){
          return view('purchasinginvoice.pi_purchase_invoice.'.$view, [
              'userGroupId' => $userGroupId,
              'listModules' => $listModules,
              'title' => $userModule,
              'supp' => $supp,
              'wh' => $wh,
              'privilaged' => $priv
          ]);
        }else{
           return Redirect::back()->withErrors(['Not permitted']);
        }
    }

    public function listPiData(Request $request, $view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $listModules = $this->listModules($userGroupId);
        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        $pi = piPurchaseInvoiceModel::join('PO_SUPP_MASTER AS PSM', 'PSM.supp_id', '=', 'AP_INVOICE_HEADER.supp_id')
                                      ->join('IM_WH_LOC AS IWL', 'IWL.wh_id', '=', 'AP_INVOICE_HEADER.wh_id')
                                      ->select(['AP_INVOICE_HEADER.inv_id', 'AP_INVOICE_HEADER.inv_no', 'AP_INVOICE_HEADER.inv_date', 'AP_INVOICE_HEADER.inv_status',
                                      'AP_INVOICE_HEADER.notes', 'AP_INVOICE_HEADER.no_inv_supp', 'AP_INVOICE_HEADER.no_inv_pajak',
                                      'AP_INVOICE_HEADER.reference', 'AP_INVOICE_HEADER.supp_id', 'AP_INVOICE_HEADER.ppn',
                                      'AP_INVOICE_HEADER.total_nett', 'PSM.supp_desc', 'IWL.wh_id', 'IWL.wh_desc'])
                                      ->where('inv_type', 1);
        if ($request->has('start_date') && $request->has('end_date')) {
            $start = strtotime($request->get('start_date'));
            $end = strtotime($request->get('end_date'));
            $pi->whereBetween('inv_date', [date('Y-m-d', $start), date('Y-m-d', $end)]);
        }

        return Datatables::of($pi)
        ->addColumn('delete_bulk', function($data){
            return "
              <input type='checkbox' name='inv_id[]' value='".$data->inv_id."' />
            ";
        })
        ->editColumn('inv_date', function ($data) {
               return $data->inv_date ? with(new Carbon($data->inv_date))->format('d M y') : '';
        })
        ->addColumn('action', function ($data) use ($priv) {
            return view('purchasinginvoice.pi_purchase_invoice.piPurchaseInvoiceDataTable', ['privilaged' => $priv, 'data' => $data]);
       })
       ->make(true);
    }

    private function piCodeGenerate($piDate, $maxPi){
      $code = "PI";
      $piDate = strtotime($piDate);
      $yy = date("y", $piDate);
      $mm = date("m", $piDate);

      if($maxPi != ""){
        $maxPi = substr($maxPi, -6);
        $pi_no = $maxPi + 1;
        $pi_no = str_pad($pi_no, 6, '0',STR_PAD_LEFT);
      }else{
        $pi_no = "000001";
      }

      $pi_no_final = $code.$yy.$mm.$pi_no;
      return $pi_no_final;
    }

    public function addPi(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);
      $maxpi = piPurchaseInvoiceModel::select(DB::raw('MAX(RIGHT(inv_no, 6)) as inv_no'))->where('inv_type', 1)->first();
      $piDate = strtotime($request->inv_date);
      $pi_no = $this->piCodeGenerate($request->inv_date, $maxpi->inv_no);
      $supp_term = supplierModel::select('supp_term')->where('supp_id', $request->supp_id)->first();
      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
            $piData = new piPurchaseInvoiceModel();
            $piData->inv_no = $pi_no;
            $piData->notes = $request->notes;
            $piData->inv_status = "open";
            $piData->inv_type = "1";
            $piData->supp_id = $request->supp_id;
            $piData->wh_id = $request->wh_id;
            $piData->term = ($supp_term != null ? $supp_term->supp_term:0);
            $piData->no_inv_supp = $request->no_inv_supp;
            $piData->no_inv_pajak = $request->no_inv_pajak;
            $piData->reference = $request->reference;
            $piData->ppn = $request->input('ppn', 0);
            $piData->inv_date = date("Y-m-d", $piDate);
            $piData->created_date = \Carbon\Carbon::now();
            $piData->updated_date = \Carbon\Carbon::now();
            $piData->created_by = $userId;
            $insert =  $piData->save();
        } catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error inserting');
        }
        return Redirect::back()->with('status', 'Pi added');
      }

    }

    public function editPi(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);
      $piId = $request->inv_id;
      $piDate = strtotime($request->inv_date);
      $supp_term = supplierModel::select('supp_term')->where('supp_id', $request->supp_id)->first();
      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
          $piData = new piPurchaseInvoiceModel();
          $piData::where('inv_id', $piId)
                     ->update([
                            'inv_status'=>$request->input('inv_status', 'open'),
                            'inv_date'=>date("Y-m-d", $piDate),
                            'supp_id'=> $request->supp_id,
                            'wh_id'=> $request->wh_id,
                            'term'=> ($supp_term != null ? $supp_term->supp_term:0),//$request->term,
                            'inv_type'=>1,//$request->inv_type,
                            'no_inv_supp'=>$request->no_inv_supp,
                            'no_inv_pajak'=>$request->no_inv_pajak,
                            'reference'=>$request->reference,
                            'ppn'=>$request->input('ppn', 0),
                            'notes'=>$request->notes,
                            'updated_date' => \Carbon\Carbon::now(),
                            'updated_by' => $userId
                      ]);
          } catch(\Illuminate\Database\QueryException $ex){
              return Redirect::back()->with('status', 'Error updating');
          }
        return Redirect::back()->with('status', 'Pi updated');
      }

    }

    public function deletePi(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $piId = $request->inv_id;

      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $piData = new piPurchaseInvoiceModel();
        $piData::where('inv_id', $piId)->delete();

        $piDetailData = new piPurchaseInvoiceDetailModel();
        $piDetailData::where('inv_id', $piId)->delete();
        return Redirect::back()->with('status', 'Pi deleted');
      }

    }

    public function deleteBulkPi(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $piId = $request->inv_id;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $piData = new piPurchaseInvoiceModel();
        $piData::whereIn('inv_id', $piId)->delete();

        $piDetailData = new piPurchaseInvoiceDetailModel();
        $piDetailData::whereIn('inv_id', $piId)->delete();

        $status = "success";

      }
      return $status;
    }
}
