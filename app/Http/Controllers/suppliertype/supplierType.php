<?php

namespace App\Http\Controllers\suppliertype;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\usermanagement\um_module\umModuleModel;
use App\purchaseorder\po_supp_type\supplierTypeModel;
use App\glaccount\gl_account\glAccountModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Datatables;

class supplierType extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /* public function ajaxCoaCatalog($view, $keyword){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('purchaseorder.po_supp_type.'.$view, [
            'keyword' => $keyword,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    } */

    public function listSuppTypeView($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);
        return view('purchaseorder.po_supp_type.'.$view, [
            'userGroupId' => $userGroupId,
            'listModules' => $listModules,
            'title' => $userModule,
            'privilaged' => $priv
        ]);
    }

    public function listSuppTypeData($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        $suppType = supplierTypeModel::join('GL_ACCOUNT AS GA', 'GA.coa_id', '=', 'PO_SUPP_TYPE.coa_id_ap')
                                       ->join('GL_ACCOUNT AS GB', 'GB.coa_id', '=', 'PO_SUPP_TYPE.coa_id_unbill')
                                       ->join('GL_ACCOUNT AS GC', 'GC.coa_id', '=', 'PO_SUPP_TYPE.coa_id_ppn')
                                       ->join('GL_ACCOUNT AS GD', 'GD.coa_id', '=', 'PO_SUPP_TYPE.coa_id_ppv')
                                       ->select('PO_SUPP_TYPE.supp_type_id', 'PO_SUPP_TYPE.supp_type_code', 'PO_SUPP_TYPE.supp_type_desc',
                                       'PO_SUPP_TYPE.coa_id_ap AS coa_id_ap', 'GA.coa_desc AS coa_desc_ap', 'GA.coa_code AS coa_code_ap',
                                       'PO_SUPP_TYPE.coa_id_unbill AS coa_id_unbill', 'GB.coa_desc AS coa_desc_unbill','GB.coa_code AS coa_code_unbill',
                                       'PO_SUPP_TYPE.coa_id_ppn AS coa_id_ppn', 'GB.coa_desc AS coa_desc_ppn', 'GC.coa_code AS coa_code_ppn',
                                       'PO_SUPP_TYPE.coa_id_ppv AS coa_id_ppv', 'GC.coa_desc AS coa_desc_ppv', 'GD.coa_code AS coa_code_ppv'
                                      );
        return Datatables::of($suppType)
        ->addColumn('delete_bulk', function($data){
            return "
              <input type='checkbox' name='supp_type_id[]' value='".$data->supp_type_id."' />
            ";
        })
        ->addColumn('action', function ($data) use ($priv) {
            return view('purchaseorder.po_supp_type.poSuppTypeDataTable', ['privilaged' => $priv, 'data' => $data]);
       })
        ->make(true);
    }

    public function add(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
         try {
            $suppTypeData = new supplierTypeModel();
            $suppTypeData->supp_type_code = $request->supp_type_code;
            $suppTypeData->supp_type_desc = $request->supp_type_desc;
            $suppTypeData->coa_id_ap = $request->coa_id_ap;
            $suppTypeData->coa_id_unbill = $request->coa_id_unbill;
            $suppTypeData->coa_id_ppn = $request->coa_id_ppn;
            $suppTypeData->coa_id_ppv = $request->coa_id_ppv;
            $suppTypeData->created_date = \Carbon\Carbon::now();
            $suppTypeData->updated_date = \Carbon\Carbon::now();
            $suppTypeData->created_by = $userId;
            $insert =  $suppTypeData->save();

            return Redirect::back()->with('status', 'Supplier type added');
          } catch(\Illuminate\Database\QueryException $ex){
            if($ex->getCode() == 23000){
              return Redirect::back()->with('status', 'error : duplicate entry');
            }else{
              return Redirect::back()->with('status', 'Error inserting');
            }
          }
      }

    }

    public function edit(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idSuppType = $request->supp_type_id;

      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
          $suppData = new supplierTypeModel();
          $suppData::where('supp_type_id', $idSuppType)
                     ->update([
                            'supp_type_code'=>$request->supp_type_code,
                            'supp_type_desc'=>$request->supp_type_desc,
                            'coa_id_ap'=>$request->coa_id_ap,
                            'coa_id_unbill'=>$request->coa_id_unbill,
                            'coa_id_ppn'=>$request->coa_id_ppn,
                            'coa_id_ppv'=>$request->coa_id_ppv,
                            'updated_date' => \Carbon\Carbon::now(),
                            'updated_by' => $userId
                      ]);
          return Redirect::back()->with('status', 'Supplier type updated');
        } catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error updating');
        }
      }

    }

    public function delete(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idSuppType = $request->supp_type_id;

      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $suppTypeData = new supplierTypeModel();

        $suppTypeData::where('supp_type_id', $idSuppType)->delete();

        return Redirect::back()->with('status', 'Supplier type deleted');
      }

    }

    public function deleteBulk(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idSuppType = $request->supp_type_id;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $suppTypeData = new supplierTypeModel();
        $suppTypeData::whereIn('supp_type_id', $idSuppType)->delete();
        $status = "success";

      }
      return $status;
    }

    public function exportToExcel($view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv->canExport)){
        $datamodel = supplierTypeModel::join('GL_ACCOUNT AS GA', 'GA.coa_id', '=', 'PO_SUPP_TYPE.coa_id_ap')
                                       ->join('GL_ACCOUNT AS GB', 'GB.coa_id', '=', 'PO_SUPP_TYPE.coa_id_unbill')
                                       ->join('GL_ACCOUNT AS GC', 'GC.coa_id', '=', 'PO_SUPP_TYPE.coa_id_ppn')
                                       ->join('GL_ACCOUNT AS GD', 'GD.coa_id', '=', 'PO_SUPP_TYPE.coa_id_ppv')
                                        ->select('supp_type_code', 'supp_type_desc')
                                        ->get();
         $data = null;
         foreach($datamodel as $datas){
             $data[] = array(
               $datas->supp_type_code,
               $datas->supp_type_desc,
             );
         }
         if($data != null){
           $column = array('SUPPLIER TYPE CODE', 'SUPPLIER TYPE DESC');
           $this->excelDownload($data, "Data Supplier type", $column);
         }else{
           return Redirect::back()->withErrors(['No data to export']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }
}
