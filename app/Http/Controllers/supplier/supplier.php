<?php

namespace App\Http\Controllers\supplier;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\usermanagement\um_module\umModuleModel;
use App\purchaseorder\po_supp\supplierModel;
use App\purchaseorder\po_supp_type\supplierTypeModel;
use App\term\termModel;
use Illuminate\Support\Facades\Auth;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Illuminate\Support\Facades\DB;
use Datatables;

class supplier extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listView($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);
        $supps = supplierTypeModel::join('GL_ACCOUNT AS GAAP', 'GAAP.coa_id', '=', 'PO_SUPP_TYPE.coa_id_ap')
                                    ->join('GL_ACCOUNT AS GAUB', 'GAUB.coa_id', '=', 'PO_SUPP_TYPE.coa_id_unbill')
                                    ->join('GL_ACCOUNT AS GAPPN', 'GAPPN.coa_id', '=', 'PO_SUPP_TYPE.coa_id_ppn')
                                    ->join('GL_ACCOUNT AS GAPPV', 'GAPPV.coa_id', '=', 'PO_SUPP_TYPE.coa_id_ppv')
                                    ->select(DB::raw('CAST("PO_SUPP_TYPE".supp_type_id AS varchar) AS data'), 'PO_SUPP_TYPE.supp_type_desc AS value')
                                    ->get();

        $term = termModel::select('term_id AS data', 'pptc_term_desc AS value')->get();

        return view('purchaseorder.po_supp.'.$view, [
            'userGroupId' => $userGroupId,
            'listModules' => $listModules,
            'title' => $userModule,
            'privilaged' => $priv,
            'supps' => $supps,
            'term' => $term
        ]);
    }

    public function listData($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        $supps = supplierModel::join('PO_SUPP_TYPE AS PST', 'PST.supp_type_id', '=', 'PO_SUPP_MASTER.supp_type_id')
                                ->join('GS_TERM AS GT', 'GT.term_id', '=', 'PO_SUPP_MASTER.supp_term')
                                ->select('PO_SUPP_MASTER.*', 'PST.supp_type_desc', 'GT.pptc_term_desc');

        $suppType = supplierTypeModel::join('GL_ACCOUNT AS GAAP', 'GAAP.coa_id', '=', 'PO_SUPP_TYPE.coa_id_ap')
                                    ->join('GL_ACCOUNT AS GAUB', 'GAUB.coa_id', '=', 'PO_SUPP_TYPE.coa_id_unbill')
                                    ->join('GL_ACCOUNT AS GAPPN', 'GAPPN.coa_id', '=', 'PO_SUPP_TYPE.coa_id_ppn')
                                    ->join('GL_ACCOUNT AS GAPPV', 'GAPPV.coa_id', '=', 'PO_SUPP_TYPE.coa_id_ppv')
                                    ->select(DB::raw('CAST("PO_SUPP_TYPE".supp_type_id AS varchar) AS data'), 'PO_SUPP_TYPE.supp_type_desc AS value')
                                    ->get();

        $term = termModel::select('term_id AS data', 'pptc_term_desc AS value')->get();


        return Datatables::of($supps)
        ->addColumn('delete_bulk', function($data){
            return "
              <input type='checkbox' name='supp_id[]' value='".$data->supp_id."' />
            ";
        })
        ->editColumn('supp_active_flag', function($data){
          if($data->supp_active_flag == 1){
            return "
            <span class='label label-md label-success'>Active</span>
            ";
          }else{
            return "
              <span class='label label-md label-danger'>Non active</span>
            ";
          }

        })
        ->addColumn('action', function ($data) use ($priv, $suppType, $term) {
            return view('purchaseorder.po_supp.poSuppDataTable', ['privilaged' => $priv, 'data' => $data, 'suppstype' => $suppType, 'term' => $term]);
       })
        ->make(true);
    }

    public function add(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
         try {
            $suppData = new supplierModel();
            $suppData->supp_code = $request->supp_code;
            $suppData->supp_desc = $request->supp_desc;
            $suppData->supp_type_id = $request->supp_type_id;
            $suppData->supp_address1 = $request->supp_address1;
            $suppData->supp_address2 = $request->supp_address2;
            $suppData->supp_address3 = $request->supp_address3;
            $suppData->supp_city = $request->supp_city;
            $suppData->supp_country = $request->supp_country;
            $suppData->supp_phone = $request->supp_phone;
            $suppData->supp_fax = $request->supp_fax;
            $suppData->supp_email = $request->supp_email;
            $suppData->supp_post_code = $request->supp_post_code;
            $suppData->supp_contact_person = $request->supp_contact_person;
            $suppData->supp_term = $request->supp_term;
            $suppData->supp_pkp_flag = $request->input('supp_pkp_flag', 0);
            $suppData->supp_active_flag = $request->input('supp_active_flag', 0);
            $suppData->created_date = \Carbon\Carbon::now();
            $suppData->updated_date = \Carbon\Carbon::now();
            $suppData->created_by = $userId;
            $insert =  $suppData->save();

            return Redirect::back()->with('status', 'Supplier added');
          } catch(\Illuminate\Database\QueryException $ex){
              return Redirect::back()->with('status', 'Error inserting');
          }
      }

    }

    public function edit(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idSupp = $request->supp_id;

      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
          $suppData = new supplierModel();
          $suppData::where('supp_id', $idSupp)
                     ->update([
                            'supp_code'=>$request->supp_code,
                            'supp_desc'=>$request->supp_desc,
                            'supp_type_id'=>$request->supp_type_id,
                            'supp_address1'=>$request->supp_address1,
                            'supp_address2'=>$request->supp_address2,
                            'supp_address3'=>$request->supp_address3,
                            'supp_city'=>$request->supp_city,
                            'supp_country'=>$request->supp_country,
                            'supp_phone'=>$request->supp_phone,
                            'supp_fax'=>$request->supp_fax,
                            'supp_email'=>$request->supp_email,
                            'supp_post_code'=>$request->supp_post_code,
                            'supp_contact_person'=>$request->supp_contact_person,
                            'supp_term'=>$request->supp_term,
                            'supp_pkp_flag'=>$request->input('supp_pkp_flag', 0),
                            'supp_active_flag'=>$request->input('supp_active_flag', 0),
                            'updated_date' => \Carbon\Carbon::now(),
                            'updated_by' => $userId
                      ]);
          return Redirect::back()->with('status', 'Supplier updated');
        } catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error updating');
        }
      }

    }

    public function delete(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idSupp = $request->supp_id;

      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $suppData = new supplierModel();

        $suppData::where('supp_id', $idSupp)->delete();

        return Redirect::back()->with('status', 'Supplier deleted');
      }

    }

    public function deleteBulk(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idSupp = $request->supp_id;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $suppData = new supplierModel();
        $suppData::whereIn('supp_id', $idSupp)->delete();
        $status = "success";

      }
      return $status;
    }

    public function exportToExcel($view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv->canExport)){
        $datamodel = supplierModel::select('supp_code', 'supp_desc')
                                        ->get();
         $data = null;
         foreach($datamodel as $datas){
             $data[] = array(
               $datas->supp_code,
               $datas->supp_desc,
             );
         }
         if($data != null){
           $column = array('SUPPLIER CODE', 'SUPPLIER DESC');
           $this->excelDownload($data, "Data Supplier", $column);
         }else{
           return Redirect::back()->withErrors(['No data to export']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }
}
