<?php

namespace App\Http\Controllers\purchaserequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\purchaserequest\pr_purchase_request\prPurchaseRequestModel;
use App\purchaserequest\pr_detail_purchase_request\prPurchaseRequestDetailModel;
use App\usermanagement\um_module\umModuleModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Datatables;
use Carbon\Carbon;

class prPurchaseRequest extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function exportToExcel($view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv->canExport)){
        $pr = prPurchaseRequestModel::select('pr_no', 'pr_date', 'notes', 'pr_status')->get();

         $data = null;
         foreach($pr as $datas){
           $date = date("d M Y", strtotime($datas->pr_date));
             $data[] = array(
               $datas->pr_no,
               $date,
               $datas->notes,
               $datas->pr_status,
             );
         }
         if($data != null){
            $column = array('PR NO','DATE', 'NOTES', 'STATUS');
            $this->excelDownload($data, "Data pr", $column);
         }else{
           return Redirect::back()->withErrors(['No data to export']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function listPrView($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $listModules = $this->listModules($userGroupId);
        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        if($priv != null && $priv->canView == 1){
          return view('purchaserequest.pr_purchase_request.'.$view, [
              'userGroupId' => $userGroupId,
              'listModules' => $listModules,
              'title' => $userModule,
              'privilaged' => $priv
          ]);
        }else{
           return Redirect::back()->withErrors(['Not permitted']);
        }
    }

    public function listPrData(Request $request, $view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $listModules = $this->listModules($userGroupId);
        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        $pr = prPurchaseRequestModel::select(['pr_id', 'pr_no', 'pr_date', 'pr_status', 'notes']);

        if ($request->has('start_date') && $request->has('end_date')) {
            $start = strtotime($request->get('start_date'));
            $end = strtotime($request->get('end_date'));
            $pr->whereBetween('pr_date', [date('Y-m-d', $start), date('Y-m-d', $end)]);
        }

        return Datatables::of($pr)
        ->addColumn('delete_bulk', function($data){
            return "
              <input type='checkbox' name='pr_id[]' value='".$data->pr_id."' />
            ";
        })
        ->editColumn('pr_date', function ($data) {
               return $data->pr_date ? with(new Carbon($data->pr_date))->format('d M y') : '';
        })
        ->addColumn('action', function ($data) use ($priv) {
            return view('purchaserequest.pr_purchase_request.prPurchaseRequestDataTable', ['privilaged' => $priv, 'data' => $data]);
       })
       ->make(true);
    }

    private function prCodeGenerate($prDate, $maxPr){
      $code = "PR";
      $prDate = strtotime($prDate);
      $yy = date("y", $prDate);
      $mm = date("m", $prDate);

      if($maxPr != ""){
        $maxPr = substr($maxPr, -6);
        $pr_no = $maxPr + 1;
        $pr_no = str_pad($pr_no, 6, '0',STR_PAD_LEFT);
      }else{
        $pr_no = "000001";
      }

      $pr_no_final = $code.$yy.$mm.$pr_no;
      return $pr_no_final;
    }

    public function addPr(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);
      $prDate = strtotime($request->pr_date);
      $maxpr = prPurchaseRequestModel::select(DB::raw('MAX(RIGHT(pr_no, 6)) as pr_no'))->first();
      $pr_no = $this->prCodeGenerate($request->pr_date, $maxpr->pr_no);
      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
            $prData = new prPurchaseRequestModel();
            $prData->pr_no = $pr_no;
            $prData->notes = $request->notes;
            $prData->pr_status = $request->pr_status;
            $prData->pr_date = date("Y-m-d", $prDate);
            $prData->created_date = \Carbon\Carbon::now();
            $prData->updated_date = \Carbon\Carbon::now();
            $prData->created_by = $userId;
            $insert =  $prData->save();
        } catch(\Illuminate\Database\QueryException $ex){
          if($ex->getCode() == 23000){
            return Redirect::back()->with('status', 'error : duplicate entry');
          }else{
            return Redirect::back()->with('status', 'Error inserting');
          }
        }
        return Redirect::back()->with('status', 'Pr added');
      }

    }

    public function editPr(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $prId = $request->pr_id;
      $prDate = strtotime($request->pr_date);
      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
          $prData = new prPurchaseRequestModel();
          $prData::where('pr_id', $prId)
                     ->update([
                            'pr_no'=>$request->pr_no,
                            'notes'=>$request->notes,
                            'pr_status'=>$request->pr_status,
                            'pr_date'=>date("Y-m-d", $prDate),
                            'updated_date' => \Carbon\Carbon::now(),
                            'updated_by' => $userId
                      ]);
          } catch(\Illuminate\Database\QueryException $ex){
            if($ex->getCode() == 23000){
              return Redirect::back()->with('status', 'error : duplicate entry');
            }else{
              return Redirect::back()->with('status', 'Error updating');
            }
          }
        return Redirect::back()->with('status', 'Pr updated');
      }

    }

    public function deletePr(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $prId = $request->pr_id;

      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $prData = new prPurchaseRequestModel();
        $prData::where('pr_id', $prId)->delete();

        $prDetailData = new prPurchaseRequestDetailModel();
        $prDetailData::where('pr_id', $prId)->delete();
        return Redirect::back()->with('status', 'Pr deleted');
      }

    }

    public function deleteBulkPr(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $prId = $request->pr_id;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $prData = new prPurchaseRequestModel();
        $prData::whereIn('pr_id', $prId)->delete();

        $prDetailData = new prPurchaseRequestDetailModel();
        $prDetailData::whereIn('pr_id', $prId)->delete();

        $status = "success";

      }
      return $status;
    }
}
