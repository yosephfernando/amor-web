<?php

namespace App\Http\Controllers\purchaserequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\inventory\im_prd_master\imPrdMasterModel;
use App\inventory\im_prd_unit\imPrdUnitModel;
use App\purchaseorder\po_pr\poPrModel;
use App\purchaserequest\pr_purchase_request\prPurchaseRequestModel;
use App\purchaserequest\pr_detail_purchase_request\prPurchaseRequestDetailModel;
use App\usermanagement\um_module\umModuleModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Illuminate\Support\Facades\DB;
use Datatables;

class prDetailPurchaseRequest extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /* public function exportToExcel($view, $prId){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv->canExport)){
        $detailPr = prPurchaseRequestDetailModel::join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'PR_DETAIL.uom_id')
                                  ->join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'PR_DETAIL.prd_id')
                                  ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IPM.size_id')
                                  ->select('PR_DETAIL.pr_detail_id', 'IPU.uom_desc', 'PR_DETAIL.uom_conversion', 'IPM.prd_desc', 'PR_DETAIL.qty_pr', 'IPS.size_code')
                                  ->where('PR_DETAIL.pr_id', $prId)
                                  ->get();
         $data = null;
         foreach($detailPr as $datas){
             $data[] = array(
               $datas->pr_detail_id,
               $datas->prd_desc,
               $datas->qty_pr,
               $datas->uom_desc,
               $datas->uom_conversion,
               $datas->size_code,
             );
         }
         if($data != null){
           $column = array('ID DETAIL PR','PRODUCT', 'QTY', 'UOM', 'UOM CONVERSION','SIZE');
           $this->excelDownload($data, "Data pr", $column);
         }else{
           return Redirect::back()->withErrors(['No data to export']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    } */

    public function seacrhDetailPoAjax($view, $prId, $keyword)
    {
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      $detailPr = prPurchaseRequestDetailModel::join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'PR_DETAIL.uom_id')
                                ->join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'PR_DETAIL.prd_id')
                                ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IPM.size_id')
                                ->select('PR_DETAIL.pr_detail_id', 'IPU.uom_id', 'IPU.uom_desc', 'PR_DETAIL.uom_conversion', 'IPM.prd_desc', 'PR_DETAIL.qty_pr', 'IPS.size_desc')
                                ->where('PR_DETAIL.pr_id', $prId)
                                ->where('IPM.prd_desc', 'LIKE', '%'.$keyword.'%')
                                ->get();

      $detailPrId = prPurchaseRequestDetailModel::select('pr_detail_id')->where('pr_id', $prId)->first();
      $prId = prPurchaseRequestModel::select('pr_id')->where('pr_id', $prId)->first();

      $uoms = imPrdUnitModel::select('uom_id', 'uom_desc')->get();

      if($priv != null && $priv->canView == 1){
        return view('purchaserequest.pr_detail_purchase_request.'.$view, [
          'title' => $userModule,
          'detailPr' => $detailPr,
          'prId' => $prId,
          'detailPrId' => $detailPrId,
          'uoms' => $uoms,
          'privilaged' => $priv,
          'keyword' => $keyword
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function listDetailPrView(Request $request, $view, $prId){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $sumPr = prPurchaseRequestDetailModel::join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'PR_DETAIL.prd_id')
                                             ->where('PR_DETAIL.pr_id', $prId)
                                             ->sum('PR_DETAIL.qty_pr');

     $detailPr = prPurchaseRequestDetailModel::join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'PR_DETAIL.uom_id')
                               ->join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'PR_DETAIL.prd_id')
                               ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IPM.size_id')
                               ->select('PR_DETAIL.pr_detail_id', 'IPU.uom_id', 'IPU.uom_desc', 'PR_DETAIL.uom_conversion', 'IPM.prd_desc', 'IPM.prd_code',  'PR_DETAIL.qty_pr', 'IPS.size_desc')
                               ->where('PR_DETAIL.pr_id', $prId)
                               ->get();

      $detailPrId = prPurchaseRequestDetailModel::select('pr_detail_id')->where('pr_id', $prId)->first();
      $prId = prPurchaseRequestModel::join('GS_USERS AS GU', 'GU.id', '=', 'PR_HEADER.created_by')
                                      ->select('PR_HEADER.pr_id', 'PR_HEADER.created_date', 'PR_HEADER.pr_no', 'PR_HEADER.notes', 'GU.name', 'PR_HEADER.pr_status')
                                      ->where('pr_id', $prId)
                                      ->first();
      $uoms = imPrdUnitModel::select('uom_id', 'uom_desc')->get();

      $listModules = $this->listModules($userGroupId);
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('purchaserequest.pr_detail_purchase_request.'.$view, [
            'userGroupId' => $userGroupId,
            'listModules' => $listModules,
            'title' => $userModule,
            'privilaged' => $priv,
            'sumPr' => $sumPr,
            'prId' => $prId,
            'uoms' => $uoms,
            'detailPr' => $detailPr,
            'detailPrId' => $detailPrId
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function listDetailPrData(Request $request, $view, $prId){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $detailPr = prPurchaseRequestDetailModel::join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'PR_DETAIL.uom_id')
                                ->join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'PR_DETAIL.prd_id')
                                ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IPM.size_id')
                                ->select('PR_DETAIL.pr_detail_id', 'IPU.uom_id', 'IPU.uom_desc', 'PR_DETAIL.uom_conversion', 'IPM.prd_desc', 'IPM.prd_code',  'PR_DETAIL.qty_pr', 'IPS.size_desc')
                                ->where('PR_DETAIL.pr_id', $prId);

      $pr_status = prPurchaseRequestModel::select('pr_status')->where('pr_id', $prId)->first();

      $uoms = imPrdUnitModel::select('uom_id', 'uom_desc')->get();

      $listModules = $this->listModules($userGroupId);
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return Datatables::of($detailPr)
        ->addColumn('delete_bulk', function($data){
            return "
              <input type='checkbox' name='pr_detail_id[]' value='".$data->pr_detail_id."' />
            ";
        })
        ->editColumn('qty_pr', function ($data) {
               return number_format($data->qty_pr);
        })
        ->editColumn('uom_conversion', function ($data) {
               return number_format($data->uom_conversion);
        })
        ->addColumn('action', function ($data) use ($priv, $uoms, $pr_status) {
            return view('purchaserequest.pr_detail_purchase_request.prDetailPurchaseRequestDataTable', ['privilaged' => $priv, 'data' => $data, 'uoms' => $uoms, 'pr_status' => $pr_status]);
       })
       ->filter(function ($detailPr) use ($request) {
                if ($request->has('prd_desc')) {
                    $detailPr->where('IPM.prd_desc', 'LIKE', '%'.$request->prd_desc.'%');
                }
        })
        ->make(true);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function addDetailPr(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
          $detailPrData = new prPurchaseRequestDetailModel();
          $detailPrData->pr_id =  $request->pr_id;
          $detailPrData->prd_id = $request->prd_id;
          $detailPrData->qty_pr = $request->qty_pr;
          $detailPrData->uom_id = $request->uom_id;
          $detailPrData->uom_conversion = $request->uom_conversion;
          $insert =  $detailPrData->save();
        } catch(\Illuminate\Database\QueryException $ex){
            if($ex->getCode() == 23000){
              return Redirect::back()->with('status', 'error : duplicate entry');
            }else{
              return Redirect::back()->with('status', 'Error inserting');
            }
        }

          return Redirect::back()->with('status', 'Detail pr added');
      }

    }

    public function editDetailPr(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $pr_detail_id = $request->pr_detail_id;

      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
          $detailPrData = new prPurchaseRequestDetailModel();
          $detailPrData::where('pr_detail_id', $pr_detail_id)
                     ->update([
                            'uom_id'=>$request->uom_id,
                            'uom_conversion'=>$request->uom_conversion,
                            'qty_pr'=>$request->qty_pr,
                      ]);
       } catch(\Illuminate\Database\QueryException $ex){
         if($ex->getCode() == 23000){
           return Redirect::back()->with('status', 'error : duplicate entry');
         }else{
           return Redirect::back()->with('status', 'Error inserting');
         }
       }
        return Redirect::back()->with('status', 'Detail pr updated');
      }

    }

    public function deleteDetailPr(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $pr_detail_id = $request->pr_detail_id;

      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $detailPrData = prPurchaseRequestDetailModel::where('pr_detail_id', $pr_detail_id)->delete();
        $poPrData = poPrModel::where('pr_detail_id', $pr_detail_id)->delete();
        return Redirect::back()->with('status', 'Detail pr deleted');
      }

    }

    public function deleteBulkDetailPr(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $pr_detail_id = $request->pr_detail_id;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $detailPrData = prPurchaseRequestDetailModel::whereIn('pr_detail_id', $pr_detail_id)->delete();
        $poPrData = poPrModel::whereIn('pr_detail_id', $pr_detail_id)->delete();
        $status = "success";
      }
      return $status;
    }

    /* public function ajaxProdCatalog($view, $keyword){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('purchaserequest.pr_detail_purchase_request.'.$view, [
            'keyword' => $keyword,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    } */

    public function ajaxUomCatalog($view, $prdId){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      $uomFromPrd = imPrdMasterModel::join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'IM_PRD_MASTER.uom_id1')
                                      ->join('IM_PRD_UNIT AS IPU2', 'IPU2.uom_id', '=', 'IM_PRD_MASTER.uom_id2')
                                      ->select('IM_PRD_MASTER.uom_id1', 'IM_PRD_MASTER.uom_id2', 'IM_PRD_MASTER.uom_conversion', 'IPU.uom_desc AS desc1', 'IPU2.uom_desc AS desc2')
                                      ->where('IM_PRD_MASTER.prd_id', $prdId)
                                      ->first();

      if($priv != null && $priv->canView == 1){
        return view('purchaserequest.pr_detail_purchase_request.'.$view, [
            'uomFromPrd' => $uomFromPrd,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }
}
