<?php

namespace App\Http\Controllers\purchaseorder;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\purchaseorder\po_purchase_order\poPurchaseOrderModel;
use App\purchaseorder\po_detail_purchase_order\poPurchaseOrderDetailModel;
use App\purchaseorder\po_pr\poPrModel;
use App\purchaseorder\po_supp\supplierModel;
use App\usermanagement\um_module\umModuleModel;
use App\warehouse\im_wh_loc\imWhLocModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Datatables;
use Carbon\Carbon;

class poPurchaseOrder extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function exportToExcel($view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv->canExport)){
        $po = poPurchaseOrderModel::select('po_no', 'po_date', 'total_qty', 'total_gross', 'total_ppn', 'notes', 'po_status')->get();
        $data = null;
        foreach($po as $datas){
           $date = date("d M Y", strtotime($datas->po_date));
             $data[] = array(
               $datas->po_no,
               $date,
               $datas->total_qty,
               $datas->total_gross,
               $datas->total_ppn,
               $datas->notes,
               $datas->po_status,
             );
         }
         if($data != null){
           $column = array('PO NO','DATE', 'TOTAL QTY', 'TOTAL GROSS', 'TOTAL PPN', 'NOTES', 'STATUS');
           $this->excelDownload($data, "Data po", $column);
         }else{
           return Redirect::back()->withErrors(['No data to export']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function listPo($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $listModules = $this->listModules($userGroupId);
        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        if($priv != null && $priv->canView == 1){
          return view('purchaseorder.po_purchase_order.'.$view, [
              'userGroupId' => $userGroupId,
              'listModules' => $listModules,
              'title' => $userModule,
              'privilaged' => $priv
          ]);
        }else{
           return Redirect::back()->withErrors(['Not permitted']);
        }
    }

    public function listPoData(Request $request, $view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $listModules = $this->listModules($userGroupId);
        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        $po = poPurchaseOrderModel::join('IM_WH_LOC AS WH', 'WH.wh_id', '=', 'PO_HEADER.wh_id')
                                    ->join('PO_SUPP_MASTER AS PSM', 'PSM.supp_id', '=', 'PO_HEADER.supp_id')
                                    ->select('PO_HEADER.*', 'WH.wh_id', 'WH.wh_desc', 'PSM.supp_id', 'PSM.supp_desc');
        if ($request->has('start_date') && $request->has('end_date')) {
            $start = strtotime($request->get('start_date'));
            $end = strtotime($request->get('end_date'));
            $po->whereBetween('po_date', [date('Y-m-d', $start), date('Y-m-d', $end)]);
        }

        return Datatables::of($po)
        ->addColumn('delete_bulk', function($data){
            return "
              <input type='checkbox' name='po_id[]' value='".$data->po_id."' />
            ";
        })
        ->editColumn('po_date', function ($data) {
               return $data->po_date ? with(new Carbon($data->po_date))->format('d M y') : '';
        })
        ->editColumn('total_qty', function ($data) {
               return number_format($data->total_qty);
        })
        ->addColumn('action', function ($data) use ($priv) {
            return view('purchaseorder.po_purchase_order.poPurchaseOrderDataTable', ['privilaged' => $priv, 'data' => $data]);
       })
        ->make(true);
    }

    private function poCodeGenerate($poDate, $maxPo){
      $code = "PO";
      $poDate = strtotime($poDate);
      $yy = date("y", $poDate);
      $mm = date("m", $poDate);

      if($maxPo != ""){
        $maxPo = substr($maxPo, -6);
        $po_no = $maxPo + 1;
        $po_no = str_pad($po_no, 6, '0',STR_PAD_LEFT);
      }else{
        $po_no = "000001";
      }

      $po_no_final = $code.$yy.$mm.$po_no;
      return $po_no_final;
    }

    public function addPo(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);
      $po_date = strtotime($request->po_date);
      $po_final_date = date("Y-m-d", $po_date);

      $req_date = strtotime($request->req_date);
      $req_date = date("Y-m-d", $req_date);

      $maxpo = poPurchaseOrderModel::select(DB::raw('MAX(RIGHT(po_no, 6)) as po_no'))->first();
      $po_no = $this->poCodeGenerate($request->po_date, $maxpo->po_no);
      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
            $poData = new poPurchaseOrderModel();
            $poData->po_no = $po_no;
            $poData->po_date = $po_final_date;
            $poData->req_date = $req_date;
            $poData->wh_id = $request->wh_id;
            $poData->supp_id = $request->supp_id;
            $poData->ppn_type = $request->ppn_type;
            $poData->notes = $request->notes;
            $poData->po_status = $request->po_status;
            $poData->pr_flag = $request->input('pr_flag', 0);
            $poData->total_qty = 0;
            $poData->total_gross_product = 0;
            $poData->total_gross = 0;
            $poData->disc = 0;
            $poData->total_disc = 0;
            $poData->total_dpp = 0;

            if($request->ppn == 1){
                $poData->ppn = 10;
            }else{
                $poData->ppn = 0;
            }

            $poData->total_ppn = 0;
            $poData->total_nett = 0;
            $poData->created_date = \Carbon\Carbon::now();
            $poData->updated_date = \Carbon\Carbon::now();
            $poData->created_by = $userId;
            $insert =  $poData->save();
        } catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error inserting');
        }
        return Redirect::back()->with('status', 'Po added');
      }

    }

    public function editPo(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $po_id = $request->po_id;
      $po_date = strtotime($request->po_date);
      $po_date = date("Y-m-d", $po_date);

      $req_date = strtotime($request->req_date);
      $req_date = date("Y-m-d", $req_date);
      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        if($priv->canApprove != 1){
          if($request->po_status != "approve"){
                      try {
                        $poData = new poPurchaseOrderModel();
                        $poData::where('po_id', $po_id)
                                   ->update([
                                          'po_no'=>$request->po_no,
                                          'po_date'=>$po_date,
                                          'req_date'=>$req_date,
                                          'wh_id'=>$request->wh_id,
                                          'supp_id' =>  $request->supp_id,
                                          'ppn_type'=>$request->ppn_type,
                                          'notes'=>$request->notes,
                                          'po_status'=>$request->po_status,
                                          'updated_date' => \Carbon\Carbon::now(),
                                          'updated_by' => $userId
                                    ]);
                        } catch(\Illuminate\Database\QueryException $ex){
                          return Redirect::back()->with('status', 'Error updating');
                        }
            }else{
              return Redirect::back()->with('status', 'Not permitted');
            }
      }else{
          try {
            $poData = new poPurchaseOrderModel();
            $poData::where('po_id', $po_id)
                       ->update([
                              'po_no'=>$request->po_no,
                              'po_date'=>$po_date,
                              'req_date'=>$req_date,
                              'ppn_type'=>$request->ppn_type,
                              'wh_id'=>$request->wh_id,
                              'supp_id' =>  $request->supp_id,
                              'notes'=>$request->notes,
                              'po_status'=>$request->po_status,
                              'updated_date' => \Carbon\Carbon::now(),
                              'updated_by' => $userId
                        ]);
            } catch(\Illuminate\Database\QueryException $ex){
                return Redirect::back()->with('status', 'Error updating');
            }
      }

        return Redirect::back()->with('status', 'Po updated');
      }

    }

    public function deletePo(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $po_id = $request->po_id;

      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $poData = new poPurchaseOrderModel();
        $poData::where('po_id', $po_id)->delete();

        $poDetailData = new poPurchaseOrderDetailModel();
        $poDetailData::where('po_id', $po_id)->delete();

        $po_pr = new poPrModel();
        $po_pr::where('po_id', $po_id)->delete();
        return Redirect::back()->with('status', 'Po deleted');
      }

    }

    public function deleteBulkPo(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $po_id = $request->po_id;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $poData = new poPurchaseOrderModel();
        $poData::whereIn('po_id', $po_id)->delete();

        $poDetailData = new poPurchaseOrderDetailModel();
        $poDetailData::whereIn('po_id', $po_id)->delete();

        $po_pr = new poPrModel();
        $po_pr::whereIn('po_id', $po_id)->delete();
        $status = "success";

      }
      return $status;
    }
}
