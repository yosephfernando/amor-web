<?php

namespace App\Http\Controllers\purchaseorder;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\purchaseorder\po_purchase_order\poPurchaseOrderModel;
use App\purchaseorder\po_pr\poPrModel;
use App\purchaseorder\po_detail_purchase_order\poPurchaseOrderDetailModel;
use App\purchaserequest\pr_detail_purchase_request\prPurchaseRequestDetailModel;
use App\purchaserequest\pr_purchase_request\prPurchaseRequestModel;
use App\inventory\im_prd_master\imPrdMasterModel;
use App\inventory\im_prd_unit\imPrdUnitModel;
use App\usermanagement\um_module\umModuleModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Illuminate\Support\Facades\DB;
use Datatables;

class poDetailPurchaseOrder extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    private function updateTotalPo($poid, $price, $qty, $pr_flag){
      $ppn = poPurchaseOrderModel::select('ppn')->where('po_id', $poid)->first();
      if($ppn->ppn > 0){
        $percentPPN = $ppn->ppn / 100;
        $total_ppn = $percentPPN * $price;
        $total_gross = $price + $total_ppn;
      }else{
        $total_ppn = 0;
        $total_gross = $price;
      }

      $updatePo = new poPurchaseOrderModel();
      $updatePo::where('po_id', $poid)
                 ->update([
                    "total_qty" => $qty,
                    "total_gross_product" => $total_gross,
                    "total_gross" => $total_gross,
                    "total_nett" => $total_gross,
                    "total_ppn" => $total_ppn,
                    "pr_flag" => $pr_flag,
                 ]);

       return true;
    }

    private function poPrInsert($poid, $pr_detail_id, $prd_id, $qty_pr){
      $poPrInsert = new poPrModel();
      $poPrInsert->po_id = $poid;
      $poPrInsert->pr_detail_id = $pr_detail_id;
      $poPrInsert->prd_id = $prd_id;
      $poPrInsert->qty_pr = $qty_pr;
      $insert =  $poPrInsert->save();

      return true;
    }

    private function updateDetailPr($poid, $pr_detail_id){
      $updatePrDet = new prPurchaseRequestDetailModel();
      $updatePrDet::where('pr_detail_id', $pr_detail_id)
                 ->update([
                    "po_id" => $poid
                 ]);

      return true;
    }

    private function updatePrStatus($pr_id, $status){
      $updatePrStatus = prPurchaseRequestModel::where('pr_id', $pr_id)
                 ->update([
                    "pr_status" => $status
                 ]);

      return true;
    }

    public function ajaxPrdCatalog($view, $keyword){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('purchaseorder.po_detail_purchase_order.'.$view, [
            'keyword' => $keyword,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function ajaxPrCatalog($view, $keyword){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('purchaseorder.po_detail_purchase_order.'.$view, [
            'keyword' => $keyword,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function ajaxPrDetailCatalog($view, $id){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $priv = $this->checkPrivilaged($moduleId, $userGroupId);
      $prDetail = prPurchaseRequestDetailModel::select('IPM.prd_code', 'IPM.prd_desc','IPS.size_desc', 'IPM.retail_price', 'PR_DETAIL.qty_pr', 'PR_DETAIL.prd_id', 'PR_DETAIL.pr_detail_id')
                                                ->join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'PR_DETAIL.prd_id')
                                                ->join('IM_PRD_SIZE AS IPS', 'IPM.size_id', '=', 'IPS.size_id')
                                                ->where(['PR_DETAIL.pr_id' => $id, 'PR_DETAIL.po_id' => null])
                                                ->get();

      if($priv != null && $priv->canView == 1){
        return view('purchaseorder.po_detail_purchase_order.'.$view, [
            'prDetail' => $prDetail,
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }
    }

    public function loadPr(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      $pr_detail_id = $request->pr_detail_id;
      $pr_id = $request->pr_id;

      if($pr_detail_id != null){
        $pr_details =  prPurchaseRequestDetailModel::whereIn('pr_detail_id', $pr_detail_id)->get();
      }else{
        return Redirect::back()->with('status', 'error : choose one of the detail(s)');
      }

      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $i = 0;
        foreach($pr_details as $pr_detail){
              $pr_det_id = $pr_detail->pr_detail_id;
              $prd_price = preg_replace("/[^A-Za-z0-9 ]/", '', $request->input('retail_price'.$pr_det_id));
              $prd_price = str_replace('Rp', '', $prd_price);
              $qty = $pr_detail->qty_pr;
              $sub_total = $prd_price * $qty;

              try {
                $detailPoData = new poPurchaseOrderDetailModel();
                $detailPoData->po_id = $request->po_id;
                $detailPoData->prd_id = $pr_detail->prd_id;
                $detailPoData->qty_pr =  $qty;
                $detailPoData->qty_min =  0;
                $detailPoData->qty_other =  0;
                $detailPoData->qty_order =  $qty;
                $detailPoData->qty_receive =  0;
                $detailPoData->order_price =  $prd_price;
                $detailPoData->disc =  0;
                $detailPoData->disc_value =  0;
                $detailPoData->po_status_detail =  0;
                $detailPoData->uom_id = $pr_detail->uom_id;
                $detailPoData->uom_conversion = $pr_detail->uom_conversion;
                $detailPoData->sub_total = $sub_total;
                $insert =  $detailPoData->save();
              } catch(\Illuminate\Database\QueryException $ex){
                  return false;
              }

              $sum_qty = poPurchaseOrderDetailModel::where('po_id', $request->po_id)->sum('qty_pr');
              $sum_subtotal = poPurchaseOrderDetailModel::select(DB::raw("SUM(sub_total::money::numeric::float8) AS sub_total"))->where('po_id', $request->po_id)->first();
              $this->updateTotalPo($request->po_id, $sum_subtotal->sub_total, $sum_qty, 1);
              $this->poPrInsert($request->po_id, $pr_detail->pr_detail_id, $pr_detail->prd_id, $pr_detail->qty_pr);
              $this->updateDetailPr($request->po_id, $pr_detail->pr_detail_id);
        $i++;}

        $sum_qty = poPurchaseOrderDetailModel::where('po_id', $request->po_id)->sum('qty_pr');
        $cekPartial = prPurchaseRequestDetailModel::where('pr_id', $pr_id)->sum('qty_pr');
        $statusPr = "";

        if($sum_qty < $cekPartial){
            $statusPr = "partial";
        }else{
            $statusPr = "approve";
        }

        $this->updatePrStatus($pr_id, $statusPr);

        return Redirect::back()->with('status', 'Detail po added');
      }
    }

    public function exportToExcel($view, $poId){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv->canExport)){
        $detailPo = poPurchaseOrderDetailModel::join('IM_PRD_MASTER AS IPM', 'PO_DETAIL.prd_id', '=', 'IPM.prd_id')
                                  ->join('IM_PRD_SIZE AS IPZ', 'IPM.size_id', '=', 'IPZ.size_id')
                                  ->join('IM_PRD_COLOR AS IPC', 'IPM.color_id', '=', 'IPC.color_id')
                                  ->select('PO_DETAIL.po_detail_id', 'PO_DETAIL.po_id', 'IPZ.size_desc', 'IPC.color_desc', 'PO_DETAIL.qty_pr', 'IPM.prd_desc')
                                  ->where('PO_DETAIL.po_id', $poId)
                                  ->get();
         $data = null;
         foreach($detailPo as $datas){
             $data[] = array(
               $datas->po_id,
               $datas->po_detail_id,
               $datas->prd_desc,
               $datas->size_desc,
               $datas->color_desc,
               $datas->qty_pr
             );
         }
         if($data != null){
           $column = array('ID PO', 'ID DETAIL PO','NAMA BARANG', 'SIZE', 'COLOR', 'QTY');
           $this->excelDownload($data, "Data detail po", $column);
         }else{
            return Redirect::back()->withErrors(['No data']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function addDetailPo(Request $request, $view, $poId){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $detailPo = poPurchaseOrderDetailModel::join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'PO_DETAIL.prd_id')
                                ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IPM.size_id')
                                ->join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'PO_DETAIL.uom_id')
                                ->select('PO_DETAIL.po_detail_id', 'PO_DETAIL.qty_pr', 'PO_DETAIL.sub_total', 'IPM.prd_id', 'IPM.retail_price', 'IPM.prd_desc', 'IPS.size_desc', 'PO_DETAIL.uom_id', 'IPU.uom_desc', 'PO_DETAIL.uom_conversion', DB::raw('"PO_DETAIL".order_price::money::numeric::float8 AS order_price'))
                                ->where('PO_DETAIL.po_id', $poId)
                                ->get();

      $uoms = imPrdUnitModel::select('uom_id', 'uom_desc')->get();

      $po_detail_id = poPurchaseOrderDetailModel::select('po_detail_id')->where('po_id', $poId)->first();

      $po_detail_sub_sum = poPurchaseOrderDetailModel::select(DB::raw('SUM(sub_total::money::numeric::float8) as sub_total'), DB::raw('SUM(qty_pr) as qty_pr'))->where('po_id', $poId)->first();
      $po_detail_retail_price_sum = poPurchaseOrderDetailModel::join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'PO_DETAIL.prd_id')
                                                                ->select(DB::raw('SUM("IPM"."retail_price") as "retail_price"'))
                                                                ->where('po_id', $poId)
                                                                ->first();

      $ppn = poPurchaseOrderModel::select('ppn')->where('po_id', $poId)->first();
      $price = $po_detail_sub_sum->sub_total;

      if($ppn->ppn > 0){
        $percentPPN = $ppn->ppn / 100;
        $total_ppn = $percentPPN * $price;
        $total = $price + $total_ppn;
      }else{
        $total_ppn = 0;
        $total = $price;
      }

      $po_id = poPurchaseOrderModel::select('po_id', 'po_no', 'po_status', 'pr_flag',  'created_date')->where('po_id', $poId)->first();

      $prSugest = prPurchaseRequestModel::select(DB::raw('CAST(pr_id AS varchar) AS data'), 'pr_no AS value', 'pr_status')->where('pr_status', 'open')->orWhere('pr_status', 'partial')->get();

      $listModules = $this->listModules($userGroupId);
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($priv != null && $priv->canView == 1){
        return view('purchaseorder.po_detail_purchase_order.'.$view, [
            'userGroupId' => $userGroupId,
            'listModules' => $listModules,
            'title' => $userModule,
            'privilaged' => $priv,
            'detailPo' => $detailPo,
            'uoms' => $uoms,
            'po_id' => $po_id,
            'po_detail_id' => $po_detail_id,
            'po_detail_sub_sum' => $po_detail_sub_sum,
            'total' => $total,
            'total_bef_ppn' => $price,
            'total_ppn' => $total_ppn,
            'po_detail_retail_price_sum' => $po_detail_retail_price_sum,
            'prSugest' => $prSugest
        ]);
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }

    public function listDetailPo(Request $request, $view, $poId){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;

      $detailPo = poPurchaseOrderDetailModel::join('IM_PRD_MASTER AS IPM', 'IPM.prd_id', '=', 'PO_DETAIL.prd_id')
                                ->join('IM_PRD_SIZE AS IPS', 'IPS.size_id', '=', 'IPM.size_id')
                                ->join('IM_PRD_UNIT AS IPU', 'IPU.uom_id', '=', 'PO_DETAIL.uom_id')
                                ->join('IM_PRD_COLOR AS IPC', 'IPC.color_id', '=', 'IPM.color_id')
                                ->select('PO_DETAIL.po_detail_id', 'PO_DETAIL.qty_pr', 'PO_DETAIL.sub_total', 'IPM.prd_id', 'IPM.retail_price', 'IPM.prd_desc', 'IPS.size_desc', 'PO_DETAIL.uom_id', 'IPU.uom_desc', 'PO_DETAIL.uom_conversion', 'PO_DETAIL.allowance', DB::raw('"PO_DETAIL".order_price::money::numeric::float8 AS order_price'),
                                'IPC.color_desc')
                                ->where('PO_DETAIL.po_id', $poId);

      $uoms = imPrdUnitModel::select('uom_id', 'uom_desc')->get();
      $po_id = poPurchaseOrderModel::select('po_id', 'po_no', 'po_status', 'pr_flag',  'created_date')->where('po_id', $poId)->first();
      $listModules = $this->listModules($userGroupId);
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if($po_id->po_status != "approve"){
            return Datatables::of($detailPo)
            ->addColumn('delete_bulk', function($data){
                return "
                  <input type='checkbox' name='po_detail_id[]' value='".$data->po_detail_id."' />
                ";
            })
            ->editColumn('sub_total', function ($data) {
                   return $data->sub_total;
            })
            ->editColumn('qty_pr', function ($data) {
                   return number_format($data->qty_pr);
            })
            ->addColumn('action', function ($data) use ($priv, $uoms, $po_id) {
                return view('purchaseorder.po_detail_purchase_order.poDetailPurchaseOrderDataTable', ['privilaged' => $priv, 'data' => $data, 'uoms' => $uoms, 'po_id' => $po_id]);
            })
            ->filter(function ($detailPo) use ($request) {
                     if ($request->has('prd_desc')) {
                         $detailPo->where('IPM.prd_desc', 'LIKE', '%'.$request->prd_desc.'%');
                     }
             })
            ->make(true);
      }else{
            return Datatables::of($detailPo)
            ->addColumn('delete_bulk', function($data){
                return "
                  <input type='checkbox' name='po_detail_id[]' value='".$data->po_detail_id."' />
                ";
            })
            ->editColumn('sub_total', function ($data) {
                   return $data->sub_total;
            })
            ->editColumn('qty_pr', function ($data) {
                   return number_format($data->qty_pr);
            })
            ->filter(function ($detailPo) use ($request) {
                   if ($request->has('prd_desc')) {
                       $detailPo->where('IPM.prd_desc', 'LIKE', '%'.$request->prd_desc.'%');
                   }
             })
            ->make(true);
      }
    }

    public function addDetailPoAction(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $prd_price = imPrdMasterModel::select('retail_price')->where('prd_id', $request->prd_id)->first();

      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
          $price = ($prd_price != null ? $prd_price->retail_price:0);
          $sub_total = $price * $request->qty_pr;
        try {
          $detailPoData = new poPurchaseOrderDetailModel();
          $detailPoData->po_id = $request->po_id;
          $detailPoData->prd_id = $request->prd_id;
          $detailPoData->qty_pr =  $request->qty_pr;
          $detailPoData->qty_min =  0;
          $detailPoData->qty_other =  0;
          $detailPoData->qty_order =  $request->qty_pr;
          $detailPoData->qty_receive =  0;
          $detailPoData->order_price =  number_format($price,0,"","");
          $detailPoData->disc =  0;
          $detailPoData->disc_value =  0;
          $detailPoData->po_status_detail =  0;
          $detailPoData->uom_id = $request->uom_id;
          $detailPoData->uom_conversion = $request->uom_conversion;
          $detailPoData->sub_total = $sub_total;
          $insert =  $detailPoData->save();
        } catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error inserting');
        }

        $sum_qty = poPurchaseOrderDetailModel::where('po_id', $request->po_id)->sum('qty_pr');
        $sum_subtotal = poPurchaseOrderDetailModel::select(DB::raw("SUM(sub_total::money::numeric::float8) AS sub_total"))->where('po_id', $request->po_id)->first();
        $sum_subtotal = ($sum_subtotal->sub_total != null ? $sum_subtotal->sub_total:0);
        $this->updateTotalPo($request->po_id, $sum_subtotal, $sum_qty, 0);

        return Redirect::back()->with('status', 'Detail po added');
      }

    }

    public function editDetailPo(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $po_detail_id = $request->po_detail_id;
      $prd_price = str_replace(".", "", $request->order_price);

      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $sub_total = $prd_price * $request->qty_pr;
        $qty_order = $request->qty_pr + $request->allowance;
        try {
          $detailPoData = new poPurchaseOrderDetailModel();
          $detailPoData::where('po_detail_id', $po_detail_id)
                     ->update([
                            'po_id'=>$request->po_id,
                            'prd_id'=>$request->prd_id,
                            'qty_pr'=>$request->qty_pr,
                            'qty_order'=>$qty_order,
                            'order_price' => $prd_price,
                            'uom_id'=>$request->uom_id,
                            'allowance'=>$request->allowance,
                            'uom_conversion'=>$request->uom_conversion,
                            'sub_total'=>$sub_total,

                      ]);
          } catch(\Illuminate\Database\QueryException $ex){
              return Redirect::back()->with('status', 'Error updating');
          }

          $sum_qty = poPurchaseOrderDetailModel::where('po_id', $request->po_id)->sum('qty_pr');
          $sum_subtotal = poPurchaseOrderDetailModel::select(DB::raw("SUM(sub_total::money::numeric::float8) AS sub_total"))->where('po_id', $request->po_id)->first();
          $sum_subtotal = ($sum_subtotal->sub_total != null ? $sum_subtotal->sub_total:0);
          $this->updateTotalPo($request->po_id, $sum_subtotal, $sum_qty, 0);

        return Redirect::back()->with('status', 'Detail po updated');
      }

    }

    public function deleteDetailPo(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $po_detail_id = $request->po_detail_id;

      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{

        try {
            $detailPoData = new poPurchaseOrderDetailModel();
            $detailPoData::where('po_detail_id', $po_detail_id)->delete();
        } catch(\Illuminate\Database\QueryException $ex){
            dd($ex->getMessage());
            return Redirect::back()->with('status', 'Error deleting');
        }

        $sum_qty = poPurchaseOrderDetailModel::where('po_id', $request->po_id)->sum('qty_pr');
        $sum_subtotal = poPurchaseOrderDetailModel::select(DB::raw("SUM(sub_total::money::numeric::float8) AS sub_total"))->where('po_id', $request->po_id)->first();
        $sum_subtotal = ($sum_subtotal->sub_total != null ? $sum_subtotal->sub_total:0);
        $this->updateTotalPo($request->po_id, $sum_subtotal, $sum_qty, 0);

        return Redirect::back()->with('status', 'Detail po deleted');
      }

    }

    public function deleteBulkDetailPo(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $po_detail_id = $request->po_detail_id;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $detailPoData = new poPurchaseOrderDetailModel();
        $detailPoData::whereIn('po_detail_id', $po_detail_id)->delete();

        $sum_qty = poPurchaseOrderDetailModel::where('po_id', $request->po_id)->sum('qty_pr');
        $sum_subtotal = poPurchaseOrderDetailModel::select(DB::raw("SUM(sub_total::money::numeric::float8) AS sub_total"))->where('po_id', $request->po_id)->first();
        $sum_subtotal = ($sum_subtotal->sub_total != null ? $sum_subtotal->sub_total:0);
        $this->updateTotalPo($request->po_id, $sum_subtotal, $sum_qty, 0);

        $status = "success";

      }
      return $status;
    }
}
