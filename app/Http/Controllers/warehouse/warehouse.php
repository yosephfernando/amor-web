<?php

namespace App\Http\Controllers\warehouse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\usermanagement\um_module\umModuleModel;
use App\warehouse\im_wh_loc\imWhLocModel;
use App\warehouse\im_wh_type\imWhTypeModel;
use Illuminate\Support\Facades\Auth;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Illuminate\Support\Facades\DB;
use Datatables;

class warehouse extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listWhView($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);
        $whs = imWhTypeModel::select(DB::raw('CAST("IM_WH_TYPE".wh_type_id AS varchar) AS data'), 'IM_WH_TYPE.wh_type_desc AS value')
                                   ->get();

        return view('inventory.im_wh_loc.'.$view, [
            'userGroupId' => $userGroupId,
            'listModules' => $listModules,
            'title' => $userModule,
            'privilaged' => $priv,
            'whs' => $whs
        ]);
    }

    public function listWhData($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        $whs = imWhLocModel::join('IM_WH_TYPE AS IWT', 'IWT.wh_type_id', '=', 'IM_WH_LOC.wh_type_id')
                             ->select('IM_WH_LOC.*', 'IWT.wh_type_desc');

        $whType = imWhTypeModel::select(DB::raw('CAST("IM_WH_TYPE".wh_type_id AS varchar) AS data'), 'IM_WH_TYPE.wh_type_desc AS value')
                                     ->get();
        return Datatables::of($whs)
        ->addColumn('delete_bulk', function($data){
            return "
              <input type='checkbox' name='wh_id[]' value='".$data->wh_id."' />
            ";
        })
        ->editColumn('active_flag', function($data){
          if($data->active_flag == 1){
            return "
            <span class='label label-md label-success'>Active</span>
            ";
          }else{
            return "
              <span class='label label-md label-danger'>Non active</span>
            ";
          }

        })
        ->editColumn('wh_code1', function($data){
            return $data->wh_code1."-".$data->wh_code2;
        })
        ->addColumn('action', function ($data) use ($priv, $whType, $view) {
            return view('inventory.im_wh_loc.'.$view, ['privilaged' => $priv, 'data' => $data, 'whstype' => $whType]);
       })
        ->make(true);
    }

    public function add(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
         try {
            $whData = new imWhLocModel();
            $whData->wh_code1 = $request->wh_code1;
            $whData->wh_code2 = $request->wh_code2;
            $whData->wh_desc = $request->wh_desc;
            $whData->wh_type_id = $request->wh_type_id;
            $whData->wh_address1 = $request->wh_address1;
            $whData->wh_address2 = $request->wh_address2;
            $whData->wh_address3 = $request->wh_address3;
            $whData->wh_city = $request->wh_city;
            $whData->wh_country = $request->wh_country;
            $whData->wh_phone = $request->wh_phone;
            $whData->wh_fax = $request->wh_fax;
            $whData->wh_email = $request->wh_email;
            $whData->wh_post_code = $request->wh_post_code;
            $whData->wh_contact_person = $request->wh_contact_person;
            $whData->active_flag = $request->input('active_flag', 0);
            $whData->created_date = \Carbon\Carbon::now();
            $whData->updated_date = \Carbon\Carbon::now();
            $whData->created_by = $userId;
            $insert =  $whData->save();

            return Redirect::back()->with('status', 'Warehouse added');
          } catch(\Illuminate\Database\QueryException $ex){
              return Redirect::back()->with('status', 'Error inserting');
          }
      }

    }

    public function edit(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idWh = $request->wh_id;

      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
          $whData = new imWhLocModel();
          $whData::where('wh_id', $idWh)
                     ->update([
                            'wh_code1'=>$request->wh_code1,
                            'wh_code2'=>$request->wh_code2,
                            'wh_desc'=>$request->wh_desc,
                            'wh_type_id'=>$request->wh_type_id,
                            'wh_address1'=>$request->wh_address1,
                            'wh_address2'=>$request->wh_address2,
                            'wh_address3'=>$request->wh_address3,
                            'wh_city'=>$request->wh_city,
                            'wh_country'=>$request->wh_country,
                            'wh_phone'=>$request->wh_phone,
                            'wh_fax'=>$request->wh_fax,
                            'wh_email'=>$request->wh_email,
                            'wh_post_code'=>$request->wh_post_code,
                            'wh_contact_person'=>$request->wh_contact_person,
                            'active_flag'=>$request->input('active_flag', 0),
                            'updated_date' => \Carbon\Carbon::now(),
                            'updated_by' => $userId
                      ]);
          return Redirect::back()->with('status', 'Warehouse updated');
        } catch(\Illuminate\Database\QueryException $ex){
          dd($ex->getMessage());
            return Redirect::back()->with('status', 'Error updating');
        }
      }

    }

    public function delete(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idWh = $request->wh_id;

      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $whData = new imWhLocModel();

        $whData::where('wh_id', $idWh)->delete();

        return Redirect::back()->with('status', 'Warehouse deleted');
      }

    }

    public function deleteBulk(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idWh = $request->wh_id;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $whData = new imWhLocModel();
        $whData::whereIn('wh_id', $idWh)->delete();
        $status = "success";

      }
      return $status;
    }

    public function exportToExcel($view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv->canExport)){
        $datamodel = imWhLocModel::select('wh_code1', 'wh_code2', 'wh_desc')
                                        ->get();
         $data = null;
         foreach($datamodel as $datas){
             $data[] = array(
               $datas->wh_code1."-".$datas->wh_code2,
               $datas->wh_desc,
             );
         }
         if($data != null){
           $column = array('WAREHOUSE CODE', 'WAREHOUSE DESC');
           $this->excelDownload($data, "Data Warehouse", $column);
         }else{
           return Redirect::back()->withErrors(['No data to export']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }
}
