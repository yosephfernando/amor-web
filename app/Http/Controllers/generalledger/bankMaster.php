<?php

namespace App\Http\Controllers\generalledger;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\usermanagement\um_module\umModuleModel;
use App\glaccount\gl_currency\glCurrencyModel;
use App\glaccount\gl_bank\glBankModel;
use App\glaccount\gl_account\glAccountModel;
use Illuminate\Support\Facades\Auth;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Datatables;
use DB;

class bankMaster extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listBankView($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);
        $bank = glBankModel::select('bank_id', 'bank_code', 'bank_desc', 'bank_rek_no')->get();

        return view('generalledger.gl_bank.'.$view, [
            'userGroupId' => $userGroupId,
            'listModules' => $listModules,
            'title' => $userModule,
            'privilaged' => $priv,
            'bank' => $bank
        ]);
    }

    public function listBankData(Request $request, $view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);
        $bank = glBankModel::join('GL_ACCOUNT AS COA', 'COA.coa_id', '=', 'GL_BANK_MASTER.coa_id')
                              ->join('GL_CURR_MASTER AS CURR', 'CURR.curr_id', '=', 'GL_BANK_MASTER.curr_id')
                              ->select('GL_BANK_MASTER.bank_id', 'GL_BANK_MASTER.bank_code',
                              'GL_BANK_MASTER.bank_desc', 'GL_BANK_MASTER.bank_rek_no',
                              'COA.coa_desc', 'CURR.curr_code', 'GL_BANK_MASTER.bank_cost_center', 'GL_BANK_MASTER.coa_id', 'GL_BANK_MASTER.curr_id');

        return Datatables::of($bank)
        ->addColumn('delete_bulk', function($data){
            return "
              <input type='checkbox' name='bank_id[]' value='".$data->bank_id."' />
            ";
        })
        ->addColumn('action', function ($data) use ($priv, $view) {
            return view('generalledger.gl_bank.'.$view, ['privilaged' => $priv, 'data' => $data]);
       })
        ->make(true);
    }

    public function add(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      if(!is_null($priv) && $priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $bank_kas = preg_replace("/[^A-Za-z0-9 ]/", '', $request->input('bank_kas'));
        $bank_kas = str_replace('Rp', '', $bank_kas);
         try {
            $bankData = new glBankModel();
            $bankData->bank_code = $request->bank_code;
            $bankData->bank_desc = $request->bank_desc;
            $bankData->bank_rek_no = $request->bank_rek_no;
            $bankData->coa_id = $request->coa_id;
            $bankData->curr_id = $request->curr_id;
            $bankData->bank_cost_center = $request->bank_cost_center;
            $bankData->bank_kas = $bank_kas;
            $bankData->created_date = \Carbon\Carbon::now();
            $bankData->updated_date = \Carbon\Carbon::now();
            $bankData->created_by = $userId;
            $insert =  $bankData->save();

            return Redirect::back()->with('status', 'Bank added');
          } catch(\Illuminate\Database\QueryException $ex){
              return Redirect::back()->with('status', 'Error inserting');
          }
      }

    }

    public function edit(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idBank = $request->bank_id;

      if(!is_null($priv) && $priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
          $bankData = new glBankModel();
          $bankData::where('bank_id', $idBank)
                     ->update([
                            'bank_code'=>$request->bank_code,
                            'bank_desc'=>$request->bank_desc,
                            'bank_rek_no'=>$request->bank_rek_no,
                            'coa_id'=>$request->coa_id,
                            'curr_id'=>$request->curr_id,
                            'bank_cost_center'=>$request->bank_cost_center,
                            'bank_kas'=>$request->bank_kas,
                            'updated_date' => \Carbon\Carbon::now(),
                            'updated_by' => $userId
                      ]);
          return Redirect::back()->with('status', 'Bank updated');
        } catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error updating');
        }
      }

    }

    public function delete(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idBank = $request->bank_id;

      if(!is_null($priv) && $priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $bankData = new glBankModel();

        $bankData::where('bank_id', $idBank)->delete();

        return Redirect::back()->with('status', 'Bank deleted');
      }

    }

    public function deleteBulk(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idBank = $request->bank_id;

      if(!is_null($priv) && $priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $bankData = new glBankModel();
        $bankData::whereIn('bank_id', $idBank)->delete();
        $status = "success";

      }
      return $status;
    }

    public function exportToExcel($view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv) && !is_null($priv->canExport)){
        $datamodel = glBankModel::select('bank_code', 'bank_desc')
                                    ->get();
         $data = null;
         foreach($datamodel as $datas){
             $data[] = array(
               $datas->bank_code,
               $datas->bank_desc
             );
         }
         if($data != null){
           $column = array('BANK CODE','BANK DESC');
           $this->excelDownload($data, "Data Bank", $column);
         }else{
           return Redirect::back()->withErrors(['No data']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }
}
