<?php

namespace App\Http\Controllers\generalledger;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\usermanagement\um_module\umModuleModel;
use App\glaccount\gl_currency\glCurrencyModel;
use Illuminate\Support\Facades\Auth;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Datatables;
use DB;

class currencyMaster extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listCurView($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);
        $currency = glCurrencyModel::select('curr_id', 'curr_code', 'curr_symbol')->get();

        return view('generalledger.gl_currency.'.$view, [
            'userGroupId' => $userGroupId,
            'listModules' => $listModules,
            'title' => $userModule,
            'privilaged' => $priv,
            'currency' => $currency
        ]);
    }

    public function listCurrData(Request $request, $view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);
        $currency = glCurrencyModel::select('curr_id', 'curr_code', 'curr_symbol', 'curr_desc')->get();

        return Datatables::of($currency)
        ->addColumn('delete_bulk', function($data){
            return "
              <input type='checkbox' name='curr_id[]' value='".$data->curr_id."' />
            ";
        })
        ->addColumn('action', function ($data) use ($priv, $view) {
            return view('generalledger.gl_currency.'.$view, ['privilaged' => $priv, 'data' => $data]);
       })
        ->make(true);
    }

    public function add(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      if(!is_null($priv) && $priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
         try {
            $curData = new glCurrencyModel();
            $curData->curr_code = $request->curr_code;
            $curData->curr_desc = $request->curr_desc;
            $curData->curr_symbol = $request->curr_symbol;
            $curData->created_date = \Carbon\Carbon::now();
            $curData->updated_date = \Carbon\Carbon::now();
            $curData->created_by = $userId;
            $insert =  $curData->save();

            return Redirect::back()->with('status', 'Currency added');
          } catch(\Illuminate\Database\QueryException $ex){
              return Redirect::back()->with('status', 'Error inserting');
          }
      }

    }

    public function edit(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idCurr = $request->curr_id;

      if(!is_null($priv) && $priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
          $curData = new glCurrencyModel();
          $curData::where('curr_id', $idCurr)
                     ->update([
                            'curr_code'=>$request->curr_code,
                            'curr_desc'=>$request->curr_desc,
                            'curr_symbol'=>$request->curr_symbol,
                            'updated_date' => \Carbon\Carbon::now(),
                            'updated_by' => $userId
                      ]);
          return Redirect::back()->with('status', 'Currency updated');
        } catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error updating');
        }
      }

    }

    public function delete(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idCurr = $request->curr_id;

      if(!is_null($priv) && $priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $curData = new glCurrencyModel();

        $curData::where('curr_id', $idCurr)->delete();

        return Redirect::back()->with('status', 'Currency deleted');
      }

    }

    public function deleteBulk(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idCurr = $request->curr_id;

      if(!is_null($priv) && $priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $curData = new glCurrencyModel();
        $curData::whereIn('curr_id', $idCurr)->delete();
        $status = "success";

      }
      return $status;
    }

    public function exportToExcel($view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv) && !is_null($priv->canExport)){
        $datamodel = glCurrencyModel::select('curr_code', 'curr_desc')
                                    ->get();
         $data = null;
         foreach($datamodel as $datas){
             $data[] = array(
               $datas->curr_code,
               $datas->curr_desc
             );
         }
         if($data != null){
           $column = array('CURR CODE','CURR DESC');
           $this->excelDownload($data, "Data Currency", $column);
         }else{
           return Redirect::back()->withErrors(['No data']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }
}
