<?php

namespace App\Http\Controllers\generalledger;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\usermanagement\um_module\umModuleModel;
use App\glaccount\gl_account\glAccountModel;
use App\glaccount\gl_account_group\glAccountGroupModel;
use App\glaccount\gl_currency\glCurrencyModel;
use Illuminate\Support\Facades\Auth;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Datatables;
use DB;

class accountMaster extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listView($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);
        $accounts = glAccountModel::select('coa_code', 'coa_alias', 'coa_desc')->get();
        $accountsGroup = glAccountGroupModel::select('coa_group_id', 'coa_group_desc')->get();
        $currency = glCurrencyModel::select('curr_id', 'curr_code')->get();

        return view('generalledger.gl_account.'.$view, [
            'userGroupId' => $userGroupId,
            'listModules' => $listModules,
            'title' => $userModule,
            'privilaged' => $priv,
            'accounts' => $accounts,
            'coa_group' => $accountsGroup,
            'currency' => $currency
        ]);
    }

    public function listData(Request $request, $view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);

        $accounts = glAccountModel::join('GL_ACCOUNT_GROUP AS GAG', 'GAG.coa_group_id', '=', 'GL_ACCOUNT.coa_group_id')
                                    ->select('GL_ACCOUNT.coa_id','GL_ACCOUNT.coa_code','GL_ACCOUNT.coa_desc','GL_ACCOUNT.coa_group_id', 'GL_ACCOUNT.coa_alias', 'GL_ACCOUNT.beginning_amt', 'GAG.coa_group_desc');
        $accountsGroup = glAccountGroupModel::select('coa_group_id', 'coa_group_desc')->get();
        $currency = glCurrencyModel::select('curr_id', 'curr_code')->get();

        return Datatables::of($accounts)
        ->addColumn('delete_bulk', function($data){
            return "
              <input type='checkbox' name='coa_id[]' value='".$data->coa_id."' />
            ";
        })
        ->addColumn('action', function ($data) use ($priv, $accountsGroup, $currency) {
            return view('generalledger.gl_account.glListAccountDataTable', ['privilaged' => $priv, 'data' => $data, 'coa_group' => $accountsGroup, 'currency' => $currency]);
       })
       ->filter(function ($accounts) use ($request) {
           if ($request->has('coa_group_id_filter')) {
               $accounts->where('GL_ACCOUNT.coa_group_id' , $request->get('coa_group_id_filter'));
           }
        })
        ->make(true);
    }

    public function add(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);
      $parent = substr($request->coa_code,0,1);
      $maxLevel = glAccountModel::select(DB::raw('MAX(coa_levels) AS coa_levels'))->where('coa_parent_id',$parent)->first();
      if($priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
         try {
            $accData = new glAccountModel();
            $accData->coa_code = $request->coa_code;
            $accData->coa_alias = $request->coa_alias;
            $accData->coa_desc = $request->coa_desc;
            $accData->coa_group_id = $request->coa_group_id;

            if($maxLevel->coa_levels != null){
              $accData->coa_levels = $maxLevel->coa_levels + 1;
            }else{
              $accData->coa_levels = 1;
            }

            $accData->coa_parent_id = substr($request->coa_code,0,1);
            $accData->curr_id = $request->curr_id;

            if($request->position == "header"){
              $accData->child_flag = 0;
            }else{
              $accData->child_flag = 1;
            }

            $accData->beginning_amt_ori = str_replace(".","", $request->beginning_amt);
            $accData->beginning_amt = str_replace(".","", $request->beginning_amt);
            $accData->created_date = \Carbon\Carbon::now();
            $accData->updated_date = \Carbon\Carbon::now();
            $accData->created_by = $userId;
            $insert =  $accData->save();

            return Redirect::back()->with('status', 'Account added');
          } catch(\Illuminate\Database\QueryException $ex){
              return Redirect::back()->with('status', 'Error inserting');
          }
      }

    }

    public function edit(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idCoa = $request->coa_id;

      if($priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        try {
          $accData = new glAccountModel();
          $accData::where('coa_id', $idCoa)
                     ->update([
                            'coa_code'=>$request->coa_code,
                            'coa_alias'=>$request->coa_alias,
                            'coa_desc'=>$request->coa_desc,
                            'coa_group_id'=>$request->coa_group_id,
                            'coa_parent_id'=>substr($request->coa_code,0,1),
                            'curr_id'=>$request->curr_id,
                            'beginning_amt_ori'=>str_replace(".","", $request->beginning_amt),
                            'beginning_amt'=>str_replace(".","", $request->beginning_amt),
                            //'child_flag'=>$request->input('child_flag', 0),
                            'updated_date' => \Carbon\Carbon::now(),
                            'updated_by' => $userId
                      ]);
          return Redirect::back()->with('status', 'Account updated');
        } catch(\Illuminate\Database\QueryException $ex){
            return Redirect::back()->with('status', 'Error updating');
        }
      }

    }

    public function delete(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idCoa = $request->coa_id;

      if($priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $accData = new glAccountModel();

        $accData::where('coa_id', $idCoa)->delete();

        return Redirect::back()->with('status', 'Account deleted');
      }

    }

    public function deleteBulk(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idCoa = $request->coa_id;

      if($priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $accData = new glAccountModel();
        $accData::whereIn('coa_id', $idCoa)->delete();
        $status = "success";

      }
      return $status;
    }

    public function exportToExcel($view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv->canExport)){
        $datamodel = glAccountModel::join('GL_ACCOUNT_GROUP AS GAP', 'GAP.coa_group_id', '=', 'GL_ACCOUNT.coa_group_id')
                                    ->select('coa_code', 'coa_alias', 'coa_desc', 'GAP.coa_group_desc')
                                    ->get();
         $data = null;
         foreach($datamodel as $datas){
             $data[] = array(
               $datas->coa_code,
               $datas->coa_alias,
               $datas->coa_desc,
               $datas->coa_group_desc
             );
         }
         if($data != null){
           $column = array('COA CODE', 'COA ALIAS', 'COA DESC', 'COA GROUP');
           $this->excelDownload($data, "Data Account", $column);
         }else{
           return Redirect::back()->withErrors(['No data']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }
}
