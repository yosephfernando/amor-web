<?php

namespace App\Http\Controllers\generalledger;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use App\usermanagement\um_module\umModuleModel;
use App\glaccount\gl_currency_rate\glCurrencyRateModel;
use App\glaccount\gl_currency\glCurrencyModel;
use Illuminate\Support\Facades\Auth;
use App\Traits\privilaged;
use App\Traits\excelExport;
use Datatables;
use DB;

class currencyRateMaster extends Controller
{
    use privilaged;
    use excelExport;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listCurRateView($view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);
        $currencyRate = glCurrencyRateModel::select('curr_rate_id', 'curr_rate_exchange', 'curr_rate_exchange_date', 'curr_rate_from', 'curr_rate_to')->get();
        $currency = glCurrencyModel::select('curr_id', 'curr_code')->get();
        return view('generalledger.gl_currency_rate.'.$view, [
            'userGroupId' => $userGroupId,
            'listModules' => $listModules,
            'title' => $userModule,
            'privilaged' => $priv,
            'currencyRate' => $currencyRate,
            'currency' => $currency
        ]);
    }

    public function listCurrRateData(Request $request, $view)
    {
        $userGroupId = Auth::user()->userGroupId;
        $userEmail = Auth::user()->email;

        $listModules = $this->listModules($userGroupId);

        $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
        $moduleId = $userModule->moduleId;

        $priv = $this->checkPrivilaged($moduleId, $userGroupId);
        $currencyRate = glCurrencyRateModel::join('GL_CURR_MASTER AS GCMF', 'GCMF.curr_id', '=', 'GL_CURR_RATE_MASTER.curr_rate_from')
                                            ->join('GL_CURR_MASTER AS GCMT', 'GCMT.curr_id', '=', 'GL_CURR_RATE_MASTER.curr_rate_to')
                                            ->select('GL_CURR_RATE_MASTER.curr_rate_id', 'GL_CURR_RATE_MASTER.curr_rate_exchange', 'GL_CURR_RATE_MASTER.curr_rate_exchange_date', 'GCMT.curr_code AS curr_to',
                                            'GCMF.curr_code AS curr_from', 'GL_CURR_RATE_MASTER.curr_rate_from', 'GL_CURR_RATE_MASTER.curr_rate_to');
        $currency = glCurrencyModel::select('curr_id', 'curr_code')->get();
        return Datatables::of($currencyRate)
        ->addColumn('delete_bulk', function($data){
            return "
              <input type='checkbox' name='curr_rate_id[]' value='".$data->curr_rate_id."' />
            ";
        })
        ->addColumn('action', function ($data) use ($priv, $view, $currency) {
            return view('generalledger.gl_currency_rate.'.$view, ['privilaged' => $priv, 'data' => $data, 'currency' => $currency]);
       })
        ->make(true);
    }

    public function add(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;

      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      if(!is_null($priv) && $priv->canAdd != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{

         if($request->curr_rate_from != $request->curr_rate_to){
           try {
              $currRateData = new glCurrencyRateModel();
              $currRateData->curr_rate_exchange = $request->curr_rate_exchange;
              $currRateData->curr_rate_exchange_date = $request->curr_rate_exchange_date;
              $currRateData->curr_rate_from = $request->curr_rate_from;
              $currRateData->curr_rate_to = $request->curr_rate_to;
              $currRateData->created_date = \Carbon\Carbon::now();
              $currRateData->updated_date = \Carbon\Carbon::now();
              $currRateData->created_by = $userId;
              $insert =  $currRateData->save();

              return Redirect::back()->with('status', 'Currency rate added');
            } catch(\Illuminate\Database\QueryException $ex){
                return Redirect::back()->with('status', 'Error inserting');
            }
         }else{
           return Redirect::back()->with('status', 'Error currency from = currency to');
         }

      }

    }

    public function edit(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userId = Auth::user()->id;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idCurrRate = $request->curr_rate_id;

      if(!is_null($priv) && $priv->canEdit != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        if($request->curr_rate_from != $request->curr_rate_to){
          try {
            $currRateData = new glCurrencyRateModel();
            $currRateData::where('curr_rate_id', $idCurrRate)
                       ->update([
                              'curr_rate_exchange'=>$request->curr_rate_exchange,
                              'curr_rate_exchange_date'=>$request->curr_rate_exchange_date,
                              'curr_rate_from'=>$request->curr_rate_from,
                              'curr_rate_to'=>$request->curr_rate_to,
                              'updated_date' => \Carbon\Carbon::now(),
                              'updated_by' => $userId
                        ]);
            return Redirect::back()->with('status', 'Currency rate updated');
          } catch(\Illuminate\Database\QueryException $ex){
              return Redirect::back()->with('status', 'Error updating');
          }
        }else{
          return Redirect::back()->with('status', 'Error currency from = currency to');
        }
      }

    }

    public function delete(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idCurrRate = $request->curr_rate_id;

      if(!is_null($priv) && $priv->canDelete != 1){
        return Redirect::back()->withErrors(['Not permitted']);
      }else{
        $currRateData = new glCurrencyRateModel();

        $currRateData::where('curr_rate_id', $idCurrRate)->delete();

        return Redirect::back()->with('status', 'Currency rate deleted');
      }

    }

    public function deleteBulk(Request $request, $view){
      $userGroupId = Auth::user()->userGroupId;
      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $module_id = $userModule->moduleId;
      $priv = $this->checkPrivilaged($module_id, $userGroupId);

      $idCurrRate = $request->curr_rate_id;

      if(!is_null($priv) && $priv->canDelete != 1){
          $status = 'Not permitted';
      }else{
        $currRateData = new glCurrencyRateModel();
        $currRateData::whereIn('curr_rate_id', $idCurrRate)->delete();
        $status = "success";

      }
      return $status;
    }

    public function exportToExcel($view){
      $userGroupId = Auth::user()->userGroupId;
      $userEmail = Auth::user()->email;

      $userModule = umModuleModel::select('moduleLabel', 'moduleId')->where('moduleView',$view)->first();
      $moduleId = $userModule->moduleId;
      $priv = $this->checkPrivilaged($moduleId, $userGroupId);

      if(!is_null($priv) && !is_null($priv->canExport)){
        $datamodel = glCurrencyRateModel::join('GL_CURR_MASTER AS GCMF', 'GCMF.curr_id', '=', 'GL_CURR_RATE_MASTER.curr_rate_from')
                                          ->join('GL_CURR_MASTER AS GCMT', 'GCMT.curr_id', '=', 'GL_CURR_RATE_MASTER.curr_rate_to')
                                          ->select('curr_rate_exchange', 'curr_rate_exchange_date', 'GCMF.curr_code AS curr_from', 'GCMT.curr_code AS curr_to')
                                          ->get();
         $data = null;
         foreach($datamodel as $datas){
             $data[] = array(
               $datas->curr_rate_exchange,
               $datas->curr_rate_exchange_date,
               $datas->curr_from,
               $datas->curr_to
             );
         }
         if($data != null){
           $column = array('CURR RATE EXCHANGE','CURR RATE EXCHANGE DATE', 'CURR RATE FROM', 'CURR RATE TO');
           $this->excelDownload($data, "Data Currency Rate", $column);
         }else{
           return Redirect::back()->withErrors(['No data']);
         }
      }else{
         return Redirect::back()->withErrors(['Not permitted']);
      }

    }
}
