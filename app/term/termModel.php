<?php

namespace App\term;

use Illuminate\Database\Eloquent\Model;

class termModel extends Model
{
    protected $table = 'GS_TERM';
    protected $guarded = [''];
    protected $primaryKey = 'term_id';

    public $timestamps = false;
}
