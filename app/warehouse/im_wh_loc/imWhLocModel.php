<?php

namespace App\warehouse\im_wh_loc;

use Illuminate\Database\Eloquent\Model;

class imWhLocModel extends Model
{
    protected $table = 'IM_WH_LOC';
    protected $guarded = [''];
    protected $primaryKey = 'wh_id';

    public $timestamps = false;
}
