<?php

namespace App\warehouse\im_wh_type;

use Illuminate\Database\Eloquent\Model;

class imWhTypeModel extends Model
{
    protected $table = 'IM_WH_TYPE';
    protected $guarded = [''];
    protected $primaryKey = 'wh_type_id';

    public $timestamps = false;
}
