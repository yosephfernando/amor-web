<?php

namespace App\inventory\im_prd_unit;

use Illuminate\Database\Eloquent\Model;

class imPrdUnitModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'IM_PRD_UNIT';
    protected $guarded = [''];
    protected $primaryKey = 'uom_id';
}
