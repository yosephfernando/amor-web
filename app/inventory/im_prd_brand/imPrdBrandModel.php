<?php

namespace App\inventory\im_prd_brand;

use Illuminate\Database\Eloquent\Model;

class imPrdBrandModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'IM_PRD_BRAND';
    protected $guarded = [''];
    protected $primaryKey = 'brand_id';
}
