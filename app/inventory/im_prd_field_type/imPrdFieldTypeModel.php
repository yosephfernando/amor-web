<?php

namespace App\inventory\im_prd_field_type;

use Illuminate\Database\Eloquent\Model;

class imPrdFieldTypeModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'IM_PRD_FIELD_TYPE';
    protected $guarded = [''];
    protected $primaryKey = 'prd_field_type_id';
}
