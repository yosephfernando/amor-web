<?php

namespace App\inventory\im_prd_master;

use Illuminate\Database\Eloquent\Model;

class imPrdMasterModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'IM_PRD_MASTER';
    protected $guarded = [''];
    protected $primaryKey = 'prd_id';
}
