<?php

namespace App\inventory\im_prd_field;

use Illuminate\Database\Eloquent\Model;

class imPrdFieldModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'IM_PRD_FIELD';
    protected $guarded = [''];
    protected $primaryKey = 'prd_field_id';
}
