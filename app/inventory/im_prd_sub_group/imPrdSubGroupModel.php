<?php

namespace App\inventory\im_prd_sub_group;

use Illuminate\Database\Eloquent\Model;

class imPrdSubGroupModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'IM_PRD_SUB_GROUP';
    //public $incrementing = false;
    protected $guarded = [''];
    protected $primaryKey = 'sgroup_id';
}
