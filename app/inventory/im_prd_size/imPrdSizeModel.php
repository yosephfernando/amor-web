<?php

namespace App\inventory\im_prd_size;

use Illuminate\Database\Eloquent\Model;

class imPrdSizeModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'IM_PRD_SIZE';
    protected $guarded = [''];
    protected $primaryKey = 'size_id';
}
