<?php

namespace App\inventory\im_prd_size_group;

use Illuminate\Database\Eloquent\Model;

class imPrdSizeGroupModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'IM_PRD_SIZE_GROUP';
    protected $guarded = [''];
    protected $primaryKey = 'size_group_code';
}
