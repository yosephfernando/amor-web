<?php

namespace App\inventory\im_prd_group;

use Illuminate\Database\Eloquent\Model;

class imPrdGroupModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'IM_PRD_GROUP';
    protected $guarded = [''];
    protected $primaryKey = 'group_id';
}
