<?php

namespace App\inventory\im_prd_color;

use Illuminate\Database\Eloquent\Model;

class imPrdColorModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'IM_PRD_COLOR';
    protected $guarded = [''];
    protected $primaryKey = 'color_id';
}
