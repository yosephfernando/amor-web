<?php

namespace App\inventory\im_prd_master_dim;

use Illuminate\Database\Eloquent\Model;

class imPrdMasterDimModel extends Model
{
    protected $table = 'IM_PRD_MASTER_DIM';
    protected $guarded = [''];
    protected $primaryKey = 'prd_dim_id';

    public $timestamps = false;
}
