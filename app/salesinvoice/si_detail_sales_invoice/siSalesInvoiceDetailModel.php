<?php

namespace App\salesinvoice\si_detail_sales_invoice;

use Illuminate\Database\Eloquent\Model;

class siSalesInvoiceDetailModel extends Model
{
    protected $table = 'AR_INVOICE_DETAIL';
    protected $guarded = [''];
    protected $primaryKey = 'inv_detail_id';

    public $timestamps = false;
}
