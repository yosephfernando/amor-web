<?php

namespace App\salesinvoice\si_sales_invoice;

use Illuminate\Database\Eloquent\Model;

class siSalesInvoiceModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'AR_INVOICE_HEADER';
    protected $guarded = [''];
    protected $primaryKey = 'inv_id';
}
