<?php

namespace App\purchaserequest\pr_detail_purchase_request;

use Illuminate\Database\Eloquent\Model;

class prPurchaseRequestDetailModel extends Model
{
    protected $table = 'PR_DETAIL';
    protected $guarded = [''];
    protected $primaryKey = 'pr_detail_id';

    public $timestamps = false;
}
