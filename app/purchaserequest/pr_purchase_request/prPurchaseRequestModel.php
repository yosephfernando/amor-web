<?php

namespace App\purchaserequest\pr_purchase_request;

use Illuminate\Database\Eloquent\Model;

class prPurchaseRequestModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'PR_HEADER';
    protected $guarded = [''];
    protected $primaryKey = 'pr_id';
}
