<?php

namespace App\masterform\mf_master_form;

use Illuminate\Database\Eloquent\Model;

class mfmasterFormModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'GS_MASTER_FORM';
    protected $guarded = [''];
    protected $primaryKey = 'masterFormId';
}
