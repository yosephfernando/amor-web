<?php

namespace App\masterform\mf_supplier;

use Illuminate\Database\Eloquent\Model;

class mfSupplierModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'GS_MASTER_SUPPLIER';
    protected $guarded = [''];
    protected $primaryKey = 'supplierId';
}
