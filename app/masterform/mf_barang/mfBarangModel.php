<?php

namespace App\masterform\mf_barang;

use Illuminate\Database\Eloquent\Model;

class mfBarangModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'GS_MASTER_BARANG';
    protected $guarded = [''];
    protected $primaryKey = 'barangId';
}
