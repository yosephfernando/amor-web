<?php
namespace App\Traits;
use Illuminate\Support\Facades\DB;
use Excel;

trait excelExport {

      public function excelDownload($datas, $nameFile, $columns){
          if(!is_null($datas)){

            Excel::create($nameFile,function($excel) use ($datas, $columns){
               $excel->sheet('Sheet 1', function($sheet) use ($datas, $columns){
                 $sheet->fromArray($datas, null, 'A1');
                 $sheet->row(1, $columns);
                 $sheet->row(1, function($row){
                    $row->setBackground('#CCCCCC');
                 });
               });
            })->download('xls');

          }else{
            echo "no data";
          }
      }


}

?>
