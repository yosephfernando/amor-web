<?php
namespace App\Traits;
use App\usermanagement\um_user_permission\umUserPermissionModel;
use App\usermanagement\um_module\umModuleModel;
use Illuminate\Support\Facades\DB;
use Request;

trait treeView {

  /* public static function treeItemShow($userGroupId){
        function listModulesTree($userGroupId){
            $Mod = umModuleModel::join('GS_PERMISSIONS', 'GS_PERMISSIONS.moduleId', '=', 'GS_MODULES.moduleId')
                                       ->select('GS_MODULES.moduleName as moduleName', 'GS_MODULES.moduleLabel as moduleLabel', 'GS_MODULES.moduleLink as moduleLink', 'GS_MODULES.moduleParent as moduleParent', 'GS_MODULES.moduleView as moduleView', 'GS_PERMISSIONS.canAdd as canAdd', 'GS_PERMISSIONS.canEdit as canEdit', 'GS_PERMISSIONS.canDelete as canDelete', 'GS_PERMISSIONS.canView as canView', 'GS_MODULES.moduleParams as moduleParams', 'GS_MODULES.moduleId', 'GS_MODULES.moduleMethod as moduleMethod', 'GS_MODULES.moduleIcon as icon')
                                       ->where('GS_PERMISSIONS.userGroupId', '=', $userGroupId)
                                       ->where('GS_MODULES.moduleShowInSidebar', '!=', null)
                                       ->where('GS_MODULES.moduleShowInSidebar', '!=', 0)
                                       ->where('GS_MODULES.moduleParent', '=', null)
                                       ->where('GS_PERMISSIONS.canView', '!=', 0)
                                       ->orderBy('GS_MODULES.moduleOrder', 'ASC')
                                       ->get();
            return $Mod->toArray();
        }

        echo "<div id='tree1'></div>";

        function hasChildTree($userGroupId, $method){
          $cekMod = umModuleModel::join('GS_PERMISSIONS', 'GS_PERMISSIONS.moduleId', '=', 'GS_MODULES.moduleId')
                                     ->select('GS_MODULES.moduleName as moduleName', 'GS_MODULES.moduleLabel as moduleLabel', 'GS_MODULES.moduleLink as moduleLink', 'GS_MODULES.moduleParent as moduleParent', 'GS_MODULES.moduleView as moduleView', 'GS_PERMISSIONS.canAdd as canAdd', 'GS_PERMISSIONS.canEdit as canEdit', 'GS_PERMISSIONS.canDelete as canDelete', 'GS_PERMISSIONS.canView as canView', 'GS_MODULES.moduleParams as moduleParams', 'GS_MODULES.moduleId', 'GS_MODULES.moduleMethod as moduleMethod', 'GS_MODULES.moduleIcon as icon')
                                     ->where('GS_PERMISSIONS.userGroupId', '=', $userGroupId)
                                     ->where('GS_MODULES.moduleParent', '=', $method)
                                     ->where('GS_MODULES.moduleShowInSidebar', '!=', null)
                                     ->where('GS_MODULES.moduleShowInSidebar', '!=', 0)
                                     ->orderBy('GS_MODULES.moduleOrder', 'ASC')
                                     ->get();
           return $cekMod;
        }

        function childsTree($userGroupId, $modName){
           $sub = hasChildTree($userGroupId, $modName);
           $child = "";
           if(!$sub->isEmpty()){
               foreach($sub as $data){
                 if($data['canView'] == 1){
                        $child .=  '{ "id" : "child-'.$data['moduleId'].'","text" : "'.$data['moduleLabel'].'", "icon" : "'.$data['icon'].'", "children" : ['.childsTree($userGroupId, $data['moduleMethod']).']},';
                 }
               }
          }
          return $child;
        }

        $listModules = listModulesTree($userGroupId);
        echo "<script>";
          echo '$("#tree1").jstree({"plugins":["checkbox", "grid"], "core" : {';
            echo '"data" : [';
                $i = 0;
                foreach($listModules as $mod){
                      echo "{";
                            echo '"id": "par-'.$mod['moduleId'].'","text" : "'.$mod['moduleLabel'].'", "icon" : "'.$mod['icon'].'", "children" : ['.childsTree($userGroupId, $mod['moduleName']).'], "state": {opened: true,selected: true}, "grid" : {columns: [{width : 50, header : "add"}]}';
                       if($i < count($listModules) - 1){
                           echo '},';
                       }else{
                           echo '}';
                       }
                $i++;}
            echo "]";
         echo '}});';
       echo "</script>";
   } */

   public static function treeTableShow($userGroupId){
     function listModulesTree($userGroupId){
         $Mod = umModuleModel::join('GS_PERMISSIONS', 'GS_PERMISSIONS.moduleId', '=', 'GS_MODULES.moduleId')
                                    ->select('GS_MODULES.moduleName as moduleName', 'GS_MODULES.moduleLabel as moduleLabel', 'GS_MODULES.moduleLink as moduleLink', 'GS_MODULES.moduleParent as moduleParent', 'GS_MODULES.moduleView as moduleView', 'GS_MODULES.moduleGroup as moduleGroup', 'GS_PERMISSIONS.canAdd as canAdd', 'GS_PERMISSIONS.canEdit as canEdit', 'GS_PERMISSIONS.canDelete as canDelete', 'GS_PERMISSIONS.canView as canView', 'GS_PERMISSIONS.canPrint as canPrint', 'GS_PERMISSIONS.canExport as canExport','GS_PERMISSIONS.canApprove as canApprove', 'GS_PERMISSIONS.permissionId', 'GS_MODULES.moduleParams as moduleParams', 'GS_MODULES.moduleId', 'GS_MODULES.moduleMethod as moduleMethod', 'GS_MODULES.moduleIcon as icon')
                                    ->where('GS_PERMISSIONS.userGroupId', '=', $userGroupId)
                                    ->where('GS_MODULES.moduleShowInSidebar', '!=', null)
                                    ->where('GS_MODULES.moduleShowInSidebar', '!=', 0)
                                    ->where('GS_MODULES.moduleParent', '=', null)
                                    ->orderBy('GS_MODULES.moduleOrder', 'ASC')
                                    ->get();
         return $Mod->toArray();
     }

     function hasChildTree($userGroupId, $method){
       $cekMod = umModuleModel::join('GS_PERMISSIONS', 'GS_PERMISSIONS.moduleId', '=', 'GS_MODULES.moduleId')
                                  ->select('GS_MODULES.moduleName as moduleName', 'GS_MODULES.moduleLabel as moduleLabel', 'GS_MODULES.moduleLink as moduleLink', 'GS_MODULES.moduleParent as moduleParent', 'GS_MODULES.moduleView as moduleView', 'GS_MODULES.moduleGroup as moduleGroup', 'GS_PERMISSIONS.canAdd as canAdd', 'GS_PERMISSIONS.canEdit as canEdit', 'GS_PERMISSIONS.canDelete as canDelete', 'GS_PERMISSIONS.canView as canView', 'GS_PERMISSIONS.canPrint as canPrint', 'GS_PERMISSIONS.canExport as canExport','GS_PERMISSIONS.canApprove as canApprove', 'GS_PERMISSIONS.permissionId', 'GS_MODULES.moduleParams as moduleParams', 'GS_MODULES.moduleId', 'GS_MODULES.moduleMethod as moduleMethod', 'GS_MODULES.moduleIcon as icon')
                                  ->where('GS_PERMISSIONS.userGroupId', '=', $userGroupId)
                                  ->where('GS_MODULES.moduleParent', '=', $method)
                                  ->where('GS_MODULES.moduleShowInSidebar', '!=', null)
                                  ->where('GS_MODULES.moduleShowInSidebar', '!=', 0)
                                  ->orderBy('GS_MODULES.moduleOrder', 'ASC')
                                  ->get();
        return $cekMod;
     }

     function childsTree($userGroupId, $modName){
        $sub = hasChildTree($userGroupId, $modName);
        $child = "";
        if(!$sub->isEmpty()){
            foreach($sub as $data){
                echo '<tr data-tt-id="'.$data['moduleMethod'].'" data-tt-parent-id="'.$data['moduleParent'].'">
                    <td><i class="'.$data['icon'].'"></i>&nbsp;'.$data['moduleLabel'].'</td>
                    <td><input id="can-add-'.$data['moduleId'].'" type="checkbox" '.$checked = ($data['canAdd'] == 1) ? "checked":"";echo '/></td>
                    <td><input id="can-edit-'.$data['moduleId'].'" type="checkbox" '.$checked = ($data['canEdit'] == 1) ? "checked":"";echo '/></td>
                    <td><input id="can-delete-'.$data['moduleId'].'" type="checkbox" '.$checked = ($data['canDelete'] == 1) ? "checked":"";echo '/></td>
                    <td><input id="can-view-'.$data['moduleId'].'" type="checkbox" '.$checked = ($data['canView'] == 1) ? "checked":"";echo '/></td>
                    <td><input id="can-print-'.$data['moduleId'].'" type="checkbox" '.$checked = ($data['canPrint'] == 1) ? "checked":"";echo '/></td>
                    <td><input id="can-export-'.$data['moduleId'].'" type="checkbox" '.$checked = ($data['canExport'] == 1) ? "checked":"";echo '/></td>
                    <td><input id="can-approve-'.$data['moduleId'].'" type="checkbox" '.$checked = ($data['canApprove'] == 1) ? "checked":"";echo '/></td>
                    <td><button id="update-'.$data['moduleId'].'" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i>&nbsp;Update</button></td>
                </tr>';
                echo '
                <script>
                    $("#update-'.$data['moduleId'].'").click(function(){
                       if($("#can-add-'.$data['moduleId'].'").is(":checked")){
                         var canAdd = 1;
                       }else{
                         var canAdd = 0;
                       }

                      if($("#can-edit-'.$data['moduleId'].'").is(":checked")){
                        var canEdit = 1;
                      }else{
                        var canEdit = 0;
                      }

                      if($("#can-delete-'.$data['moduleId'].'").is(":checked")){
                          var canDelete = 1;
                      }else{
                          var canDelete = 0;
                      }

                      if($("#can-view-'.$data['moduleId'].'").is(":checked")){
                          var canView = 1;
                      }else{
                          var canView =  0;
                      }


                      if($("#can-print-'.$data['moduleId'].'").is(":checked")){
                          var canPrint = 1;
                      }else{
                          var canPrint = 0;
                      }

                      if($("#can-export-'.$data['moduleId'].'").is(":checked")){
                          var canExport = 1;
                      }else{
                          var canExport = 0;
                      }

                      if($("#can-approve-'.$data['moduleId'].'").is(":checked")){
                          var canApprove = 1;
                      }else{
                          var canApprove = 0;
                      }

                      var moduleGroup = "'.$data['moduleGroup'].'";
                      var permissionId = "'.$data['permissionId'].'";
                      var token = $("meta[name=csrf-token]").attr("content");

                      $.post( "/update-permission/userManagement/updatePermission",  {
                          "id": permissionId, "canAdd": canAdd,
                          "canEdit": canEdit, "canDelete": canDelete,
                          "canView": canView, "canPrint":canPrint,
                          "canExport": canExport, "canApprove": canApprove,
                          "moduleGroup": moduleGroup, "permissionId": permissionId,
                          "_token" : token })
                        .done(function( data ) {
                          if(data == "success"){
                             toastr.success("Permission updated");
                             toastr.options = {
                              "closeButton": false,
                              "debug": false,
                              "positionClass": "toast-top-right",
                              "onclick": null,
                              "showDuration": "300",
                              "hideDuration": "1000",
                              "timeOut": "5000",
                              "extendedTimeOut": "1000",
                              "showEasing": "swing",
                              "hideEasing": "linear",
                              "showMethod": "fadeIn",
                              "hideMethod": "fadeOut"
                            };

                          }else{
                             console.log( "Data Loaded: " + data.responseText );
                          }
                          console.log( "Data Loaded: " + data );
                        }).error(function(exception){
                          toastr.error("Permission update failed");
                          toastr.options = {
                           "closeButton": false,
                           "debug": false,
                           "positionClass": "toast-top-right",
                           "onclick": null,
                           "showDuration": "300",
                           "hideDuration": "1000",
                           "timeOut": "5000",
                           "extendedTimeOut": "1000",
                           "showEasing": "swing",
                           "hideEasing": "linear",
                           "showMethod": "fadeIn",
                           "hideMethod": "fadeOut"
                         };
                        });
                    });
                </script>
                ';
                childsTree($userGroupId, $data['moduleMethod']);
            }
       }
       return $child;
     }

        echo '<table id="example-basic">
          <thead>';
                  echo '<tr>
                      <th>Node</th>
                      <th>Add</th>
                      <th>Edit</th>
                      <th>Delete</th>
                      <th>View</th>
                      <th>Print</th>
                      <th>Export</th>
                      <th>Approve</th>
                      <th>Action</th>
                  </tr>';
        echo '</thead>';
              echo '<tbody>';
              $listModules = listModulesTree($userGroupId);
              foreach($listModules as $data){
                echo '<tr data-tt-id="'.$data['moduleName'].'">
                    <td><i class="'.$data['icon'].'"></i>&nbsp;'.$data['moduleLabel'].'</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><input id="can-view-'.$data['moduleId'].'" type="checkbox" value="1" '.$checked = ($data['canView'] == 1) ? "checked":"";echo '/></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><button id="update-'.$data['moduleId'].'" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i>&nbsp;Update</button></td>
                </tr>';
                echo '
                  <script>
                        $("#update-'.$data['moduleId'].'").click(function(){
                          if($("#can-add-'.$data['moduleId'].'").is(":checked")){
                            var canAdd = 1;
                          }else{
                            var canAdd = 0;
                          }

                         if($("#can-edit-'.$data['moduleId'].'").is(":checked")){
                           var canEdit = 1;
                         }else{
                           var canEdit = 0;
                         }

                         if($("#can-delete-'.$data['moduleId'].'").is(":checked")){
                             var canDelete = 1;
                         }else{
                             var canDelete = 0;
                         }

                         if($("#can-view-'.$data['moduleId'].'").is(":checked")){
                             var canView = 1;
                         }else{
                             var canView =  0;
                         }


                         if($("#can-print-'.$data['moduleId'].'").is(":checked")){
                             var canPrint = 1;
                         }else{
                             var canPrint = 0;
                         }

                         if($("#can-export-'.$data['moduleId'].'").is(":checked")){
                             var canExport = 1;
                         }else{
                             var canExport = 0;
                         }

                         if($("#can-approve-'.$data['moduleId'].'").is(":checked")){
                             var canApprove = 1;
                         }else{
                             var canApprove = 0;
                         }

                          var moduleGroup = "'.$data['moduleGroup'].'";
                          var permissionId = "'.$data['permissionId'].'";
                          var token = $("meta[name=csrf-token]").attr("content");

                          $.post( "/update-permission/userManagement/updatePermission",  {
                              "id": permissionId, "canAdd": canAdd,
                              "canEdit": canEdit, "canDelete": canDelete,
                              "canView": canView, "canPrint":canPrint,
                              "canExport": canExport, "canApprove": canApprove,
                              "moduleGroup": moduleGroup, "permissionId": permissionId,
                              "_token" : token })
                            .done(function( data ) {
                              if(data == "success"){
                                toastr.success("Permission updated");
                                toastr.options = {
                                 "closeButton": false,
                                 "debug": false,
                                 "positionClass": "toast-top-right",
                                 "onclick": null,
                                 "showDuration": "300",
                                 "hideDuration": "1000",
                                 "timeOut": "5000",
                                 "extendedTimeOut": "1000",
                                 "showEasing": "swing",
                                 "hideEasing": "linear",
                                 "showMethod": "fadeIn",
                                 "hideMethod": "fadeOut"
                               };
                              }else{
                                 console.log( "Data Loaded: " + data.responseText );
                              }
                              console.log( "Data Loaded: " + data );
                            }).error(function(exception){
                              toastr.error("Permission update failed");
                              toastr.options = {
                               "closeButton": false,
                               "debug": false,
                               "positionClass": "toast-top-right",
                               "onclick": null,
                               "showDuration": "300",
                               "hideDuration": "1000",
                               "timeOut": "5000",
                               "extendedTimeOut": "1000",
                               "showEasing": "swing",
                               "hideEasing": "linear",
                               "showMethod": "fadeIn",
                               "hideMethod": "fadeOut"
                             };
                            });
                        });
                  </script>
                ';
                childsTree($userGroupId, $data['moduleName']);
              }
                  echo '</tbody>
              </table>';
              echo '<script>
                $("#example-basic").treetable({ expandable: true });
              </script>';

   }
}

?>
