<?php
namespace App\Traits;
use App\usermanagement\um_user_permission\umUserPermissionModel;
use App\usermanagement\um_module\umModuleModel;
use Illuminate\Support\Facades\DB;
use Request;

trait privilaged {
      public static function listModulesSearch($userGroupId){
        $Mod = umModuleModel::join('GS_PERMISSIONS', 'GS_PERMISSIONS.moduleId', '=', 'GS_MODULES.moduleId')
                                   ->select('GS_MODULES.moduleLabel as value', 'GS_MODULES.moduleLink as data', 'GS_MODULES.moduleParent as parent', 'GS_MODULES.moduleView as view')
                                   ->where('GS_PERMISSIONS.userGroupId', '=', $userGroupId)
                                   ->where('GS_MODULES.moduleShowInSidebar', '!=', null)
                                   ->where('GS_MODULES.moduleShowInSidebar', '!=', 0)
                                   ->where('GS_MODULES.moduleLink', '!=', null)
                                   ->orderBy('GS_MODULES.moduleOrder', 'ASC')
                                   ->get();

        return $Mod;
      }

      public function listModules($userGroupId){
        $Mod = umModuleModel::join('GS_PERMISSIONS', 'GS_PERMISSIONS.moduleId', '=', 'GS_MODULES.moduleId')
                                   ->select('GS_MODULES.moduleName as moduleName', 'GS_MODULES.moduleLabel as moduleLabel', 'GS_MODULES.moduleLink as moduleLink', 'GS_MODULES.moduleParent as moduleParent', 'GS_MODULES.moduleView as moduleView', 'GS_PERMISSIONS.canAdd as canAdd', 'GS_PERMISSIONS.canEdit as canEdit', 'GS_PERMISSIONS.canDelete as canDelete', 'GS_PERMISSIONS.canView as canView', 'GS_MODULES.moduleParams as moduleParams', 'GS_MODULES.moduleId', 'GS_MODULES.moduleMethod as moduleMethod', 'GS_MODULES.moduleIcon as icon')
                                   ->where('GS_PERMISSIONS.userGroupId', '=', $userGroupId)
                                   ->where('GS_MODULES.moduleShowInSidebar', '!=', null)
                                   ->where('GS_MODULES.moduleShowInSidebar', '!=', 0)
                                   ->orderBy('GS_MODULES.moduleOrder', 'ASC')
                                   ->get();

        return $Mod->toArray();
      }


      public static function buildMenu($userGroupId, $listModules){
        function numberToWords ($number){
          $words = array ('zero','one','seco','three','four','five','six','seven','eight','nine','ten','eleven','twelve','thirteen',
          'fourteen','fifteen','sixteen','seventeen','eighteen','nineteen','twenty',30=> 'thirty',40 => 'fourty',50 => 'fifty',
          60 => 'sixty',70 => 'seventy',80 => 'eighty',90 => 'ninety',100 => 'hundred',1000=> 'thousand');
          $number_in_words = "";
              if (is_numeric ($number)){
                $number = (int) round($number);
                  if ($number < 0){
                    $number = -$number;
                    $number_in_words = 'minus';
                  }
              if ($number > 1000){
                $number_in_words = $number_in_words . numberToWords(floor($number/1000)) . " " . $words[1000];
                $hundreds = $number % 1000;
                $tens = $hundreds % 100;
                  if ($hundreds > 100){
                    $number_in_words = $number_in_words.",".numberToWords ($hundreds);
                  }elseif ($tens){
                      $number_in_words = $number_in_words."and".numberToWords ($tens);
                  }
              }elseif ($number > 100){
                $number_in_words = $number_in_words.numberToWords(floor ($number / 100)).$words[100];
                $tens = $number % 100;
                  if ($tens){
                    $number_in_words = $number_in_words."and".numberToWords ($tens);
                  }
              }elseif ($number > 20){
                  $number_in_words = $number_in_words.$words[10 * floor ($number/10)];
                  $units = $number % 10;
                    if ($units){
                      $number_in_words = $number_in_words.numberToWords($units);
                    }
              }else{
                  $number_in_words = $number_in_words.$words[$number];
              }
                  return $number_in_words;
              }
                  return false;
          }

          function ordinal($number) {
               $ends = array('th','st','nd','rd','th','th','th','th','th','th');
               if ((($number % 100) >= 11) && (($number%100) <= 13)){
                   return numberToWords($number).'th';
               }else{
                   return numberToWords($number).$ends[$number % 10];
               }
           }

        function hasChild($userGroupId, $method){
          $cekMod = umModuleModel::join('GS_PERMISSIONS', 'GS_PERMISSIONS.moduleId', '=', 'GS_MODULES.moduleId')
                                     ->select('GS_MODULES.moduleName as moduleName', 'GS_MODULES.moduleLabel as moduleLabel', 'GS_MODULES.moduleLink as moduleLink', 'GS_MODULES.moduleParent as moduleParent', 'GS_MODULES.moduleView as moduleView', 'GS_PERMISSIONS.canAdd as canAdd', 'GS_PERMISSIONS.canEdit as canEdit', 'GS_PERMISSIONS.canDelete as canDelete', 'GS_PERMISSIONS.canView as canView', 'GS_MODULES.moduleParams as moduleParams', 'GS_MODULES.moduleId', 'GS_MODULES.moduleMethod as moduleMethod', 'GS_MODULES.moduleIcon as icon')
                                     ->where('GS_PERMISSIONS.userGroupId', '=', $userGroupId)
                                     ->where('GS_MODULES.moduleParent', '=', $method)
                                     ->where('GS_MODULES.moduleShowInSidebar', '!=', null)
                                     ->where('GS_MODULES.moduleShowInSidebar', '!=', 0)
                                     ->orderBy('GS_MODULES.moduleOrder', 'ASC')
                                     ->get();
           return $cekMod;
        }

        $seg2 = Request::segment(2);
        $seg3 = Request::segment(3);

        function childs($userGroupId, $modName, $seg2, $seg3, $level, $i){
           $sub = hasChild($userGroupId, $modName);
           if(!$sub->isEmpty()){
                echo '<ul class="nav nav-'.$level.'-level">';
                     foreach($sub as $data){
                       if($data['canView'] == 1){
                           echo '<li class="'.$active = ($data['moduleView'] == $seg3 && !is_null($seg3) || $data['moduleMethod'] == $seg2) ? 'active':'' ;$active;echo '">';
                                echo '<a href="'.$data['moduleLink'].'/'.$data['moduleParent'].'/'.$data['moduleView'].'">';
                                     echo '<i class="'.$icon = (!is_null($data['icon'])) ? $data['icon']:'fa fa-angle-right';$icon;echo '"></i>';
                                     echo '<span class="submenu-title">'.$data['moduleLabel'].'</span>
                                    </a>';
                               $levels = ordinal($i+1);

                               childs($userGroupId, $data['moduleMethod'], $seg2, $seg3, $levels, $i);
                             echo '</li>';
                        }
                     }
                echo '</ul>';
          }
       }

         $i = 2;
         foreach($listModules as $mod){
            $modName = "";
            $level = "";
           if($mod['moduleParent'] == null && $mod['canView'] == 1){

             echo '<li class="'.$active = ($mod['moduleName'] == $seg2 && !is_null($seg2) || (isset($_COOKIE['moduleName']) && $_COOKIE['moduleName'] == $mod['moduleName'])) ? 'active':'' ;$active;echo '">';
                echo '<a id="'.$mod['moduleName'].'"  href="'.$mod['moduleLink'].'/'.$mod['moduleParent'].'/'.$mod['moduleView'].'">';
                   echo '<i class="'.$icon = (!is_null($mod['icon'])) ? $mod['icon']:'fa fa-angle-right';$icon;echo '"';echo 'style="font-size: 22px"">
                          <div class="icon-bg bg-orange" style="margin-top: 6px"></div>
                      </i>';
                      echo '<span class="menu-title">'.$mod['moduleLabel'].'</span>
                    </a>';
                    echo '<script>';
                      echo '$("#'.$mod['moduleName'].'").click(function(){
                        if(Cookies.get("moduleName") != null){
                            Cookies.remove("moduleName");
                        }
                          Cookies.set("moduleName", "'.$mod['moduleName'].'");

                          console.log("mod name : '.$mod['moduleName'].'");
                          console.log("cookie : "+Cookies.get("moduleName"));
                      });';
                    echo '</script>';
                   $level = ordinal($i);
                   $modName = $mod['moduleName'];
                   childs($userGroupId, $modName, $seg2, $seg3, $level, $i);
              echo '</li>';

           }
        }

      }

      public function checkPrivilaged($moduleId, $userGroupId){

          $umUserPermissionModel = new umUserPermissionModel();
          $data = $umUserPermissionModel::select('canAdd', 'canEdit', 'canDelete', 'canView', 'canPrint', 'canExport', 'canApprove')
                                    ->where(['moduleId' => $moduleId, 'userGroupId' => $userGroupId])
                                    ->first();
          return $data;
      }

      public function listPrivilaged($userGroupId){
          $umUserPermissionModel = new umUserPermissionModel();
          $data = $umUserPermissionModel::join('GS_MODULES', 'GS_MODULES.moduleId', '=', 'GS_PERMISSIONS.moduleId')
                                    ->select('GS_MODULES.moduleMethod', 'GS_MODULES.moduleName', 'GS_MODULES.moduleParent','GS_PERMISSIONS.canAdd', 'GS_PERMISSIONS.canEdit', 'GS_PERMISSIONS.canDelete', 'GS_PERMISSIONS.canView')
                                    ->where('GS_PERMISSIONS.userGroupId', $userGroupId)
                                    ->get();

          if($data != null){
              return $data;
          }else{
              return response('No permission', 500)->header('Content-Type', 'text/plain');
          }

      }

}

?>
