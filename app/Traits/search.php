<?php
namespace App\Traits;
use Illuminate\Support\Facades\DB;
use Request;

trait search {

      public static function searchMaster($query, $label, $idTextEdit, $rows){
         try{
           $data = DB::select(DB::raw($query));
           self::searchResultView($data, $label, $idTextEdit, $rows);
         } catch(\Illuminate\Database\QueryException $ex){
           dd($ex->getMessage());
         }
      }

      public static function searchResultView($data, $label, $idTextEdit, $rows){
           echo '
            <div id="modal-'.$idTextEdit.'" tabindex="-1" role="dialog" aria-labelledby="modal-responsive-label" aria-hidden="true" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" id=close-'.$idTextEdit.' aria-hidden="true" class="close">&times;</button>
                            <h4 id="modal-responsive-label" class="modal-title">'.$label.'</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                              <div class="table-responsive col-md-12">
                                 <div class="clear" style="height:10px"></div>
                                     <table class="table table-striped table-bordered table-hover">';
                                     $a = 0;
                                  if(count($data) >= 1){
                                     foreach($data as $datas){
                                      if($a ==0){
                                        $fields = array_keys((array)$datas);
                                        $countFields = count($fields);
                                         echo '<tr>';
                                              echo '<th>#</th>';
                                          $i = 0;
                                          for($i = 0;$i <= $countFields - 1;$i++){
                                              echo '<th>'.$fields[$i].'</th>';
                                          }
                                         echo '</tr>';
                                         echo '<tr>';
                                       }
                                           for($i = 0;$i <= $countFields - 1;$i++){
                                               $field = $fields[$i];
                                               if($i == 0){
                                                 echo '<td><input id="cat-'.$field.'-'.$datas->$field.'" type="radio" value="'.$datas->$field.'"></td>';
                                               }
                                               echo '<td>'.$datas->$field .'</td>';
                                               echo '<script>
                                                  $("#cat-'.$field.'-'.$datas->$field.'").click(function(){
                                                    var bc = $("#cat-'.$field.'-'.$datas->$field.'").val();
                                                    $("#content-'.$idTextEdit.'").val(bc).trigger("change");';
                                                    $j = 0;
                                                    $val = "";
                                                    for($j = 0;$j<= count($rows) - 1;$j++){
                                                      $fieldSel = $rows[$j];
                                                      if($j != count($rows) - 1){
                                                          $val .= $datas->$fieldSel.", ";
                                                      }else{
                                                          $val .= $datas->$fieldSel;
                                                      }
                                                    }
                                                    echo 'var rows = "'.$val.'";';
                                                    echo '$("#contentVal-'.$idTextEdit.'").val(rows).trigger("change");';
                                                    echo '$("#modal-'.$idTextEdit.'").modal("hide");
                                                  });
                                               </script>';
                                           }
                                         echo '</tr>';
                                      $a++;}
                                    }else{
                                      echo '<h3 class="text-center">NO DATA</h3>';
                                    }
                                     echo '</table>
                               </div>
                            </div>
                        </div>
                        <div class="modal-footer">

                        </div>
                    </div>
                </div>
            </div>

            <script>
                 $("#modal-'.$idTextEdit.'").modal("show");
                 $("#close-'.$idTextEdit.'").click(function(){
                    $("#modal-'.$idTextEdit.'").modal("hide");
                 });
            </script>';
        }

}

?>
