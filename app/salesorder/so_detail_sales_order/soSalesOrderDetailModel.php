<?php

namespace App\salesorder\so_detail_sales_order;

use Illuminate\Database\Eloquent\Model;

class soSalesOrderDetailModel extends Model
{
    protected $table = 'SO_DETAIL';
    protected $guarded = [''];
    protected $primaryKey = 'so_detail_id';

    public $timestamps = false;
}
