<?php

namespace App\salesorder\so_sales_order;

use Illuminate\Database\Eloquent\Model;

class soSalesOrderModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'SO_HEADER';
    protected $guarded = [''];
    protected $primaryKey = 'so_id';
}
