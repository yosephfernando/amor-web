<?php

namespace App\purchasinginvoice\pi_detail_purchase_invoice;

use Illuminate\Database\Eloquent\Model;

class piPurchaseInvoiceDetailModel extends Model
{
    protected $table = 'AP_INVOICE_DETAIL';
    protected $guarded = [''];
    protected $primaryKey = 'inv_detail_id';

    public $timestamps = false;
}
