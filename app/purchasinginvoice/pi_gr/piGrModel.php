<?php

namespace App\purchasinginvoice\pi_gr;

use Illuminate\Database\Eloquent\Model;

class piGrModel extends Model
{
    protected $table = 'AP_INVOICE_GR';
    protected $guarded = [''];
    protected $primaryKey = 'inv_gr_id';

    public $timestamps = false;
}
