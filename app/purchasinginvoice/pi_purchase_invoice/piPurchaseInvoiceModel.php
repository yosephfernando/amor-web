<?php

namespace App\purchasinginvoice\pi_purchase_invoice;

use Illuminate\Database\Eloquent\Model;

class piPurchaseInvoiceModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'AP_INVOICE_HEADER';
    protected $guarded = [''];
    protected $primaryKey = 'inv_id';
}
