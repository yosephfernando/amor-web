<?php

namespace App\stocktxn\im_stock_txn;

use Illuminate\Database\Eloquent\Model;

class imStockTxnModel extends Model
{
    protected $table = 'IM_STOCK_TXN';
    protected $guarded = [''];
    protected $primaryKey = 'txn_run_id';

    public $timestamps = false;
}
