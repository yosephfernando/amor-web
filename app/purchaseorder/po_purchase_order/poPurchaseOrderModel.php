<?php

namespace App\purchaseorder\po_purchase_order;

use Illuminate\Database\Eloquent\Model;

class poPurchaseOrderModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'PO_HEADER';
    protected $guarded = [''];
    protected $primaryKey = 'po_id';
}
