<?php

namespace App\purchaseorder\po_supp_type;

use Illuminate\Database\Eloquent\Model;

class supplierTypeModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'PO_SUPP_TYPE';
    protected $guarded = [''];
    protected $primaryKey = 'supp_type_id';
}
