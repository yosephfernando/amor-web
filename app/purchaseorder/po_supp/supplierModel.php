<?php

namespace App\purchaseorder\po_supp;

use Illuminate\Database\Eloquent\Model;

class supplierModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'PO_SUPP_MASTER';
    protected $guarded = [''];
    protected $primaryKey = 'supp_id';
}
