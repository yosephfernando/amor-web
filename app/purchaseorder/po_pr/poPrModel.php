<?php

namespace App\purchaseorder\po_pr;

use Illuminate\Database\Eloquent\Model;

class poPrModel extends Model
{
    protected $table = 'PO_PR';
    protected $guarded = [''];
    protected $primaryKey = 'po_pr_id';

    public $timestamps = false;
}
