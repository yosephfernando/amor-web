<?php

namespace App\purchaseorder\po_detail_purchase_order;

use Illuminate\Database\Eloquent\Model;

class poPurchaseOrderDetailModel extends Model
{
    protected $table = 'PO_DETAIL';
    protected $guarded = [''];
    protected $primaryKey = 'po_detail_id';

    public $timestamps = false;
}
