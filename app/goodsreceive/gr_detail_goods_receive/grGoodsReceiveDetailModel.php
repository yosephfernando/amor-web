<?php

namespace App\goodsreceive\gr_detail_goods_receive;

use Illuminate\Database\Eloquent\Model;

class grGoodsReceiveDetailModel extends Model
{
    protected $table = 'GR_DETAIL';
    protected $guarded = [''];
    protected $primaryKey = 'gr_detail_id';

    public $timestamps = false;
}
