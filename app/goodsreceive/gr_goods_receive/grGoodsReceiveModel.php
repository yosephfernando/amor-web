<?php

namespace App\goodsreceive\gr_goods_receive;

use Illuminate\Database\Eloquent\Model;

class grGoodsReceiveModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'GR_HEADER';
    protected $guarded = [''];
    protected $primaryKey = 'gr_id';
}
