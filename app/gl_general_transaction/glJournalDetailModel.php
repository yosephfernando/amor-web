<?php

namespace App\gl_general_transaction;

use Illuminate\Database\Eloquent\Model;

class glJournalDetailModel extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT  = 'updated_date';

    protected $table = 'GL_JOURNAL_DETAIL';
    protected $guarded = [''];
    protected $primaryKey = 'journal_detail_id';
}
