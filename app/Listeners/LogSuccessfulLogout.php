<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Logout;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogSuccessfulLogout
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Logout  $event
     * @return void
     */
    public function handle(Logout $event)
    {
          if(isset($_SERVER['HTTP_COOKIE'])){
              $cookies = explode(";", $_SERVER["HTTP_COOKIE"]);
              foreach($cookies as $cookie){
                  $parts = explode("=", $cookie);
                  $name = trim($parts[0]);
                  setCookie($name, '', time()-1000);
                  setCookie($name, '', time()-1000, '/');
              }
        }
    }
}
