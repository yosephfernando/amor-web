<?php
use App\usermanagement\um_module\umModuleModel;
use Illuminate\Support\Facades\Schema;

Auth::routes();
Route::get('/', 'HomeController@home');
Route::get('/home', 'HomeController@home');
Route::get('/lookupSugestion/{label}', 'ajaxAllSugestions\sugestionAjax@lookupCatalog');
if(Schema::hasTable('GS_MODULES')){
  $routesData = umModuleModel::all();
  foreach($routesData as $data){
      //var_dump($data['moduleLink']."/{view}".$data['moduleParams'].$data['moduleName']."@".$data['moduleMethod']);
    if($data['moduleParams'] != ""){
       if($data['moduleHttpMethod'] != "get"){
         Route::post($data['moduleLink']."/".$data['moduleParent']."/{view}".$data['moduleParams']."", $data['moduleName']."@".$data['moduleMethod']);
       }else{
         Route::get($data['moduleLink']."/".$data['moduleParent']."/{view}".$data['moduleParams']."", $data['moduleName']."@".$data['moduleMethod']);
       }
    }else{
      if($data['moduleHttpMethod'] != "get"){
        Route::post($data['moduleLink']."/".$data['moduleParent']."/{view}".$data['moduleParams']."", $data['moduleName']."@".$data['moduleMethod']);
      }else{
        Route::get($data['moduleLink']."/".$data['moduleParent']."/{view}".$data['moduleParams']."", $data['moduleName']."@".$data['moduleMethod']);
      }
    }
  }
}else{
  Route::get('/', function(){
      echo "please register module table first";
  });
}


  //Route::get('/master', function () {return view('master-data.master');});
