<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->increments('moduleId');
            $table->string('moduleLabel', 30);
            $table->string('moduleName', 50);
            $table->string('moduleView', 50)->nullable();
            $table->string('moduleMethod', 50)->nullable();
            $table->string('moduleLink', 20)->nullable();
            $table->string('moduleParams')->nullable();
            $table->string('moduleParent', 50)->nullable();
            $table->smallInteger('moduleShowInSidebar')->nullable();
            $table->string('moduleHttpMethod', 5)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modules');
    }
}
