<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('masterForm', function (Blueprint $table) {
            $table->increments('masterFormId');
            $table->string('masterFormText', 30);
            $table->string('masterFormSelect', 50);
            $table->string('masterFormCheckBox', 50)->nullable();
            $table->string('masterFormCheckBox2', 50)->nullable();
            $table->string('masterFormRadio', 50);
            $table->text('masterFormTextArea');
            $table->text('masterFormFile');
            $table->date('masterFormDateFrom');
            $table->date('masterFormDateTo');
            $table->date('masterFormDate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('masterForm');
    }
}
