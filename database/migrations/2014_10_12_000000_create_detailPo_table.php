<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailPoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detailPo', function (Blueprint $table) {
            $table->increments('detailPoId');
            $table->string('detailPoCode', 10);
            $table->string('barangCode', 10);
            $table->integer('qty');
            $table->integer('subTotal');
            $table->varchar('poCode', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detailPo');
    }
}
