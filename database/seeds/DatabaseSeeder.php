<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dateNow = date('Y-m-d H:i:s');
        // $this->call(UsersTableSeeder::class);
        DB::table('modules')->insert([
            'moduleLabel' => 'tes label',
            'moduleName' => 'tesController',
            'moduleView' => 'tesAddView',
            'moduleMethod' => 'tesAdd',
            'moduleLink' => '/tes',
            'moduleParams' => '/{fname}/{lname}',
            'moduleParent' => null,
            'created_at' => $dateNow,
            'updated_at' => $dateNow
        ]);
    }
}
